<?php

Class Mailer {


	protected $config;

	public function __construct($config){
		$this->config = $config;
	}

    // send email using postmark
    public function sendMail($email, $subject, $content) {

        if(count($email)>1){
            foreach ($email as $value) {
                if($content){
                    $body = $content;
                }else{
                    $body = "Empty Body";
                }

                $json = json_encode(array(
                    'From' => 'angbayanko.org@no-reply.com',
                    'To' => $value,
                    'Name' => 'angbayanko.org',
                    'Subject' => $subject,
                    'HtmlBody' => $body
                ));

                $ch2 = curl_init();
                curl_setopt($ch2, CURLOPT_URL, $this->config['url']);
                curl_setopt($ch2, CURLOPT_POST, true);
                curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
                    'Accept: application/json',
                    'Content-Type: application/json',
                    'X-Postmark-Server-Token: '.$this->config['token']
                ));
                curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
                $response = curl_exec($ch2);
                $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
                curl_close($ch2);
            }
            return $http_code;
        }else{
            if($content){
            $body = $content;
        }else{
            $body = "Empty Body";
        }

        $json = json_encode(array(
            'From' => 'angbayanko.org@no-reply.com',
            'FromName' => 'angbayanko.org',
            'To' => $email,
            'Subject' => $subject,
            'HtmlBody' => $body
        ));

        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, $this->config['url']);
        curl_setopt($ch2, CURLOPT_POST, true);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'X-Postmark-Server-Token: '.$this->config['token']
        ));
        curl_setopt($ch2, CURLOPT_POSTFIELDS, $json);
        $response = curl_exec($ch2);
        $http_code = curl_getinfo($ch2, CURLINFO_HTTP_CODE);
        curl_close($ch2);

        return $http_code;
        }

        
    }

    public function sample()
    {
        return $this->config;
    }
}
