<!--Modal View-->
            <div id="modalDigitalAssets" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
              <div class="modal-dialog" style="width: 900px;">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Upload picture to <span>Album: <strong id="uploadAlbumLabel"></strong></span></h4>
<!--                     <input type="hidden" id="digital-assets-year" name="digital-assets-year" value="{{ dayear }}" />
                    <input type="hidden" id="digital-assets-month" name="digital-assets-month" value="{{ damonth }}" /> -->

                    <input type="text" id="partner-id" name="partner-id" value="{{ partner.partnerID }}" />
                    <input type="text" id="partner-album" name="partner-album" value="{{ albumCurrent }}" />
                    <input type="text" id="album-id" name="album-id" value="{{ albumID }}" />
                  </div>
                  <div class="modal-body" style=" height: 400px; overflow:scroll;">
                    <div id="imageError"></div>
                    <span class="btn btn-success fileinput-button">
                      <i class="glyphicon glyphicon-plus"></i>
                      <span>Add files...</span>
                      <!-- The file input field used as target for the file upload widget -->
                      <input id="digitalAssets2" type="file" name="files[]" multiple accept="image/*">
                    </span>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                      <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <div class="gallery digital-assets-gallery">
                      <span id="selectImg">Please select files to upload</span>
                    </div>
                    <div class="clearfix"></div>
                    
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true" id="closeUpload">Close</button>
                  </div>
                </div>
              </div>
            </div>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" id="uploadPictureForm" name="uploadPictureForm" enctype="multipart/form-data">
        <input type="hidden" name="partnerID" id="partnerID" value="{{ partnerID }}">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Upload Picture</h4>
        </div>
        <div class="modal-body">
          <div id="partnerPreloader"></div>
          <div class="">
            <span>Album: <strong id="uploadAlbumLabel"></strong></span>
            <input type="hidden" id="uploadAlbumSelect">
          </div>
          <br />
          <input type="file" name="files[]" id="picturesFile" multiple>
          <div class="progress progress-animated">
            <div id="progressBar" class="progress-bar progress-bar-info" data-percentage="0" style="width: 0%;"></div>
          </div>
        </div>
        <div class="modal-footer">
          <button id="afterUpload" type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
          <button type="submit" class="btn btn-primary" disabled id="startUploadBtn">Start Upload</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div id="editCaptionModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Edit Picture</h4>
      </div>
      <form method="post" name="editPictureCaptionForm" id="editPictureCaptionForm">
        <div class="modal-body">
          <div id="editCaptionBody"></div>
        </div>
        <div class="modal-footer" id="modal-footer" style="display:none">
          <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
          <button type="submit" class="btn btn-primary" id="saveCaption">Save</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div id="deletePictureConf" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Delete Picture</h4>
      </div>
      <div class="modal-body">
        <span id="deletePictureConfMessage"></span>
      </div>
      <div class="modal-footer" id="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <input type="button" id="deletePicture" value="Delete" class="btn btn-danger">
      </div>
    </div>
  </div>
</div>

<div id="createAlbumModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header-now" style="background-color: blue;height: 40px;">
        <button type="button" class="close" style="color: white;font-size: 17px;" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" style="color: white;font-size: 17px;"><p style="margin-left: 10px;margin-top: 10px;">Create Album</p></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="partnerID" value="{{ partnerID }}">
        <div id="albumResult"></div>
        <label>Album Name</label>
        <input type="text" name="albumName" id="albumName" placeholder="Album Name" class="form-control">
        <div id="erredit"></div>
      </div>
      <div class="modal-footer" id="modal-footer">
        <button type="button" id="createAlbumBtn" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Save</button>
        <!-- <span id="createAlbumClose"> -->
          <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span> Close</button>
        <!-- </span> -->
      </div>
    </div>
  </div>
</div>

<div id="editAlbumModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header-edit" style="background-color: #eea236; height: 40px;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" style="color: white;"><p style="margin-top: 10px;margin-left: 10px;"> Edit Album</p></h4>
      </div>
      <div class="modal-body">
        <div id="editAlbumContent"></div>
        
      </div>
      <div id="erredit"></div>
      <div class="modal-footer" id="modal-footer">
        <button style="display:none" type="button" id="editAlbumBtn" class="btn btn-primary" data-partner-id="{{ partnerID }}"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span> Close</button>
      </div>
    </div>
  </div>
</div>

<div id="deleteAlbumConf" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header-del" style="background-color: #d9534f; height: 40px;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" style="color: white;"><p style="margin-top: 10px;margin-left: 10px;"> Delete Album</p></h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="deleteHiddenAlbumID" id="deleteHiddenAlbumID">
        <span id="deleteMessage"></span>
      </div>
      <div class="modal-footer" id="modal-footer">
        <button type="button" name="deleteAlbumBtn" id="deleteAlbumBtn" data-dismiss="modal" class="btn btn-primary"><span class="glyphicon glyphicon-trash"></span> Delete</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span> Close</button>     
      </div>
    </div>
  </div>
</div>

<div id="movePictureModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Move Picture(s)</h4>
      </div>
      
      <div class="modal-body">
        <label>Move selected pictures(s) to</label>
        <select id="moveAlbumSelect" class="form-control albums">
          <option value="">--Album--</option>
          <?php foreach ($albums as $key => $value) {
            if(!empty($selectedAlbum) && $selectedAlbum == $value->albumID){
              $selected = 'selected';
            }else{
              $selected = '';
            }
            echo '<option '.$selected.' value="'.$value->albumID.'">'.$value->albumName.'</option>';
          } ?>
        </select>
      </div>

      <div class="modal-footer" id="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
        <button type="submit" class="btn btn-primary" id="movePictureBtn">Move</button>
      </div>
      
    </div>
  </div>
</div>

<div id="deleteSelectedAlbumConf" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Delete Album</h4>
      </div>
      <div class="modal-body">
        <span id="deleteMessage">Are you sure you want to delete selected album(s)?</span>
      </div>
      <div class="modal-footer" id="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
        <button type="button" id="deleteSelectedAlbumBtn" class="btn btn-danger">Delete</button>
      </div>
    </div>
  </div>
</div>


<!-- Page heading -->
<div class="page-head">
  <h2 class="pull-left"><i class="icon-suitcase"></i> ABK Partner</h2>

  <!-- Breadcrumb -->
  <div class="bread-crumb pull-right">
    <a href="{{ url('admin') }}"><i class="icon-home"></i> Home</a>
    <!-- Divider -->
    <span class="divider">/</span>
    <a href="{{ url('admin/partners') }}">Partners</a>
    <span class="divider">/</span>
    <span>Pictures</span>
  </div>

  <div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">
  {{ form('class': 'form-horizontal', 'id':'picturesForm') }}
  <input type="hidden" id="pictureHiddenID" name="pictureHiddenID">
  <input type="hidden" id="pictureAlbumHiddenID" name="pictureAlbumHiddenID">
  <div class="container">

    <!-- Table -->

    <h2>{{ partner.partnerName }}</h2>

    <div class="row">
      <div class="col-md-12">

        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">ABK Partner Details</div>
            <div class="widget-icons pull-right">
              <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> -->
              <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
            <div class="padd">
              <?php if($userlevel!=2){ ?>
              <ul class="nav nav-tabs" role="tablist">
                <li><a href="/admin/partnersinfo/{{ partnerID }}/">Partner Details</a></li>
                <li><a href="/admin/partnersinfo/{{ partnerID }}/events">Current Events</a></li>
                <li class="active"><a href="/admin/partnersPictures/{{ partnerID }}">Pictures</a></li>
              </ul>
              <?php } ?>
              <br>
              {{ content() }}


              {% if albumView == true %}
              <div class="form-group">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="control-label col-lg-3">Search</label>
                    <div class="col-lg-9"> 
                      {{ text_field('search_album_text' , 'class':'form-control') }}
                    </div>
                  </div>
                </div>

                <div class="col-lg-6">
<!--                   {{ submit_button('Search', 'name':'searchBtn', 'class':'btn btn-default') }} -->
<button type="submit" name="searchBtn" class="btn btn-success" ><span class="icon-search"> </span>Search</button>
                  <button type="submit" name="clear_search" class="btn btn-danger " value="Clear Search"><span class="icon-refresh"> </span>Refresh</button>
                  <a href="#createAlbumModal" style="margin-right:25px" class="btn btn-primary pull-right" data-toggle="modal">+ Create New Album</a>
                </div>
              </div>
              <div id="noticeErr"></div>
              <hr>

              <table class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th width="10">
                      <span class="uni">
                        <input type="checkbox" name="select_all[]" class="tbl_select_all">
                      </span>
                    </th>
                    <th><a href="?sort={{ nameHref }}">Album Name <i class="{{ nameIndicator ? nameIndicator : "" }}"></i></a></th>
                    <th><a href="?sort={{ picturesHref }}">Pictures <i class="{{ picturesIndicator ? picturesIndicator : "" }}"></i></a></th>
                    <th><a href="?sort={{ dateHref }}">Date Created <i class="{{ dateIndicator ? dateIndicator : "" }}"></i></a></th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($page->items as $al) { ?>
                  <tr id="album<?php echo $al['albumID'] ?>">
                    <td>                            
                      <span class="uni">
                        <input type="checkbox" name="tbl_id[]" data-albumName="<?php echo $al['albumName'] ?>" class="tbl_select_row" value="<?php echo $al['albumID'] ?>">
                      </span>
                    </td>
                    <td>
                      <a id="albumLink<?php echo $al['albumID'] ?>" href="/admin/partnersPictures/{{ partnerID }}/<?php echo $al['albumID'] ?>"><?php echo $al['albumName'] ?></a>
                    </td>
                    <td><?php echo $al['picCount'] ?></td>
                    <td><?php echo date("m-d-Y", $al['dateCreated']) ?></td>
                    <td width="100">
                      <a data-album-id="<?php echo $al['albumID']?>" data-partner-id="{{ partnerID }}" href="#editAlbumModal" class="editAlbumNameLink btn btn-xs btn-warning" data-toggle="modal" title="Edit"><i class="icon-pencil"></i></a>
                      <?php
                      $notempty = ($al['picCount'] != 'empty')?0:1;
                      ?>
                      <a data-empty="<?php echo $notempty ?>" data-album-id="<?php echo $al['albumID'] ?>" data-album-name="<?php echo $al['albumName'] ?>" href="#deleteAlbumConf" class="deleteAlbumLink btn btn-xs btn-danger" data-toggle="modal" title="Delete"><i class="icon-trash"></i></a>

                    <!--    <a id="addPictures" data-album-name="<?php //echo $albumCurrent ?>" data-album-id="<?php //echo $currentAlbum ?>" href="#modalDigitalAssets" class="btn btn-xs btn-primary" data-toggle="modal"><i class="icon icon-picture"></i></a> -->

                    </td>
                  </tr>
                  <?php } ?>
                  <?php if(empty($page->items)){ ?>
                    <tr style="text-align:center;"><td colspan="5">NO ALBUM FOUND</td></tr>
                  <?php } ?>
                </tbody>
              </table>

              <div class="tblbottomcontrol" style="display: none;">
                <a href="#deleteSelectedAlbumConf" id="deleteAllSelectedAlbumLink" data-toggle="modal"> Delete all Selected </a> |
                <a href="#" class="tbl_unselect_all"> Unselect </a>
              </div>

            </div>

            <div class="widget-foot">
              <!-- Footer goes here -->
              {% if showBackToList %} 
              <!-- {{ link_to("admin/partners", "Back to Partner List", 'class':'btn btn-primary') }}  -->
              <button class="btn btn-primary"> <a href="/admin/partners" style="text-decoration: none;color: white;"><span class="glyphicon glyphicon-circle-arrow-left"></span> Back to Partner List</a></button>
              {% endif %}
               {% set limit = 10 %}
                 {% set start = (limit * (page.current - 1)) + 1 %}
                 {% set end = (limit * (page.current-1)) + limit %}
                 {% if end > page.total_items %}
                 {% set end = page.total_items %}
                 {% endif %}
                 {% if limit %}
                 <div style="margin-left: 85.5%">
                 <span>&nbsp;Showing {{ start }} - {{ end  }} of {{ page.total_items }}</span>
                 </div>
                 {% endif %}
              {% if page.items and page.total_pages > 0 %}
                            <ul class="pagination pull-right">
                                <!---->
                                {% if page.current == 1 and page.total_pages >= 5 and page.total_pages > 0 %}                                
                                 <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current == 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current != 1 and page.current < 4 and page.total_pages >= 5 %}
                                <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"", 'First') }}</li>
                                <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and page.current+2 < page.total_pages%}
                                <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"", 'First') }}</li>
                                <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.current-2..page.current+2 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and (page.current+2 == page.total_pages or page.current+2 > page.total_pages) %}
                                <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"", 'First') }}</li>
                                <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.total_pages-4..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != page.last %}                 
                                <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ page.next, 'Next') }}</li>
                                <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ page.last, 'Last') }}</li>
                                </ul>
                                {% else %}
                                <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                                {% endif %}
                                <!---->
                              {% elseif page.total_pages == 0 %}
                              <ul class="pagination pull-right">
                              <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                              <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                 {% endif %}
              </ul>
              <div class="clearfix"></div>
            </div>


          </div>

          {% else %}
          <div class="form-group">
            <div class="col-lg-6">
              <div class="form-group">
                <label class="control-label col-lg-3">Search</label>
                <div class="col-lg-9"> 
                  {{ text_field('search_text' , 'class':'form-control') }}
                </div>
              </div>
            </div>

            <div class="col-lg-6">
              {{ submit_button('Search', 'name':'searchBtn', 'class':'btn btn-default') }}
              {# submit_button('Clear Search', 'class':'btn btn-default', 'name':'clear_search') #}
              <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
              <a id="addPictures" data-album-name="<?php echo $albumCurrent ?>" data-album-id="<?php echo $currentAlbum ?>" href="#modalDigitalAssets" class="btn btn-primary pull-right" data-toggle="modal">+ Add Pictures</a>
            </div>
          </div> 

          <div class="">
            <select id="albumSelect" name="album" class="form-control albums">
              <option value="">--Album--</option>
              <?php foreach ($albums as $key => $value) {
                if(!empty($selectedAlbum) && $selectedAlbum == $value->albumID){
                  $selected = 'selected';
                }else{
                  $selected = '';
                }
                echo '<option '.$selected.' value="/admin/partnersPictures/'.$value->partnerID.'/'.$value->albumID.'">'.$value->albumName.'</option>';
              } ?>
            </select>
            <!--<a href="#createAlbumModal" data-toggle="modal">Create Album</a>-->
          </div>

          <hr>

          <table class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th>
                  <span class="uni">
                    <input type="checkbox" name="select_all[]" class="tbl_select_all">
                  </span>
                </th>
                <th>Picture</th>
                <th>Caption</th>
                <th>Date</th>
                <th>Size</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($page->items as $pic) {
                ?>
                <tr id="picture<?php echo $pic['pictureID'] ?>">
                  <td>                            
                    <span class="uni">
                      <input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $pic['pictureID'] ?>">
                    </span>
                  </td>
                  <td><a class="prettyPhoto[pp_gal]" href="<?php echo $pic['path'] ?>"><img src="<?php echo $pic['thumbPath'] ?>" alt="<?php echo $pic['pictureCaption'] ?>" title="<?php echo $pic['pictureCaption'] ?>"></a></td>
                  <td><p id="caption<?php echo $pic['pictureID'] ?>"><?php echo $pic['pictureCaption'] ?></p></td>
                  <td><?php echo date("m-d-Y", $pic['dateUploaded']) ?></td>
                  <td><?php echo $pic['pictureSize'] ?></td>
                  <td>

                    <a data-picture-id="<?php echo $pic['pictureID']?>" href="#editCaptionModal" class="editCaptionLink btn btn-xs btn-warning" data-toggle="modal"><i class="icon-pencil"></i></a>

                    <a data-picture-id="<?php echo $pic['pictureID'] ?>" data-picture-album-id="<?php echo $pic['albumID'] ?>" href="#deletePictureConf" class="deletePictureLink btn btn-xs btn-danger" data-toggle="modal"><i class="icon-trash"></i></a>

                  </td>
                </tr>
                <?php
              } ?>

              <?php if(empty($page->items)){ ?>
                    <tr><td colspan="6" align="center">NO RESULTS FOUND</td></tr>
                  <?php } ?>
            </tbody>
          </table>
          <div class="tblbottomcontrol" style="display: none;">
            <a href="#deletePictureConf" id="deleteAllPictureSelectedLink" data-toggle="modal"> Delete all Selected </a> |
            <input type="hidden" name="moveALbumID" id="moveALbumID">
            <a href="#movePictureModal" data-toggle="modal" id="moveLink"> Move </a> |
            <a href="#" class="tbl_unselect_all"> Unselect </a>
          </div>

          {% endif %}

        </div>
      </div>

      {% if albumView == true %}
      {% else %}
      <div class="widget-foot">
        <!-- Footer goes here -->
        {{ link_to("admin/partnersPictures/"~ partnerID, "Back to Album List", 'class':'btn btn-default') }}
        {% if page.total_pages > 1 %}   
        <ul class="pagination pull-right">
          {% if page.current != 1 %}
          <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"/"~ currentAlbum ~"?page=" ~ page.before, 'Prev') }}</li>
          {% endif %}

          {% for index in 1..page.total_pages %}
          {% if page.current == index %}
          <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"/"~ currentAlbum ~"?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
          {% else %}
          <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"/"~ currentAlbum ~"?page=" ~ index, index) }}</li>
          {% endif %}
          {% endfor %}         

          {% if page.current != page.total_pages %}                 
          <li>{{ link_to("admin/partnersPictures/"~ partnerID ~"/"~ currentAlbum ~"?page=" ~ page.next, 'Next') }}</li>
          {% endif %}
        </ul>
        <div class="clearfix"></div>
        {% endif %}
      </div>
      {% endif %}

    </div>


  </div>

</div>


</div>



</form>
</div>

<!-- Matter ends