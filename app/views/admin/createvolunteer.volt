      <!-- Page heading -->
      <div class="page-head">
        <!-- Page heading -->
        <h2 class="pull-left"> 
          <!-- page meta -->
          <span class="page-meta">Volunteers</span>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="/admin"><i class="icon-home"></i> Home</a> 
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="/admin/abkrescueteam">Volunteers</a>
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="" class="bread-current">Create</a>          
        </div>

        <div class="clearfix"></div>

      </div>
      <!-- Page heading ends -->

      <!-- Matter -->

      <div class="matter">
        <div class="container">

          <div class="row">

            <div class="col-md-12">
             <!--  {{ content() }} -->
             {{ success }}

              <div class="widget wgreen">
                
                <div class="widget-head">
                  <div class="pull-left">Create Volunteer</div>
                  <div class="widget-icons pull-right">
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                    <h6>Details</h6>
                    <hr />
                    <!-- Form starts.  -->
                     {{ form('admin/createvolunteer', 'class': 'form-horizontal') }}

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label class="">{{ form.label('firstname') }}</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    {{ form.render('firstname') }}
                                    {{ form.messages('firstname') }}
                                    <span id="fnamehide">  {{ fname }} </span>
                                  </div>
                                 </div>
                              
                               <div class="form-group">
                                  <div class="col-lg-2">
                                  <label class="">{{ form.label('lastname') }}</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    {{ form.render('lastname') }}
                                    {{ form.messages('lastname') }}
                                  <span id="lnamehide">  {{ lname }} </span>
                                  </div>
                                </div>           

                               <div class="form-group">
                                  <div class="col-lg-2">
                                  <label class="">{{ form.label('email') }}</label>
                                  <span class="asterisk">*</span>
                                </div>
                                  <div class="col-lg-8">
                                    {{ form.render('email') }}
                                    {{ form.messages('email') }}
                                     <span id="emailhide">{{ emailval }}</span>
                                     <div class="label label-danger" id="valemail"></div>
                                  </div>
                                </div> 

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label class="">{{ form.label('address') }}</label>
                                </div>
                                  <div class="col-lg-8">
                                    {{ form.render('address') }}
                                    {{ form.messages('address') }}

                                  </div>
                                </div>

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('city') }}</label>
                                </div>
                                  <div class="col-lg-8">
                                    {{ form.render('city') }}
                                    {{ form.messages('city') }}
                                  </div>                                  
                                </div>  

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('country') }}</label>
                                   </div>
                                  <div class="col-lg-8"> 
                                    {{ form.render('country', ["class":'form-control'])
                                    }}
                                    {{ form.messages('country') }}
                                  </div>
                                </div>  

                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('state') }}</label>
                                   </div>
                                  <div class="col-lg-8">
                                    {{ form.render('state') }}
                                    {{ form.messages('state') }}
                                  </div>
                                </div> 


                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>Zip Code</label>
                                   </div>
                                  <div class="col-lg-8">
                                    {{ form.render('zip') }}
                                    {{ form.messages('zip') }}
                                  </div>
                                </div> 
                                
                                <div class="form-group">
                                  <div class="col-lg-2">
                                  <label>{{ form.label('contact') }}</label>
                                  <span class="asterisk">*</span>
                                  </div>
                                  <div class="col-lg-8">
                                    {{ form.render('contact') }}
                                    <!-- <input type="text" name="contactnomask" id="contactnomask" maxlength="11" class="form-control"> -->
                                    {{ form.messages('contact') }}
                                      <span id="contacthide">  {{ contactErr }} </span>
                                  </div>
                                </div> 

                                    <hr />
                                  {#{ form.render('csrf', ['value': security.getToken()]) }#}
                                  {#{ form.messages('csrf') }#}                                    
                                <div class="form-group">
                                  <div class="col-lg-offset-1 col-lg-9">
                                    {{ submit_button('Save Volunteer' , 'id':'btnSubmit', 'class':'btn btn-primary','id':'savepassBtn') }}
                                    <button type="reset" id="btnReset" class="btn btn-danger">Reset</button>
                                  </div>
                                </div>
                              </form>
                  </div>
                </div>
                  <div class="widget-foot">
                    <!-- <a href="{{ url('admin/abkrescueteam') }}">Back to Volunteer list</a> -->
                  </div>
              </div>  

            </div>

          </div>

        </div>
      </div>

    <!-- Matter ends -->