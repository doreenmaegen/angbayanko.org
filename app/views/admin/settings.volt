	    <!-- Modal Prompt-->
      <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title">Alert!</h4>
            </div>
            <div class="modal-body">
              <p class="modal-message"></p>
              <span class="modal-list-names"></span>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Cancel</button>
              <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
            </div>
          </div>
        </div>
      </div>
      <!-- Page heading -->
	    <div class="page-head">
        <!-- Page heading -->
	      <h2 class="pull-left"> 
          <!-- page meta -->
          <span class="page-meta">Settings</span>
        </h2>


        <!-- Breadcrumb -->
        <div class="bread-crumb pull-right">
          <a href="/admin"><i class="icon-home"></i> Home</a> 
          <!-- Divider -->
          <span class="divider">/</span> 
          <a href="#" class="bread-current">Settings</a>        
        </div>

        <div class="clearfix"></div>

	    </div>
	    <!-- Page heading ends -->
      <!-- Matter -->

        <div class="matter">
          <div class="container">
            {{ content()}}
            <div class="row">

            <div class="col-md-6">              
              <div class="widget">
                <div class="widget-head">
                  <div class="pull-left">{{ edit ? "Edit" : "Create" }} Slider</div>
                  <div class="widget-icons pull-right">
                  </div>  
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                    <!-- Form starts.  -->
                      {% if edit %}
                        <form action="{{ url('admin/settings/' ~ eslide.slideID ) }}" method="post" class="form-horizontal">
                      {% else %}
                        {{ form('admin/settings', 'class': 'form-horizontal') }}
                      {% endif %}
                      <div class="form-group" id="imageBanner">
                        <div class="col-lg-3">
                          <label class="">Preview</label>
                        </div>
                        <div class="col-lg-9" id="imagePreview">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-3">
                          <label class="">{{ form.label('imageUrl') }}</label>
                          <span class="asterisk">*</span>
                        </div>
                        <div class="col-lg-9">
                        {% if edit %}
                          <?php echo $form->render('imageUrl', array( 'value' => $eslide->slideImgUrl )); ?>
                        {% else %}
                          {{ form.render('imageUrl') }}
                        {% endif %}
                          {{ form.messages('imageUrl') }}{{ imgUrlError }}
                        </div>
                      </div>
                      <div class="form-group" id="embedVideo">
                        <div class="col-lg-3">
                          <label class="">Preview</label>
                        </div>
                        <div class="col-lg-9" id="videoPreview" class="embed-responsive embed-responsive-16by9">
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-lg-3">
                        <label class="">{{ form.label('videoUrl') }}</label>
                        </div>
                        <div class="col-lg-9">
                          {% if edit %}
                            <?php echo $form->render('videoUrl', array( 'value' => $eslide->slideVideoUrl )); ?>
                          {% else %}
                            {{ form.render('videoUrl') }}
                          {% endif %}
                          {{ form.messages('videoUrl') }}{{ vidUrlError }}
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-lg-3">
                        <label>Video Alignment</label>
                         </div>
                        <div class="col-lg-9">
                          <div class="radio">
                            <label>
                            {% if edit %}
                              {% if eslide.slideVideoAlignment == "left" %}
                                {{ radio_field('alignment', 'value':'left', 'checked':'checked') }}
                              {% else %}
                                {{ radio_field('alignment', 'value':'left') }}
                              {% endif %}
                            {% else %}
                              {{ radio_field('alignment', 'value':'left', 'checked':'checked') }}
                            {% endif %}
                              Left
                            </label>
                          </div>
                          <div class="radio">
                            <label>
                              {% if edit %}
                                {% if eslide.slideVideoAlignment == "center" %}
                                  {{ radio_field('alignment', 'value':'center', 'checked':'checked') }}
                                {% else %}
                                  {{ radio_field('alignment', 'value':'center') }}
                                {% endif %}
                              {% else %}
                                {{ radio_field('alignment', 'value':'center', 'checked':'checked') }}
                              {% endif %}
                              Center
                            </label>
                          </div>
                          <div class="radio">
                            <label>
                              {% if edit %}
                                {% if eslide.slideVideoAlignment == "right" %}
                                  {{ radio_field('alignment', 'value':'right', 'checked':'checked') }}
                                {% else %}
                                  {{ radio_field('alignment', 'value':'right') }}
                                {% endif %}
                              {% else %}
                                {{ radio_field('alignment', 'value':'right', 'checked':'checked') }}
                              {% endif %}
                              Right
                            </label>
                          </div>
                        </div>
                      </div>

                      <hr />
                      {#{ form.render('csrf', ['value': security.getToken()]) }}
                      {{ form.messages('csrf') }#}                                    
              <div style="text-align: center;">
                          {% if edit %}
                            {{ submit_button('Save Changes' , 'id':'btnSubmit', 'class':'btn btn-primary') }}
                          {% else %}
                            {{ submit_button('Create Slider' , 'id':'btnSubmit', 'class':'btn btn-primary') }}
                          {% endif %}
                          {% if edit %}
                            <a href="/admin/settings" class="btn btn-default">Cancel</a>
                          {% else %}
                            <button class="btn btn-danger" type="reset" onclick="javascript:removePreview()">Reset</button>
                          {% endif %}
                  </div>
                     </form>
                  </div>
                </div>
                <div class="widget-foot">
                  <!-- Footer goes here -->
                </div>
              </div>
            </div>

            <div class="col-md-6">              
              <div class="widget">
                <div class="widget-head">
                  <div class="pull-left">Manage Sliders</div>
                  <div class="widget-icons pull-right">
                  </div>  
                  <div class="clearfix"></div>
                </div>
                <div class="widget-content">
                  <div class="padd">
                    {{ form('admin/settings', 'class': 'form-horizontal', 'id':'main-table-form') }}
                      {#{ form.render('csrf', ['value': security.getToken()]) }}
                      {{ form.messages('csrf') }#}
                      <!-- Widget content -->
                      <input type="hidden" class="tbl-action" name="action" value=""/>
                      <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                      <input type="hidden" class="tbl-edit-url" name="editurl" value=""/>
                      <table class="table table-striped table-bordered table-hover">
                        <tbody>
                          <?php if($slides == true){ ?>
                          {% for s in slides %}
                          <tr>
                            <td class="name">
                              <label>Banner URL:</label> 
                                {% if s.slideImgUrl|length > 50 %}
                                  <span><?php echo substr($s->slideImgUrl, 0, 49) ?>...</span>
                                {% else %}
                                  <span>{{ s.slideImgUrl }}</span>
                                {% endif %}
                              <br/>
                              <label>Video URL:</label> <span>{{ s.slideVideoUrl ? s.slideVideoUrl : "None" }}</span>
                              <?php
                                if( !is_null($s->slideVideoUrl)) {
                                  echo "<br><label>Video Alignment:</label> <span>".ucwords($s->slideVideoAlignment)."</span>";
                                }
                              ?>
                            </td>
                            <td>
                              <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit-slide" data-recorID="{{ s.slideID }}"><i class="icon-pencil"></i>Edit </a>
                              <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="{{ s.slideID }}"><i class="icon-remove"></i>Delete </a>
                            </td>
                          </tr> 

                          {% endfor %}      
                          <?php } else {
                                  echo "<tr><td>No Slides Yet.</td></tr>";
                                } ?>
                        </tbody></table>
                      </form>
                  </div>
                </div>
               
              </div>
            </div>

          </div> <!-- row -->
        </div> <!-- container -->
      </div> <!-- matter -->\

         <div class="col-lg-12">
                    <div class="widget">
                      <div class="widget-head">
                      <div class="pull-left">Digital Assets (<?php echo count($digital_assets).' files'?>)</div>
                        <div class="clearfix"></div>
                      </div>
                      <div class="widget-content">
                        <div class="padd">
                            <span class="btn btn-success fileinput-button">
                              <i class="glyphicon glyphicon-plus"></i>
                              <span>Add files...</span>
                              <!-- The file input field used as target for the file upload widget -->
                              <input id="digitalAssets" type="file" name="files[]" multiple accept="image/*">
                            </span>
                            <!-- The global progress bar -->
                            <div id="progress" class="progress">
                              <div class="progress-bar progress-bar-success"></div>
                            </div>
                            <div class="gallery digital-assets-gallery">
                              {% for img in digital_assets %}
                                <div class="program-digital-assets-library pull-left" style="position: relative;">
                                  <a href='{{ url("img/programs/" ~ prog.programID ~ "/" ~ img) }}' class='prettyPhoto[pp_gal]'>
                                  <img src='{{ url("img/programs/" ~ prog.programID ~ "/" ~ img) }}' alt=""></a>
                                  <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value='{{ url("img/programs/" ~ prog.programID ~ "/" ~ img) }}'>
                                  <button type="button" class="btn btn-xs btn-danger digital-assets-delete" data-filename="{{ img }}" data-folder="{{ prog.programID }}" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
                                </div>
                              {% endfor %}
                              <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="widget-foot">
                        </div>
                      </div>
                    </div>
                    </div>  

                     <div class="widget-foot">
                  <!-- Footer goes here -->
                </div>  

        
