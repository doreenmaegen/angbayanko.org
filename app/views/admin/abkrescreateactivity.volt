 <!--Modal View-->
<div id="modalView" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog" style="width: 800px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">View Content</h4>
      </div>
      <div class="modal-body" style=" height: 400px; overflow:scroll;">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
      </div>
    </div>
  </div>
</div>

<?php $token = $this->security->getToken(); ?>
<div class="page-head">

  <h2 class="pull-left">
    <span class="page-meta">Activities</span>
  </h2>

  <div class="bread-crumb pull-right">
    <a href="/admin"><i class="icon-home"></i> Home</a>
    <span class="divider">/</span>
    <a href="/admin/pages">Pages</a>
    <span class="divider">/</span>
    <a href="" class="bread-current">Create</a>
  </div>
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
    {{ form('admin/abkrescreateactivity', 'class': 'form-horizontal') }}
    <!--start row-->
    <div class="row">
      <div class="col-md-8">
        <div class="widget">
          <div class="widget-head">
            Create Activity
            <div class="widget-icons pull-right">
            </div>
          </div>

          <div class="widget-content">
            <div class="padd">

              {{ success }}
              <div>
                <label>Activity Title</label>
                <span class="asterisk">*</span><span id="at">{{ acttil  }}</span>
              </div>
              <div class="form-group">
                <div class="col-lg-12">
                  {{ text_field('page_title', 'name':'page_title' , 'placeholder':'Enter title', 'class':'form-control') }}
                  {{ hidden_field('hpage_slug', 'id':'hpage_slug') }}
                  <label>Slug:</label> <span id="page-slug"></span>
                <!--   {{ actslug }} -->
                </div>
              </div>

              <label>Activity Keyword</label>
              <span class="asterisk">*</span><span id="ak">{{ actkey }}</span>
              <div class="form-group">

                <div class="col-lg-12">
                  {{ text_field('page_keyword', 'class':'form-control', 'placeholder':'Enter Keywords separated by comma. Ex. disaster, philippine climate change, philippine emergency') }}
                </div>
              </div>

              <label>Activity Description</label>
              <span class="asterisk">*</span><span id="ad" >{{ actdis }}</span>
              <div class="form-group">

                <div class="col-lg-12">
                  {{ text_field('page_description', 'class':'form-control', 'placeholder':'Enter Description of Activity') }}
                </div>
              </div>


              <label>Activity Content</label>
              <span class="asterisk">*</span><span id="errdetails" > {{ actcon }} </span>
              {{ text_area('page_content', 'name':'page_content' , 'placeholder':'Enter content', 'class':'form-control programPageText') }}

              <!-- <em>Last Updated: October 21, 2014 09:36:20 pm</em> -->

              <br /><br />
              <input type="checkbox" name="page_active" value="1" checked> <label>Active</label>
              <br/> <br/>
              {{ submit_button("save_page", 'class':'btn btn-primary', 'value':'Publish Activity', 'name':'save_page','id':'pubact' ) }}
              <button type="reset" value="Reset" class="btn btn-danger" onClick="CKEDITOR.instances.page_content.setData( '', function() { this.upd@media (min-width: 1400) and (max-width: 1660px){}ateElement(); } )">Reset <i class="glyphicon glyphicon-refresh"></i></button>

              <a href="#modalView" data-toggle="modal" type="button" class="btn btn-info view-page-sample" data-page-content-id="page_content">Info</a>

              <input type="hidden" name="csrf"value="<?php echo $token ?>"/>

            </div>
          </div>
        </div>
      </div>

      {#<div class="col-md-4">
      <div class="widget">
      <div class="widget-head">
      Options
      <div class="widget-icons pull-right">
    </div>
  </div>

  <div class="widget-content">
  <div class="padd">

  <div class="clearfix"></div>
  <div class="form-horizontal">
  <div class="form-group">

  <div class="col-lg-7">
  <div>
  <label> Page Order</label>
</div>
</div>
<div class="col-lg-5">
<?php $stat = count($find); ?>
{{ numeric_field('page_order', 'placeholder':'0', 'class':'form-control','min':'0',
'id':'actorder', 'value':stat) }}
</div>
</div>

<!-- <div class="form-group">
<div class="col-lg-7">
<div>
<label> Featured </label>
</div>
</div>
<div class="col-lg-5">
<input type="checkbox" name="page_featured" id="featured" value="1"> <label>Featured</label>
</div>
</div> -->

</div>


<button type="button" class="btn btn-default pull-right" id="removeSideBarImg" style="display:none">Remove Image</button>
<div class="clearfix"></div>

</div>
</div>

</div>
</div>#}


<div class="col-md-4">
  <div class="widget">
    <div class="widget-head">
      <div class="pull-left">Activity Banner</div>
      <span class="asterisk">*</span> <span id="pB">{{ actban }}</span>
      <div class="widget-icons pull-right">
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="widget-content">
      <div class="padd">
        <div class="form-group">
          <div class="col-lg-12">
            <div id="pageBannerImgWrapper"></div>
            <!-- <input type="text" id="pageBannerUrl" name="page_banner" class="form-control" placeholder="Place banner link here."> -->

            {{ text_field('pageBannerUrl', 'class':'form-control', 'placeholder':'Page banner URL here.') }}
          </div>
        </div>

        <button type="button" class="btn btn-default pull-right" id="removeBanner" style="display:none">Reset Banner</button>
        <!--<input type="submit" name="updatebanner" value="Update Banner" class="btn btn-primary pull-right"> -->

        <div class="clearfix"></div>
      </div>
      <div class="widget-foot"></div>
    </div>
  </div>
</div>

</div><!--end row-->




<!--start row-->
<div class="row">
  <div class="col-lg-12">
    <div class="widget">
      <div class="widget-head">
        <div class="pull-left">Digital Assets (<span id="fileCount"><?php echo count($pictures)?></span> files)</div>
        <div class="widget-icons pull-right">
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="widget-content">
        <div class="padd">
          <div id="imageError"></div>
          <span class="btn btn-success fileinput-button">
            <i class="glyphicon glyphicon-plus"></i>
            <span>Add Photo</span>
            <!-- The file input field used as target for the file upload widget -->
            <input id="actpictures" type="file" name="files[]" multiple accept="image/*">
          </span>
          <!-- The global progress bar -->
          <div id="progress" class="progress">
            <div class="progress-bar progress-bar-success"></div>
          </div>
          <div class="gallery" id="pages-gallery">
            {% for img in pictures %}
            <div class="program-digital-assets-library pull-left" style="position: relative">
              <a href="{{ url(img.imgpath~img.fileName) }}" class='prettyPhoto[pp_gal]'>
                <img src="{{ url(img.imgpath~img.fileName) }}" alt=""></a>
                <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="{{ url(img.imgpath~img.fileName) }}">
                <button type="button" class="btn btn-xs btn-danger delete-recent-upload-page-pic" data-picture-id="{{ img.imgID }}"  style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
              </div>
              {% endfor %}
              <div class="clearfix"></div>
            </div>
          </div>
          <div class="widget-foot">
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--end row-->



</form>
</div><!--end container-->
</div><!--end matter


<script>
  function clearck()
    {
      CKEDITOR.instances.pagecontent.setData("");
    }
</script>
