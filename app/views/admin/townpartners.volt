<?php $token = $this->security->getToken() ?>
<!-- Modal Prompt-->
<div id="addpartner" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header-color-now" style="background-color: #428bca;height: 40px;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;">×</button>
        <label><h4 class="modal-title-ok" style="color: white;font-size: 17px;">Add Partner</h4></label>
      </div>

	{{ form('class': 'form-horizontal', 'id':'main-table-form') }}
		<input type="hidden" name="csrf_add_partner" value="<?php echo $token ?>"/>
		<div class="modal-body">
			<select id="partnerSelect" name="partnerSelect[]" multiple data-placeholder="Select Partner" class="chosen-select form-control" onchange="go(this.value);">
	            <option value=""></option>
	            {{ partnerOptions }}
          </select>
		</div>
		<div class="modal-footer">
			<!-- {{ submit_button('Save' , 'class':'btn btn-success', 'name':'savePartner', 'id': 'savePartner') }} -->
			<button type="submit" id="savePartner" value="send" class="btn btn-success" name="savePartner" disabled="true"><span class="glyphicon glyphicon-floppy-disk"> </span> Save</button>
			<button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
		</div>
    </form>

    </div>
  </div>
</div>

<!-- Modal Prompt-->
  <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Alert!</h4>
        </div> -->
         <div id="headerColor">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
         <span  style="color: white;font-size: 17px" id="changewarning"></span>
       </div>
        <div class="modal-body">
          <p class="modal-message"></p>
          <span class="modal-list-names"></span>
        </div>
        <div class="modal-footer">

          <button type="button" class="btn btn-primary modal-btn-yes"><a href="#" data-form='userform' style="color: white; text-decoration: none;"><span class="glyphicon glyphicon-ok"></span> Yes</a></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span> No</button>

        </div>
      </div>
    </div>
  </div>


<!-- Page heading -->
<div class="page-head">
	<h2 class="pull-left">Towns</h2>

	<!-- Breadcrumb -->
	<div class="bread-crumb pull-right">
		<a href="{{ url('admin') }}"><i class="icon-home"></i> Home</a>
		<!-- Divider -->
		<span class="divider">/</span>
		{{ link_to('admin/towns', 'Towns') }}
		<span class="divider">/</span>
		<span>ABK Partners</span>
	</div>

	<div class="clearfix"></div>

</div>


<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">

	{{ form('class': 'form-horizontal', 'id':'main-table-form') }}
	<div class="container">

		<!-- Table -->

		<div class="row">


			<div class="col-md-12">
				{{ content() }}


				<div class="widget">

					<div class="widget-head">
						<div class="pull-left">Town Info</div>
						<div class="widget-icons pull-right">
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="padd">
                    {{ townTab }}
                    </div>




					<div class="widget-content">
						<input type="hidden" name="csrf" value="<?php echo $token ?>"/>
						<input type="hidden" class="tbl-action" name="action" value=""/>
						<input type="hidden" class="tbl-recordID" name="recordID" value=""/>
						<input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
						<table class="table table-striped table-bordered table-hover tblusers">
							<thead>
								<tr>
									<th colspan="3">
										<div class="col-lg-6">
											<div class="form-group">
												<label class="control-label col-lg-3">Search</label>
												<div class="col-lg-9">
													{{ text_field('search_text' , 'class':'form-control') }}
												</div>
											</div>
										</div>

										<div class="col-lg-6">
											   <button type="submit" name="searchBtn" class="btn btn-success" value="Clear Search"><span class="glyphicon glyphicon-search"></span> Search</button>
                                               <button type="submit" name="clear_search" class="btn btn-danger" value="Clear Search"><span class="icon-refresh"></span> Refresh</button>
											<a id="addpartnerlink" href="#addpartner" style="margin-right:25px" class="btn btn-primary pull-right" data-toggle="modal">+ Add Partner</a>
										</div>
									</th>
								</tr>
								<tr>
									<th width="10">{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th>
									<th><a href="?sort={{ nameHref }}">Partner Name <i class="{{ nameIndicator ? nameIndicator : "" }}"></i></a></th>
									<th width="10">Action</th>
								</tr>
							</thead>
							<tbody>
								{% if page.total_pages == 0 %}
		                          <tr>
		                            <td colspan="2" style="text-align: center;">No partners found</td>
		                          </tr>
		                          {% else %}
		                          {% for post in page.items %}
		                          <tr>
		                            <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="{{ post['partnerID']}}"> </td>
		                            <td class="name">{{ link_to('admin/partnersinfo/'~post['partnerID'], post['partnerName']) }}</td>
		                            <td>

		                              <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="{{ post['partnerID'] }}"><i class="icon-remove"></i> </a>
		                            </td>
		                          </tr>
		                          {% endfor %}
		                          {% endif %}

							</tbody>
						</table>

						<div class="tblbottomcontrol" style="display:none">
							<a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Remove all Selected </a> |
							<a href="#" class="tbl_unselect_all"> Unselect </a>
						</div>

						<div class="widget-foot">

							{% set limit = 10 %}
                              {% set start = (limit * (page.current - 1)) + 1 %}
                              {% set end = (limit * (page.current-1)) + limit %}

                              {% if end > page.total_items %}
                              {% set end = page.total_items %}
                              {% endif %}
                              {% if limit %}
                              <div style="margin-left: 85.5%">
                                <span>&nbsp;Showing {{ start }} - {{ end  }} of {{ page.total_items }}</span>
                              </div>
                              {% endif %}

                              {% if page.items %}
                                <ul class="pagination pull-right">
                                <!---->
                                {% if page.current == 1 and page.total_pages >= 5 %}
                                 <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/towns?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/towns?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current == 1 and page.total_pages < 5 %}
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/towns?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/towns?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->

                                <!---->
                                {% if page.current != 1 and page.total_pages < 5 %}
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/towns?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/towns?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current != 1 and page.current < 4 and page.total_pages >= 5 %}
                                <li>{{ link_to("admin/towns", 'First') }}</li>
                                <li>{{ link_to("admin/towns?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/towns?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/towns?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and page.current+2 < page.total_pages%}
                                <li>{{ link_to("admin/towns", 'First') }}</li>
                                <li>{{ link_to("admin/towns?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.current-2..page.current+2 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/towns?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/towns?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and (page.current+2 == page.total_pages or page.current+2 > page.total_pages) %}
                                <li>{{ link_to("admin/towns", 'First') }}</li>
                                <li>{{ link_to("admin/towns?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.total_pages-4..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/towns?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/towns?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->

                                <!---->
                                {% if page.current != page.last %}
                                <li>{{ link_to("admin/towns?page=" ~ page.next, 'Next') }}</li>
                                <li>{{ link_to("admin/towns?page=" ~ page.last, 'Last') }}</li>
                                </ul>
                                {% else %}
                                <li>{{ link_to("admin/towns?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/towns?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:btn-default') }}</li>
                                </ul>
                                {% endif %}
                                <!---->
                              {% elseif !page.items %}
                              <ul class="pagination pull-right">
                              <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                              <li>{{ link_to("admin/towns?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/towns?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:btn-default') }}</li>
                                </ul>
                              {% endif %}
							<a href="/admin/towns" class="btn btn-primary"><span class="glyphicon glyphicon-circle-arrow-left"></span> Back to Town List</a>
							<div class="clearfix"></div>

						</div>

					</div>

				</div>


			</div>

		</div>


	</div>
</form>

</div>
<script type="text/javascript">
	function go(x){
		console.log(x);
		if(x != ''){
			$('#savePartner').prop('disabled',false);
		}else{
			$('#savePartner').prop('disabled',true);
		}
	}
</script>
<!-- Matter ends -->
