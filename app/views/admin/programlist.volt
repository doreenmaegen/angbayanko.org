 <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div id="headerColor">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
        <span  style="color: white;font-size: 17px" id="changewarning"></span>
      </div>
      <div class="modal-body">
        <p class="modal-message"></p>
        <span class="modal-list-names"></span>
      </div>
      <div class="modal-footer">
<!--                     <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'>Yes</a>
        <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</a> -->
    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form='userform'><span class="icon glyphicon glyphicon-ok"></span> Yes</a>
    <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> No</a>
      </div>
    </div>
  </div>
</div>


<div class="col-md-12">
      <div class="widget">
        <div class="widget-head">
          <div class="pull-left">List of Programs</div>
            <div class="widget-icons pull-right">
            </div>  
          <div class="clearfix"></div>
        </div>
          <div class="widget-content referrer">
            {{ form('admin/programs', 'class': 'form-horizontal', 'id':'main-table-form') }}
            <input type="hidden" class="tbl-action" name="action" value=""/>
            <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
            <input type="hidden" class="tbl-edit-url" name="editurl" value=""/>
              <table class="table table-striped table-bordered table-hover">
                <thead>
                  <th>Title</th>
                  <th>SLUGS</th>
                  <th style="width: 200px;">Action</th>
                </thead>
                <tbody>   
                <?php if($prog == true){ ?>
                {% for p in prog %}
                <tr>
                <td class="name">{{ p.programName }}</td>
                <td class="name">{{ p.programPage }}</td>
                <td>
                <a href="#modalPrompt" class="btn btn-xs btn-primary tbl_manage_row modal-control-button" data-toggle="modal" data-action="manage" data-recorID="{{ p.programID }}"><i class="glyphicon glyphicon-th-list"></i> Manage </a>
                <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="programs/{{ p.programID }}"><i class="icon-pencil"></i> Edit </a>
                <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="programs/{{ p.programID }}"><i class="icon-remove"></i> Delete</a>
                </td>
                </tr> 

                {% endfor %}      
                <?php } else {
                echo "<tr><td>No Programs Yet.</td></tr>";
                } ?>
                </tbody>
              </table>
            </form>
          </div>
      </div>
    </div>