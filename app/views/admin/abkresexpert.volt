
<div class="page-head">

  <h2 class="pull-left">
    <span class="page-meta">ABKRESCUE EXPERTIES</span>
  </h2>

  <div class="bread-crumb pull-right">
    <a href="/admin"><i class="icon-home"></i> Home</a>
    <span class="divider">/</span>
    <a href="" class="bread-current">Create</a>
  </div>
  <div class="clearfix"></div>
</div>

<div class="matter">
  <div class="container">
  {{ content() }}
  {{ form('admin/abkresexpert', 'class': 'form-horizontal') }}
    <!--start row-->
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <div class="widget-head">
            EXPERTIES
            <div class="widget-icons pull-right">
            </div>
          </div>

          <div class="widget-content">
            <div class="padd">


              <div>
              <!-- <label>Page Title</label>
              <span class="asterisk">*</span>{{ titleError }} -->
            </div>

            <div class="form-group">
            <div class="col-sm-12">
             <div class="form-group">
                <div class="col-lg-12">
                <label class="">Content</label>
                  <span class="asterisk">*</span>
                </div>
                <div class="col-lg-12">
                  <!-- <?php echo $form->render('desc', array( 'value' => $event->shortdesc)); ?> -->
                  {{ text_area('page_content', 'name':'description' , 'placeholder':'Enter content', 'class':'form-control programPageText','value':expert['description']) }}

                </div>
              </div>
            </div>

            </div>


              <br /><br />
              {{ submit_button("save_page", 'class':'btn btn-primary', 'value':'Update', 'name':'save_page','id':'savepassBtn' ) }}
    <button class="btn btn-danger "><a href="/admin" style="text-decoration: none; color: white;"><span class="glyphicon glyphicon-remove"></span>  Cancel</a></button>
              <input type="hidden" name="csrf"value="<?php echo $token ?>"/>


          </div>
        </div>
      </div>
    </div>


    </div><!--end row-->


      </form>
    </div><!--end container-->
</div><!--end matter
