      <style type="text/css">
              #saveBtn{
                  background: none;
                  border: none;
                  outline: none;
                  color: white;
                  text-decoration:none;
                  cursor: pointer;

              }
              #saveBtn:hover {
              background-color:none;

                }
              </style>
            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> Suggested Programs / Activities</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="{{ url('admin') }}"><i class="icon-home"></i> Home</a>
                <!-- Divider -->
                <span class="divider">/</span>
                <a href="" class="bread-current">Suggested Programs</a>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->




{{ form('admin/suggestedprograms', 'id':'main-table-form') }}
  <div class="container">


      <?php if($viewMode){ ?>
        <!-- Modal Prompt-->
        <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">

              <div id="headerColor">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
                <h4 class="modal-title" style="font-size: 17px;color:white;">Alert!</h4>
              </div>
              <div class="modal-body">
                <p class="modal-message"></p>
                <span class="modal-list-names"></span>
              </div>
              <div class="modal-footer">
                <a href="#" type="button" class="btn btn-primary " data-form='userform'><span class="icon glyphicon glyphicon-ok"></span> ssYes</a>
               <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> No</a>
             </div>
            </div>
          </div>
        </div>

        <div class="col-md-12">
        {{ content() }}


        <div class="widget">

          <div class="widget-head">
            <div class="pull-left">Program Information</div>
            <div class="widget-icons pull-right">
            </div>
            <div class="clearfix"></div>
          </div>


              <div class="widget-content">
                {{ hidden_field('csrf', 'value': security.getToken())}}
              <input type="hidden" class="tbl-action" name="action" value=""/>
              <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
              <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>
                <div class="padd">
                  <label>Title</label><br />
                  <span class="name"><?php echo $suggested['progTitle'] ?></span>
                  <br /><br />
                  <label>Description</label><br />
                  <?php echo nl2br($suggested['progDesc']) ?>

                  <hr />
                  <label>Suggested By:</label><br />
                  <strong><?php echo $suggested['suggBy'] ?></strong>
                  <br />
                  Email: <?php echo $suggested['email'] ?> <br />
                  Address: <?php echo $suggested['address'] ?> <br />
                  Phone: <?php echo $suggested['phone'] ?> <br />

                </div>
                <div class="widget-foot">
                  <a href="/admin/suggestedprograms" class="btn btn-default">Back to Suggested Program List</a>

                  <div class="clearfix"></div>
                </div>


              </div>

            </div>

          </div>

        </div>
      <?php }else{ ?>

      <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div id="headerColor">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <span class="modal-title" id="changewarning" style="font-size: 17px"></span>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>

                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                <a  type="button" class="btn btn-primary modal-btn-yes" data-form='userform'><span class="icon glyphicon glyphicon-ok"></span> Yes</a>
                <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> No</a>
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
                  <div class="col-md-2">
                    <label>Search by Text</label>
                  </div>
                  <div class="col-md-6 form-group">
                    <div class="col-md-12">
                      {{ text_field('search_text' , 'class':'form-control'  , 'id':'GG') }}
                    </div>
                  </div>
                  <div class="col-md-3 form-group">
                   <!--  {{ submit_button('Search', 'class':'btn btn-default') }} -->
            <button type="submit" name="Search" class='btn btn-success'><span class="icon-search"></span>&nbsp;Search</button>
                    <!-- <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"><a href="/admin/suggestedprograms"></a></span></button> -->
                     <!-- <button name="clear_search" class="btn btn-danger"><span class="icon-refresh"><a href="/admin/suggestedprograms"></a></span> Refresh</button> -->
                     <button class="btn btn-danger"><span class="icon-refresh"></span>
                      <input type="submit" name="clear_search" value="Refresh" id="saveBtn">
                     </button>
                  </div>
                </div>

                <!-- Table -->
                <div class="row">
                  <div class="col-md-2">
                    <label>Filter by Date</label>
                  </div>
                  <div class="col-md-4  form-group start" id="fromDatepicker" class="input-append">
                    <label class="col-md-2 control-label">From</label>
                    <div class="col-md-10">
                      {{ text_field('fromDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-3 form-group end" id="toDatepicker" class="input-append" style="margin-left: -8.5%;">
                    <label class="col-md-2 control-label">To</label>
                    <div class="col-md-12">
                      {{ text_field('toDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-2">
                    {{ submit_button('Filter', 'class':'btn btn-primary') }}
                  </div>
                </div>

         {{ content() }}
      <!-- <div class="form-group">
        <label class="col-lg-1 control-label">Search</label>
        <div class="col-lg-4">
              {#{ text_field('search_text' , 'class':'form-control') }#}
          </div>
        <div class="col-lg-6">
          {#{ submit_button('Search', 'class':'btn btn-default') }#}
          <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
        </div>

      </div>   -->

      <div class="">


        <div class="widget">

          <div class="widget-head">
            <div class="pull-left">Programs List</div>
            <div class="widget-icons pull-right">
            </div>
            <div class="clearfix"></div>
          </div>


          <div class="widget-content">
            {{ hidden_field('csrf', 'value': security.getToken())}}
            <input type="hidden" class="tbl-action" name="action" value=""/>
            <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
            <input type="hidden" class="tbl-edit-url" name="editurl" value="editpage/"/>



            <table class="table table-striped table-bordered table-hover tblusers">
              <thead>
                <tr>
                  <th width="10">{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th>
                  <th><a href="?sort={{ programHref }}">Program / Activity <i class="{{ programIndicator ? programIndicator : "" }}"></i></a></th>
                  <th width="100"><a href="?sort={{ usernameHref }}">Username <i class="{{ usernameIndicator ? usernameIndicator : "" }}"></i></a></th>
                  <th width="210"><a href="?sort={{ nameHref }}">Name <i class="{{ nameIndicator ? nameIndicator : "" }}"></i></a></th>
                  <th width="150"><a href="?sort={{ dateHref }}">Date <i class="{{ dateIndicator ? dateIndicator : "" }}"></i></a></th>
                  <th width="120">Action</th>
                </tr>
              </thead>
              <tbody>

                <?php
                if($page->items != 0){
                  foreach ($page->items as $key => $value) { ?>
                    <tr>
                      <td><input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="<?php echo $value['vActivityID'] ?>"></td>
                        <td class="name">
                          <?php echo $value['program'] ?>
                        </td>
                        <td><?php echo $value['username'] ?></td>
                        <td><?php echo $value['name'] ?></td>
                        <td><?php echo date("F j, Y", $value['dateAdded']) ?></td>
                        <td style="width: 150px">

                           <a href="/admin/suggestedprograms/<?php echo $value['vActivityID'] ?>" class="btn btn-xs btn-success modal-control-button"><i class="icon-ok"></i> View</a>

                           <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button " data-toggle="modal" data-action="delete" data-recorID="<?php echo $value['vActivityID'] ?>"><i class="icon-remove"></i> Delete</a>

                        </td>
                    </tr>

                <?php
                  }
                }

                if(empty($page->items)){
                  echo '<tr><td colspan="6"><center>NO RESULTS FOUND.<center></td></tr>';

              }
                ?>
              </tbody>
              </table>

              <div class="tblbottomcontrol" style="display:none">
                <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> |
                <a href="#" class="tbl_unselect_all"> Unselect </a>
              </div>


                  <div class="widget-foot">
                              {% set limit = 10 %}
                              {% set start = (limit * (page.current - 1)) + 1 %}
                              {% set end = (limit * (page.current-1)) + limit %}

                              {% if end > page.total_items %}
                              {% set end = page.total_items %}
                              {% endif %}
                              {% if limit %}
                              <div style="margin-left: 85.5%">
                                <span>&nbsp;Showing {{ start }} - {{ end  }} of {{ page.total_items }}</span>
                              </div>
                              {% endif %}
                              {% if page.items and page.total_pages > 0 %}
                                <ul class="pagination pull-right">
                                    <!---->
                                {% if page.current == 1 and page.total_pages >= 5 and page.total_pages > 0 %}
                                 <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/suggestedprograms?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/suggestedprograms?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current == 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/suggestedprograms?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/suggestedprograms?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/suggestedprograms?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/suggestedprograms?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current != 1 and page.current < 4 and page.total_pages >= 5 %}
                                <li>{{ link_to("admin/suggestedprograms", 'First') }}</li>
                                <li>{{ link_to("admin/suggestedprograms?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/suggestedprograms?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/suggestedprograms?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and page.current+2 < page.total_pages%}
                                <li>{{ link_to("admin/suggestedprograms", 'First') }}</li>
                                <li>{{ link_to("admin/suggestedprograms?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.current-2..page.current+2 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/suggestedprograms?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/suggestedprograms?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and (page.current+2 == page.total_pages or page.current+2 > page.total_pages) %}
                                <li>{{ link_to("admin/suggestedprograms", 'First') }}</li>
                                <li>{{ link_to("admin/suggestedprograms?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.total_pages-4..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/suggestedprograms?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/suggestedprograms?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != page.last %}
                                <li>{{ link_to("admin/suggestedprograms?page=" ~ page.next, 'Next') }}</li>
                                <li>{{ link_to("admin/suggestedprograms?page=" ~ page.last, 'Last') }}</li>
                                </ul>
                                {% else %}
                                <li>{{ link_to("admin/suggestedprograms?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/suggestedprograms?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                                {% endif %}
                                <!---->
                              {% elseif page.total_pages == 0 %}
                              <ul class="pagination pull-right">
                              <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                              <li>{{ link_to("admin/suggestedprograms?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/suggestedprograms?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                 {% endif %}

                              <div class="clearfix"></div>

                            </div>

            </div>

          </div>

        </div>
        <?php } ?>

      </div>
    </form>

</div>

<!-- Matter ends -->
<script type="text/javascript">
  $().ready(function(){
                            $("#GG").keyup(removeextra).blur(removeextra);
                          });
                          function removeextra() {
                            var initVal = $(this).val();
                            outputVal = initVal.replace(/[^a-zA-Z .]/g,"");
                            if (initVal != outputVal) {
                              $(this).val(outputVal);
                            }
                          };
</script>
