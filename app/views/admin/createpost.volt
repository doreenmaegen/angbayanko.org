             <!-- Modal View-->


            <div id="modalDigitalAssets" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
              <div class="modal-dialog" style="width: 900px;">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Digital Assets Library</h4>
                    <input type="hidden" id="digital-assets-year" name="digital-assets-year" value="{{ dayear }}" />
                    <input type="hidden" id="digital-assets-month" name="digital-assets-month" value="{{ damonth }}" />
                  </div>
                  <div class="modal-body" style=" height: 400px; overflow:scroll;">
                    <span class="btn btn-success fileinput-button">
                      <i class="glyphicon glyphicon-plus"></i>
                      <span>Add files...</span>
                      <!-- The file input field used as target for the file upload widget -->
                      <input id="digitalAssets" type="file" name="files[]" multiple accept="image/*">
                    </span>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                      <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <label>Recently Added</label>
                    <div class="gallery digital-assets-gallery">
                      None
                    </div>
                    <div class="clearfix"></div>
                    <div class="tabbable" style="margin-bottom: 18px;">
                      <ul class="nav nav-tabs">
                        <?php
                        $y = 0;
                        foreach ($folders as $key => $value) {
                          if($y==0){
                            echo '<li class="active"><a href="#'.$key.'" data-toggle="tab" class="post-year-tab">'.$key.'</a></li>';
                          }else{
                            echo '<li><a href="#'.$key.'" data-toggle="tab" class="post-year-tab">'.$key.'</a></li>';
                          }
                          $y++;
                        }
                        ?>
                      </ul>
                      <div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
                        <?php
                        foreach ($folders as $key => $value) {
                          ?>
                          <div class="tab-pane active" id="<?php echo $key?>">
                            <br/>
                            <select class="form-control post-assets-month">
                              <?php
                              $x=0;
                              foreach($value as $v){
                                $jd=gregoriantojd($v,13,1998);
                                if($x==0){
                                  echo '<option value="'.$v.'" selected="selected">'.jdmonthname($jd,0).'</option>';
                                }else{
                                  echo '<option value="'.$v.'">'.jdmonthname($jd,0).'</option>';
                                }
                                $x++;
                              }
                              ?>
                            </select>
                            <br/>
                            <div class="gallery digital-assets-monthly">

                            </div>
                          </div>
                          <?php
                        }
                        ?>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- Page heading -->
            <div class="page-head">
              <!-- Page heading -->
              <h2 class="pull-left">
                <!-- page meta -->
                <span class="page-meta">Create News Post</span>
              </h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="/admin"><i class="icon-home"></i> Home</a>
                <!-- Divider -->
                <span class="divider">/</span>
                <a href="/admin/allnewspost">News Post</a>
                <!-- Divider -->
                <span class="divider">/</span>
                <a href="/admin/createpost" class="bread-current">Create</a>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->
            <!-- Matter -->
            {{ script }}
            <div class="matter">
              <div class="container">
                <form name="postform" method="post" action="">
                  <div class="row">
                     <!--  {{ titlexist }} -->
                    <div class="col-md-8">
                 <!--      {{ content() }} -->
                    {{ success }}
                      <div class="widget">
                        <div class="widget-head">
                          <div class="pull-left">Create Post</div>
                          <div class="widget-icons pull-right">
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                          <div class="padd">
                            <div class="form-group">
                              <div class="control-label">
                              <label>News Title</label>
                              <span class="asterisk">*</span>
                            </div>
                              <div>
                             <!--  {{ text_field("Enter Title", 'class':'form-control', 'placeholder':'Enter News Title', 'name':'post_title','id':'post_title') }} -->
                                {{ form.render('post_title') }}
                                <span id="post_title_hide">{{ titleError }}</span>


                             <br>
                              <!--   <label>Slug: </label>  -->
                             <!--    <input type="text" name="slug"> -->
                             {{ hidden_field('hpage_slug', 'id':'hpage_slug','name':'post_slug','value':post.postSlug) }}
                             <label>Slug:</label> <span id="page-slug">{{ post.postSlug }}</span>
                              </div>

                            </div>

                            <div class="text-area">
                              <div class="control-label">
                              <label>Content</label>
                              <span class="asterisk">*</span>
                            </div>
                              {{ form.render('post_content') }}
                            </div>
                                <span id="errdetails">{{ contentError }}</span>
                          </div>

                        </div>
                      </div>
                    </div>

                    <!-- post sidebar -->

                       <div class="col-md-4">

                      <div class="widget">
                        <div class="widget-head">
                          <div class="pull-left">Options</div>
                          <div class="widget-icons pull-right">
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                          <div class="padd">
                            <hr/>
                            <div class="check-box">
                              <label>{{ check_field('post_status', 'class':'tbl_select_row', 'value':'draft') }} Save as Draft</label>
                            </div>
                            <hr/>
                           <div class="form-group">
                              <label>Publish on: </label>
                              <br>
                              <div class="input-append pull-left start">
                        {{ text_field('startdate', 'data-format':'MM-dd-yyyy','class':'form-control dtpicker', 'data-time-icon':'icon-time','placeholder':'') }}
                        <span class="add-on">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg glyphicon glyphicon-time"></i>
                        </span>
                                <br/>
                              </div>
                            </div>
                           <br>
                          </div>

                        </div>
                      </div>
                    </div>

                    <!-- page banner start here.  -->
                    <div class="col-md-4">

                      <div class="widget">
                        <div class="widget-head">
                          <div class="pull-left">Featured Image
                            <span class="asterisk">*</span>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                          <div class="padd">

                            <div class="form-group">
                            <div id="pageBannerImgWrapper"></div>
                            {{ text_field('pageBannerUrl','id':'pageBannerUrl', 'class':'form-control', 'placeholder':'Image URL here.') }}
                            <br>
                             <span id="post_featimage_hide">{{ featimageError }}</span>
                            </div>

                          </div>
                          <div class="widget-foot">
                             <button type="button" class="btn btn-danger pull-right" id="removeBanner" style="display:none"><span class=" glyphicon glyphicon-refresh"></span>&nbsp;Reset</button>
                             <div class="clearfix"></div>
                            <!-- Footer goes here -->
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- page banner end. -->

                    <div class="col-lg-8">

    <div class="row"><!--start row-->
      <div class="col-lg-12">
        <div class="widget">
          <div class="widget-head">
          <div class="pull-left">Digital Assets (<span id="fileCount"><?php echo count($digital_assets)?></span> files)</div>
            <div class="widget-icons pull-right">
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
            <div class="padd">
              <div id="imageError"></div>
              <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Add image...</span>
                <!-- The file input field used as target for the file upload widget -->
                <input id="postpictures" type="file" name="files[]" multiple accept="image/*">
              </span>
              <!-- The global progress bar -->
              <!-- <div id="progress" class="progress">
                <div class="progress-bar progress-bar-success"></div>
              </div> -->
              <div class="gallery" id="pages-gallery">
                {% for img in digital_assets %}
                <div class="program-digital-assets-library pull-left" style="position: relative">
                  <a href="{{ url("img/programs/" ~ prog~ "/" ~ img) }}" class='prettyPhoto[pp_gal]'>
                    <img src="{{ url("img/programs/" ~ prog ~ "/" ~ img) }}" alt=""></a>
                    <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="{{ url("img/programs/" ~ prog~ "/" ~ img) }}">
                    <button type="button" class="btn btn-xs btn-danger digital-assets-delete" data-filename="{{ img }}" data-folder="{{ prog}}" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
                  </div>
                  {% endfor %}
                  <div class="clearfix"></div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div><!--end row-->
                          </div>
 <div class="col-md-4">
                      <div class="widget">
                        <div class="widget-head">
                          <div class="pull-left">Other Details</div>
                          <div class="widget-icons pull-right">
                          </div>
                          <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                          <div class="padd">
                            {{ checkedProgsError }}
                            <h6>Programs</h6>
                            <hr />
                            {% for prog in programs %}
                            <div class="check-box">
                              <label>
                                {% if prog.programID == 1 %}
                                {{ check_field('chkProg' ~ prog.programID, 'value':prog.programID) }}
                                {% else %}
                                {{ check_field('chkProg' ~ prog.programID, 'value':prog.programID) }}
                                {% endif %}
                                {{ prog.programName}}
                              </label>
                              </div>

                              {% endfor  %}

                               <h6>Pages</h6>
                               <hr />

                              {% for prog in pages %}

                              <div class="check-box">
                                  {% if prog.pageID == 1 %}
                                  {{ check_field('chkPages' ~ prog.pageID, 'value':prog.pageID) }}
                                  {% else %}
                                  {{ check_field('chkPages' ~ prog.pageID, 'value':prog.pageID) }}
                                  {% endif %}
                                  {{ prog.pageTitle}}</label>

                                </div>
                                {% endfor  %}

                                <hr />
                                <h6>Keywords</h6>
                                 {{ text_field('post_keywords', 'class':'form-control col-lg-12','placeholder':'keywords..') }}

                                 <span id="post_keywords">{{ keywordsError }}</span><br>
                                Enter Keywords separated by comma. Ex. philippines, news, happening
                              </div>
                              <div class="widget-foot">
                                 <div class="buttons">

                              <!-- {{ submit_button('submit_post', 'name':'submit_post','value':'Publish News', 'class': 'btn btn-primary pull-right') }} -->
                              <button name="submit_post" id="submit_post" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-floppy-disk"></span> Publish News</button>
                              <!-- <input type="hidden" name="csrf" value="<?php // echo $this->security->getToken() ?>"/> -->
                            </div>
                            <div class="clearfix"></div>
                                <!-- Footer goes here -->
                              </div>
                            </div>

                          </div>

                        </div>







                        </div>
                      </form>
                    </div>
                  </div>

    <!-- Matter ends -->
