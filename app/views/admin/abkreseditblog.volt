<!-- Modal View-->
<div id="modalDigitalAssets" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog" style="width: 900px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Digital Assets Library</h4>
        <input type="hidden" id="digital-assets-year" name="digital-assets-year" value="{{ dayear }}" />
        <input type="hidden" id="digital-assets-month" name="digital-assets-month" value="{{ damonth }}" />
      </div>
      <div class="modal-body" style=" height: 400px; overflow:scroll;">
        <span class="btn btn-success fileinput-button">
          <i class="glyphicon glyphicon-plus"></i>
          <span>Add files...</span>
          <!-- The file input field used as target for the file upload widget -->
          <input id="digitalAssets" type="file" name="files[]" multiple>
        </span>
        <!-- The global progress bar -->
        <div id="progress" class="progress">
          <div class="progress-bar progress-bar-success"></div>
        </div>
        <label>Recently Added</label>
        <div class="gallery digital-assets-gallery">
          None
        </div>
        <div class="clearfix"></div>
        <div class="tabbable" style="margin-bottom: 18px;">
          <ul class="nav nav-tabs">
            <?php
            $y = 0;
            foreach ($folders as $key => $value) {
            if($y==0){
            echo '<li class="active"><a href="#'.$key.'" data-toggle="tab" class="post-year-tab">'.$key.'</a></li>';
          }else{
          echo '<li><a href="#'.$key.'" data-toggle="tab" class="post-year-tab">'.$key.'</a></li>';
        }
        $y++;
      }
      ?>
    </ul>
    <div class="tab-content" style="padding-bottom: 9px; border-bottom: 1px solid #ddd;">
      <?php
      foreach ($folders as $key => $value) {
      ?>
      <div class="tab-pane active" id="<?php echo $key?>">
        <br/>
        <select class="form-control post-assets-month">
          <?php
          $x=0;
          foreach($value as $v){
          $jd=gregoriantojd($v,13,1998);
          if($x==0){
          echo '<option value="'.$v.'" selected="selected">'.jdmonthname($jd,0).'</option>';
        }else{
        echo '<option value="'.$v.'">'.jdmonthname($jd,0).'</option>';
      }
      $x++;
    }
    ?>
  </select>
  <br/>
  <div class="gallery digital-assets-monthly">

  </div>
</div>
<?php
}
?>
</div>
</div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
</div>
</div>
</div>
</div>
<!-- Matter -->
{{ script }}

<!-- Page heading -->
<div class="page-head">

  <!-- Breadcrumb -->
  <div class="bread-crumb pull-right">
    <a href="{{ url('admin') }}"><i class="icon-home"></i> Home</a>
    <span class="divider">/</span>
    <a href="/admin/allblogspost">Blog Post</a>
    <!-- Divider -->
    <span class="divider">/</span>
    <a href="" class="bread-current">Edit</a>
  </div>
  <div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->
<div class="matter">
  <div class="container">
    <form name="postform" method="post" action="{{ url('admin/abkreseditblog/' ~ post.blogID  ) }}">
      <div class="row">

        <div class="col-md-8">
          {{ content() }}
          <div class="widget">
            <div class="widget-head">
              <div class="pull-left">Update Post</div>
              <div class="clearfix"></div>
            </div>
            <div class="widget-content">
              <div class="padd">
                <div class="form-group">

                  <span class="asterisk">*</span>
                  <label>Enter Title</label>
                  <div>
                    <?php echo $form->render('blog_title', array( 'value' => $post->blogTitle)); ?>
                  <!--   {{ form.messages('blog_title') }} -->
                    <input type="hidden" name="blog_slug" class="post-slug" value="<?php echo $post->blogTitle ?>">

                    <label> Slug: </label>
                    {{ hidden_field('hpage_slug', 'id':'hpage_slug') }}
                    <span class="post-slug-text"><span id="page-slug"><?php echo $post->blogSlug; ?></span></span>
                    <br><span id="blogTitle2">{{ blogtitle }}</span>
                               <div class="label label-danger" id="valuser0"></div>
                               <div class="label label-danger" id="valuser1"></div>
                               <div class="label label-danger" id="valuser3"></div>
                  </div>

                <span class="asterisk">*</span>
                <label>Author Name</label>
                <div>
                  <?php echo $form->render('blog_author', array( 'value' => $post->blogAuthor)); ?>
                 <span id="authname"> {{ blogauth }}</span>
                </div>


              <div class="text-area">
                <span class="asterisk">*</span>
                <label>Content</label>
                <?php echo $form->render('blog_content', array( 'value' => $post->blogContent)); ?>
              </div>
          <!--     {{ form.messages('blog_content') }} -->
          <span id="errdetails" >{{ blogcontent }}</span>
            </div>
            <div class="widget-foot">
            </div>
          </div>
        </div>
      </div>

      <!-- post sidebar -->

     

      <!-- page banner start here.  -->
   

        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">Featured Image  <span class="asterisk">*</span></div>
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
            <div class="padd">

              <div class="form-group">
                <div id="pageBannerImgWrapper">
                  {% if post.blogFeatureImage %}
                  <img src="{{post.blogFeatureImage}}" style="width: 100%" />
                  {% endif %}
                </div>
               <!--  {{ text_field('pageBannerUrl', 'class':'form-control', 'placeholder':'Image URL here.', 'value':post.blogFeatureImage ) }} -->
                <?php echo $form->render('pageBannerUrl', array( 'value' => $post->blogFeatureImage)); ?>
                <span id="post_featimage_hide">{{ featimageError }}</span>
              </div>

            </div>
            <div class="widget-foot">
              <button type="button" class="btn btn-default pull-right" id="removeBanner" style="display:none">Reset</button>
              <div class="buttons">
                {{ submit_button('update_blog', 'name':'update_blog','value':'Save Changes', 'class': 'btn btn-info') }}

                <div class="clearfix"></div>
                <!-- Footer goes here -->
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- page banner end. -->


       <div class="col-md-4">

        <div class="widget">
          <div class="widget-head">
            <div class="pull-left">Options</div>
            <div class="clearfix"></div>
          </div>
          <div class="widget-content">
            <div class="padd">
              <hr/>
              <div class="check-box">
                <label>{{ check_field('blog_status', 'class':'tbl_select_row', 'value':'draft') }} Save as Draft</label>
              </div>
              <hr/>
              <div class="form-group">
                <label>Publish on: </label>
                <span class="asterisk">*</span>
                <br>
                <div class="input-append pull-left start">
                  {{ text_field('blogDatePublish', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time','placeholder':'') }}
                    <span class="add-on" id="pubdate">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg glyphicon glyphicon-time" id="subdate"></i>
                    </span>
                    </span>
                    <br/>
                </div>
              </div>

              <div class="clearfix"></div>
            </div>
            <div class="widget-foot">
              <!-- Footer goes here -->
            </div>
          </div>

        </div>

      </div>


      <div class="col-lg-8">

        <div class="row"><!--start row-->
          <div class="col-lg-12"> 
            <div class="widget">
              <div class="widget-head">
                <div class="pull-left">Digital Assets (<span id="fileCount"><?php echo count($digital_assets)?></span> files)</div>
                <div class="widget-icons pull-right">
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="widget-content">
                <div class="padd">
                  <div id="imageError"></div>
                  <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add Photo...</span>
                    <!-- The file input field used as target for the file upload widget -->
                    <input id="pagespictures" type="file" name="files[]" multiple accept="image/*">
                  </span>
                  <!-- The global progress bar -->
                  <div id="progress" class="progress">
                    <div class="progress-bar progress-bar-success"></div>
                  </div>
                  <div class="gallery" id="pages-gallery">
                    {% for img in digital_assets %}
                    <div class="program-digital-assets-library pull-left" style="position: relative">
                      <a href="{{ url("img/programs/" ~ prog~ "/" ~ img) }}" class='prettyPhoto[pp_gal]'>
                        <img src="{{ url("img/programs/" ~ prog ~ "/" ~ img) }}" alt=""></a>
                        <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="{{ url("img/programs/" ~ prog~ "/" ~ img) }}">
                        <button type="button" class="btn btn-xs btn-danger digital-assets-delete" data-filename="{{ img }}" data-folder="{{ prog}}" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
                      </div>
                      {% endfor %}
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="widget-foot">

                  </div>
                </div>
              </div>
            </div>
          </div><!--end row-->
        </div>


      </div>
    </form>
  </div>
</div>

<!-- Matter ends -->
