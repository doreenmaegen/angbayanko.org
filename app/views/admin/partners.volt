            <div id="modalView" class="modal fade modalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header-color">
                    <button type="button" style="color: white;font-size: 17px" class="close" data-dismiss="modal" aria-hidden="true" >×</button>
                    <span class="modal-title" style="color: white;font-size: 17px">View Record</span>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span>&nbsp;Close</button>
                  </div>
                </div>
              </div>
            </div>
          <!-- Modal Prompt-->
            <div id="modalPrompt" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
<!--                   <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title"></h4>
                  </div> -->
                 <div id="headerColor">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: white;font-size: 17px">×</button>
                    <span  style="color: white;font-size: 17px" id="changewarning"></span>
                  </div>
                  <div class="modal-body">
                    <p class="modal-message"></p>
                    <span class="modal-list-names"></span>
                  </div>
                  <div class="modal-footer">
                    <a href="#" type="button" class="btn btn-primary modal-btn-yes" data-form="userform"><span class="icon glyphicon glyphicon-ok"></span> Yes</a>
                    <a type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><span class="icon glyphicon glyphicon-remove"></span> No</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- Page heading -->
            <div class="page-head">
              <h2 class="pull-left"><i class="icon-table"></i> ABK Partner List</h2>

              <!-- Breadcrumb -->
              <div class="bread-crumb pull-right">
                <a href="index.html"><i class="icon-home"></i> Home</a>
                <!-- Divider -->
                <span class="divider">/</span>
                <span>Partners</span>
              </div>

              <div class="clearfix"></div>

            </div>
            <!-- Page heading ends -->

            <!-- Matter -->

            <div class="matter">
              {{ form('admin/partners', 'id':'main-table-form') }}
              <div class="container">

                <div class="row">
                  <div class="col-md-2">
                    <label>Search by Text</label>
                  </div>
                  <div class="col-md-6 form-group">
                    <div class="col-md-12">
                      {{ text_field('search_text' , 'class':'form-control') }}
                    </div>
                  </div>
                  <div class="col-md-3 form-group">
                   <!--  {{ submit_button('Search', 'class':'btn btn-default') }} -->
                     <button type="submit" name="Search" id="Search" class="btn btn-success"><span class="icon-search"></span> Search</button>
                    <button type="reset" name="clear_search" class="btn btn-danger" value="Clear Search" onclick="window.location.reload()"><span class="icon-refresh"></span> Refresh</button>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Filter by Date</label>
                  </div>
                  <div class="col-md-4 form-group" id="fromDatepicker" class="input-append">
                    <label class="col-md-2 control-label">From</label>
                    <div class="col-md-10">
                      {{ text_field('fromDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-3 form-group" id="toDatepicker" class="input-append" style="margin-left: -8.5%;">
                    <label class="col-md-2 control-label">To</label>
                    <div class="col-md-12">
                      {{ text_field('toDate', 'readonly': 'true', 'data-format':'yyyy-MM-dd','class':'form-control dtpicker', 'data-time-icon':'icon-time') }}
                      <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="btn btn-info btn-lg icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="col-md-2">
                {{ submit_button('Filter', 'class':'btn btn-danger', 'name':'submit', 'id':'filter-btn') }}
                    <!-- <button type="submit" name="Filter" class="btn btn-primary" value="Filter">Filter</button> -->
                  </div>
                </div>

                <!-- Table -->
                <div class="row">

                  <!-- <div class="form-group">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="control-label col-lg-3">Search</label>
                        <div class="col-lg-10">
                          {#{ text_field('search_text' , 'class':'form-control') }#}
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      {#{ submit_button('Search', 'class':'btn btn-default') }#}
                      <button type="submit" name="clear_search" class="btn btn-default" value="Clear Search"><span class="icon-refresh"></span></button>
                    </div>
                  </div> -->

                  <div class="col-md-12">
                    {{ content() }}


                    <div class="widget">

                      <div class="widget-head">
                        <div class="pull-left">Partners</div>
                        <div class="widget-icons pull-right">
                          <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a>  -->
                          <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                        </div>
                        <div class="clearfix"></div>
                      </div>


                      <div class="widget-content">
                        {{ hidden_field('csrf', 'value': security.getToken())}}
                        <input type="hidden" class="tbl-action" name="action" value=""/>
                        <input type="hidden" class="tbl-recordID" name="recordID" value=""/>
                        <input type="hidden" class="tbl-edit-url" name="editurl" value="editPartner/"/>
                        <table class="table table-striped table-bordered table-hover tblusers">
                          <thead>
                            <tr>
                              <?php if($userlevel!=2){ ?>
                              <th>{{ check_field('select_all[]', 'class':'tbl_select_all') }}</th>
                              <!-- <th>UserId</th> -->
                              <?php } ?>
                              <th><a href="?sort={{ partnernameHref }}">Partner Name <i class="{{ partnernameIndicator ? partnernameIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ usernameHref }}">Username <i class="{{ usernameIndicator ? usernameIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ nameHref }}">Name <i class="{{ nameIndicator ? nameIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ datecreatedHref }}">Date Created <i class="{{ datecreatedIndicator ? datecreatedIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ positionHref }}">Position <i class="{{ positionIndicator ? positionIndicator : "" }}"></i></a></th>
                              <th><a href="?sort={{ statusHref }}">Status <i class="{{ statusIndicator ? statusIndicator : "" }}"></i></a></th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($page->items as $user) {
                            $statustext = ($user['status']=='active')?'<span class="label label-success">Active</span> <a href="#modalPrompt" class="btn btn-xs btn-default tbl_delete_row modal-control-button" data-toggle="modal" data-action="deactivate" data-recorID="'.$user['userID'].'"><i class="icon-ban-circle"></i> </a>':'<span class="label label-default">Inactive</span> <a href="#modalPrompt" class="btn btn-xs btn-success tbl_delete_row modal-control-button" data-toggle="modal" data-action="activate" data-recorID="'.$user['userID'].'"><i class="icon-ok"></i> </a>';
                            $actions = '<a style="font-size:30px" href="#modalView" data-toggle="modal" class="btn btn-xs btn-success modal-record-view" data-href="ajaxUserView/'.$user['userID'].'" ><i class="icon-ok"></i>View</a>
                                <a href="#modalPrompt" class="btn btn-xs btn-warning tbl_edit_row modal-control-button" data-toggle="modal" data-action="edit" data-recorID="'.$user['userID'].'"><i class="icon-pencil"> </i>Edit</a>
                                <a href="#modalPrompt" class="btn btn-xs btn-danger tbl_delete_row modal-control-button" data-toggle="modal" data-action="delete" data-recorID="'.$user['userID'].'"><i class="icon-remove"></i>Delete</a>';
                            $checkboxes = '<td>
                                <input type="checkbox" name="tbl_id[]" class="tbl_select_row" value="'.$user['userID'].'">
                              </td>';
                            $userids = '<td>'.$user['userID'].'</td>';
                            $partnerNames = '<a href="/admin/partnersinfo/'.$user['partnerID'].'">'.$user['partnerName'].'</a>';
                            if($userlevel==2){
                              $statustext = ($user['status']=='active')?'<span class="label label-success">Active</span>':'<span class="label label-default">Inactive</span>';
                              $actions = '<a  href="#modalView" data-toggle="modal" class="btn btn-xs btn-success modal-record-view" data-href="ajaxUserView/'.$user['userID'].'" ><i class="icon-ok"></i> </a>';
                              $checkboxes = '';
                              $userids = '';
                              $partnerNames = $user['partnerName'];
                            }
                            echo '<tr>
                              '.$checkboxes.'
                              <td class="name">'.$partnerNames.'</td>
                              <td>'.$user['username'].'</td>
                              <td>'.$user['name'].'</td>
                              <td>'.date("F j, Y", $user['dateCreated']).'</td>
                              <td>'.$user['position'].'</td>
                              <td>'.$statustext.'</td>
                              <td>'.$actions.'</td>
                            </tr>';
                          }

                          if(empty($page->items)){
                            echo '<tr><td colspan="8" align="center">No Records Found</td></tr>';
                          }
                          ?>
                          </tbody>
                          </table>

                          <div class="tblbottomcontrol" style="display:none">
                            <a href="#modalPrompt" class="tbl_delete_all" data-toggle="modal" data-action="delete_selected" > Delete all Selected </a> |
                            <a href="#" class="tbl_unselect_all"> Unselect </a>
                          </div>

                            <div class="widget-foot">
                              {% set limit = 10 %}
                              {% set start = (limit * (page.current - 1)) + 1 %}
                              {% set end = (limit * (page.current-1)) + limit %}

                              {% if end > page.total_items %}
                              {% set end = page.total_items %}
                              {% endif %}
                              {% if limit %}
                              <div style="margin-left: 85.5%">
                                <span>&nbsp;Showing {{ start }} - {{ end  }} of {{ page.total_items }}</span>
                              </div>
                              {% endif %}
                              {% if page.items and page.total_pages > 0 %}
                            <ul class="pagination pull-right">
                                <!---->
                                {% if page.current == 1 and page.total_pages >= 5 and page.total_pages > 0 %}
                                 <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partners?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partners?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current == 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partners?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partners?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != 1 and page.total_pages < 5 and page.total_pages > 0 %}
                                <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                                  {% for index in 1..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partners?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partners?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current != 1 and page.current < 4 and page.total_pages >= 5 %}
                                <li>{{ link_to("admin/partners", 'First') }}</li>
                                <li>{{ link_to("admin/partners?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in 1..5 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partners?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partners?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and page.current+2 < page.total_pages%}
                                <li>{{ link_to("admin/partners", 'First') }}</li>
                                <li>{{ link_to("admin/partners?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.current-2..page.current+2 %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partners?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partners?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% elseif page.current >=4 and (page.current+2 == page.total_pages or page.current+2 > page.total_pages) %}
                                <li>{{ link_to("admin/partners", 'First') }}</li>
                                <li>{{ link_to("admin/partners?page=" ~ page.before, 'Prev') }}</li>
                                  {% for index in page.total_pages-4..page.total_pages %}
                                    {% if page.current == index %}
                                      <li>{{ link_to("admin/partners?page=" ~ index, index, 'style':'background-color:#eee') }}</li>
                                      {% else %}
                                      <li>{{ link_to("admin/partners?page=" ~ index, index) }}</li>
                                    {% endif %}
                                  {% endfor %}
                                {% endif %}
                                <!---->
                                <!---->
                                {% if page.current != page.last %}
                                <li>{{ link_to("admin/partners?page=" ~ page.next, 'Next') }}</li>
                                <li>{{ link_to("admin/partners?page=" ~ page.last, 'Last') }}</li>
                                </ul>
                                {% else %}
                                <li>{{ link_to("admin/partners?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/partners?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                                {% endif %}
                                <!---->
                              {% elseif page.total_pages == 0 %}
                              <ul class="pagination pull-right">
                              <li><a href="" onclick="return false" style="cursor:default;">First</a></li><li><a href="" onclick="return false" style="cursor:default;">Prev</a></li>
                              <li>{{ link_to("admin/partners?page=" ~ page.next, 'Next','onclick':'return false','style':'cursor:default') }}</li>
                                <li>{{ link_to("admin/partners?page=" ~ page.last, 'Last','onclick':'return false','style':'cursor:default') }}</li>
                                </ul>
                 {% endif %}

                              <div class="clearfix"></div>

                            </div>

                          </div>

                        </div>


                      </div>

                    </div>


                  </div>
                </form>
              </div>

<!-- Matter ends -->
<script>
if ( window.history.replaceState ) {
window.history.replaceState( null, null, window.location.href );
}
</script>
