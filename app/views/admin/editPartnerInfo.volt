
<div id="partnerGallery" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" style="width: 900px;">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Partners Pictures</h4>
        </div>
        <div class="modal-body">
          <div id="partnerPreloader"></div>
          <input type="hidden" id="partnerID" value="{{ partnerID }}">
          {{ partnerAlbumSelect }}
          <div class="gallery" style="max-height:400px; overflow-y:auto" id="partnerGalleryWrapper">
          {{ partnerGallery }}
          </div>
        </div>
        <div class="modal-footer">
          <button id="afterUpload" type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </div>
  </div>
</div>
<!-- Page heading -->
<div class="page-head">
    <h2 class="pull-left"><i class="icon-suitcase"></i> ABK Partner</h2>

    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
        <a href="{{ url('admin') }}"><i class="icon-home"></i> Home</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="{{ url('admin/partners') }}">Partners</a>
        <span class="divider">/</span>
        <span>View</span>
    </div>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->

<!-- Matter -->

<div class="matter">
    {{ form('class': 'form-horizontal', 'id':'main-table-form') }}
    <div class="container">

        <!-- Table -->

        <h2>{{ partner.partnerName }}</h2>
        {{ content() }}

        <div class="row">
            <div class="col-md-12">

                <div class="widget">
                    <div class="widget-head">
                        <div class="pull-left">ABK Partner Details</div>
                        <div class="widget-icons pull-right">
                            <!-- <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> -->
                            <!-- <a href="#" class="wclose"><i class="icon-remove"></i></a> -->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="widget-content">
                        <div class="padd">
                            <?php if($userlevel!=2){ ?>
                            {{ tab }}
                            <?php } ?>
                            <div class="padd">
                                <a href="#partnerGallery" data-toggle="modal" class="btn btn-default pull-right post-add-media"><i class="icon-paper-clip"></i> Add Media</a>
                                <div class="clearfix"></div>
                            </div>

                            <textarea name="partnerInfo" class="richText">
                            	{{ partner.partnerInfo }}
                            </textarea>
                            <br />
                            <input type="submit" class="btn btn-primary" value="Save Changes" name="saveInfo">
                        </div>
                    </div>

                    <div class="widget-foot">
                        <!-- Footer goes here -->
                        {% if showBackToList %} {{ link_to("admin/partners", "Back to Partner List", 'class':'btn btn-default') }} {% endif %}
                    </div>
                </div>

                
            </div>

        </div>


    </div>



    </form>
</div>

<!-- Matter ends -->