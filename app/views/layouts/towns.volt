<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>Ang Bayan Ko</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>

  <!-- Nomarlize -->
  {{ stylesheet_link('css/normalize.css') }}
  {{ stylesheet_link('css/bootstrap.css') }}
  {{ stylesheet_link('css/prettyPhoto.css') }}
  {{ stylesheet_link('css/flexslider.css') }}
  {{ stylesheet_link('css/font-awesome.css') }}
  {{ stylesheet_link('css/font.css') }}
  {{ stylesheet_link('css/slider.css') }}
  {{ stylesheet_link('css/refineslide.css') }}
  {{ stylesheet_link('css/style.css') }}
  <!-- Stylesheet for Color
  <link href="style/blue.css" rel="stylesheet">

  <!-- HTML5 Support for IE -->
  <!--[if lt IE 9]>
  <script src="js/html5shim.js"></script>
  <![endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon/favicon.ico">
</head>

<body>
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
  <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
  <!-- Header starts -->
  <header class="inner-header">
    <div class="container">
      <div class="row">

      <div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
              <div class="modal-content border-flat">
                <form method="post" id="loginModalForm">
                  <input type="hidden" name="loginFormActive" value="1">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="townAlbumModalTitle">Login</h4>
                  </div>
                  <div id="loginModalBody" class="modal-body">
                    <div id="loginErrorMessage"></div>
                    <label>Username</label>
                    <input type="text" name="username" class="form-control border-flat" placeholder="Username">
                    <br />
                    <label>Password</label>
                    <input type="password" name="password" class="form-control border-flat" placeholder="Password">
                    <br />
                    <a class="" href="/myaccount/forgotpassword">I forgot my password</a>
                  </div>
                  <div class="modal-footer" id="modal-footer">
                   <!--  <label class="pull-left"><input type="checkbox" name="rememberMe"> Remember Me</label> -->
                    <button type="button" class="btn btn-default border-flat" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <input id="loginModalBtn" type="submit" name="login" class="border-flat btn btn-primary" value="Login">
                  </div>
                </form>
              </div>
            </div>
          </div>

        <div id="tellafriendModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tellafriendModal" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
              <div class="modal-content border-flat">
                <form method="post" id="tellafriendModalForm">
                  <input type="hidden" name="tellfriendFormActive" value="1">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="townAlbumModalTitle">Tell A Friend</h4>
                  </div>
                  <div id="tellafriendModalBody" class="modal-body">
                    <div id="tellFriendErrorMessage"></div>
                    <label>Email</label>
                    <input type="text" name="email" class="form-control border-flat" placeholder="Your friend's email">
                    <br />
                    <label>Your Message</label>
                    <textarea name="message" class="form-control border-flat limitChar" maxlength="200" placeholder="Your message"></textarea>
                    <div class="maxlength"></div>
                    <br />
                    <strong>http://angbayanko.org/</strong><br />
                    <small>ABK URL will be attached along with your message</small>
                  </div>
                  <div class="modal-footer" id="modal-footer">
                    <button type="button" class="btn btn-default border-flat" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <button type="submit" id="tellFriendModalBtn" class="border-flat btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

        <div class="col-md-12 upper-mini-nav">
          {{ form('search') }}
            <ul>
              <li><a href="/abkrescue">ABK Rescue</a></li>
              <li><a href="/about">About ABK</a></li>
              <li><a href="/forum">Forum</a></li>
              <li><a href="/contactus">Contact ABK</a></li>
              <li><a href="#tellafriendModal" data-toggle="modal">Tell a Friend</a></li>
              <li>
                <?php
                if(!empty($abk_vol_username)){
                  echo '<a href="/myaccount" style="padding:0"><i class="icon-user"></i> <strong>'.$abk_vol_username.'</strong></a> | <a style="padding-left:0" href="'.$this->url->get().'index/logout">Logout</a>';
                }else{
                  echo '<a href="#loginModal" data-toggle="modal">Login</a>';
                }
                ?>
                </li>
              <li>
                <input type="text" name="keyword" class="searchinput" placeholder="Search" />
                <input type="submit" name="search" class="btnSearch" value="Search">
              </li>
            </ul>
          </form>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row inner-logo">
        <div class="inner-mainlogo pull-left"><a href="{{ url() }}"><img src="{{ url('img/inner-logo.png')}}" /></a></div>
        <div class="inner-txtlogo pull-left"><div class="inner-textlogo"></div></div>
        <div class="navbar-header">
         <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
           <span class="sr-only">Toggle navigation</span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
         </button>
       </div>
       <div class="inner-menu pull-left collapse navbar-collapse bs-navbar-collapse">
        <ul>
          <li class="dm"><a href="{{ url('programs/page/' ~ prog_menu1['url']) }}">{{ prog_menu1['title'] }}</a></li>
          <li class="cp"><a href="{{ url('programs/page/' ~ prog_menu2['url']) }}">{{ prog_menu2['title'] }}</a></li>
          <li class="he"><a href="{{ url('programs/page/' ~ prog_menu3['url']) }}">{{ prog_menu3['title'] }}</a></li>
          <li class="el"><a href="{{ url('programs/page/' ~ prog_menu4['url']) }}">{{ prog_menu4['title'] }}</a></li>
          <li class="ee"><a href="{{ url('programs/page/' ~ prog_menu5['url']) }}">{{ prog_menu5['title'] }}</a></li>
          <li class="ep"><a href="{{ url('programs/page/' ~ prog_menu6['url']) }}">{{ prog_menu6['title'] }}</a></li>
        </ul>
      </div>
    </div>
  </div>
</header>
<!-- Programs and Announcements-->
<div class="container">
  <div class="row">
    <div class="inner-bread-crumbs">{{ bread_crumbs }}</div>
    <h1 class="inner-program-title">{{ townTitle }}</h1>
    {{ content() }}

    <div class="col-md-3 col-sm-3 col-xs-5 sidebar pull-left">
      <div class="connectwithus">
          <a href="/site/pages/support-your-chosen-program">
            <div class="connectTiles left-tile sscp">
              <div class="connectTitle-Title" style="background-color: rgb(8, 74, 178);">Support your Chosen Program</div>
            </div>
          </a>
          <a href="{{ url('towns') }}">
            <div class="connectTiles right-tile swhh">
              <div class="connectTitle-Title" style="background-color: rgb(99, 47, 0);">See What's Happening in your Hometown</div>
            </div>
          </a>
          <a href="/site/pages/retiring-to-make-a-difference">
            <div class="connectTiles left-tile bottom-tile rmd">
              <div class="connectTitle-Title" style="background-color: rgb(210, 71, 38);">Retiring to Make a Difference</div>
            </div>
          </a>
          <a href="/site/pages/businesses-for-social-change">
            <div class="connectTiles right-tile bottom-tile bsc">
              <div class="connectTitle-Title" style="background-color: rgb(62, 85, 109);">Businesses for Social Change</div>
            </div>
          </a>
        </div>
      <div class="social-links">
        <ul class="social-list">
          <li class="social-icons"><a href="https://www.facebook.com/Ang-Bayan-Ko-Foundation-1664040513867744/" target="_blank" class="fb"></a></li>
          <li class="social-icons"><a href="https://plus.google.com/u/0/101853314610354158387" target="_blank" class="gplus"></a></li>
          <li class="social-icons"><a href="https://twitter.com/abk_foundation" target="_blank" class="twitter"></a></li>
          <li class="social-icons"><a href="https://www.linkedin.com/in/ang-bayan-ko-foundation-7a33a6113" target="_blank" class="in"></a></li>
          <li class="social-icons"><a href="#" class="pi"></a></li>
          <li class="social-icons sc-last"><a href="https://www.youtube.com/channel/UCA9g-GNgrI0Eq28lMt7g0Iw" target="_blank" class="youtube"></a></li>
        </ul>
      </div>
        <a href="{{ url('forum') }}">
          <div class="joinforum">
            <div class="forum-icon"></div>
            <h2>Join the discussions at the Forums</h2>
          </div>
        </a>
        <div style="clear:both"></div>
        <div id="side-donation">
          <a href="/donate"><img height="74" width="307" src="{{ url('img/paypal.jpg')}}"></a>
        </div>

               <?php if(count($announcements) > 0){ ?>
        <div class="announcements-container">
          <br>
          <table width="100%" style="word-break: break-all;">
          <tr>
            <td class="annhead"><h5>Announcements</h5></td>
          </tr>
          <tr style="padding-top: 20px; display:block;">

          </tr>
          <tr style="padding-bottom: 0px;">
            <?php
          foreach ($announcements as $ann) { ?>
            <td><a href="/announcements/view/<?php echo $ann['annID'] ?>" class="pull-left ann-title"><?php echo $ann['annTitle'] ?></a></td>
          </tr>
          <tr>
            <td><?php echo $ann['annDesc'] ?></td>
          </tr>
          <tr>
            <?php
              $myDate = date("Y-m-d", $ann['annDate']);
              $myDates = str_replace('.', '-', $myDate);
              {# $mydate = $ann['annDate']; #}
            ?>
            <td style="float: right;position: relative;top: 0px;"><span class="text-danger">Posted on  <?=date("F jS, Y", strtotime($myDates))?></span> </td>
          </tr>
          <tr>
            <td> <hr style="margin: 0px 0px 5px 0px"> </td>
          </tr>
          <?php } ?>
          <tr>
            <td><span class="pull-right"> <a href="/announcements">View all announcements</a></span></td>
          </tr>

        </table>
        </div>
        <?php }else{ ?>
        <div class="announcements-container">
          <br>
          <table width="100%">
          <tr>
            <td class="annhead"><h5>Announcements</h5></td>
          </tr>
          <tr><td>&nbsp;</td>
          </tr>
          <tr>
            <td>No Announcements Posted.</td>
            <tr>
        </table>
      </div>
        <?php } ?>


              </div>
            </div>
          </div>

          <!-- Programs and Announcements Ends-->

          <div class="container section-feature ">
            <div class="row">
              <div class="tile">
              </div>
              <div class="tile">
              </div>
              <div class="tile">
              </div>
            </div>
          </div>

          <!-- Footer starts -->
          <footer>
            <div class="container">
              <div class="row">
                <div class="col-md-12 col-md-offset-1 col-sm-12 ">
                  {# <div class="row"> #}
                    <div class="col-md-4 col-sm-4 contact-us-footer pull-left">
                      <h3>Contact Us</h3>
                        <strong> Philippines:</strong> <br/>
                        <i class="icon-home"></i>#97 Mabini St., Tayug,  Pangasinan <br/>
                        <i class="icon-phone"></i> 09999990161 (SMART) <br/>
                        <i class="icon-phone"></i>  09173033838(GLOBE) <br/>
                        <i class="icon-envelope-alt"></i> angbayankoorg@gmail.com <br/> <br/> <br>
                        <strong> United States of America:</strong> <br/>
                        <i class="icon-home"></i> 123, Some Area. Los Angeles, CA. <br/>
                        <i class="icon-phone"></i> +239-3823-3434 <br/>
                        <i class="icon-envelope-alt"></i> someone@company.com <br/> <br/>
                      </div>
                      <div class="col-md-4 col-sm-4 more-info-footer pull-left">
                          <h3>More Info</h3>
                          <ul class="more-info">
                            <li><a href="/site/pages/support-your-chosen-program">Support your Chosen Program</a></li>
                            <li><a href="/site/pages/retiring-to-make-a-difference">Retiring to make a difference</a></li>
                            <li><a href="/site/pages/businesses-for-social-change">Businesses for Social Change</a>
                            <li><a href="/site/pages/volunteering">Volunteering</a></li>
                            <li><a href="/site/pages/nature">Nature</a></li>
                            <li><a href="/site/pages/reminder">Reminder</a></li>
                            <br><br><br><br>
                            <li><a href="/site"><strong>See more...</strong></a></li>
                          </ul>
                      </div>
                      <div class="col-md-4 col-sm-4 programs-footer pull-left">
                        <h3>Programs</h3>
                          <ul class="more-info">
                            <li><a href="/programs/page/disaster-risk-managements">Disaster Risk Management</a></li>
                            <li><a href="/programs/page/create-your-personal-cause">Create Your Personal Cause</a></li>
                            <li><a href="/programs/page/healthcare-for-everyone">Healthcare For Everyone</a></li>
                            <li><a href="/programs/page/economic-livelihood">Economic Livelihood</a></li>
                            <li><a href="/programs/page/early-childhood-education">Early Childhood Education</a></li>
                            <li><a href="/programs/page/environmental-preservation">Environmental Preservation</a></li>
                            <br><br><br><br>
                            <li><a href="/programs/"><strong>See more...</strong></a></li>
                          </ul>
                    </div>
                    <div class="col-md-12 col-sm-12" style="clear:both;">
                          <span style="font-weight:lighter;">Copyright &copy; 2014 | <a href="#">AngBayanKo Site</a> - <a href="#">Home</a> </span>
                    </div>
                    {# <div class="col-md-3 others-footer pull-left">
                        <h3>Others</h3>
                        <ul class="more-info">
                          <li><a href="/site/pages/support-your-chosen-program">Support your Chosen Program</a></li>
                          <li><a href="/site/pages/retiring-to-make-a-difference">Retiring to make a Difference</a></li>
                          <li><a href="/site/pages/businesses-for-social-change">Businesses for Social Change</a>
                          <li><a href="/site/pages/volunteering">Volunteering</a></li>
                          <li><a href="/site/pages/nature">Nature</a></li>
                          <li><a href="/site/pages/reminder">Reminder</a></li>
                          <br><br><br><br>
                          <li><a href="/site"><strong>See more...<strong></a></li>
                        </ul>
                      </div>
                    </div> #}
                    {# Buti nahana moko  #}
                  </div>
                {# </div> #}
                <br>

          </div>
        </footer>

        <!-- Footer ends -->

        <!-- Scroll to top -->
        <span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span>

        <!-- JS -->
        {{ javascript_include('js/jquery.js') }}
        {{ javascript_include('js/bootstrap.js') }}
        {{ javascript_include('js/jquery.isotope.js') }}
        {{ javascript_include('js/jqury.prettyPhoto.js') }}
        {{ javascript_include('js/filter.js') }}

        {{ javascript_include('js/jquery.flexslider-min.js') }}
        {{ javascript_include('js/sparkjquery.csliderlines.js') }}
        {{ javascript_include('js/modernizr.custom.28468.js') }}

        {{ javascript_include('js/jquery.carouFredSel-6.1.0-packed.js') }}
        {{ javascript_include('js/jquery.refineslide.min.js') }}
        {{ javascript_include('js/jquery.backgroundSize.js') }}
        {{ javascript_include('js/respond.src.js') }}
        {{ javascript_include('js/jquery.maxlength.min.js') }}
        {{ javascript_include('js/custom.js') }}

       <script>
      if($("#map_wrapper")[0]){


        jQuery(function($) {
            // Asynchronously Load the map API
            var script = document.createElement('script');
            script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
            document.body.appendChild(script);


        });

        function initialize() {
            var map;
            var bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                mapTypeId: 'roadmap',
                //scrollwheel: false,
                disableDoubleClickZoom: true,
                maxZoom: 16,
                minZoom: 6,
                disableDefaultUI: true,
                //center: new google.maps.LatLng(13.165321128848765, 122.43349619726564)
            };

            // Display a map on the page
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
            map.setTilt(45);

            // Multiple Markers
            var markers = [
                <?php
                $townInfo = null;

                if(!empty($viewlist) && !$viewlist){


                }elseif(!empty($townMarkers)){
                  foreach ($townMarkers as $key => $value) {
                    echo "['".$value['townName']."', ".$value['townLat'].", ".$value['townLong']."],";
                    $townInfo .= "['<div class=\"info_content\" style=\"overflow: auto; width: 500px; height: 118px;\"><h3>".$value['townName']."</h3><p>".preg_replace('/[\n\r]/',"<br/>",$value['townInfo'])."<br /><a href=\"".$value['townLink']."\">Read more...</a></p><br /><a href=\"".$value['townEventLink']."\"><i class=\"icon-calendar\"></i> Events</a> &nbsp; <a href=\"".$value['townGalleryLink']."\"><i class=\"icon-picture\"></i> Photos</a></div>'],";
                  }
                }

                ?>

            ];

            // Info Window Content
            var infoWindowContent = [
                <?php echo $townInfo; ?>
            ];

            // Display multiple markers on a map
            var infoWindow = new google.maps.InfoWindow(), marker, i;

            // Loop through our array of markers & place each one on the map
            for( i = 0; i < markers.length; i++ ) {
                var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: markers[i][0]
                });

                // Allow each marker to have an info window
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infoWindow.setContent(infoWindowContent[i][0]);
                        infoWindow.open(map, marker);
                    }
                })(marker, i));

                // Automatically center the map fitting all markers on the screen
                map.fitBounds(bounds);
            }


            // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
            /*var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
                this.setZoom(7);
                //this.maxZoom(10);
                google.maps.event.removeListener(boundsListener);
            });*/


            if($(".sidebar").height() > $("#map_wrapper").height()){
              $("#map_wrapper").css('height', $(".sidebar").height()+'px');
            }
            //alert($(".sidebar").height());

        }
      }

    </script>

      </body>
      </html>
