<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <!-- Title and other stuffs -->
  <title>Ang Bayan Ko</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">

  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>

  <!-- Nomarlize -->
  {{ stylesheet_link('css/normalize.css') }}
  <!-- Stylesheets -->
  {{ stylesheet_link('css/bootstrap.css') }}
  <!-- Pretty Photo -->
  {{ stylesheet_link('css/prettyPhoto.css') }}
  <!-- Flex slider -->
  {{ stylesheet_link('css/flexslider.css') }}
  <!-- Font awesome icon -->
  {{ stylesheet_link('css/font-awesome.css') }}
  <!-- Load Fonts -->
  {{ stylesheet_link('css/font.css') }}
  <!-- Parallax slider -->
  {{ stylesheet_link('css/slider.css') }}
  <!-- Refind slider -->
  {{ stylesheet_link('css/refineslide.css') }}
  <!-- Main stylesheet -->
  {{ stylesheet_link('css/style.css') }}
  <!-- Stylesheet for Color-->
  {{ stylesheet_link('css/blue.css') }}

  <!-- HTML5 Support for IE-->
  <!--[if lt IE 9]-->
  <script src="js/html5shim.js"></script>
  <!--[endif]-->

  <!-- Favicon -->
  <link rel="shortcut icon" href="img/favicon/favicon.ico">

  <script src="js/bootstrap-datepicker.js" type="text/javascript"></script>
</head>

<body>

  <!-- Header starts -->
  <header>
    <div class="container">
      <div class="row">

      <div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
              <div class="modal-content border-flat">
                <form method="post" id="loginModalForm">
                  <input type="hidden" name="loginFormActive" value="1">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="townAlbumModalTitle">Login</h4>
                  </div>
                  <div id="loginModalBody" class="modal-body">
                    <div id="loginErrorMessage"></div>
                    <label>Username</label>
                    <input type="text" name="username" class="form-control border-flat" placeholder="Username">
                    <br />
                    <label>Password</label>
                    <input type="password" name="password" class="form-control border-flat" placeholder="Password">
                    <br />
                    <a class="" href="/myaccount/forgotpassword">I forgot my password</a>
                  </div>
                  <div class="modal-footer" id="modal-footer">
                    <!-- <label class="pull-left"><input type="checkbox" name="rememberMe"> Remember Me</label>
                    -->
                    <input id="loginModalBtn" type="submit" name="login" class="border-flat btn btn-primary" value="Login">
                    <button type="button" class="btn btn-default border-flat" data-dismiss="modal" aria-hidden="true">Cancel</button>

                  </div>
                </form>
              </div>
            </div>
          </div>

        <div id="tellafriendModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="tellafriendModal" aria-hidden="true" style="display: none; position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%);" >
            <div class="modal-dialog">
              <div class="modal-content border-flat">
                <form method="post" id="tellafriendModalForm">
                  <input type="hidden" name="tellfriendFormActive" value="1">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="townAlbumModalTitle">Tell A Friend</h4>
                  </div>
                  <div id="tellafriendModalBody" class="modal-body">
                    <div id="tellFriendErrorMessage"></div>
                    <label>Email</label>
                    <input type="text" name="email" class="form-control border-flat" placeholder="Your friend's email">
                    <br />
                    <label>Your Message</label>
                    <textarea name="message" class="form-control border-flat limitChar" maxlength="200" placeholder="Your message"></textarea>
                    <div class="maxlength"></div>
                    <br />
                    <strong>http://angbayanko.org/</strong><br />
                    <small>ABK URL will be attached along with your message</small>
                  </div>
                  <div class="modal-footer" id="modal-footer">
                    <button type="button" class="btn btn-default border-flat" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <button type="submit" id="tellFriendModalBtn" class="border-flat btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>


        <div class="col-md-12 upper-mini-nav">
          {{ form('search') }}
            <ul class="topmenu">
              <li><a href="/abkrescue">ABK Rescue</a></li>
              <li><a href="/about" class="aboutabktop">About ABK</a></li>
              <li><a href="/forum" hidden>Forum</a></li>
              <li><a href="/contactus">Contact ABK</a></li>
              <li><a href="#tellafriendModal" data-toggle="modal">Tell a Friend</a></li>
              <li>
                <?php
                if(!empty($abk_vol_username)){
                  echo '<a href="/myaccount" style="padding:0"><i class="icon-user"></i> <strong>'.$abk_vol_username.'</strong></a> | <a style="padding-left:0" href="'.$this->url->get().'index/logout">Logout</a>';
                }else{
                  echo '<a href="#loginModal" data-toggle="modal">Login</a>';
                }
                ?>
                </li>
              <li >
                <!-- <div class="search-box"> -->
                <input type="text" name="keyword" id="inputsearch" class="searchinput" placeholder="Search" />
                <input type="submit" id="btnsearch" name="search" class="btnSearch" value="Search">
              <!-- </div> -->
              </li>
            </ul>
          </form>
        </div>

      </div>
    </div>
    <div class="container">
      <div class="row logo">
        <div class="mainlogo pull-left"><img src="{{ url('img/toppage/abklogo.png')}}" /></div>
        <div class="txtlogo pull-left"><div class="textlogo"></div></div>
        <div class=" texttag pull-left">
          {{ about.content }}
        </div>
      </div>
    </div>
  </header>


  <!-- Header ends -->
  <!-- Flexslider Header -->
  <div class="header-flex flexslider">
    <ul class="slides">
      {{ slides }}
      <!-- <li>
        <div class="header-images img1">
          <iframe class="youtube-float-center" src="https://www.youtube.com/watch?v=LUdBAgIHJmE" frameborder="0" allowfullscreen></iframe>
        </div>
      </li>
      <li>
        <div class="header-images img2">

        </div>
      </li>
      <li>
        <div class="header-images img3">

        </div>
      </li>
      <li>
        <div class="header-images img4">

        </div>
      </li> -->
    </ul>
  </div>

  <!-- End Flexslider Header -->

<!-- youtube for smart phone -->
 <div class="youtube-container">
  <center>
    {{ youtubeoutbox }}
  </center>
  </div>
<!-- end youtube for smart phone -->
  <!-- Programs and Announcements-->
  <div class="container">

    <div class="row">
<div class="programs pull-left">
        <h5 class="section-title">AngBayanKo Programs</h5>
        <div class="programTiles">
          <a href="{{ url('programs/page/' ~ prog1['url']) }}">
            <div class="tile-flex flexslider dm">
              <div class="tile-title tile1">{{ prog1['title'] }}</div>
              <ul class="slides" style="height: 160px">
                <!-- Each slider should be enclosed inside li tag -->
                {% for pi in progimg1 %}
                <li class="slide"><div style="background: url('{{ url('img/programs/' ~ pi) }}') no-repeat center;"></div></li>
                {% endfor %}
              </ul>
              <div class="tile-tagline transparent">{{ prog1['tagline'] }}</div>
            </div>
          </a>
        </div>
        <div class="programTiles">
          <a href="{{ url('programs/page/' ~ prog2['url']) }}">
            <div class="tile-flex flexslider pc">
              <div class="tile-title tile3">{{ prog2['title'] }}</div>
              <ul class="slides" style="height: 160px">
                <!-- Each slider should be enclosed inside li tag -->
                {% for pi in progimg2 %}
                <li class="slide"><div style="background: url('{{ url('img/programs/' ~ pi) }}') no-repeat center;"></div></li>
                {% endfor %}
              </ul>
              <div class="tile-tagline transparent">{{ prog2['tagline'] }}</div>
            </div>
          </a>
        </div>
        <div class="programTiles">
          <a href="{{ url('programs/page/' ~ prog3['url']) }}">
            <div class="tile-flex flexslider hc">
              <div class="tile-title tile2">{{ prog3['title'] }}</div>
              <ul class="slides" style="height: 160px">
                <!-- Each slider should be enclosed inside li tag -->
                {% for pi in progimg3 %}
                <li class="slide"><div style="background: url('{{ url('img/programs/' ~ pi) }}') no-repeat center;"></div></li>
                {% endfor %}
              </ul>
              <div class="tile-tagline transparent">{{ prog3['tagline'] }}</div>
            </div>
          </a>
        </div>
        <div class="programTiles">
          <a href="{{ url('programs/page/' ~ prog4['url']) }}">
            <div class="tile-flex flexslider el">
              <div class="tile-title tile4">{{ prog4['title'] }}</div>
              <ul class="slides" style="height: 160px">
                <!-- Each slider should be enclosed inside li tag -->
                {% for pi in progimg4 %}
                <li class="slide"><div style="background: url('{{ url('img/programs/' ~ pi) }}') no-repeat center;"></div></li>
                {% endfor %}
              </ul>
              <div class="tile-tagline transparent">{{ prog4['tagline'] }}</div>
            </div>
          </a>
        </div>
        <div class="programTiles">
          <a href="{{ url('programs/page/' ~ prog5['url']) }}">
            <div class="tile-flex flexslider fc">
              <div class="tile-title tile5">{{ prog5['title'] }} </div>
              <ul class="slides" style="height: 160px">
                <!-- Each slider should be enclosed inside li tag -->
                {% for pi in progimg5 %}
                <li class="slide"><div style="background: url('{{ url('img/programs/' ~ pi) }}') no-repeat center;"></div></li>
                {% endfor %}
              </ul>
              <div class="tile-tagline transparent">{{ prog5['tagline'] }}</div>
            </div>
          </a>
        </div>
        <div class="programTiles">
          <a href="programs/page/environmental-preservation">
            <div class="tile-flex flexslider ep">
              <div class="tile-title tile6">Environmental Preservation</div>
              <ul class="slides" style="height: 160px">
                <!-- Each slider should be enclosed inside li tag -->
                {% for pi in progimg6 %}
                <li class="slide"><div style="background: url('../img/programs/1ce8e20da1694ab2369e79382d6f6ea4img2 (2).png') no-repeat center;"></div></li>
                {% endfor %}
              </ul>
              <div class="tile-tagline transparent">What's left for the next generation</div>
            </div>
          </a>
        </div>

        <?php if(!empty($somePartners)){ ?>
          <h5 class="section-title abkpartners-title">ABK Partners</h5>



          <div class="other-left-contents endorsements pull-left">


          <?php foreach ($somePartners as $key) { ?>
          <div class="testimonial">
            <div class="test-info">
              <span class="test-name"><a href="<?php echo $this->url->get().'partners/view/'.$key['partnerID'] ?>"><?php echo $key['partnerName']?></a></span><br/>
              <p class=""><?php echo $key['partnerInfo']; ?></p>
            </div>
          </div>
          <?php } ?>
        </div>

          <div class="clearfix"></div>

          <br />
          <div class="">
            <a href="<?php echo $this->url->get() ?>partners">View all ABK partners</a>
          </div>
        <?php } ?>
      </div>

      <div class="col-md-3 col-sm-3 col-xs-5 sidebar pull-right">
        <h5 class="section-title">Connect with us</h5>
        <div class="connectwithus">
          <a href="/site/pages/support-your-chosen-program">
            <div class="connectTiles left-tile sscp">
              <div class="connectTitle-Title" style="background-color: rgb(8, 74, 178);">Support your Chosen Program</div>
            </div>
          </a>
          <a href="{{ url('towns') }}">
            <div class="connectTiles right-tile swhh">
              <div class="connectTitle-Title" style="background-color: rgb(99, 47, 0);">See What's Happening in your Hometown</div>
            </div>
          </a>
          <a href="/site/pages/retiring-to-make-a-difference">
            <div class="connectTiles left-tile bottom-tile rmd">
              <div class="connectTitle-Title" style="background-color: rgb(210, 71, 38);">Retiring to Make a Difference</div>
            </div>
          </a>
          <a href="/site/pages/businesses-for-social-change">
            <div class="connectTiles right-tile bottom-tile bsc">
              <div class="connectTitle-Title" id="box-program" style="background-color: rgb(62, 85, 109);">Businesses for Social Change</div>
            </div>
          </a>
        </div>
        <div class="social-links">
          <ul class="social-list">
            <li class="social-icons"><a href="https://www.facebook.com/Ang-Bayan-Ko-Foundation-1664040513867744/" target="_blank" class="fb"></a></li>
            <li class="social-icons"><a href="https://plus.google.com/u/0/101853314610354158387" target="_blank" class="gplus"></a></li>
            <li class="social-icons"><a href="https://twitter.com/abk_foundation" target="_blank" class="twitter"></a></li>
            <li class="social-icons"><a href="https://www.linkedin.com/in/ang-bayan-ko-foundation-7a33a6113" target="_blank" class="in"></a></li>
            <li class="social-icons"><a href="#" class="pi"></a></li>
            <li class="social-icons sc-last"><a href="https://www.youtube.com/channel/UCA9g-GNgrI0Eq28lMt7g0Iw" target="_blank" class="youtube"></a></li>
          </ul>
        </div>
        <a href="{{ url('forum') }}">
          <div class="joinforum" hidden>
            <div class="forum-icon"></div>
            <h2>Join the discussions at the Forums</h2>
          </div>
        </a>

        <div style="clear:both"></div>
        <div id="side-donation">
          <a href="/donate"><img height="74" width="307" src="{{ url('img/paypal.jpg')}}"></a>
        </div>

        <div class="e-newsletter">
          <h5>E-Newsletter</h5>
          {{ form('index/submitEmailNewsLetter', 'id':'e-newsletterForm') }}
            <div id="enewsMessage"></div>
            <input type="hidden" name="emailnewsletterAjax" value="1">
            {{ text_field('name', 'class':'input-newsletter-name', 'name':'name', 'placeholder':'Your full name') }}
            {{ text_field('email', 'class':'input-newsletter-email', 'name':'email', 'placeholder':'Your Email') }}
            {{ submit_button("subscribe", 'class':'input-newsletter-button', 'value':'Subscribe', 'name':'subscribe' ) }}
          </form>
        </div>

        <?php if(count($announcements) > 0){ ?>
        <div class="announcements-container">
          <br>
          <table width="100%" style="word-break: break-all;">
          <tr>
            <td class="annhead"><h5>Announcements</h5></td>
          </tr>
          <tr style="padding-top: 20px; display:block;">

          </tr>
          <tr style="padding-bottom: 0px;">
            <?php
          foreach ($announcements as $ann) {
            if($ann['annStatus'] == 'activated'){
            ?>
            <th style="padding-bottom: 0px;">
              <h4><a href="/announcements/view/<?php echo $ann['annID'] ?>" class="pull-left ann-title"><?php echo $ann['annTitle'] ?></a></h4>
            </th>
          </tr>
          <tr>
            <td><?php echo $ann['annDesc'] ?></td>
          </tr>
          <tr>
            {# <td><span class="pull-right"><?php echo date("m-d-Y H:i:sP", $ann['annDate']) ?> <?=$ann['annDate']?></span></td> #}
            <?php
              $myDate = date("Y-m-d", $ann['annDate']);
              $myDates = str_replace('.', '-', $myDate);
              {# $mydate = $ann['annDate']; #}
            ?>
            <td style="float: right;position: relative;top: 0px;"><span class="text-danger">Posted on  <?=date("F jS, Y", strtotime($myDates))?></span> </td>
            {# <td><?=date("jS F, Y", strtotime($myDates))?></td> #}
          </tr>
          <tr>
            <td> <hr style="margin: 0px 0px 5px 0px"> </td>
          </tr>
        <?php }} ?>
          <tr>
            <td><span class="pull-right"> <a href="/announcements">View all announcements</a></span></td>
          </tr>

        </table>
        </div>
        <!-- <div class="announcements">
          <h5>Announcements</h5>
          <?php
          foreach ($announcements as $ann) { ?>
          <div class="annList">
          <a href="/announcements/view/<?php echo $ann['annID'] ?>" class="pull-left ann-title"><?php echo $ann['annTitle'] ?></a>
            <div class="pull-right"><?php echo date("m-d-Y", $ann['annDate']) ?></div>
            <div class="pull-left">
              <p>
                <?php echo $ann['annDesc'] ?>
              </p>
            </div>
          </div>
          <?php } ?>

          <div class="annViewAll pull-right">
            <a href="/announcements">View all announcements</a>
          </div>
        </div> -->
        <?php }else{ ?>
        <div class="announcements-container">
          <br>
          <table width="100%">
          <tr>
            <td class="annhead"><h5>Announcements</h5></td>
          </tr>
          <tr><td>&nbsp;</td>
          </tr>
          <tr>
            <td>No Announcements Posted.</td>
            <tr>
        </table>
      </div>
        <?php } ?>

      </div>
    </div>
  </div>

  <!-- Programs and Announcements Ends-->

  <div class="container section-feature ">
    <div class="row">
      <div class="tile">
      </div>
      <div class="tile">
      </div>
      <div class="tile">
      </div>
    </div>
  </div>

  <!-- Footer starts -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-md-offset-1 col-sm-12 ">
          {# <div class="row"> #}
            <div class="col-md-4 col-sm-4 contact-us-footer pull-left">
              <h3>Contact Us</h3>
                <strong> Philippines:</strong> <br/>
                <i class="icon-home"></i>#97 Mabini St., Tayug,  Pangasinan <br/>
                <i class="icon-phone"></i> 09999990161 (SMART) <br/>
                <i class="icon-phone"></i>  09173033838(GLOBE) <br/>
                <i class="icon-envelope-alt"></i> angbayankoorg@gmail.com <br/> <br/>
                <strong> United States of America:</strong> <br/>
                <i class="icon-home"></i> 123, Some Area. Los Angeles, CA. <br/>
                <i class="icon-phone"></i> +239-3823-3434 <br/>
                <i class="icon-envelope-alt"></i> someone@company.com <br/> <br/><br>
              </div>
              <div class="col-md-4 col-sm-4 more-info-footer pull-left">
                  <h3>More Info</h3>
                  <ul class="more-info">
                    <li><a href="/site/pages/support-your-chosen-program">Support your Chosen Program</a></li>
                    <li><a href="/site/pages/retiring-to-make-a-difference">Retiring to make a difference</a></li>
                    <li><a href="/site/pages/businesses-for-social-change">Businesses for Social Change</a>
                    <li><a href="/site/pages/volunteering">Volunteering</a></li>
                    <li><a href="/site/pages/nature">Nature</a></li>
                    <li><a href="/site/pages/reminder">Reminder</a></li>
                    <br><br><br><br>
                    <li><a href="/site"><strong>See more...</strong></a></li>
                  </ul>
              </div>
              <div class="col-md-4 col-sm-4 programs-footer pull-left">
                <h3>Programs</h3>
                  <ul class="more-info">
                    <li><a href="/programs/page/disaster-risk-managements">Disaster Risk Management</a></li>
                    <li><a href="/programs/page/create-your-personal-cause">Create Your Personal Cause</a></li>
                    <li><a href="/programs/page/healthcare-for-everyone">Healthcare For Everyone</a></li>
                    <li><a href="/programs/page/economic-livelihood">Economic Livelihood</a></li>
                    <li><a href="/programs/page/early-childhood-education">Early Childhood Education</a></li>
                    <li><a href="/programs/page/environmental-preservation">Environmental Preservation</a></li>
                    <br><br><br><br>
                    <li><a href="/programs/"><strong>See more...</strong></a></li>
                  </ul>
            </div>
            <div class="col-md-12 col-sm-12" style="clear:both;">
                  <span style="font-weight:lighter;">Copyright &copy; 2014 | <a href="#">AngBayanKo Site</a> - <a href="#">Home</a> </span>
            </div>
            {# <div class="col-md-3 others-footer pull-left">
                <h3>Others</h3>
                <ul class="more-info">
                  <li><a href="/site/pages/support-your-chosen-program">Support your Chosen Program</a></li>
                  <li><a href="/site/pages/retiring-to-make-a-difference">Retiring to make a Difference</a></li>
                  <li><a href="/site/pages/businesses-for-social-change">Businesses for Social Change</a>
                  <li><a href="/site/pages/volunteering">Volunteering</a></li>
                  <li><a href="/site/pages/nature">Nature</a></li>
                  <li><a href="/site/pages/reminder">Reminder</a></li>
                  <br><br><br><br>
                  <li><a href="/site"><strong>See more...<strong></a></li>
                </ul>
              </div>
            </div> #}
            {# Buti nahana moko  #}
          </div>
        {# </div> #}
        <br>

  </div>
</footer>
<!-- Footer ends -->

<!-- Scroll to top -->
<span class="totop"><a href="#"><i class="icon-chevron-up"></i></a></span>

<!-- JS -->
<script src="js/jquery.js"></script> <!-- jQuery -->
<script src="js/bootstrap.js"></script> <!-- Bootstrap -->
<script src="js/jquery.isotope.js"></script> <!-- Isotope -->
<script src="js/jquery.prettyPhoto.js"></script> <!-- Pretty Photo -->
<script src="js/filter.js"></script> <!-- Filter for support page -->

<script src="js/jquery.flexslider-min.js"></script> <!-- Flex slider -->
<script src="js/jquery.cslider.js"></script> <!-- Parallax Slider -->
<script src="js/modernizr.custom.28468.js"></script> <!-- Parallax slider extra -->

<script src="js/jquery.carouFredSel-6.1.0-packed.js"></script> <!-- Carousel for recent posts -->
<script src="js/jquery.refineslide.min.js"></script> <!-- Refind slider -->
<script src="js/jquery.backgroundSize.js"></script>
<script src="js/respond.src.js"></script>
{{ javascript_include('js/jquery.maxlength.min.js') }}
<script src="js/custom.js"></script> <!-- Custom codes -->

</body>
</html>
