<?php

return new \Phalcon\Config(array(
    'database' => array(
        'adapter'     => 'Mysql',
        'host'        => '127.0.0.1',
        'username'    => 'root',
        'password'    => 'adminadmin123',
        'dbname'      => 'dbabk',
        'charset'  => 'utf8',

    ),
    'application' => array(
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'modelsDir'      => __DIR__ . '/../../app/models/',
        'viewsDir'       => __DIR__ . '/../../app/views/',
        'pluginsDir'     => __DIR__ . '/../../app/plugins/',
        'libraryDir'     => __DIR__ . '/../../app/library/',
        'cacheDir'       => __DIR__ . '/../../app/cache/',
        'formsDir'       => __DIR__ . '/../../app/forms/',
        'baseUri'        => '/',
    ),
    'postmark' => array(
        'url' => 'https://api.postmarkapp.com/email',
        'token' => '016e0c8c-d974-491f-b17d-f9c89915ec0a',
        'signature' => 'contact@geeksnest.com'
    )
));
