<?php
use Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email as EmailValidator,
    Phalcon\Validation\Validator\Regex,
    Phalcon\Validation\Validator\Confirmation,
    Phalcon\Validation\Validator\StringLength as StringLength,
    Phalcon\PHPMailer as PHPMailer;
class SiteController extends ControllerBase
{

    // protected $breadCrumbs = "<a href='/'>Home</a> >Sites";
    protected $breadCrumbs = "<a href='/'>Home</a> > <a href='/site'>Site List<a>";
    public function initialize()
    {
        parent::initialize();
        date_default_timezone_set('Asia/Manila');
        $this->view->bread_crumbs = $this->breadCrumbs ."  <a href=''>". $program->programName."</a>";
        $this->validateLoginVolunteer();
    }

    public function indexAction(){

      $contact= Tblcontact::find();
      $this->view->contacts=$contact;

       $numberPage = $this->request->getQuery("page", "int");
       $numberPage = !empty($numberPage)?$numberPage:1;

       $phql = 'SELECT
               Tblpages.pageID,
               Tblpages.pageTitle,
               Tblpages.pageSlug
               FROM Tblpages WHERE specialPage = 1
                ORDER BY specialPage DESC';
       $result = $this->modelsManager->executeQuery($phql);

       $dataArray = array();
       foreach ($result as $key => $value) {
           $dataArray[] = array(
               'pageID'=>$value->pageID,
               'pageTitle'=>$value->pageTitle,
               'pageSlug'=>$this->_truncateHtml($value->pageSlug),
               );
       }

       $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
           "data" => $dataArray,
           "limit"=> 10,
           "page" => $numberPage
           ));
       $this->view->page = $paginator->getPaginate();
    }

    public function signupAction(){
        $this->view->disable();
        $response = array();
        if($this->request->isPost() && $this->request->isAjax()){
            $validation = new Phalcon\Validation();

            $validation
            ->add('title', new PresenceOf(array(
                'message' => 'The title is required',
            )))
            ->add('fname', new PresenceOf(array(
                'message' => 'The first name is required',
            )))
            // ->add('mname', new PresenceOf(array(
            //     'message' => 'The middle name is required',
            // )))
            ->add('lname', new PresenceOf(array(
                'message' => 'The last name is required',
            )))
            ->add('address', new PresenceOf(array(
                'message' => 'The address is required',
            )))
            ->add('email', new PresenceOf(array(
                'message' => 'The email is required',
            )))
            ->add('username', new PresenceOf(array(
                'message' => 'The username is required',
            )))
            ->add('password', new PresenceOf(array(
                'message' => 'The password is required',
            )))
            ->add('repassword', new PresenceOf(array(
                'message' => 'The confirm password is required',
                'cancelOnFail' => false
            )))
            ->add('password', new Confirmation(array(
               'message' => 'Password doesn\'t match confirmation',
               'with' => 'repassword',
               'cancelOnFail' => false
            )))
            ->add('fname', new StringLength(array(
                  'max' => 255,
                  'min' => 2,
                  'messageMaximum' => 'Your name is too long',
                  'messageMinimum' => 'Your username must be atleast 2 character long'
            )))
            // ->add('mname', new StringLength(array(
            //       'max' => 255,
            //       'min' => 2,
            //       'messageMaximum' => 'Your middle name is too long',
            //       'messageMinimum' => 'Your middle name must be atleast 2 character long'
            // )))
            ->add('lname', new StringLength(array(
                  'max' => 255,
                  'min' => 2,
                  'messageMaximum' => 'Your last name is too long',
                  'messageMinimum' => 'Your last name must be atleast 2 character long'
            )))
            ->add('extname', new StringLength(array(
                  'max' => 255,
                  'min' => 0,
                  'messageMaximum' => 'Your extension name is too long',
                  'messageMinimum' => 'Your last extension name must be atleast 0 character long'
            )))
            ->add('address', new StringLength(array(
                  'max' => 255,
                  'min' => 1,
                  'messageMaximum' => 'Your address is too long',
                  'messageMinimum' => 'Your last address must be atleast 1 character long'
            )))
            ->add('username', new StringLength(array(
                  'max' => 255,
                  'min' => 6,
                  'messageMaximum' => 'We don\'t like really long usernames',
                  'messageMinimum' => 'Your username must be atleast 6 characters long'
            )))
            ->add('password', new StringLength(array(
                  'max' => 255,
                  'min' => 6,
                  'messageMaximum' => 'We don\'t like really long passwords',
                  'messageMinimum' => 'Your password must be atleast 6 characters long',
                  'cancelOnFail' => false
            )))
            ->add('email', new EmailValidator(array(
               'message' => 'The e-mail is not valid'
            )))
            ;

            $validation->setFilters('username', 'trim');
            $validation->setFilters('fname', 'trim');
            // $validation->setFilters('mname', 'trim');
            $validation->setFilters('lname', 'trim');
            $validation->setFilters('extname', 'trim');
            $validation->setFilters('address', 'trim');
            $validation->setFilters('email', 'trim');
            $validation->setFilters('phone', 'trim');

            $messages = $validation->validate($_POST);
            $errMessage = null;
            if (count($messages)) {
                foreach ($messages as $message) {
                    $errMessage .= '<li>'.$message. '</li>';
                }
                $response['success'] = false;
                $response['message'] = '
                    <div class="alert alert-danger alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <strong>Please fix the following:</strong>'.$errMessage.'
                    </div>
                ';
            }else{

                $checkUsername = Tblvolunteers::findFirst('username = "'.$this->request->getPost('username').'"');
                if($checkUsername){
                    $response['success'] = false;
                    $response['message'] = '
                        <div class="alert alert-danger alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          Username not available. Please choose another username.
                        </div>
                    ';
                }else{
                    $phql = 'SELECT activityID, activity, Tblprograms.programID, programName
                    FROM Tblporgramactivities
                    LEFT JOIN Tblprograms ON Tblprograms.programID = Tblporgramactivities.programID
                    WHERE Tblporgramactivities.status = 1
                    ORDER BY Tblprograms.programID
                    ';
                    $activityOptions =  $this->modelsManager->executeQuery($phql);
                    $actOptions = null;
                    $pID = 0;

                    /*echo '<pre>';
                    print_r($activityOptions[0]->programID);
                    echo '</pre>';exit;*/

                    if(!empty($activityOptions)){
                        $pID = $activityOptions[0]->programID;
                        $actOptions .= '<div class="activitySet"><strong>'.$activityOptions[0]->programName.'</strong><ul style="list-style:none">';
                    }
                    foreach ($activityOptions as $key => $value) {

                        if($pID != $value->programID){
                            $actOptions .= '</ul></div><div class="activitySet"><strong>'.$value->programName.'</strong><ul style="list-style:none">';
                            $pID = $value->programID;
                        }
                        $actOptions .= '<li><input name="activities[]" type="checkbox" value="'.$value->activityID.'"> '.$value->activity.'</li>';

                    }
                    $actOptions .= !empty($actOptions)?'</ul></div>':'';
                    $html = '
                        <form method="post" id="finalSignupForm" action="/site/finalsignup">
                            <input type="hidden" name="title" value="'.$this->request->getPost('title').'">
                            <input type="hidden" name="username" value="'.$this->request->getPost('username').'">
                            <input type="hidden" name="fname" value="'.$this->request->getPost('fname').'">
                            <input type="hidden" name="mname" value="'.$this->request->getPost('mname').'">
                            <input type="hidden" name="lname" value="'.$this->request->getPost('lname').'">
                            <input type="hidden" name="extname" value="'.$this->request->getPost('extname').'">
                            <input type="hidden" name="address" value="'.$this->request->getPost('address').'">
                            <input type="hidden" name="email" value="'.$this->request->getPost('email').'">
                            <input type="hidden" name="phone" value="'.$this->request->getPost('phone').'">
                            <input type="hidden" name="password" value="'.$this->request->getPost('password').'">
                            <h3 class="fontNormal">2. Select Activity</h3>
                            <small>Step 2 of 2</small>
                            <hr/>

                            <div id="finalFormResult"></div>
                            <div class="well well-sm border-flat">
                                <label style="cursor:pointer">
                                    <!--- <input type="radio" name="actSelect" class="actSelect" value="1" checked> --->
                                    Choose activities from our programs
                                </label>
                            </div>
                            <div id="existingAct">
                                '.$actOptions.'
                            </div>

                            <div class="well well-sm border-flat" id="showblk">
                                <label style="cursor:pointer">
                                  <!---  <input type="radio" name="actSelect" class="actSelect" value="2"> ---> 
                                  <input type="checkbox" id="myCheck"  onclick="myFunction()">
                                  Suggest your own program or activity (Optional)
                                </label>
                            </div>
                            <div id="suggestAct" style="display:none">
                                <label>Program Title / Activity</label>
                                <input type="text" name="suuggestedAct" class="form-control border-flat" placeholder="Title">
                                <br />
                                <label>Description</label>
                                <textarea name="suuggestedActDesc" class="form-control border-flat limitChar" rows="10" placeholder="Enter your desired program or activity description"></textarea>
                                <div class="maxlength"></div>
                            </div>
                            <br />
                            <button type="button" class="btn btn-default" id="signGoBack">Go Back</button>
                            <button type="submit" class="btn btn-primary" id="signupCreateActBtn">Create my account</button>
                        </form>

<script>
function myFunction() {
  var checkBox = document.getElementById("myCheck");
  var text = document.getElementById("suggestAct");
  if (checkBox.checked == true){
    text.style.display = "block";
  } else {
     text.style.display = "none";
  }
}
</script>';
                    $response['success'] = true;
                    $response['message'] = $html;
                }
            }
            echo json_encode($response);
        }
    }


    public function finalsignupAction(){

        $this->view->disable();
        $response = array();
        if($this->request->isPost() && $this->request->isAjax()){
            $selectedAct = $this->request->getPost('actSelect');

            $key = md5(microtime().rand());
            //$activationLink = 'http://angbayanko.site/site/acivateAccount/'.$key;
             $activationLink = 'http://abksite.gotitgenius.com/site/acivateAccount/'.$key; //link for test site
            // link below for live site
            //$activationLink = 'http://angbayanko.org/site/acivateAccount/'.$key;
            $now = time();
            $userData = array(
                'title'=>$this->request->getPost('title', 'striptags'),
                'fname'=>$this->request->getPost('fname', 'striptags'),
                'mname'=>$this->request->getPost('mname', 'striptags'),
                'lname'=>$this->request->getPost('lname', 'striptags'),
                'extname'=>$this->request->getPost('extname', 'striptags'),
                'address'=>$this->request->getPost('address', 'striptags'),
                'email'=>$this->request->getPost('email', 'striptags'),
                'phone'=>$this->request->getPost('phone', 'striptags'),
                'username'=>$this->request->getPost('username', 'striptags'),
                'password'=>sha1($this->request->getPost('password', 'striptags')),
                'status'=>0,
                'activationKey'=>$key,
                'dateAdded'=>$now
                );
            
            $body = "
            <div style='text-align: center; border: solid; padding: 20px; width: 50%; margin-left: auto; margin-right:auto;'>
            <img src='http://abksite.gotitgenius.com/img/toppage/abklogo.png' alt='img'>
            <img src='http://abksite.gotitgenius.com/img/toppage/abktxtlogo.png' alt='imgtxt'  style='padding-bottom: 45px;''>
                <br /><br />
                <h1>Account Confirmation</h1> <br /><br />
                <div style='text-align: left;'>
                Hello ".$userData['title']." ".$userData['fname'].",
                <br /><br />
                We have received your registration information for the account name: <br /><br />
                Username: ".$userData['username']."<br /><br />
                to verify your account please click the link below or copy then paste it to your browser's address bar. Thank you.<br /><br />
                <a href='$activationLink'>".$activationLink."</a>
                <br /><br />
                If you believe that this is a mistake, please ignore this message.<br /><br />
                This is a auto generated email, please do not reply. <br /><br />
                angbayanko.org</div>
                </div>";


            $mailObjects = array(
            'From'=> 'angbayanko.org@no-reply.com',
            'FromName' => 'angbayanko.org',
            'AddAddress'=> $userData['email'],
            'Subject' => 'Confirm Registration',
            'Body' => $body
          );


            $successSent = '
            <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              Thank you for registering. An email has been sent to <strong>'.$userData['email'].'</strong>.
            </div>';

            if($selectedAct = 1){
                $activities = $this->request->getPost('activities');
                if(!empty($activities)){

                    if($this->sendmailer($userData['email'], 'Confirm Registration', $body)){
                        $volunteers = new Tblvolunteers();
                        $volunteers->assign($userData);
                        $volunteers->save();
                        $this->_sendmail($mailObjects);
                        $lastID = $volunteers->volunteerID;

                        foreach ($activities as $acVal) {
                            $volActivity = new Tblvolunteeractivities();
                            $volActivity->assign(array(
                            'volunteerID'=>$lastID,
                            'activityID'=>$acVal,
                            'custom'=>$this->request->getPost('suuggestedAct'),
                            'customDesc'=>$this->request->getPost('suuggestedActDesc'),
                            'dateAdded'=>$now));
                            $volActivity->save();
                        }


                        $response['success'] = true;
                        $response['message'] = $successSent;
                    }
                }else{
                    $response['success'] = false;
                     $response['message'] = '
                        <div class="alert alert-danger alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          Please select atleast one activity
                        </div>
                    ';
                }
            }elseif($selectedAct = 2){
                $validation = new Phalcon\Validation();
                $validation
                ->add('suuggestedAct', new PresenceOf(array(
                    'message' => 'The suggested activity is required',
                )))
                ->add('suuggestedActDesc', new PresenceOf(array(
                    'message' => 'The suggested activity description is required',
                    'cancelOnFail' => true
                )))
                ->add('suuggestedAct', new StringLength(array(
                  'max' => 255,
                  'min' => 3,
                  'messageMaximum' => 'We don\'t like really long activity title',
                  'messageMinimum' => 'Your suggested activity must be atleast 3 characters long'
                )))
                /*->add('suuggestedActDesc', new StringLength(array(
                  'max' => 255,
                  'min' => 3,
                  'messageMaximum' => 'We don\'t like really long activity description',
                  'messageMinimum' => 'Your suggested activity description must be atleast 3 characters long'
                )))*/
                ;

                $validation->setFilters('suuggestedAct', 'trim');
                $messages = $validation->validate($_POST);

                $errMessage = null;
                if (count($messages)) {
                    foreach ($messages as $message) {
                        $errMessage .= '<li>'.$message. '</li>';
                    }
                    $response['success'] = false;
                    $response['message'] = '
                        <div class="alert alert-danger alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>Please fix the following:</strong>'.$errMessage.'
                        </div>
                    ';
                }else{
                    if($this->_sendmail($mailObjects)){
                        $volunteers = new Tblvolunteers();
                        $volunteers->assign($userData);
                        $volunteers->save();
                        $lastID = $volunteers->volunteerID;

                        $volActivity = new Tblvolunteeractivities();
                        $volActivity->assign(array(
                            'volunteerID'=>$lastID,
                            'activityID'=>$acVal,
                            'custom'=>$this->request->getPost('suuggestedAct'),
                            'customDesc'=>$this->request->getPost('suuggestedAct'),
                            'dateAdded'=>$now));
                        $volActivity->save();

                        $response['success'] = true;
                        $response['message'] = $successSent;
                    }else{
                        die ('error sending email');
                    }
                }
            }
            echo json_encode($response);
        }

    }

    public function pagesAction($slug){
        $about=Tblother::findfirst("title='Main Tagline'");
       $this->view->about=$about;
       $contact= Tblcontact::find();
       $this->view->contacts=$contact;

        $this->view->actOptions = null;

        //$this->session->remove('vol_auth');

        $this->view->page = $page =  Tblpages::findFirst('pageSlug="'.$slug.'" AND pageType="pages" AND pageActive="1"');

        $subpages = Tblpages::find('pageParent = '.$page->pageID.' AND pageType="pages" AND pageActive="1"');

        $this->view->bread_crumbs = $this->breadCrumbs ." > <a href=''>". $page->pageTitle."</a>";

        $tabs = null;
        foreach ($subpages as $key => $value) {
            $tabs .= '<li class=""><a href="/site/pages/'.$value->pageSlug.'">'.$value->pageTitle.'</a></li>';

        }
        $this->view->subpages = $tabs;

        if($page->specialPage == 1){
            $numberPage = $this->request->getQuery("page", "int");
                //Post
            $builder = $this->modelsManager->createBuilder()
                ->from('Tblpost')
                ->innerJoin('Tblpostcat', 'Tblpost.postID = Tblpostcat.postID')
                ->where('relatedID = '.$page->pageID)
                ->andWhere("relatedtype = 'pages'")
                ->andWhere('postPublishDate <= '.time() )
                ->orderBy('postDate DESC');

            $paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
              "builder" => $builder,
              "limit"=> 5,
              "page" => $numberPage
              ));
            // Get the paginated results
            $this->view->post = $post = $paginator->getPaginate();
        }
    }

    public function acivateAccountAction($key){
        $checkKey = Tblvolunteers::findFirst('activationKey = "'.$key.'"');
        if($checkKey == true){
            $checkKey->assign(array('status'=>1, 'activationKey'=>null));
            if($checkKey->save()){
                $redirectPage = $this->url->get().'myaccount';
                $this->session->set('vol_auth', array(
                    'abk_vol_id' => $checkKey->volunteerID,
                    'abk_vol_fname' => $checkKey->fname,
                    'abk_vol_username' => $checkKey->username
                ));
                header('location: '.$redirectPage);
            }
        }else{
            echo 'Invalid activation key.';
            exit;
        }
    }

    public function testAction() {

    }
}
