<?php
use Phalcon\Tag as Tag,
Phalcon\Mvc\View,
Phalcon\Validation\Validator\PresenceOf,
Phalcon\Validation\Validator\StringLength,
Phalcon\Validation\Validator\Confirmation,
Phalcon\Validation\Validator\Regex as Regex,
Phalcon\Validation\Validator\Email,
Phalcon\Validation\Validator\Uniqueness as UniquenessValidator,
Phalcon\Mvc\Model\Criteria;

class AdminController extends ControllerBase
{
  private $subpages = null;

  public function query($phql){
    $dbbnb = \Phalcon\DI::getDefault()->get('db');
    $stmt = $dbbnb->prepare($phql);
    $stmt->execute();
    $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    return $result;
  }


  public function initialize()
  {

    parent::initialize();
    date_default_timezone_set('Asia/Manila');
    // date_default_timezone_set('Asia/Taipei');
    // date_default_timezone_set('UTC');
    $role = $this->session->get('roles');
    $this->view->userpages = $role['roles'];
    $this->view->allnewspost = count(Tblpost::find());
    $this->view->townscount = count(Tbltowns::find());
    $this->view->pagescount = count(Tblpages::find("pageType='pages'"));
    $this->view->contactscount = count(Tblinquiries::find());
    $this->view->contactscount = count(Tblinquiries::find());
    $this->view->programMenu = Tblprograms::find();
    $this->view->usersCount = count(Tblusers::find("userLevel = 0"));
    $this->view->partnerCount = count(Tblusers::find("userLevel = 2"));
    $this->view->volCount = count(Tblvolunteers::find("status = 1"));
    $this->view->subsCount = count(Tblnewsletteremails::find());
    $this->view->activitiesCount = count(Activities::find());
    $this->view->progCount = count(Tblprograms::find());
    /* volunteers count*/
    $andWhere .='';
    $andWhere .= !empty($volFilter)?' AND Tblvolunteeractivities.activityID = '.$volFilter:$andWhere;
    $phql = 'SELECT DISTINCT Tblvolunteers.volunteerID,username, title, fname, mname, lname, extname, email, phone, Tblvolunteers.dateAdded
    FROM Tblvolunteeractivities
    LEFT JOIN Tblvolunteers ON Tblvolunteers.volunteerID = Tblvolunteeractivities.volunteerID
    WHERE status = 1 '.$andWhere;
    $volunteermember =  $this->modelsManager->executeQuery($phql);
    $this->view->volunteerscount = count($volunteermember);
    /* end vol count */
    /* suggested program count */
    $phql = 'SELECT custom, customDesc, Tblvolunteeractivities.dateAdded, vActivityID, Tblvolunteeractivities.volunteerID,
    title, fname, mname, lname, extname, username, address, email, phone
    FROM Tblvolunteeractivities
    LEFT JOIN Tblvolunteers ON Tblvolunteers.volunteerID = Tblvolunteeractivities.volunteerID
    WHERE vActivityID != 0 AND status = 1';
    $suggested = $this->modelsManager->executeQuery($phql);
    $this->view->suggestedCount = count($suggested);
    /* end suggested count */

    $phql = 'SELECT Tbldonations.payment_amount FROM Tbldonations';
    $result = $this->modelsManager->executeQuery($phql);
    $donateCount = 0;
    foreach($result as $r) {
      $donateCount += $r->payment_amount;
    }
    $this->view->donateCount = $donateCount;
    $this->view->annCount = count(Tblannouncements::find());
    //Check if the variable is defined
    if ($this->session->has("auth")) {
      $abk_user = $this->session->get('auth');
      //Retrieve its value
      $this->view->abk_user = $abk_user['abk_fullname'];
      $this->view->abk_userlevel = $abk_user['abk_userlevel'];
    }
    // $fb = $this->_facebookCount('https://www.facebook.com/Ang-Bayan-Ko-Foundation-1664040513867744');
    // $this->view->fbLikes = $fb[0]->like_count ? $fb[0]->like_count : "0";
    $this->view->fbLikes = "NA";
    $this->view->followerCount = null;

    // $twitter = $this->_followerCount("abk_foundation");
    $this->view->followerCount = $twitter[0]->user->followers_count ? $twitter[0]->user->followers_count : "0";
    $this->view->plusonersCount = $this->_plusonersCount() ? $this->_plusonersCount() : "0";

  }

  // https://www.googleapis.com/plus/v1/people/101853314610354158387?key=AIzaSyBORHIAVXRxXsKecuIvHNB-DWK7KjQBotU
  private function _plusonersCount(){
    $google_api_key = 'AIzaSyBORHIAVXRxXsKecuIvHNB-DWK7KjQBotU';
    $page_id = '101853314610354158387';
    $data = @file_get_contents("https://www.googleapis.com/plus/v1/people/$page_id?key=$google_api_key");
    $data = json_decode($data, true);
    return $data['circledByCount'];
  }

  private function _followerCount($screen_name){
    $token = '4722321314-eDCfLmgcDyEv611X3kY4lIhWVZvDnQ7Sa5DEQ4l';
    $token_secret = 'EcUJ6EKzYqp5JdfRMXeTpMCvo4LVys5UFEAccWeZTrUBl';
    $consumer_key = 'v7Aai3H2h2KqFoSbst1yzupWa';
    $consumer_secret = '3oBbRNUcgIkiZKhHCCtBcbq2G3VmjGbVWsuouDdckbAkeGuJOc';

    $host = 'api.twitter.com';
    $method = 'GET';
    $path = '/1.1/statuses/user_timeline.json';

    $query = array( // query parameters
      'screen_name' => $screen_name,
      'count' => '5'
      );

    $oauth = array(
      'oauth_consumer_key' => $consumer_key,
      'oauth_token' => $token,
      'oauth_nonce' => (string)mt_rand(), // a stronger nonce is recommended
      'oauth_timestamp' => time(),
      'oauth_signature_method' => 'HMAC-SHA1',
      'oauth_version' => '1.0'
      );

    $oauth = array_map("rawurlencode", $oauth); // must be encoded before sorting
    $query = array_map("rawurlencode", $query);

    $arr = array_merge($oauth, $query); // combine the values THEN sort

    asort($arr); // secondary sort (value)
    ksort($arr); // primary sort (key)

    // http_build_query automatically encodes, but our parameters
    // are already encoded, and must be by this point, so we undo
    // the encoding step
    $querystring = urldecode(http_build_query($arr, '', '&'));

    $url = "https://$host$path";

    // mash everything together for the text to hash
    $base_string = $method."&".rawurlencode($url)."&".rawurlencode($querystring);

    // same with the key
    $key = rawurlencode($consumer_secret)."&".rawurlencode($token_secret);

    // generate the hash
    $signature = rawurlencode(base64_encode(hash_hmac('sha1', $base_string, $key, true)));

    // this time we're using a normal GET query, and we're only encoding the query params
    // (without the oauth params)
    $url .= "?".http_build_query($query);
    $url=str_replace("&amp;","&",$url); //Patch by @Frewuill

    $oauth['oauth_signature'] = $signature; // don't want to abandon all that work!
    ksort($oauth); // probably not necessary, but twitter's demo does it

    // also not necessary, but twitter's demo does this too
    if (!function_exists('add_quotes')) {
      function add_quotes($str) { return '"'.$str.'"'; }
    }
    $oauth = array_map("add_quotes", $oauth);

    // this is the full value of the Authorization line
    $auth = "OAuth " . urldecode(http_build_query($oauth, '', ', '));

    // if you're doing post, you need to skip the GET building above
    // and instead supply query parameters to CURLOPT_POSTFIELDS
    $options = array( CURLOPT_HTTPHEADER => array("Authorization: $auth"),
    //CURLOPT_POSTFIELDS => $postfields,
      CURLOPT_HEADER => false,
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_SSL_VERIFYPEER => false);

    // do our business
    $feed = curl_init();
    curl_setopt_array($feed, $options);
    $json = curl_exec($feed);
    curl_close($feed);
    return $twitter_data = json_decode($json);
  }

  private function _facebookCount($url){
    // Query in FQL
    $fql  = "SELECT share_count, like_count, comment_count ";
    $fql .= " FROM link_stat WHERE url = '$url'";

    $fqlURL = "https://api.facebook.com/method/fql.query?format=json&query=" . urlencode($fql);

    // Facebook Response is in JSON
    $response = file_get_contents($fqlURL);
    return json_decode($response);
  }

  public function otherAction()
  {
    \Phalcon\Tag::prependTitle('Other | ');
    $this->view->menu = $this->_menuActive('pagerole');

    $other= Tblother::find();
    $this->view->others=$other;

    $contact= Tblcontact::find();
    $this->view->contacts=$contact;
  }
  public function helpAction()
  {
    \Phalcon\Tag::prependTitle('Help | ');
    $this->view->menu = $this->_menuActive('dashboard');

  }
  public function editcontactAction($id)
  {
    \Phalcon\Tag::prependTitle('Edit Contact | ');
    $this->view->menu = $this->_menuActive('pagerole');
    $request=new Phalcon\Http\Request();

    $contact=Tblcontact::findfirst("id=$id");
    $this->view->contacts=$contact;
    $this->view->required="";
    $this->view->addError="";
    $this->view->numError="";
    $this->view->emailError="";

    /* this is for updating contact ♪♪♪ */

    if($request->isPost() && $this->request->getPost('save_contact'))
    {
      $contact->location=$request->getPost("place");
      $contact->home=$request->getPost("home");
      $contact->number=$request->getPost("number");
      $contact->email=$request->getPost("email");

      if($contact->save())
      {
        $this->flash->success("Contact was successfully updated. <a href='/admin/other'>Back to Other Pages</a>");
      }else{
        if (empty($_POST['place'])) {
          $this->view->required = '<label class="label label-danger">Place is required</label>' ;
        }elseif(empty($_POST['home'])){
          $this->view->addError = '<label class="label label-danger">Address is required</label>' ;
        }elseif(empty($_POST['number'])){
          $this->view->numError = '<label class="label label-danger">Contact number is required</label>' ;
        }elseif(empty($_POST['email'])){
          $this->view->emailError = '<label class="label label-danger">Email is required</label>' ;
        }
      }


    }
    /* end */
  }
  public function editotherAction($id)
  {
    \Phalcon\Tag::prependTitle('Edit Other Pages | ');
    $this->view->menu = $this->_menuActive('pagerole');

    $this->view->required="";
    $request=new Phalcon\Http\Request();

    $about=Tblother::findfirst("id=$id");
    $this->view->about=$about;

    /* this is for updating other ♪♪♪ */


    if($request->isPost() && $this->request->getPost('save_page'))
    {
      $about->content=$request->getPost("page_content");
      if($about->id == 3)
      {
        $about->content = str_replace('<p>', '', $about->content);
        $about->content = str_replace('</p>', '<br/>', $about->content);
      }
      if($about->save())
      {
        $this->flash->success("Page was successfully updated. <a href='/admin/other'>Back to Other Pages</a>");
      }else{
        if (empty($_POST['page_content'])) {
          $this->view->required = '<label class="label label-danger">You must enter a content</label>' ;
        }
      }
    }
    /* end */

  }

  public function indexAction(){

    \Phalcon\Tag::prependTitle('Dashboard | ');
    $this->view->menu = $this->_menuActive('dashboard');
    $form = new CreateannForm();
    $this->view->required=null;
    $this->view->requiredcont=null;
    $postcheck = Tblannouncements::findFirst('annTitle="'.$this->request->getPost('ann_title', 'striptags').'"');
    //echo "<h1> This is an example</h1>";
    $createDate =  strtotime(date("Y-m-d"));
    //$this->view->message="";

    /* this is for saving quick announcements ♪♪♪ */
    $request=new Phalcon\Http\Request();
    $validation = new Phalcon\Validation();
        $this->view->TitleErr = null;
        $this->view->ContentErr= null;
        $this->view->StartDateErr= null;
        $this->view->EndDateErr= null;
//title validation
        $validation ->add('ann_title', new PresenceOf(array(
        'message' => 'Title is required',
        )));
//ContentDash validation
        $validation ->add('ann_contentDash', new PresenceOf(array(
        'message' => 'Content is required',
        )));
//Start Date validation
        $validation ->add('ann_start_dash', new PresenceOf(array(
        'message' => 'Start date is required',
        )));
//End Date validation
        $validation ->add('ann_end_dash', new PresenceOf(array(
        'message' => 'End date is required',
        )));
//end validation
    if($request->isPost() ){

        if($request->isPost() && $this->request->getPost('ann_draft')){

                    $startDateday = strtotime($this->request->getPost('ann_start'));
                    $end=$this->request->getPost('ann_end');
                    $startDate=strtotime($this->request->getPost('ann_start_dash'));
                    $endDate=strtotime($this->request->getPost('ann_end_dash'));
                    $startDateday=strtotime("$start -1 day");

                    $ann_draft=new Tblannouncements();
                    $ann_draft->assign(
                      array(
                        "annTitle"=>$request->getPost("ann_title"),
                        "annDesc"=>$request->getPost("ann_contentDash"),
                        'annStart' => $startDate,
                        'annEnd' => $endDate,
                        "annDate"=>$createDate,
                        "annStatus"=> "deactivated"
                        ));

                 if(count($validation->validate($_POST))){
          //execute validation for title
                  foreach ($validation->getMessages()->filter('ann_title') as $message) {
                    $this->view->TitleErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
          //execute validation for ContentDash
                  foreach ($validation->getMessages()->filter('ann_contentDash') as $message) {
                    $this->view->ContentErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
          //execute validation for Start Date
                  foreach ($validation->getMessages()->filter('ann_start_dash') as $message) {
                    $this->view->StartDateErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
          //execute validation for End Date
                  foreach ($validation->getMessages()->filter('ann_end_dash') as $message) {
                    $this->view->EndDateErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
                    }else if($postcheck==true){

                    $this->flash->error('Announcement title already taken.');

                  }elseif($ann_draft->save()){
                  $form->clear();
                  $this->flash->success("Quick Announcement was successfully saved as Draft");

                }
              }
              else if($request->isPost() && $this->request->getPost('ann_drafts')){
              }
              else{

                $startDate = strtotime($this->request->getPost('ann_start_dash'));
                $endDate=strtotime($this->request->getPost('ann_end_dash'));
                //

                if($this->request->getPost('ann_start_dash') == date("Y-m-d") ){
                  $dateF= "activated";

                }else{
                  $dateF= "deactivated";

                }
                //
                // $endDate=strtotime("$end +1 day");

                $startDate = strtotime($this->request->getPost('ann_start_dash'));
                $endDateSet = new DateTime($this->request->getPost('ann_end_dash'));

                $endDateSet->setTime(23, 59, 59);
                $endDateSet = $endDateSet->format('Y-m-d H:i:s');
                $endDate = strtotime($endDateSet);

                $dateNow = strtotime(date("Y-m-d H:i:s"));
                // var_dump(time(),$startDate, $endDate);
                // $view->disable();
                $ann = new Tblannouncements();
                $ann->assign(array(
                  'annTitle' => trim($this->request->getPost('ann_title')),
                  'annDesc' => trim($this->request->getPost('ann_contentDash')),
                  'annStart' => $startDate,
                  'annEnd' => $endDate,
                  'annDate' => $dateNow,
                  "annStatus"=> $dateF
                  ));

                // $ann_publish=new Tblannouncements();
                // $ann_publish->assign(array(
                //     "annTitle"=>$request->getPost("ann_title"),
                //     "annDesc"=>$request->getPost("ann_contentDash"),
                //     'annStart' => $startDate,
                //     'annEnd' => $endDate,
                //     "annDate"=> $createDate,
                //     "annStatus"=> $dateF
                //     ));

                  if(count($validation->validate($_POST))){
                    //execute validation for title
                  foreach ($validation->getMessages()->filter('ann_title') as $message) {
                    $this->view->TitleErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
                  //execute validation for ContentDash
                  foreach ($validation->getMessages()->filter('ann_contentDash') as $message) {
                    $this->view->ContentErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
                  //execute validation for Start Date
                  foreach ($validation->getMessages()->filter('ann_start_dash') as $message) {
                    $this->view->StartDateErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
                  //execute validation for End Date
                  foreach ($validation->getMessages()->filter('ann_end_dash') as $message) {
                    $this->view->EndDateErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
                  }else if($postcheck==true){

                    $this->flash->error('Announcement title already taken.');

                  }elseif($ann->save()){
                  $form->clear();
                  // $this->flash->success("Quick Announcement was created successfully.");
                  // header( 'Refresh:2; url=/admin', true, 303);
                  $this->flashSession->success('Quick Announcement was created successfully.');
                    // Make a full HTTP redirection
                    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    return $this->response->redirect($actual_link);
                }
              }
    }
    /* this is for saving quick announcements as draft*/
       // $request=new Phalcon\Http\Request();

       // if($request->isPost() && $this->request->getPost('ann_draft')){


    // }

    $this->view->form = $form;
  }

  public function loginAction()
  {
    $auth = $this->session->get('auth');
    if ($auth){
      $this->response->redirect('admin');
    }

    $this->view->error = null;
    if ($this->request->isPost()) {
      $username = $this->request->getPost('username');
      $password = $this->request->getPost('password');
      //$hashpass = $this->security->hash($password);
      $user = Tblusers::findFirst("userName='$username'");
      if($user){
        if(sha1($password) == $user->userPassword){
          if($user->userStatus == "deactivate") {
            $this->flash->warning('Your account is deactivated, please contact the admin to reactivate your account.');
            Tag::resetInput();
          } else {
            $this->_registerSession($user);
            $this->response->redirect('admin');
          }
        } else {
          $this->flash->warning('Wrong Username/Password');
        }
      } else {
        $this->flash->warning('Wrong Username/Password');
      }
    }
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

  }
  /**
  * Create user Function
  */
  public function createuserAction(){

    \Phalcon\Tag::prependTitle('Create User | ');
    $this->view->menu = $this->_menuActive('umrole');
    $tblroles = Tblroles::find();
    $this->view->tblroles = $tblroles;
    $form = new CreateuserForm();
    $form->csrf = $this->security->getToken();
    $this->view->roleError = null;

    $numberPage = 1;
    $username = $this->request->getPost('username', 'striptags');
    $user = new Tblusers();
    $password = sha1($this->request->getPost('password'));
    $repassword = sha1($this->request->getPost('repassword'));
    $user->assign(array(
      'userName' => trim($this->request->getPost('username', 'striptags')),
      'userEmail' => trim($this->request->getPost('email', 'striptags')),
      'userPassword' => trim($password),
      'userFirstname' => trim($this->request->getPost('firstname', 'striptags')),
      'userLastname' => trim($this->request->getPost('lastname', 'striptags')),
      'userMiddlename' => trim($this->request->getPost('middlename', 'striptags')),
      'userAddress' => trim($this->request->getPost('address', 'striptags')),
      'userCompany' => trim($this->request->getPost('company', 'striptags')),
      'userContact' => trim($this->request->getPost('contact', 'striptags')),
      'userPosition' => trim($this->request->getPost('position', 'striptags')),
      'userStatus' => trim($this->request->getPost('status', 'striptags')),
      'userLevel' => 0,
      'dateCreated' => time()
      ));
     $roles = array();
     $roles[] = $donrole = $this->request->getPost('donrole');
     $roles[] = $enmrole = $this->request->getPost('enmrole');
     $roles[] = $vmrole = $this->request->getPost('vmrole');
     $roles[] = $umrole = $this->request->getPost('umrole');
     $roles[] = $annrole = $this->request->getPost('annrole');
     $roles[] = $pmrole = $this->request->getPost('pmrole');
     $roles[] = $partrole = $this->request->getPost('partrole');
     $roles[] = $townrole = $this->request->getPost('townrole');
     $roles[] = $conrole = $this->request->getPost('conrole');
     $roles[] = $pagerole = $this->request->getPost('pagerole');
     $roles[] = $extrole = $this->request->getPost('extrole');
     $roles[] = $postrole = $this->request->getPost('postrole');
     $roles[] = $rescuerole = $this->request->getPost('rescuerole');
   //  elseif($this->request->isPost() && $this->request->getPost('clear_search')){

   //    $this->session->remove("user_search_text");
   //    $this->session->remove("user_filter_date");
   //    $this->session->remove("user_filter_date_from");
   //    $this->session->remove("user_filter_date_to");
   //    $this->persistent->searchUserParams = null;
   // //   unset($_POST);

   //  }
    if($this->request->isPost() && $this->request->getPost('resetbtn')){
        $form->clear();
     //   echo "<script>window.location.href='/admin/createuser'</script>";
   }else if ($this->request->isPost() ) {

        $validation = new Phalcon\Validation();
        $this->view->usernameErr = null;
        $this->view->emailErr= null;
        $this->view->fnameErr= null;
        $this->view->lnameErr= null;
        $this->view->contactErr= null;
        $this->view->passErr= null;
        $this->view->repassErr= null;
//username validation
        $validation ->add('username', new PresenceOf(array(
        'message' => 'Username is required.',
        )));

        $validation ->add('username', new StringLength(array(
        'min' => 6,
        'messageMinimum' => 'User name should have at least 6 minimum characters'
        )));

        $validation ->add('username', new Regex(array(
        'message'    => 'User name is invalid. Avoid spaces and symbols.',
        'pattern'    => '/^[a-zA-Z0-9._]+/',
        'allowEmpty' => false
        )));
//email validation
        $validation ->add('email', new PresenceOf(array(
        'message' => 'Email Address is required',
        )));

        $validation ->add('email', new Regex(array(
        'message'    => 'Not a valid email address format',
        'pattern'    => '/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i',
        'allowEmpty' => false
        )));
//password validation
        $validation ->add('password', new PresenceOf(array(
        'message' => 'Password is required',
        )));

        $validation ->add('password', new StringLength(array(
        'min' => 8,
        'messageMinimum' => 'Password should have at least 8 minimum characters'
        )));
//firstname validation
        $validation ->add('firstname', new PresenceOf(array(
        'message' => 'First Name is required',
        )));
//lastname validation
        $validation ->add('lastname', new PresenceOf(array(
        'message' => 'Last Name is required',
        )));
//coontact validation
        $validation ->add('contact', new PresenceOf(array(
        'message' => 'Contact number is required',
        )));
        $validation ->add('contact', new StringLength(array(
        'max' => 11,
        'messageMinimum' => 'Contact should only accept 11 digits'
        )));

     if ($form->isValid($this->request->getPost()) != true) {
      if(count($validation->validate($_POST)) && empty($donrole) && empty($enmrole) && empty($vmrole) && empty($umrole) && empty($annrole) && empty($pmrole) && empty($partrole) && empty($townrole) && empty($conrole) && empty($pagerole) && empty($extrole) && empty($postrole) && empty($rescuerole) ){
       // $form->get('username')->clear();
//execute validation for username
        foreach ($validation->getMessages()->filter('username') as $message) {
          $this->view->usernameErr .= "<div class='label label-danger' >".$message."</div> ";
        }

//execute validation for email
        foreach ($validation->getMessages()->filter('email') as $message) {
           $this->view->emailErr .= "<div class='label label-danger' id='emailhideerror'>".$message."</div> ";
        }
//execute validation for password
        foreach ($validation->getMessages()->filter('password') as $message) {
           $this->view->passErr .= "<div class='label label-danger'>".$message."</div> ";
        }
        if($password != $repassword){
           $this->view->repassErr.="<div class='label label-danger'>Password doesn't match confirmation.</div> ";
        }
//execute validation for firstname and lastname
        foreach ($validation->getMessages()->filter('firstname') as $message) {
          $this->view->fnameErr .= "<div class='label label-danger'>".$message."</div> ";
        }

        foreach ($validation->getMessages()->filter('lastname') as $message) {
          $this->view->lnameErr .= "<div class='label label-danger'>".$message."</div> ";
        }
//execute validation for contact
        foreach ($validation->getMessages()->filter('contact') as $message) {
          $this->view->contactErr .= "<div class='label label-danger'>".$message."</div> ";
        }
        $this->view->roleError = "<div class='label label-danger'>Select atleast one (1) role</div>";

       // $form->clear();

        }else if(empty($donrole) && empty($enmrole) && empty($vmrole) && empty($umrole) && empty($annrole) && empty($pmrole) && empty($partrole) && empty($townrole) && empty($conrole) && empty($pagerole) && empty($extrole) && empty($postrole) && empty($rescuerole)){

          $this->view->roleError = "<div class='label label-danger'>Select atleast one (1) role</div>";

      }else if($password != $repassword){
          $this->view->repassErr.= "<div class='label label-danger'>Password doesn't match confirmation.</div> ";

      }else if(count($validation->validate($_POST))){
            //execute validation for username
                  foreach ($validation->getMessages()->filter('username') as $message) {
                    $this->view->usernameErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
            //execute validation for email
                  foreach ($validation->getMessages()->filter('email') as $message) {
                    $this->view->emailErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
            //execute validation for password
                    foreach ($validation->getMessages()->filter('password') as $message) {
                       $this->view->passErr .= "<div class='label label-danger'>".$message."</div> ";
                    }
            //execute repassword
                    if($password != $repassword){
                      $this->view->repassErr.="<div class='label label-danger'>Password doesn't match confirmation.</div> ";
                    }
            //execute validation for firstname and lastname
                  foreach ($validation->getMessages()->filter('firstname') as $message) {
                    $this->view->fnameErr .= "<div class='label label-danger'>".$message."</div> ";
                  }

                  foreach ($validation->getMessages()->filter('lastname') as $message) {
                    $this->view->lnameErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
            //execute validation for contact
                  foreach ($validation->getMessages()->filter('contact') as $message) {
                    $this->view->contactErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
      }else{
//         if(count($validation->validate($_POST))){
// //execute validation for username
//         foreach ($validation->getMessages()->filter('username') as $message) {
//           $this->view->usernameErr .= "<div class='label label-danger'>".$message."</div> ";
//         }
// //execute validation for email
//         foreach ($validation->getMessages()->filter('email') as $message) {
//           $this->view->emailErr .= "<div class='label label-danger'>".$message."</div> ";
//         }
// //execute validation for firstname and lastname
//         foreach ($validation->getMessages()->filter('firstname') as $message) {
//           $this->view->fnameErr .= "<div class='label label-danger'>".$message."</div> ";
//         }

//         foreach ($validation->getMessages()->filter('lastname') as $message) {
//           $this->view->lnameErr .= "<div class='label label-danger'>".$message."</div> ";
//         }
// //execute validation for contact
//         foreach ($validation->getMessages()->filter('contact') as $message) {
//           $this->view->contactErr .= "<div class='label label-danger'>".$message."</div> ";
//         }

//         }else {

        $userName = Tblusers::findFirst("userName='".$this->request->getPost('username', 'striptags')."'");
        $userEmail = Tblusers::findFirst("userEmail='".$this->request->getPost('email', 'striptags')."'");

        if ($userEmail==true){

          $this->view->emailErr  .= "<div class='label label-danger'>Username and Email Address are already taken</div>";

        }

        if($userName==true){

          $this->view->usernameErr .= "<div class='label label-danger'>Username and Email Address are already taken</div>";

        } else {

          if (!$user->save()) {

            $this->flash->error($user->getMessages());

          } else {
             Tag::resetInput();
            $this->flash->success("New user successfully created");
            header( 'Refresh:2; url=/admin/users', true, 303);
            $this->_insertRoles($user->userID, $roles);
            if(in_array('partrole', $roles)){

              $username = $user->userName;
              $password = $this->request->getPost('password');
              $abk_user = $this->session->get('auth');
              $fullname = $abk_user['abk_fullname'];

              $body = "
              <h4>An ABK Partner Account has been created</h4>
              <p>Congratulations, a new ABK Partner Account has been created for you by your ABK Foundation administrator, $fullname.</p>
              <p>Your username is : <strong>$username</strong></p>
              <p>Your password is : <strong>$password</strong></p>
              <p>You can login your account at <a href='http://angbayanko.org/admin' target='_blank'>http://angbayanko.org/admin</a></p>
              <p>Sincerely,</p>
              <p>ABK Foundation</p>
              ";
              $subject = "ABK Foundation: Registration Successful";
              // $mailObjects = array(
              //   'From'=> 'angbayanko.org@no-reply.com',
              //   'FromName' => 'angbayanko.org',
              //   'AddAddress'=> $user->userEmail,
              //   'Subject' => ,
              //   'Body' => $body
              //   );
/////////////////////////////////////////////////
              if($this->sendmailer($user->userEmail, $subject, $body)) {
                $this->flash->success("An ABK Partner Account has been successfully created. Account details were sent on the registered email address.");
                Tag::resetInput();
              }
///////////////////////////////////////////////////
            }
            Tag::resetInput();
            $this->view->roleError = null;
              // Tag::resetInput();
          }
        }
     //--
    //  }
     //--
}
    }else{
      $this->view->roleError = "<div class='label label-danger'>Select atleast one (1) role</div>";
        // Tag::resetInput();
    }
  }
  $this->view->form = $form;
}
  /*
  * AJAX Action User View
  */
  public function ajaxUserViewAction($userID,$txt,$annDate){
    if ($this->request->isAjax()){
      if($txt != null && $txt == 'view' && $annDate == null){
        $vol = Tbltownevents::findFirst($userID);

        echo "<label>Title: </label> ".$vol->eventTitle."<br>";
        echo "<label>Date: </label> ".$vol->eventDate."<br>";
        echo "<label>Venue: </label> ".$vol->eventVenue."<br>";
        echo "<label>Details: </label> ".$vol->eventDetails."<br>";
        echo "";
      }

      elseif($txt != null && $txt != 'view' && $annDate == null){//pag may laman yung $txt
        $vol = Tblvolunteers::findFirst($userID);

        echo "<label>Name: </label> ".$vol->fname." ".$vol->mname." ".$vol->fname." ".$vol->extname."<br>";
        echo "<label>Address: </label> ".$vol->address."<br>";
        echo "<label>Email Address: </label> ".$vol->email."<br>";
        echo "<label>Contact: </label> ".$vol->phone."<br>";
        echo "<label>Date Created: </label> ".date('F j, Y',$vol->dateAdded)."<br>";
        echo "";
        //add the activities
        // $volunteer = Tblvolunteers::findFirst('volunteerID='.$userID);
        $phql = 'SELECT  Tblprograms.programName, Tblporgramactivities.activity
        FROM Tblvolunteeractivities
        LEFT JOIN Tblporgramactivities ON Tblporgramactivities.activityID = Tblvolunteeractivities.activityID
        LEFT JOIN Tblprograms ON Tblprograms.programID = Tblporgramactivities.programID
        WHERE Tblvolunteeractivities.volunteerID = '.$userID.' AND Tblvolunteeractivities.activityID != 0
        ORDER BY Tblprograms.programName
        ';
        $volunteerActivities =  $this->modelsManager->executeQuery($phql);
        if(count($volunteerActivities) > 0){
          $progName=$volunteerActivities[0]->programName;
          echo "<hr /><div>";
          echo "<h3>Signed up activities</h3>";
          foreach ($volunteerActivities as $key => $value) {
            if($proName != $value->programName){
            $progName = $value->programName;
            echo '</ul><strong>'.$progName.'</strong><ul>';
            }
            echo "<li>".$value->activity."</li>";
          }
          echo "</div>";
        }
        //add activities end
        //add suggested activities

        $sugg = Tblvolunteeractivities::find('volunteerID = '.$userID.' AND activityID = 0'
        );

        echo "<hr/><div><h3>Suggested activities</h3>";
        foreach ($sugg as $key => $value) {
          echo "<li>".$value->custom."</li>";
        }
        echo "</div>";

        unset($vol);
        unset($volunteerActivities);
        unset($sugg);

        //add suggested activities end
      }elseif($annDate != null){
        $ann = Tblannouncements::findFirst($userID);
        echo "<span class='pull-right muted'>". date("F j, Y", $ann->annDate)."</span>";
        echo "<label>Event Title </label> <br>".$ann->annTitle."<br>";
        echo "<label>Content </label> <br>".$ann->annDesc."<br>";
        echo "<label>Date Duration </label><br> ";
        echo "".date('F j,Y',$ann->annStart)."";
        echo "<span> To </span> ".date('F j,Y',$ann->annEnd)."<br>";


      }else{//if walang laman yunh $txt
        //
        $user = Tblusers::findFirst($userID);
        echo "<label>Username: </label> ".$user->userName."<br>";
        echo "<label>Email Address: </label> ".$user->userEmail."<br>";
        echo "<label>Name: </label> ".$user->userFirstname." ".$user->userMiddlename." ".$user->userLastname."<br>";
        echo "<label>Address: </label> ".$user->userAddress."<br>";
        echo "<label>Company: </label> ".$user->userCompany."<br>";
        echo "<label>Position: </label> ".$user->userPosition."<br>";
        echo "<label>Contact: </label> ".$user->userContact."<br>";
        echo "<label>Status: </label> ".$user->userStatus."<br>";
        echo "<label>Date Created: </label> ".date('F j, Y',$user->dateCreated)."<br>";

        $phql = 'SELECT Tblroles.roleDescription FROM Tbluserroles ' .
        ' INNER JOIN Tblroles ON Tblroles.roleCode = Tbluserroles.userRoles WHERE Tbluserroles.userID = '.$userID;
        $result = $this->modelsManager->executeQuery($phql);
        $rr = null;
        foreach($result as $r){
          $rr .= $r->roleDescription . ', ';
        }
        echo "<label>Roles:</label> <br/>";
        if(empty($rr)){
          echo 'None';
        }else
        {
          echo substr($rr, 0, -2);
        }
        //
      }
    }else{
      echo "This Page is not for you.";
    }
    $this->view->disableLevel(View::LEVEL_LAYOUT);
  }
  /*
  User Table
  */
  public function usersAction(){

    \Phalcon\Tag::prependTitle('Users | ');
    $this->view->menu = $this->_menuActive('umrole');
    $numberPage = 1;
    $this->session->remove("user_search_text");
    if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
      // echo 'invalid token';
      Tag::resetInput();

    }elseif ($this->request->isPost() && $this->request->getPost('action') == "activate") {

      $id = $this->request->getPost('recordID');

      if(!empty($id)){

        $user = Tblusers::findFirst('userID='+$id);
        $user->userStatus = "active";

        if($user->save()){
          $this->flash->success("User Account has been activated");

        } else {

          $this->flash->error("Something went wrong. Please try again later.");

        }
      }
    }elseif ($this->request->isPost() && $this->request->getPost('action') == "deactivate") {

      $id = $this->request->getPost('recordID');

      if(!empty($id)){

        $user = Tblusers::findFirst('userID='+$id);
        $user->userStatus = "deactivate";

        if($user->save()){

          $this->flash->success("User Account has been deactivated");

        } else {

          $this->flash->error("Something went wrong. Please try again later.");
        }
      }

    }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {

      $id = $this->request->getPost('recordID');

      if(!empty($id)){

        $this->_deleteUser($id);
      }
      /* stay on search pages*/
      if ($this->request->isPost()) {

        $keyword = $this->request->getPost('search_text');
        $this->session->set("user_search_text", $keyword);

      } else {
        // $numberPage = $this->request->getQuery("page", "int");
        $searchtext = $this->session->get("user_search_text");
        $keyword = !empty($searchtext)?$searchtext:'';
      }
      /**/
    }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){

      $id = $this->request->getPost('tbl_id');

      if(!empty($id)){

        $this->_deleteUser($id);
      }
      /* stay on search pages*/
      if ($this->request->isPost()) {
        $keyword = $this->request->getPost('search_text');
        $this->session->set("user_search_text", $keyword);
      } else {
        $searchtext = $this->session->get("user_search_text");
        $keyword = !empty($searchtext)?$searchtext:'';
      }
      /**/
    }elseif($this->request->isPost() && $this->request->getPost('action') == "edit"){

      $id = $this->request->getPost('recordID');

      if(!empty($id)){

        return $this->dispatcher->forward(array(
          'controller' => 'admin',
          'action' => 'edituser',
          'params' => array($id)
          ));
      }
    }elseif($this->request->isPost() && $this->request->getPost('clear_search')){

      $this->session->remove("user_search_text");
      $this->session->remove("user_filter_date");
      $this->session->remove("user_filter_date_from");
      $this->session->remove("user_filter_date_to");
      $this->persistent->searchUserParams = null;
   //   unset($_POST);

    }else{

      if ($this->request->isPost()) {

        $keyword = $this->request->getPost('search_text');
        $this->session->set("user_search_text", $keyword);

      } else {
        $searchtext = $this->session->get("user_search_text");
        $keyword = !empty($searchtext)?$searchtext:'';
      }
    }
    // $parameters = array();
    // if ($this->persistent->searchUserParams) {
    //     $parameters = $this->persistent->searchUserParams;
    // }
    // $builder = $this->modelsManager->createBuilder()
    // ->columns('userID, userName, userEmail, userLastname, userFirstname, userMiddlename, userAddress, userPosition, userContact,userStatus,dateCreated')
    // ->from('Tblusers')
    // ->where('userLevel = 0');

    // if(!empty($parameters)){
    //     $this->flash->notice('Search results for "<strong>' . $this->session->get("user_search_text") .'</strong>"');
    //     $builder->andWhere($parameters['conditions'], $parameters['bind']);
    // }

    // $paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
    //     "builder" => $builder,
    //     "limit"=> 10,
    //     "page" => $numberPage
    //     ));
    if(!empty($keyword)){

      $keyword = trim(preg_replace('/\s+/',' ', $keyword));
      $where = ' AND (userName LIKE "%'.$keyword.'%"
      OR CONCAT(userLastname, ", ", userFirstname, " ", userMiddlename) LIKE "%'.$keyword.'%" OR userMiddlename LIKE "%'.$keyword.'%" OR userLastname LIKE "%'.$keyword.'%" OR userFirstname LIKE "%'.$keyword.'%"  OR userEmail LIKE "%'.$keyword.'%")';
      $this->flash->notice('Search results for "<strong>' . $keyword .'</strong>"');

    }else{

       $where = null;
      $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):"";
      $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):"";

      if(($fromDate && $toDate) || $this->session->has("user_filter_date") ) {

        if($fromDate > $toDate) {

          return $this->flash->warning("Invalid request. End date must be later than the start date.");

        }

        if($this->session->has("user_filter_date")) {

          if(!$fromDate && !$toDate){

            $fromDate = trim($this->session->get('user_filter_date_from'));
            $toDate = trim($this->session->get('user_filter_date_to'));

          }

        } else {

          $this->session->set("user_filter_date", true);
          $this->session->set("user_filter_date_from", $fromDate);
          $this->session->set("user_filter_date_to", $toDate);

        }

        $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
        $where = " AND (dateCreated BETWEEN $fromDate AND $toDate)";

      }
    }

    $numberPage = $this->request->getQuery("page", "int");
    $numberPage = empty($numberPage)?1:$numberPage;

    // SORTING
    // Added a server side sorting on all tables using the columns shown
    // Dont forget to insert na href params on the view
    $sort = $this->request->getQuery("sort");
    $order = "";
     // "order" => "name DESC, status"
    $this->view->usernameHref = "userName-asc";
    $this->view->nameHref = "name-asc";
    $this->view->emailHref = "userEmail-asc";
    $this->view->datecreatedHref = "dateCreated-asc";
    $this->view->positionHref = "userPosition-asc";
    $this->view->statusHref = "userStatus-asc";

    $arr = explode("-", $sort);

    switch ($arr[0]) {

      case 'userName':
      $arr[1] == "asc" ? $this->view->usernameHref = $arr[0] . "-desc" : "";
      $order = " ORDER BY $arr[0] $arr[1]";
      $this->view->usernameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;

      case 'name':
      $arr[1] == "asc" ? $this->view->nameHref = $arr[0] . "-desc" : "";
      $order = " ORDER BY $arr[0] $arr[1]";
      $this->view->nameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;

      case 'userEmail':
      $arr[1] == "asc" ? $this->view->emailHref = $arr[0] . "-desc" : "";
      $order = " ORDER BY $arr[0] $arr[1]";
      $this->view->emailIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;

      case 'dateCreated':
      $arr[1] == "asc" ? $this->view->datecreatedHref = $arr[0] . "-desc" : "";
      $order = " ORDER BY $arr[0] $arr[1]";
      $this->view->datecreatedIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;

      case 'userPosition':
      $arr[1] == "asc" ? $this->view->positionHref = $arr[0] . "-desc" : "";
      $order = " ORDER BY $arr[0] $arr[1]";
      $this->view->positionIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;

      case 'userStatus':
      $arr[1] == "asc" ? $this->view->statusHref = $arr[0] . "-desc" : "";
      $order = " ORDER BY $arr[0] $arr[1]";
      $this->view->statusIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;

      default:
      $order = " ORDER BY dateCreated DESC";
      break;
    }

    $phql = 'SELECT
    Tblusers.userID,
    Tblusers.userName,
    Tblusers.userEmail,
    CONCAT(Tblusers.userLastname, ", ",Tblusers.userFirstname, " ", Tblusers.userMiddlename) AS name,
    Tblusers.dateCreated,
    Tblusers.userPosition,
    Tblusers.userStatus
    FROM Tblusers
    WHERE userLevel = 0'.$where.$order;

    $result = $this->modelsManager->executeQuery($phql);
    $dataArray = array();

    foreach ($result as $key => $value) {

      $dataArray[] = array(
        'userID'=>$value->userID,
        'username'=>$value->userName,
        'email'=>$value->userEmail,
        'name'=>$value->name,
        'dateCreated'=>$value->dateCreated,
        'position'=>$value->userPosition,
        'status'=>$value->userStatus
        );

    }

    $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
      "data" => $dataArray,
      "limit"=> 10,
      "page" => $numberPage

      ));

    // Get the paginated results
    $this->view->page = $paginator->getPaginate();
  }
  /**
  * Edit User Action
  */
  public function edituserpassAction($userID,$pass){
    $this->view->setRenderLevel(
            View::LEVEL_NO_RENDER
        );
    // $x = Tblusers::findFirst([
    //   'conditions' => 'userPassword='. $pass . ' AND userID='.$userID
    // ]);
    // if(!$x){
    //   echo "mali";
    //   //"<script type='text/javascript'>alert('ERROR');</script>";
    // }
    // return "asdasd" . $userID . $pass;
    echo json_encode(sha1($pass));
  }

  public function sampleAction($subject, $message){
    $this->view->setRenderLevel(
            View::LEVEL_NO_RENDER
        );

    $phql = 'SELECT email FROM Tblnewsletteremails';
        $suggested = $this->modelsManager->executeQuery($phql);
        if(count($suggested) > 0){
            $dataArray = array();
            foreach ($suggested as $key => $value) {
                $dataArray[] = "arielbermudez0901@gmail.com";
            }

           echo json_encode($this->Mailer->sendMail($dataArray,$subject,$message));
        }else{
            echo "<script>console.log(".$subject.")</script>";
            return true;
        }




    //echo json_encode($this->Mailer->sendMail("medisource@mailinator.com", "TEST POSTMARK", "IM A BODY"));
  }


  public function edituserAction($userID){
    \Phalcon\Tag::prependTitle('Edit User | ');
    $validation = new Phalcon\Validation();
    $this->view->menu = $this->_menuActive('umrole');
    $this->view->roleError = null;
    $user = Tblusers::findFirst($userID);
    $role = Tbluserroles::find('userID ='.$userID);
    $tblroles = Tblroles::find();
    $this->view->tblroles = $tblroles;
    $this->view->user = $user;
    $this->view->usernameErr= null;
    $this->view->emailErr= null;
    $this->view->fnameErr= null;
    $this->view->lnameErr= null;
    $this->view->contactErr= null;
    $this->view->passError = null;
    $this->view->oldpassError = null;
    $this->view->repassError = null;
    $form = new CreateuserForm($user, array(
      'edit' => true
      ));
    $form->csrf = $this->security->getToken();

    if ($this->request->isPost()) {//
      if($this->request->getPost('update_password')){

        $validation->add('password', new PresenceOf(array(
          'message' => 'The password is required',
          'cancelOnFail' => true
          )));
        $validation->add('password', new StringLength(array(
          'max' => 20,
          'min' => 8,
          'messageMaximum' => 'Thats an exagerated password.',
          'messageMinimum' => 'Password should be Minimum of 8 characters.'
          )));

        $validation->add('repassword', new PresenceOf(array(
          'message' => 'Retyping your password is required'
          )));
        $validation->add('repassword', new Confirmation(array(
          'message' => 'Password doesn\'t match confirmation',
          'with' => 'password'
          )));

        if(count($validation->validate($_POST))){
          // foreach ($validation->getMessages()->filter('oldpassword') as $message) {
          //     $this->view->oldpassError .= "<div class='label label-danger'>".$message."</div> ";
          // }
          foreach ($validation->getMessages()->filter('password') as $message) {
            $this->view->passError .= "<div class='label label-danger'>".$message."</div> ";
          }
          foreach ($validation->getMessages()->filter('repassword') as $message) {
            $this->view->repassError .= "<div class='label label-danger'>".$message."</div> ";
          }
        }else{
          // if(sha1($this->request->getPost('oldpassword')) == $user->userPassword) {

          // } else {
          //     $this->view->oldpassError = "<div class='label label-danger'>Wrong Password</div> ";
          //     Tag::resetInput();
          // }
          $user->assign(array(
            'userPassword' => sha1($this->request->getPost('password'))
            ));

          if (!$user->save()) {
            $this->flash->error($user->getMessages());
          } else {

            $this->flash->success("Password successfully changed.");
            echo "<script type='text/javascript'>$('#oldpass').val('');
            $('#password').val('');
            $('#repassword').val('');
            </script>";
            Tag::resetInput();
          }

        }

      }else if($this->request->getPost('update_button')){
       //
        $roles = array();
        $roles[] = $donrole = $this->request->getPost('donrole');
        $roles[] = $enmrole = $this->request->getPost('enmrole');
        $roles[] = $vmrole = $this->request->getPost('vmrole');
        $roles[] = $umrole = $this->request->getPost('umrole');
        $roles[] = $annrole = $this->request->getPost('annrole');
        $roles[] = $pmrole = $this->request->getPost('pmrole');
        $roles[] = $partrole = $this->request->getPost('partrole');
        $roles[] = $townrole = $this->request->getPost('townrole');
        $roles[] = $conrole = $this->request->getPost('conrole');
        $roles[] = $pagerole = $this->request->getPost('pagerole');
        $roles[] = $extrole = $this->request->getPost('extrole');
        $roles[] = $postrole = $this->request->getPost('postrole');
        $roles[] = $rescuerole = $this->request->getPost('rescuerole');

//validation
//username validation
        $validation ->add('username', new PresenceOf(array(
        'message' => 'Username is required.',
        )));

        $validation ->add('username', new StringLength(array(
        'min' => 6,
        'messageMinimum' => 'Username should have at least 6 minimum characters'
        )));

        $validation ->add('username', new Regex(array(
        'message'    => 'Username is invalid. Avoid spaces and symbols.',
        'pattern'    => '/^[a-zA-Z0-9._]+/',
        'allowEmpty' => false
        )));
//email validation
        $validation ->add('email', new PresenceOf(array(
        'message' => 'Email Address is required',
        )));

        $validation ->add('email', new Regex(array(
        'message'    => 'Not a valid email address format',
        'pattern'    => '/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i',
        'allowEmpty' => false
        )));
//firstname validation
        $validation ->add('firstname', new PresenceOf(array(
        'message' => 'Firstname is required',
        )));
//lastname validation
        $validation ->add('lastname', new PresenceOf(array(
        'message' => 'Lastname is required',
        )));
//coontact validation
        $validation ->add('contact', new PresenceOf(array(
        'message' => 'Contact number is required',
        )));
        $validation ->add('contact', new StringLength(array(
        'max' => 11,
        'messageMinimum' => 'Contact should only accept 11 digits'
        )));
//
       if ($form->isValid($_POST)) {
         $form->getMessages();
          //    $this->view->roleError = "<div class='label label-danger'>Select atleast one (1) role.</div>";
       }else{
        if(count($validation->validate($_POST)) && empty($donrole) && empty($enmrole) && empty($vmrole) && empty($umrole) && empty($annrole) && empty($pmrole) && empty($partrole) && empty($townrole) && empty($conrole) && empty($pagerole) && empty($extrole) && empty($postrole) && empty($rescuerole) ){
       // $form->get('username')->clear();
//execute validation for username
        foreach ($validation->getMessages()->filter('username') as $message) {
          $this->view->usernameErr .= "<div class='label label-danger' >".$message."</div> ";
        }

//execute validation for email
        foreach ($validation->getMessages()->filter('email') as $message) {
           $this->view->emailErr .= "<div class='label label-danger' id='emailhideerror'>".$message."</div> ";
        }
//execute validation for firstname and lastname
        foreach ($validation->getMessages()->filter('firstname') as $message) {
          $this->view->fnameErr .= "<div class='label label-danger'>".$message."</div> ";
        }

        foreach ($validation->getMessages()->filter('lastname') as $message) {
          $this->view->lnameErr .= "<div class='label label-danger'>".$message."</div> ";
        }
//execute validation for contact
        foreach ($validation->getMessages()->filter('contact') as $message) {
          $this->view->contactErr .= "<div class='label label-danger'>".$message."</div> ";
        }
        $this->view->roleError = "<div class='label label-danger'>Select atleast one (1) role</div>";
          // $form->get('username')->clear();
          $role = null;
          $tblroles = Tblroles::find();
       // $form->clear();

        }elseif(count($validation->validate($_POST))){
//execute validation for username
        foreach ($validation->getMessages()->filter('username') as $message) {
          $this->view->usernameErr .= "<div class='label label-danger' >".$message."</div> ";
        }

//execute validation for email
        foreach ($validation->getMessages()->filter('email') as $message) {
           $this->view->emailErr .= "<div class='label label-danger' id='emailhideerror'>".$message."</div> ";
        }
//execute validation for firstname and lastname
        foreach ($validation->getMessages()->filter('firstname') as $message) {
          $this->view->fnameErr .= "<div class='label label-danger'>".$message."</div> ";
        }

        foreach ($validation->getMessages()->filter('lastname') as $message) {
          $this->view->lnameErr .= "<div class='label label-danger'>".$message."</div> ";
        }
//execute validation for contact
        foreach ($validation->getMessages()->filter('contact') as $message) {
          $this->view->contactErr .= "<div class='label label-danger'>".$message."</div> ";
        }
        }else if(empty($donrole) && empty($enmrole) && empty($vmrole) && empty($umrole) && empty($annrole) && empty($pmrole) && empty($partrole) && empty($townrole) && empty($conrole) && empty($pagerole) && empty($extrole) && empty($postrole) && empty($rescuerole)){
            $this->view->roleError = "<div class='label label-danger'>Select atleast one (1) role.</div>";
          $role = null;
          $tblroles = Tblroles::find();
        }else{



        $userName = Tblusers::findFirst("userName='".$this->request->getPost('username', 'striptags')."' AND userID != ".$userID);
        $userEmail = Tblusers::findFirst("userEmail='".$this->request->getPost('email', 'striptags')."' AND userID != ".$userID);

        if($userName==true){
          echo "username error";
          $this->flash->error("Username already taken");

        }elseif($userEmail==true){
          echo "password error";
          $this->flash->error("Email Address already taken");

        }else{

          $user->assign(array(
            'userName' => trim($this->request->getPost('username', 'striptags')),
            'userEmail' => trim($this->request->getPost('email', 'striptags')),
            'userFirstname' => trim($this->request->getPost('firstname', 'striptags')),
            'userLastname' => trim($this->request->getPost('lastname', 'striptags')),
            'userMiddlename' => trim($this->request->getPost('middlename', 'striptags')),
            'userAddress' => trim($this->request->getPost('address', 'striptags')),
            'userCompany' => trim($this->request->getPost('company', 'striptags')),
            'userContact' => trim($this->request->getPost('contact', 'striptags')),
            'userPosition' => trim($this->request->getPost('position', 'striptags')),
            'userStatus' => trim($this->request->getPost('status', 'striptags')),
            'dateCreated' => time()
            ));
          if (!$user->save()) {
            $this->flash->error($user->getMessages());
          } else {
            $this->_insertRoles($user->userID, $roles);
            $role = Tbluserroles::find('userID ='.$userID);
            $this->flash->success("User was updated successfully");
            header( 'Refresh:1; url=/admin/users', true, 303);
            $this->view->roleError = null;
          }
        }
       }//end if-else saving
      }
    }
   }
    foreach($role as $r){
    $this->tag->setDefault($r->userRoles, $r->userRoles);
  }
  $this->tag->setDefault("status", $user->userStatus);
  $this->view->form = $form;
  }
  /**
  * Finishes the active session redirecting to the index
  *
  * @return unknown
  */
  public function logoutAction()
  {
    $this->session->remove('auth');
    $this->flash->success('Goodbye!');
    $this->dispatcher->forward(
      array(
        'controller' => 'admin',
        'action' => 'index'
        )
      );
    header( 'Refresh:3; url=/admin/login');

  }
/*
For PROGRAM MANAGEMENT
*/
public function programsAction($id=null){

  \Phalcon\Tag::prependTitle('Programs | ');
  $this->view->menu = $this->_menuActive('pmrole');
  $programs = Tblprograms::find();
  $form = new ProgramsForm();
  $form->csrf = $this->security->getToken();
  $this->view->titletaken = null;
  $this->view->edit = false;
  $this->view->pictureerror = null;

  $validation = new Phalcon\Validation();
  $this->view->titleErr = null;
  $this->view->tooltipErr= null;
  $this->view->taglineErr= null;
  $this->view->program_pictureErr = null;
  $this->view->program_urlErr = null;


  $validation ->add('title', new PresenceOf(array(
        'message' => 'Title is required',
        )));

  $validation ->add('tooltip', new PresenceOf(array(
        'message' => 'Tooltip is required',
        )));

  $validation ->add('tagline', new PresenceOf(array(
        'message' => 'Tagline is required',
        )));

  $validation ->add('program_picture', new PresenceOf(array(
        'message' => 'Please add pictures',
        )));
  $validation ->add('programurl', new PresenceOf(array(
        'message' => 'Program URL is required',
        )));

  if(!empty($id)){

    $this->view->edit = true;
    $eprogs = Tblprograms::findFirst("programID=".$id);
    $this->view->eprogs = $eprogs;
    $eprogsi = Tblprogramsimg::find("programID=".$id);
    $this->view->eprogsi = $eprogsi;
  }

  if ($this->request->isPost() && $this->request->getPost('action') == "delete") {

    $id = $this->request->getPost('recordID');

    if(!empty($id)){

      $this->_deleteProgram($id);
      $programs = Tblprograms::find();
      $this->view->programMenu = $programs;

    }

  }

  elseif($this->request->isPost() && $this->request->getPost('update-program')){

      $pictures = $this->request->getPost('program_picture');
      $programname = Tblprograms::findFirst("programName='".$this->request->getPost('title', 'striptags')."' AND programID != ".$id);

      if(count($validation->validate($_POST))){

        //execute validation for title
        foreach ($validation->getMessages()->filter('title') as $message) {
          $this->view->titleErr .= "<div class='label label-danger' >".$message."</div> ";
        }
        //execute validation for tooltip
        foreach ($validation->getMessages()->filter('tooltip') as $message) {
          $this->view->tooltipErr .= "<div class='label label-danger' >".$message."</div> ";
        }
        //execute validation for tagline
        foreach ($validation->getMessages()->filter('tagline') as $message) {
          $this->view->taglineErr .= "<div class='label label-danger' >".$message."</div> ";
        }
        //execute validation for program_picture
        foreach ($validation->getMessages()->filter('program_picture') as $message) {
          $this->view->program_pictureErr .= "<div class='label label-danger' >".$message."</div> ";
        }

        //execute validation for program_url
        foreach ($validation->getMessages()->filter('programurl') as $message) {
          $this->view->program_urlErr .= "<div class='label label-danger' >".$message."</div> ";
        }

        }else{

        $eprogs->assign(array(

          'programName' => $this->request->getPost('title', 'striptags'),
          'programTooltip' => $this->request->getPost('tooltip', 'striptags'),
          'programTagline' => $this->request->getPost('tagline', 'striptags'),
          'programPage' => $this->request->getPost('programurl', 'striptags'),
          'programLastUpdated' => time()
          ));

      if($programname){

        $this->view->titletaken.="<div class='label label-danger' >Program Name already taken</div>" ;

      }else{

        if (!$eprogs->save()) {

          $this->flash->error($user->getMessages());

        } else {
          //Clean other files not included
          $tpi = Tblprogramsimg::find('programID='.$id);
          foreach ($tpi as $t) {
            $ch = false;
            foreach($pictures as $pic){
              if($t->imgname == $pic){
                $ch=true;


              }
            }

            if(!$ch){
              if(is_file('../public/img/programs/'.$t->imgname)){
                unlink('../public/img/programs/'.$t->imgname);
              }

              if(is_file('../public/img/programs/thumbnail/'.$t->imgname)){
                unlink('../public/img/programs/thumbnail/'.$t->imgname);
              }
            }
          }

          $this->modelsManager->executeQuery("DELETE FROM Tblprogramsimg WHERE Tblprogramsimg.programID = ".$id);

          $phql = "INSERT INTO Tblprogramsimg ( imgname, imgpath, programID) "
          . "VALUES (:imgname:, :imgpath:, :programID:)";
          foreach($pictures as $pic){
            if(!is_file('../public/img/programs/'.$pic)){
              $newpicname = md5(uniqid(rand(),true)).$pic;
              $res = $this->modelsManager->executeQuery($phql,
                array(
                  'imgname' => $newpicname,
                  'imgpath' => 'public/programs/',
                  'programID' => $id,
                  )
                );
              rename('../public/server/php/files/'.$pic, '../public/img/programs/'.$newpicname);
              rename('../public/server/php/files/thumbnail/'.$pic, '../public/img/programs/thumbnail/'.$newpicname);
            }else{
              $res = $this->modelsManager->executeQuery($phql,
                array(
                  'imgname' => $pic,
                  'imgpath' => 'public/programs/',
                  'programID' => $id,
                  )
                );
            }
          }
          $form->clear();
          // $this->flash->success("Program has been successfully updated ");
          $this->view->eprogsi = $eprogsi;
          header( 'Refresh:1; url=/admin/programs/0');
          $programs = Tblprograms::find();
          $this->view->programMenu = $programs;
          $eprogs = Tblprograms::findFirst("programID=".$id);
          $this->view->eprogs = $eprogs;
          $eprogsi = Tblprogramsimg::find("programID=".$id);

        }
      }
    }



  }elseif ($this->request->isPost()) {

    /*if($this->security->getSessionToken() == $this->request->getPost('csrf')){*/
      $progs = Tblprograms::findFirst("programName='".$this->request->getPost('title', 'striptags')."'");
      $tbp = new Tblprograms();

      $tbp->assign(array(
            'programName' => $this->request->getPost('title', 'striptags'),
            'programTooltip' => $this->request->getPost('tooltip', 'striptags'),
            'programTagline' => $this->request->getPost('tagline', 'striptags'),
            'programPage' => $this->request->getPost('programurl', 'striptags'),
            'programLastUpdated' => time()
            ));
          $pictures = $this->request->getPost('program_picture');

 //     if ($form->isValid($this->request->getPost()) != false) {



        if(count($validation->validate($_POST))){

        //execute validation for title
        foreach ($validation->getMessages()->filter('title') as $message) {
          $this->view->titleErr .= "<div class='label label-danger' >".$message."</div> ";
        }
        //execute validation for tooltip
        foreach ($validation->getMessages()->filter('tooltip') as $message) {
          $this->view->tooltipErr .= "<div class='label label-danger' >".$message."</div> ";
        }
        //execute validation for tagline
        foreach ($validation->getMessages()->filter('tagline') as $message) {
          $this->view->taglineErr .= "<div class='label label-danger' >".$message."</div> ";
        }
        //execute validation for program_picture
        foreach ($validation->getMessages()->filter('program_picture') as $message) {
          $this->view->program_pictureErr .= "<div class='label label-danger' >".$message."</div> ";
        }

        //execute validation for program_url
        foreach ($validation->getMessages()->filter('programurl') as $message) {
          $this->view->program_urlErr .= "<div class='label label-danger' >".$message."</div> ";
        }

        }else{
          if($progs){

            $this->view->titletaken = "<div class='label label-danger'>Program Name already exist.</div>";

          }else{

            if (!$tbp->save()) {

              $this->flash->error($tbp->getMessages());

            } else {

              $phql = "INSERT INTO Tblprogramsimg ( imgname, imgpath, programID) "
              . "VALUES (:imgname:, :imgpath:, :programID:)";

              foreach($pictures as $pic){

                $newpicname = md5(uniqid(rand(),true)).$pic;
                $res = $this->modelsManager->executeQuery($phql,

                  array(
                    'imgname' => $newpicname,
                    'imgpath' => 'public/programs/',
                    'programID' => $tbp->programID,
                    )
                  );

                rename('../public/server/php/files/'.$pic, '../public/img/programs/'.$newpicname);
                rename('../public/server/php/files/thumbnail/'.$pic, '../public/img/programs/thumbnail/'.$newpicname);

              }
              $this->_createProgramFolder($tbp->programID);
              $programs = Tblprograms::find();
              $this->view->programMenu = $programs;
              $form->clear();

              $this->flashSession->success('Program was created successfully.');
                // Make a full HTTP redirection
              $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
              return $this->response->redirect($actual_link);

              // $this->flash->success("Program was created successfully");
              // header( 'Refresh:1; url=/admin/programs', true, 303);
              // $body = 'Please visit '
              // .'http://angbayanko.org/programs/page/'.$this->request->getPost('programurl', 'striptags');
              // $this->sendnewsletter('ANG BAYAN KO NEW PROGRAM - '.$this->request->getPost('title', 'striptags'), $body);

              Tag::resetInput();
            }
          }
        }
   //   }
/*  }else{
Tag::resetInput();
}*/
}

$this->view->form = $form;
$this->view->prog = $programs;
}


/*
*  Program Pages
*/
public function programpagesAction($programID){
  $this->view->updated = null;
  $this->view->menu = $this->_menuActive('pmrole');
  $programs = Tblprograms::findFirst('programID='.$programID);
  $this->view->script = '<script> var CURRENT_PROGRAM_FOLDER = '.$programID.'; var CURRENT_FOLDER_CAT = "program"; </script>';
  $this->view->prog = $programs;
  $this->view->titleError = null;
  $this->view->titleErrorAct = null;
  $this->view->pagetitleError = null;
  $this->_createProgramFolder($programID);
  \Phalcon\Tag::prependTitle($programs->programName . ' | ');

  ///////////////// program page add
  if($this->request->isPost() && $this->request->getPost('program_page_add')){
        $this->view->pageadd_titleErr = null;
    if($this->security->getSessionToken() == $this->request->getPost('csrf')){
      $validation = new Phalcon\Validation();
      $validation->add('program_page_title', new PresenceOf(array(
        'message' => 'Page Title is required.'
        )));

      if(count($validation->validate($_POST))){
        foreach ($validation->getMessages()->filter('program_page_title') as $message) {
          $this->view->pageadd_titleErr .= "<div class='label label-danger'>".$message."</div>";

        }
      }else{
        $pages = Tblpages::findFirst('pageTitle="'.$this->request->getPost('program_page_title').'" AND pageParent = '.$programID);
        if($pages){
          $this->flash->titleError = "Page title already taken.";
        }else{
        
          $result = $this->modelsManager->executeQuery('SELECT tblpages.pageID FROM tblpages WHERE tblpages.pageParent='.$programID.' AND tblpages.pageType="program"  ORDER BY tblpages.pageOrder ');
          $phql = "UPDATE tblpages SET tblpages.pageOrder = :pageorder: WHERE tblpages.pageID=:pageid:";
          $x = 1;
          foreach($result as $p){
          $x = $x + 1;
          $this->modelsManager->executeQuery($phql, array(
          'pageorder' => $x + $this->request->getPost('sort_field'.$p->pageID),
          'pageid' => $p->pageID
            ));
          }

          $page = new Tblpages();
          $page->assign(array(
            'specialPage' => 0,
            'pageParent' => $programID,
            'pageTitle' => trim($this->request->getPost('program_page_title', 'striptags')),
            'pageSlug' => strtolower(trim(str_replace(" ", "-", $this->request->getPost('program_page_title', 'striptags')))),
            'pageSlug' => strtolower(trim(str_replace(" ", "-", $this->request->getPost('program_page_title', 'striptags')))),
            'pageType' => 'program',
            'pageOrder' => 1,
            'pageLastUpdated' => time()
            ));


          if (!$page->save()) {
            $this->flash->error("");
            $this->view->titleError .= "<div class='label label-danger'>Page Title is required</div> ";
          } else {
            $this->flash->success("Page was created successfully.");
            Tag::resetInput();
          }
        }
      }
    }else{
      Tag::resetInput();
    }

  }
  //////////////// update banner
  elseif($this->request->isPost() && $this->request->getPost('updatebanner')){

    if($this->security->getSessionToken() == $this->request->getPost('csrf')){
      $programs = Tblprograms::findFirst('programID='.$programID);
      $programs->assign(array(
        'programBanner' => $this->request->getPost('pageBannerUrl', 'striptags')
        ));
      if (!$programs->save()) {
        $this->flash->error($programs->getMessages());
      } else {
        $this->flash->success($programs->programName." Banner update successful.");
        Tag::resetInput();
      }
    }else{
      Tag::resetInput();
    }
    $programs = Tblprograms::findFirst('programID='.$programID);
    $this->view->prog = $programs;
  }
//////////// program page update
  elseif($this->request->isPost() && $this->request->getPost('program_page_update')){
      $this->view->pageupdate_titleErr = null;
      $this->view->pageupdate_contentErr = null;
    if($this->security->getSessionToken() == $this->request->getPost('csrf')){
      $validation = new Phalcon\Validation();
      $validation->add('program_page_title', new PresenceOf(array(
        'message' => 'Page Title is required.'
        )));
      $validation->add('programPageText', new PresenceOf(array(
        'message' => 'Page Content is required.'
        )));
      if(count($validation->validate($_POST))){
        foreach ($validation->getMessages()->filter('program_page_title') as $message) {
          $this->view->pageupdate_titleErr .= "<div class='label label-danger' >".$message."</div> <br>";
        }
        foreach ($validation->getMessages()->filter('programPageText') as $message) {
          $this->view->pageupdate_contentErr .= "<div class='label label-danger' >".$message."</div> <br>";
        }
      }else{

        $pages = Tblpages::findFirst('pageID='.$this->request->getPost('program_pageID'));
        $pages->assign(array(
          'pageTitle' => trim($this->request->getPost('program_page_title', 'striptags')),
          'pageContent' => trim($this->request->getPost('programPageText')),
          'pageKeywords' => trim($this->request->getPost('program_page_keyword', 'striptags')),
          'pageSlug' => trim($this->request->getPost('program_page_slug', 'striptags'),'-'),
          'pageActive' => trim($this->request->getPost('program_page_active')),
          'pageLastUpdated' => time()
          ));
        if (!$pages->save()) {
          $this->view->titleErrorAct="<label class='label label-danger'> Page title is required.</label>";
        } else {
          echo '<script type="text/javascript">return false;</script>';
          $this->view->updated = $pages->pageTitle;
          $this->flash->success($pages->pageTitle." updated successfully.");
          //Tag::resetInput();
        }
      }
    }else{
      Tag::resetInput();
    }
  
  }
  ////////////// program update sort
  elseif($this->request->isPost() && $this->request->getPost('updatesort')){
    $result = $this->modelsManager->executeQuery('SELECT tblpages.pageID FROM tblpages WHERE tblpages.pageParent='.$programID.' AND tblpages.pageType="program"
      ORDER BY tblpages.pageOrder');
    $result2 = $this->modelsManager->executeQuery('SELECT tblpages.pageID FROM tblpages WHERE tblpages.pageParent='.$programID.' AND tblpages.pageType="program"
      ORDER BY tblpages.pageOrder');
    $phql = "UPDATE tblpages SET tblpages.pageOrder = :pageorder: WHERE tblpages.pageID=:pageid:";

     foreach($result as $p){  
       $x = $this->request->getPost('sort_field'.$p->pageID);
       $count = 0;
       // $this->flash->warning($count);

        foreach($result2 as $p){
          if ($x == $this->request->getPost('sort_field'.$p->pageID)) {
              $count = $count + 1;

                if ($count >= 2) {
                  
                   $checker = true;

                }


          }
      }
    }


  if ($checker == false) {
  foreach($result as $p){
      $this->modelsManager->executeQuery($phql, array(
        'pageorder' => $this->request->getPost('sort_field'.$p->pageID),
        'pageid' => $p->pageID
        ));
    }
    $this->flash->success("Page sorting updated.");
    Tag::resetInput();
    }
    else{ $this->flash->warning("Duplicate was found");}
  }
  /////////////// program page delete yes
  elseif($this->request->isPost() && $this->request->getPost('program_page_delete_yes')){
    if($this->security->getSessionToken() == $this->request->getPost('csrf')){
      $pageID = $this->request->getPost('page_program_delete_id');
      $pages = Tblpages::findFirst("pageID=".$pageID);
      if (!$pages) {
        $this->flash->error("User was not found");
      }

      if (!$pages->delete()) {
        foreach ($pages->getMessages() as $message) {
          $this->flash->error((string) $message);
        }
      } else {


          $result = $this->modelsManager->executeQuery('SELECT tblpages.pageID FROM tblpages WHERE tblpages.pageParent='.$programID.' 
          AND tblpages.pageType="program"  ORDER BY tblpages.pageOrder ');
          $phql = "UPDATE tblpages SET tblpages.pageOrder = :pageorder: WHERE tblpages.pageID=:pageid:";
          foreach($result as $p){
          $x = $x + 1;
          $this->modelsManager->executeQuery($phql, array(
          'pageorder' => $x + $this->request->getPost('sort_field'.$p->pageID),
          'pageid' => $p->pageID
            ));
          }


        $this->flash->success("Page was deleted");
      }
    }
  }
  //////////////// save activity
  elseif($this->request->isPost() && $this->request->getPost('saveActivity')){
    $activity = trim($this->request->getPost('activity', 'striptags'));

    if(empty($activity)){
      $this->view->activityFormResult = '<span class="label label-danger">Activity is required</span>';
    }else{
      if($this->security->getSessionToken() == $this->request->getPost('actCsrf')){
        $activities = new Tblporgramactivities;
        $activities->assign(array(
          'programID' => $programID,
          'activity' => $activity,
          'status' => 1
          ));
        if (!$activities->save()) {
          //$this->view->activityFormResult = '<span class="label label-danger">Unable to save activity</span>';
          $this->flash->error("Unable to save activity.");
        } else {
          //$this->view->activityFormResult = '<span class="label label-success">New activity has been added</span>';
          $this->flash->success("New activity has been added.");
          Tag::resetInput();
        }
      }else{
        Tag::resetInput();
      }
    }
  }
///////// hdelActivity and ddhiddenaction
  elseif($this->request->isPost() && $this->request->getPost('hdelActivity') && $this->request->getPost('ddhiddenaction')){
    $delActivity = Tblporgramactivities::findFirst('activityID='.$this->request->getPost('hdelActivity', 'int'));
    if($this->request->getPost('ddhiddenaction') == 'delete'){
      if($delActivity){
        $delActivity->delete();
        $this->flash->success("Activity was deleted successfully.");
      }
    }elseif($this->request->getPost('ddhiddenaction') == 'disable'){
      if($delActivity){
        $delActivity->assign(array('status'=>0));
        $delActivity->save();
        $this->flash->success("Activity was disabled.");
      }
    }elseif($this->request->getPost('ddhiddenaction') == 'activate'){
      if($delActivity){
        $delActivity->assign(array('status'=>1));
        $delActivity->save();
        $this->flash->success("Activity was activated.");
      }
    }
  }

  /////// save edit activity
  elseif($this->request->isPost() && $this->request->getPost('saveEditActivity')){
    $editActivity = Tblporgramactivities::findFirst('activityID='.$this->request->getPost('heditActivity', 'int'));
    if($editActivity){
      $editActivity->assign(array('activity'=>$this->request->getPost('activitytextArea', 'striptags')));
      $editActivity->save();
      $this->flash->success("Activity was updated successfully.");
    }
  }
/////end
  $image_files = array();
  $files = glob('../public/img/programs/'.$programID.'/*.*');
  usort($files, create_function('$a,$b', 'return filemtime($a)<filemtime($b);'));
  foreach($files as $filename){
    $image_files[] = basename($filename);
  }
  $this->view->digital_assets = $image_files;

  $pages = Tblpages::find('pageParent='.$programID.' AND pageType="program" ORDER BY pageOrder ');
  if($pages){
    $this->view->pages = $pages;
  }

  $this->view->activities = Tblporgramactivities::find('programID = '.$programID);

}
/*
* Move File Upload of Programs
*/
public function ajaxmoveuploadprogramAction($filename, $folder){
  $newpicname = $filename;
  if(is_file('../public/server/php/files/'.$filename)){
    rename('../public/server/php/files/'.$filename, '../public/img/programs/'.$folder.'/'.$newpicname);
  }
  if(is_file('../public/server/php/files/thumbnail/'.$filename)){
    rename('../public/server/php/files/thumbnail/'.$filename, '../public/img/programs/'.$folder.'/thumbnail/'.$newpicname);
  }

  echo '<div class="program-digital-assets-library pull-left" style="position: relative">
  <a href="'.$this->url->get().'img/programs/'.$folder.'/'.$newpicname.'" class="prettyPhoto[pp_gal]"><img src="'.$this->url->get().'img/programs/'.$folder.'/'.$newpicname.'" alt=""></a>
  <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="'.$this->url->get().'img/programs/'.$folder.'/'.$newpicname.'">
  <button class="btn btn-xs btn-danger digital-assets-delete" data-filename="'.$filename.'" data-folder="'.$folder.'" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
</div>';
$this->view->disableLevel(View::LEVEL_LAYOUT);
}

/*
* Delete File Uploaded
*/
public function ajaxdeleteuploadAction($filename, $folder, $type='program'){
  if($type=='program'){
    if(is_file('../public/img/programs/'.$folder.'/'.$filename)){
      unlink('../public/img/programs/'.$folder.'/'.$filename);
    }
    if(is_file('../public/img/programs/'.$folder.'/thumbnail/'.$filename)){
      unlink('../public/img/programs/'.$folder.'/thumbnail/'.$filename);
    }
  }elseif($type=='post'){
    $folder = explode('-', $folder);
    if(is_file('../public/img/assets/'.$folder[0].'/'.$folder[1].'/'.$filename)){
      unlink('../public/img/assets/'.$folder[0].'/'.$folder[1].'/'.$filename);
    }
  }
  $this->view->disableLevel(View::LEVEL_LAYOUT);
}
/*
* Ajax View Page
*/
public function ajaxviewpageAction($pagID){

  $pages = Tblpages::findFirst('pageID='.$pagID);
  if (!$pages) {
    echo "Page was not found";
  }else{
    echo $pages->pageContent;
  }

  $this->view->disableLevel(View::LEVEL_LAYOUT);
}



/*
* =======================================================================================================================
* NEWS POST
* =======================================================================================================================
*/
/*
* Create a News Post
*/
public function createpostAction(){
  \Phalcon\Tag::prependTitle('Create News Post | ');
  $this->view->menu = $this->_menuActive('postrole');
  $programs = Tblprograms::find();
  $pages = Tblpages::find('specialPage=1');
  $this->view->script = '<script> var CURRENT_FOLDER_CAT = "post"; </script>';
  // $this->view->script = '<script> var CURRENT_PROGRAM_FOLDER = "post"; var CURRENT_FOLDER_CAT = "program"; </script>';
  $form = new CreatepostForm();
  $form->csrf = $this->security->getToken();
  $this->view->pictures = $pictures = Tblpagesimg::find();
  $this->view->folders = $this->_getFolderAssets();
  $this->view->success=null;


  // $this->_createPostFolder("post");
 $this->_createPostFolder($form->blogID);

  $f = $this->_getFolderAssets();
  $toparray = array();

  foreach ($f as $fkey => $fvalue) {
    $toparray[] = $fkey;
    foreach($fvalue as $ff){
      $toparray[] = $ff;
      break;
    }
    break;
  }

  $this->view->dayear = $toparray[0];
  $this->view->damonth = $toparray[1];

  $post = new Tblpost();
  $poststatus = ($this->request->getPost('post_status') ? $this->request->getPost('post_status') : 'publish');
  $postcheck = Tblpost::findFirst('postTitle="'.$this->request->getPost('post_title', 'striptags').'"');
  $post->assign(array(
    'postTitle' => trim($this->request->getPost('post_title', 'striptags')),
    'postSlug' =>  trim($this->request->getPost('post_slug', 'striptags'),'-'),
    'postContent' => trim($this->request->getPost('post_content', 'striptags')),
    'postStatus' => $poststatus,
    'postDate' => time(),
    'postPublishDate' => ($this->request->getPost('postDatePublish', 'striptags') ? strtotime($this->request->getPost('postDatePublish', 'striptags')):time()),
    'postKeyword' => trim($this->request->getPost('post_keywords', 'striptags')),
    'postFeatureImage' => trim($this->request->getPost('pageBannerUrl', 'striptags')),
    ));

  $this->view->post=$post;

  if($this->request->isPost()) {
    /*if($this->security->getSessionToken() == $this->request->getPost('csrf')){*/
        $validation = new Phalcon\Validation();
          $this->view->titleError = null;
          $this->view->contentError = null;
          $this->view->checkedProgsError = null;
          $this->view->featimageError = null;
          $this->view->keywords=null;

/// post_title validation
        $validation ->add('post_title', new PresenceOf(array(
        'message' => 'Title is required',
        )));
/// post_content validation
        $validation ->add('post_content', new PresenceOf(array(
        'message' => 'Content is required',
        )));
/// post_featured image validation
        $validation ->add('pageBannerUrl', new PresenceOf(array(
        'message' => 'Featured Image is required',
        )));
         $validation ->add('post_keywords', new PresenceOf(array(
        'message' => 'Keywords is required',
        )));

      if ($form->isValid($this->request->getPost()) != true) {

        if(count($validation->validate($_POST))){

//execute validation for post_title

        foreach ($validation->getMessages()->filter('post_title') as $message) {
          $this->view->titleError .= "<div class='label label-danger'>".$message."</div> ";
        }
//execute validation for post_content

        foreach ($validation->getMessages()->filter('post_content') as $message) {
          $this->view->contentError .= "<div class='label label-danger'>".$message."</div> ";
        }
//execute validation for featured image

        foreach ($validation->getMessages()->filter('pageBannerUrl') as $message) {
          $this->view->featimageError .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('post_keywords') as $message) {
          $this->view->keywordsError .= "<div class='label label-danger'>".$message."</div> ";
        }

        $count = 0;
            foreach($programs as $p){
              if($this->request->getPost('chkProg'.$p->programID)){
                $count += 1;
              }
            }
            foreach($pages as $p){
              if($this->request->getPost('chkPages'.$p->pageID)){
                $count += 1;
              }
            }

            if(!$count){
              $this->view->checkedProgsError = "<div class='label label-danger'> Select alteast one(1) program/page</div>";
            }

        }else if($postcheck==true){

          $this->flash->error('Page title already taken');

        }else{

          if (!$post->save()) {

            $this->flash->error($post->getMessages());


          } else {

            $count = 0;
            foreach($programs as $p){
              if($this->request->getPost('chkProg'.$p->programID)){
                $count += 1;
              }
            }
            foreach($pages as $p){
              if($this->request->getPost('chkPages'.$p->pageID)){
                $count += 1;
              }
            }

            if(!$count){

              // $post->delete();
         $this->view->checkedProgsError.= "<div class='label label-danger'>Select alteast one(1) program/page</div>";

            } else {

              $checkedProgs = $this->_insertPostCat($post->postID, $programs, "programs");
              $checkedPages = $this->_insertPostCat($post->postID, $pages, "pages");
              Tag::resetInput();

////////////send email to subscribers


              if($poststatus == 'publish'){
                //
                // $body = $this->request->getPost('post_content').'<br /><br />'
                // .'http://angbayanko.org/post/news/'.trim($this->request->getPost('post_slug', 'striptags'),'-');
                // $this->sendnewsletter($this->request->getPost('post_title', 'striptags'), $body);

              }

              // $this->flash->success("News Post has been successfully created");
              $this->view->success.="<div class='alert alert-success'>News Post has been successfully created</div>";
               header('Refresh: 1; URL=/admin/allnewspost');
              Tag::resetInput();
              $form->clear();
               $this->view->post=null;
            }
            /**/
          }
        }

      }else{
        Tag::resetInput();
      }
    }


    $this->view->form = $form;
    $this->view->programs = $programs;
    $this->view->pages = $pages;

$programID = "post"; //Temporary
$this->view->prog = $programID ; //Temporary

$image_files = array();
$files = glob('../public/img/programs/'.$programID.'/*.*');
usort($files, create_function('$a,$b', 'return filemtime($a)<filemtime($b);'));
foreach($files as $filename){
  $image_files[] = basename($filename);
}
$this->view->digital_assets = $image_files;
}
/*
* All Post
*/
public function allnewspostAction(){
  \Phalcon\Tag::prependTitle('News Posts | ');
  $this->persistent->searchAllNewsParams = null;
  $this->view->menu = $this->_menuActive('postrole');
  $numberPage = 1;
  if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
    Tag::resetInput();
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $this->_deletePost($id);
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      $keyword = $this->request->getPost('search_text');
      $this->session->set("user_search_text", $keyword);
    } else {
      // $numberPage = $this->request->getQuery("page", "int");
      $searchtext = $this->session->get("user_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
  }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
    $id = $this->request->getPost('tbl_id');
    if(!empty($id)){
      $this->_deletePost($id);
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      // $query = Criteria::fromInput($this->di, 'Tblusers', array('userName' => $this->request->getPost('search_text'), 'userFirstname' => $this->request->getPost('search_text')));
      // $this->persistent->searchUserParams = $query->getParams();
      // $this->session->set("user_search_text", $this->request->getPost('search_text'));
      $keyword = $this->request->getPost('search_text');
      $this->session->set("user_search_text", $keyword);
    } else {
      // $numberPage = $this->request->getQuery("page", "int");
      $searchtext = $this->session->get("user_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
  }elseif($this->request->isPost() && $this->request->getPost('action') == "edit"){
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      return $this->dispatcher->forward(array(
        'controller' => 'admin',
        'action' => 'editPost',
        'params' => array($id)
        ));
    }
  }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
    $this->session->remove("allNews_search_text");
    $this->session->remove("allNews_filter_date");
    $this->session->remove("allNews_filter_date_from");
    $this->session->remove("allNews_filter_date_to");
    $this->persistent->searchAllNewsParams = null;
    unset($_POST);
  }else{
    if ($this->request->isPost()) {
      $query = Criteria::fromInput($this->di, 'Tblpost', array('postTitle' => trim($this->request->getPost('search_text'))));
      $this->persistent->searchAllNewsParams = $query->getParams();
      $this->session->set("allNews_search_text", $this->request->getPost('search_text'));
    } else {
      $numberPage = $this->request->getQuery("page", "int");
    }
  }


  $parameters = array();
  if ($this->persistent->searchAllNewsParams) {
    $parameters = $this->persistent->searchAllNewsParams;
  }

  // SORTING
  // Added a server side sorting on all tables using the columns shown
  // Dont forget to insert na href params on the view
  $sort = $this->request->getQuery("sort");
  $order = "postPublishDate DESC";
  $this->view->posttitleHref = "postTitle-asc";
  $this->view->keywordHref = "postKeyword-asc";
  $this->view->postdateHref = "postDate-asc";
  $this->view->publishdateHref = "postPublishDate-asc";
  $arr = explode("-", $sort);
  switch ($arr[0]) {
    case 'postTitle':
    $arr[1] == "asc" ? $this->view->posttitleHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->posttitleIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'postKeyword':
    $arr[1] == "asc" ? $this->view->keywordHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->keywordIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'postDate':
    $arr[1] == "asc" ? $this->view->postdateHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->postdateIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'postPublishDate':
    $arr[1] == "asc" ? $this->view->publishdateHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->publishdateIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    default:
    $order = " postDate ASC";
    break;
  }

  $builder = $this->modelsManager->createBuilder()
  ->columns('Tblpost.postID, postTitle, postDate, postPublishDate, postStatus, postKeyword')
  ->from('Tblpost')->orderBy($order);

  if(!empty($parameters)){
    $this->flash->notice('Search results for "<strong>' . $this->session->get("allNews_search_text") .'</strong>"');
    $builder->andWhere($parameters['conditions'], $parameters['bind']);
  } else{
   if($this->request->getpost('submit')){

    $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):"";
    $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):"";
    if(($fromDate && $toDate) || $this->session->has("allNews_filter_date") ) {
      if($fromDate > $toDate) {
        return $this->flash->warning("Invalid request. End date must be later than the start date.");
      }
      if($fromDate < $toDate && !empty($fromDate)){
        $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
      }
      // if(empty($fromDate) || empty($toDate)){
      //   return $this->flash->warning("From or To date is required");
      // }
      // if(empty($toDate)){
      //   return $this->flash->warning("To date is required");
      // }
      if($this->session->has("allNews_filter_date")) {
        if(!$fromDate && !$toDate){
          $fromDate = trim($this->session->get('allNews_filter_date_from'));
          $toDate = trim($this->session->get('allNews_filter_date_to'));
          return $this->flash->warning("From and To date is required");
        }
      }
      else {
        $this->session->set("allNews_filter_date", true);
        $this->session->set("allNews_filter_date_from", $fromDate);
        $this->session->set("allNews_filter_date_to", $toDate);
      }
      //$this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
       $builder->where("postDate BETWEEN $fromDate AND $toDate");
    }
    else{
        return $this->flash->warning("From and To date is required");
      // $this->view->searchResultStat1='<div class="alert alert-warning" style="color: red;">From and To date is required</div>';
  }
}}

    // if($this->request->getPost('search') ){
    //   if(empty($this->request->getPost('search_text'))){
    //   return $this->flash->warning("Search field is required");
    // }
    // }

  $paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
    "builder" => $builder,
    "order" => "postPublishDate",
    "limit"=> 10,
    "page" => $numberPage
    ));

  // Get the paginated results
  $this->view->page = $paginator->getPaginate();
}
/*
* Edit Post
*/
public function editnewsAction($id){
  \Phalcon\Tag::prependTitle('Edit News | ');
  $this->view->menu = $this->_menuActive('postrole');
  $pages = Tblpages::find('specialPage=1');
  $programs = Tblprograms::find();
  $post = Tblpost::findFirst($id);
  $this->view->post = $post;
  $this->view->script = '<script> var CURRENT_FOLDER_CAT = "post"; </script>';
  $form = new CreatepostForm();
  $this->view->form = $form;
  $this->view->titleError = null;
  $this->view->contentError = null;
  $this->view->checkedProgsError = null;
  $this->view->folders = $this->_getFolderAssets();




  $f = $this->_getFolderAssets();
  $toparray = array();
  foreach ($f as $fkey => $fvalue) {
    $toparray[] = $fkey;
    foreach($fvalue as $ff){
      $toparray[] = $ff;
      break;
    }
    break;
  }
  $this->view->dayear = $toparray[0];
  $this->view->damonth = $toparray[1];

  if($this->request->isPost()){

    if($this->security->getSessionToken() == $this->request->getPost('csrf')){
      //echo "new updates error";
      // if ($form->isValid($this->request->getPost()) != true) {
      $postcheck = Tblpost::findFirst('postTitle="'.$this->request->getPost('post_title', 'striptags').'" AND  postID != "'.$id.'"');


       $validation = new Phalcon\Validation();
          $this->view->titleError = null;
          $this->view->contentError = null;
          $this->view->checkedProgsError = null;
          $this->view->featimageError = null;
          $this->view->keywords=null;


/// post_title validation
        $validation ->add('post_title', new PresenceOf(array(
        'message' => 'Title is required',
        )));
/// post_content validation
        $validation ->add('post_content', new PresenceOf(array(
        'message' => 'Content is required',
        )));
/// post_featured image validation
        // $validation ->add('pageBannerUrl', new PresenceOf(array(
        // 'message' => 'Featured Image is required',
        // )));


       if(count($validation->validate($_POST))){

//execute validation for post_title

        foreach ($validation->getMessages()->filter('post_title') as $message) {
          $this->view->titleError .= "<div class='label label-danger'>".$message."</div> ";
        }
//execute validation for post_content

        foreach ($validation->getMessages()->filter('post_content') as $message) {
          $this->view->contentError .= "<div class='label label-danger'>".$message."</div> ";
        }
//execute validation for featured image

        foreach ($validation->getMessages()->filter('pageBannerUrl') as $message) {
          $this->view->featimageError .= "<div class='label label-danger'>".$message."</div> ";
        }

        // $count = 0;
        //     foreach($programs as $p){
        //       if($this->request->getPost('chkProg'.$p->programID)){
        //         // $unchk=$this->request->getPost('chkProg'.$p->programID);
        //         $count += 1;
        //       }
        //     }
        //     foreach($pages as $p){
        //       if($this->request->getPost('chkPages'.$p->pageID)){
        //         $count += 1;
        //       }
        //     }

        //     if(!$count){
        //       $this->view->checkedProgsError .= "<div class='label label-danger'> Select alteast one(1) program</div>";

        //       $postcat=null;
        //     }

        } else if($postcheck==true){


        $this->flash->error('Page title already taken.');

      }else{

         $post->assign(array(
          'postTitle' => trim($this->request->getPost('post_title', 'striptags')),
          'postSlug' =>  trim($this->request->getPost('post_slug', 'striptags'),'-'),
          'postContent' => trim($this->request->getPost('post_content')),
          'postStatus' => ($this->request->getPost('post_status') ? $this->request->getPost('post_status') : 'publish'),
          // 'postDate' => time(),
          'postPublishDate' => ($this->request->getPost('postDatePublish', 'striptags') ? strtotime($this->request->getPost('postDatePublish', 'striptags')):time()),
          'postKeyword' => trim($this->request->getPost('post_keywords', 'striptags')),
          'postFeatureImage' => trim($this->request->getPost('pageBannerUrl', 'striptags'))
          ));

        if (!$post->save()) {
          /*  $this->flash->error($post->getMessages());*/
          $this->flash->error($post->getMessages());

        } else {

          $count = 0;

          foreach($programs as $p){

            if($this->request->getPost('chkProg'.$p->programID)){
              $count += 1;

            }
          }

          foreach($pages as $p){

            if($this->request->getPost('chkPages'.$p->pageID)){
              $count += 1;
            }


          }

          // if(!$count){
          //
          //
          //   $this->view->checkedProgsError .= "<div class='label label-danger'>Select alteast one(1) program</div>";
          //      $postcat = null;
          //       $postcat = Tblpostcat::find('postID='.$id);
          //
          // } else {

            $checkedProgs = $this->_insertPostCat($post->postID, $programs, "programs");
            $checkedPages = $this->_insertPostCat($post->postID, $pages, "pages");

            $this->view->success.="<div class='alert alert-success'>News Post has been successfully updated.</div>";
            // Tag::resetInput();
            header('Refresh: 1; URL=/admin/allnewspost');

          // }

        }

      }
      // }
    }else{

      Tag::resetInput();

    }

  }//end of save edit

  $postcat = Tblpostcat::find('postID='.$id);

  foreach($postcat as $p){

    $this->tag->setDefault('chkProg'.$p->relatedID, $p->relatedID);

  }

   foreach($postcat as $p){

    $this->tag->setDefault('chkPages'.$p->relatedID, $p->relatedID);

  }

  if($post->postStatus=='draft'){

    $this->tag->setDefault('post_status', $post->postStatus);

  }
  /**/
  if($p->relatedtype == "programs"){
    $this->tag->setDefault('chkProg'.$p->relatedID, $p->relatedID);
  } else if($p->relatedtype == "pages") {
    $this->tag->setDefault('chkPages'.$p->relatedID, $p->relatedID);
  }  /**/

  $this->view->programs = $programs;
  $this->view->pages = $pages;

        $programID = "post"; //Temporary
        $this->view->prog = $programID ; //Temporary
        $image_files = array();
        $files = glob('../public/img/programs/'.$programID.'/*.*');
        usort($files, create_function('$a,$b', 'return filemtime($a)<filemtime($b);'));
        foreach($files as $filename){
          $image_files[] = basename($filename);
        }
        $this->view->digital_assets = $image_files;
}
/*
* Edit Profile
*/
/**
* Edit User Action
*/
public function editprofileAction(){
  \Phalcon\Tag::prependTitle('Edit Profile | ');
  $userid = $this->session->get('auth');
  $this->view->menu = null;
  $this->view->partnerSubMenu = $this->_createPartnerMenu();
  $this->view->roleError = null;
  $userID = $userid['abk_id'];
  $user = Tblusers::findFirst($userID);
  $this->view->user = $user;
  $this->view->passError = null;
  $this->view->repassError = null;
  $this->view->oldpassError = null;
  $form = new CreateuserForm($user, array(
    'editown' => true,
    'edit' => true,
    'editprofile'=>true
    ));
  if ($this->request->isPost()) {
   if($this->request->getPost('update_password')){
    $validation = new Phalcon\Validation();
        // $validation->add('oldpassword', new PresenceOf(array(
        //     'message' => 'The old password is required'
        //     )));
    $validation->add('password', new PresenceOf(array(
      'message' => 'The password is required',
      'cancelOnFail' => true
      )));
    $validation->add('password', new StringLength(array(
      'max' => 20,
      'min' => 8,
      'messageMaximum' => 'Thats an exagerated password.',
      'messageMinimum' => 'Password should be Minimum of 8 characters.'
      )));

    $validation->add('repassword', new PresenceOf(array(
      'message' => 'Retyping your password is required'
      )));
    $validation->add('repassword', new Confirmation(array(
      'message' => 'Password doesn\'t match confirmation',
      'with' => 'password'
      )));

    if(count($validation->validate($_POST))){
          // foreach ($validation->getMessages()->filter('oldpassword') as $message) {
          //     $this->view->oldpassError .= "<div class='label label-danger'>".$message."</div> ";
          // }
      foreach ($validation->getMessages()->filter('password') as $message) {
        $this->view->passError .= "<div class='label label-danger'>".$message."</div> ";
      }
      foreach ($validation->getMessages()->filter('repassword') as $message) {
        $this->view->repassError .= "<div class='label label-danger'>".$message."</div> ";
      }
    }else{
          // if(sha1($this->request->getPost('oldpassword')) == $user->userPassword) {

          // } else {
          //     $this->view->oldpassError = "<div class='label label-danger'>Wrong Password</div> ";
          //     Tag::resetInput();
          // }
      $user->assign(array(
        'userPassword' => sha1($this->request->getPost('password'))
        ));

      if (!$user->save()) {
        $this->flash->error($user->getMessages());
      } else {

        $this->flash->success("Password successfully changed.");
        Tag::resetInput();
      }
    }
  }elseif($this->request->getPost('update_button')){
    if ($form->isValid($_POST)) {
      $form->getMessages();
    }else{
      $userName = Tblusers::findFirst("userName='".$this->request->getPost('username', 'striptags')."' AND userID != ".$userID);
      $userEmail = Tblusers::findFirst("userEmail='".$this->request->getPost('email', 'striptags')."' AND userID != ".$userID);
      if($userName==true){
        $this->flash->error("Username already taken");
      }elseif($userEmail==true){
        $this->flash->error("Email Address already taken");
      }else{
        $user->assign(array(
          'userName' => $this->request->getPost('username', 'striptags'),
          'userEmail' => $this->request->getPost('email', 'striptags'),
          'userFirstname' => $this->request->getPost('firstname', 'striptags'),
          'userLastname' => $this->request->getPost('lastname', 'striptags'),
          'userMiddlename' => $this->request->getPost('middlename', 'striptags'),
          'userAddress' => $this->request->getPost('address', 'striptags'),
          'userCompany' => $this->request->getPost('company', 'striptags'),
          'userContact' => $this->request->getPost('contact', 'striptags'),
          'userPosition' => $this->request->getPost('position', 'striptags'),
          'userStatus' => $user->userStatus,
          'dateCreated' => time()
          ));
        if (!$user->save()) {
          $this->flash->error($user->getMessages());
        } else {
          $role = Tbluserroles::find('userID ='.$userID);
          $this->flash->success("You account was updated successfully");
          $this->session->set('auth', array(
            'abk_id' => $user->userID,
            'abk_fullname' => $user->userFirstname .' '.$user->userLastname,
            'abk_userlevel' => $user->userLevel
            ));
          $this->view->roleError = null;
        }
      }
    }
  }
}
$this->view->form = $form;
}
/*
* Move Upload for Post Digital Assets
*/
public function ajaxmoveuploadpostAction($filename){
  $path = null;
  if(is_file('../public/server/php/files/'.$filename)){
    $path = '../public/img/assets/'.date('Y').'/'.date('m');
    if(!is_dir('../public/img/assets/'.date('Y'))){
      mkdir('../public/img/assets/'.date('Y'));
    }
    if(!is_dir('../public/img/assets/'.date('Y').'/'.date('m'))){
      mkdir($path);
      mkdir($path.'/thumbnail');
    }
    rename('../public/server/php/files/'.$filename, $path.'/'.$filename);
  }
  if(is_file('../public/server/php/files/thumbnail/'.$filename)){
    rename('../public/server/php/files/thumbnail/'.$filename, $path.'/thumbnail/'.$filename);
  }
  $ext = pathinfo($filename, PATHINFO_EXTENSION);
  $imgpath = $this->url->get().'img/assets/'.date('Y').'/'.date('m').'/'.$filename;
  $newfilename = strlen($filename) > 15 ? substr($filename,0,15)."...".$ext : $filename;
  echo '<div class="program-digital-assets-library pull-left" style="position: relative">
  <div style="padding-left: 25px; width: 100%; word-wrap:break-word;">'.$newfilename.'</div>
  <a href="'.$imgpath.'" class="prettyPhoto[pp_gal]"><img src="'.$imgpath.'" alt=""></a>
  <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="'.$imgpath.'">
  <button class="btn btn-xs btn-danger digital-assets-post-delete" data-filename="'.$filename.'" data-folder="'.date('Y').'-'.date('m').'" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
  <div class="clearfix"></div>
</div>';
$this->view->disableLevel(View::LEVEL_LAYOUT);
}
/*
* Ajax View Digital Assets
*/
public function ajaxviewdigitalassetsAction($year, $month){
  if(is_dir('../public/img/assets/'.$year.'/'.$month)){
    $path = '../public/img/assets/'.$year.'/'.$month;
    $files = glob($path.'/*.*');
    usort($files, create_function('$a,$b', 'return filemtime($a)<filemtime($b);'));
    foreach($files as $filename){

      $ext = pathinfo($filename, PATHINFO_EXTENSION);
      //$imgpath = $this->url->get().'public/img/assets/'.$year.'/'.$month.'/'.$filename;
      $exts = array('jpg','jpeg','png','gif');

      $imgpath = $this->url->get().'img/assets/'.$year.'/'.$month.'/'.basename($filename);
      if(in_array($ext, $exts)){
        $img = '<a href="'.$imgpath.'" class="prettyPhoto[pp_gal]"><img src="'.$imgpath.'" alt=""></a>';
      }else{
        $img = '<a href="'.$imgpath.'" target="_blank"><img src="'.$this->url->get().'public/img/file.png" alt="" style="height: 100px;"></a>';
      }

      $name = basename($filename);
      $newfilename = strlen($name) > 15 ? substr($name,0,15)."...".$ext : $name;
      echo '<div class="program-digital-assets-library pull-left" style="position: relative">
      <div style="padding-left: 25px; width: 100%; word-wrap:break-word;">'.$newfilename.'</div>
      <a href="'.$filename.'" class="prettyPhoto[pp_gal]">'.$img.'</a>
      <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="'.$imgpath.'">
      <button class="btn btn-xs btn-danger digital-assets-post-delete" data-filename="'.basename($filename).'" data-folder="'.$year.'-'.$month.'" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
      <div class="clearfix"></div>
    </div>';
  }
}
$this->view->disableLevel(View::LEVEL_LAYOUT);
}
private function _getFolderAssets(){
  $path = '../public/img/assets/';
  $assets = scandir($path);
  $folders = array();
  foreach ($assets as $a) {
    if ($a === '.' or $a === '..') continue;

    if (is_dir($path . '/' . $a)) {
      $subassets = scandir($path . '/' . $a);
      $subfolders = array();
      foreach ($subassets as $sa){
        if ($sa === '.' or $sa === '..') continue;

        if (is_dir($path . '/' . $a . '/'. $sa)) {
          $subfolders[] = $sa;
        }
      }
      arsort($subfolders);
      $folders[$a] = $subfolders;
    }
  }
  arsort($folders);
  return $folders;
}

private function _createProgramFolder($programID){
  $dir = '../public/img/programs';
  if(is_dir($dir)){
    $path = $dir.'/'.$programID;
    if(!is_dir($path)){
      mkdir($path);
      mkdir($path.'/thumbnail');
    }
    return $path;
  }else{
    die('directorynoexist');
  }
}


private function _createPostFolder($programID){
  $dir = '../public/img';

  // var_dump($programID);
  // die();

  if(is_dir($dir)){
    $path = $dir.'/'.$programID;
    if(!is_dir($path)){
      mkdir($path);
      mkdir($path.'/thumbnail');
    }
    return $path;
  }else{
    die('directorynoexist');
  }
}


private function _registerSession($user){
  $this->session->set('auth', array(
    'abk_id' => $user->userID,
    'abk_fullname' => $user->userFirstname .' '.$user->userLastname,
    'abk_userlevel' => $user->userLevel
    ));
  $phql = 'SELECT Tblroles.rolePage,Tblroles.roleDescription, Tblroles.roleCode FROM Tbluserroles ' .
  ' INNER JOIN Tblroles ON Tblroles.roleCode = Tbluserroles.userRoles WHERE Tbluserroles.userID = '.$user->userID;
  $result = $this->modelsManager->executeQuery($phql);
  $sessrole = array();
  $sesspage = array();

  if($user->userLevel == 1){
    $phql = 'SELECT Tblroles.rolePage, Tblroles.roleCode FROM Tblroles ';
    $result = $this->modelsManager->executeQuery($phql);
    foreach($result as $r){
      $sessrole[] = $r->roleCode;
      $sesspage[] = explode(',', $r->rolePage);
    }
  } else if($user->userLevel == 2){
    $phql = "SELECT Tblroles.rolePage, Tblroles.roleCode FROM Tblroles WHERE Tblroles.roleCode='partrole'";
    $result = $this->modelsManager->executeQuery($phql);
    foreach($result as $r){
      $sessrole[] = $r->roleCode;
      $sesspage[] = explode(',', $r->rolePage);
    }
  }else{
    foreach($result as $r){
      $sessrole[] = $r->roleCode;
      $sesspage[] = explode(',', $r->rolePage);
    }
  }
  $roles = array('roles' => $sessrole, 'page' => array_filter(call_user_func_array('array_merge', $sesspage)));

  $this->session->set('roles', $roles );

  //Set SuperAdmin
  if($user->userLevel){
    $this->session->set('SuperAdmin', true );
  }
}
private function _menuActive($menu, $options = null){
  $index = (!empty($options['partnerIndex']) && $options['partnerIndex']==true)?true:false;
  $curPartID = !empty($options['partnerCurPartID'])?$options['partnerCurPartID']:null;
  $this->view->partnerSubMenu = $this->_createPartnerMenu($index, $curPartID);

  $phql = 'SELECT Tblroles.rolePage, Tblroles.roleCode FROM Tblroles ';
  $result = $this->modelsManager->executeQuery($phql);
  $menus = array();
  $menus['dashboard'] = false;
  foreach($result as $r){
    $menus[$r->roleCode] = false;
  }
  $menus[$menu] = true;
  return $menus;
}
private function _deletePartner($id){
  $param=null;
  if(is_array($id)){
    $newid = array();
    foreach ($id as $i) {
      $theID = $this->filter->sanitize($i, array("int"));

      $partnersInfo = Tblpartners::findFirst('userID='.$theID);
      if(!empty($partnersInfo->partnerID)){
        $picturesPath = '../public/img/partnerspictures/'.$partnersInfo->partnerID;
        if($this->_deletefolder($picturesPath)){
          $param .= 'partnerID = '. $partnersInfo->partnerID . ' OR ';
        }
      }


    }
    $param = substr($param, 0, -4);
  }else{
    $id = $this->filter->sanitize($id, array("int"));
    $partnersInfo = Tblpartners::findFirst('userID='.$id);
    if(!empty($partnersInfo->partnerID)){
      $picturesPath = '../public/img/partnerspictures/'.$partnersInfo->partnerID;
      if($this->_deletefolder($picturesPath)){
        $param = 'partnerID = '. $partnersInfo->partnerID;
      } else {
        $param = 'partnerID = '. $partnersInfo->partnerID;
      }
    }
  }

  if(!is_null($param)){
    $partners = Tblpartners::find($param);
    $events = Tblpartnerevents::find($param);
    $albums = Tblpartnersalbums::find($param);
    $pictures = Tblpartnerspictures::find($param);
    if (!$partners) {
      $this->flash->error("ABK partner was not found");
    }

    if (!$partners->delete()) {
      foreach ($partners->getMessages() as $message) {
        $this->flash->error((string) $message);
      }
      return $this->forward("admin/partners");
    } else {
      // if($this->router->getActionName() == "partners"){
      //     $this->flash->clear();
      //     $this->flash->success("Partner Account has been successfully deleted");
      // }
      return true;
      $events->delete();
      $albums->delete();
      $pictures->delete();
    }
  }
}
private function _deleteUser($id){
  $param=null;
  if(is_array($id)){
    $newid = array();
    $count = 1;
    foreach ($id as $i) {
      $param .= 'userID = '. $this->filter->sanitize($i, array("int")) . ' OR ';
      // $this->_deletePartner($this->filter->sanitize($i, array("int")));
      if($this->_deletePartner($this->filter->sanitize($i, array("int"))) && $count == 1 ){
        if($this->router->getActionName() == "partners"){
          $this->flash->clear();
          $this->flash->success("Partner Account has been successfully deleted.");
        }
      }
      $count += 1;
    }
    $param = substr($param, 0, -4);
  }else{
    $id = $this->filter->sanitize($id, array("int"));
    $param = 'userID=' . $id;
    $this->_deletePartner($id);
  }

  $products = Tblusers::find($param);
  if (!$products) {
    $this->flash->error("User was not found");
  }

  if (!$products->delete()) {
    foreach ($products->getMessages() as $message) {
      $this->flash->error((string) $message);
    }
    return $this->forward("admin/users");
  } else {
    if($this->router->getActionName() == "users"){
      $this->flash->clear();
      $this->flash->success("User(s) has been successfully deleted.");
    }
  }
}
private function _deleteProgram($id){
  $id = $this->filter->sanitize($id, array("int"));
  $param = 'programID=' . $id;

  $tbp = Tblprograms::find($param);
  if(!$tbp){
    $this->flash->error("Program was not found");
  }

  $tbpage = Tblpages::find('pageParent='.$id.' AND pageType="program"');
  if(!$tbpage){
    $this->flash->error("Page was not found");
  }

  $tbpi = Tblprogramsimg::find($param);
  if(count($tbpi)){
    foreach($tbpi as $t){
      if(is_file('../public/img/programs/thumbnail/'.$t->imgname)){
        unlink('../public/img/programs/thumbnail/'.$t->imgname);
      }
      if(is_file('../public/img/programs/'.$t->imgname)){
        unlink('../public/img/programs/'.$t->imgname);
      }
    }
  }
  if(is_dir('../public/img/programs/'.$id)){
    $this->_deletefolder('../public/img/programs/'.$id);
  }
  if(!$tbp->delete() || !$tbpi->delete() || !$tbpage->delete()){
    foreach ($tbp->getMessages() as $message) {
      $this->flash->error((string) $message);
    }
    foreach ($tbpi->getMessages() as $message) {
      $this->flash->error((string) $message);
    }
    foreach ($tbpage->getMessages() as $message) {
      $this->flash->error((string) $message);
    }
  }else{
    $this->flash->success('Program was deleted. <a href="#" class="close remove-1x" data-dismiss="alert" aria-label="close">&times;</a>');
    header( 'Refresh:1; url=/admin/programlist', true, 303);
  }

}
private function _deletefolder($path){
  if (is_dir($path) === true)
  {
    $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::CHILD_FIRST);

    foreach ($files as $file)
    {
      if (in_array($file->getBasename(), array('.', '..')) !== true)
      {
        if ($file->isDir() === true)
        {
          rmdir($file->getPathName());
        }

        else if (($file->isFile() === true) || ($file->isLink() === true))
        {
          unlink($file->getPathname());
        }
      }
    }

    return rmdir($path);
  }

  else if ((is_file($path) === true) || (is_link($path) === true))
  {
    return unlink($path);
  }

  return false;
}
private function _insertRoles($id, $roles){
  //Delete if there is existing
  $this->modelsManager->executeQuery('DELETE FROM Tbluserroles WHERE Tbluserroles.userID = '.$id);
  // Inserting using placeholders
  $phql = "INSERT INTO Tbluserroles ( userID, userRoles) "
  . "VALUES (:userid:, :role:)";
  foreach($roles as $r){
    if(!empty($r)){
      $this->modelsManager->executeQuery($phql,
        array(
          'userid'     => $id,
          'role' => $r,
          )
        );
    }
  }
}
private function _insertPostCat($id, $programs, $type){
  //Delete if there is existing

  $this->modelsManager->executeQuery('DELETE FROM Tblpostcat WHERE Tblpostcat.postID = '.$id." AND Tblpostcat.relatedtype = '$type'");
  // Inserting using placeholders

  $phql = "INSERT INTO Tblpostcat ( postID, relatedID, relatedtype) "
  . "VALUES (:id:, :prog:, :type:)";

  if($type == 'programs'){
    foreach($programs as $p){
      if($this->request->getPost('chkProg'.$p->programID)){
        $this->modelsManager->executeQuery($phql,
          array(
            'id'     => $id,
            'prog' => $p->programID,
            'type' => $type
            ));
      }

    }

  }elseif($type == 'pages'){
    foreach($programs as $p){
      if($this->request->getPost('chkPages'.$p->pageID)){
        $this->modelsManager->executeQuery($phql,
          array(
            'id'     => $id,
            'prog' => $p->pageID,
            'type' => $type
            )
          );
      }

    }
  }

}
private function _deletePost($id){
  $param=null;
  if(is_array($id)){
    $newid = array();
    foreach ($id as $i) {
      $param .= 'postID = '. $this->filter->sanitize($i, array("int")) . ' OR ';
    }
    $param = substr($param, 0, -4);
  }else{
    $id = $this->filter->sanitize($id, array("int"));
    $param = 'postID=' . $id;
  }

  $post = Tblpost::find($param);
  if (!$post) {
    $this->flash->error("Post was not found");
  }
  $postcat = Tblpostcat::find($param);
  if (!$post) {
    $this->flash->error("Post was not found");
  }

  if (!$post->delete() && !$postcat->delete()) {
    foreach ($post->getMessages() as $message) {
      $this->flash->error((string) $message);
    }
    foreach ($postcat->getMessages() as $message) {
      $this->flash->error((string) $message);
    }
    return $this->forward("admin/allnewspost");
  } else {
    $this->flash->success("Post News has been successfully deleted.");
  }
}


/*
* JP
* Announcements
*
*/
public function announcementsAction(){

  \Phalcon\Tag::prependTitle('Announcements | ');
    $this->session->remove("ann_search_text");
    $this->session->remove("ann_filter_date");
    $this->session->remove("ann_filter_date_from");
    $this->session->remove("ann_filter_date_to");
    $this->persistent->searchAnnParams = null;
  $this->view->menu = $this->_menuActive('annrole');
  $today = strtotime('today');
  $this->view->setVar("today", $today);
  //search all announcement
  $phql=Tblannouncements::find();
  $result = $phql;
  $dataArray = array();
  foreach ($result as $key => $value) {
      $dataArray[] = array(
          'annID'=>$value->annID,
          'annTitle'=>$value->annTitle,
          'annDesc'=>$this->_truncateHtml($value->annDesc),
          'annDate'=>$value->annStart,
          'annEnd'=>$value->annEnd,
          'annStatus'=>$value->annStat
          );
  }
  //deactivate data if the announcement expired
  if ($dataArray) {
    foreach ($dataArray as $data) {
      if ($data['annEnd'] < strtotime('today')) {
        $ann = Tblannouncements::findFirst('annID='.$data['annID']);
        $ann->annStatus = "deactivate";
        if ($ann->save()) {
          // code...
        }
      }
      else if($data['annEnd'] < strtotime('today') AND $data['annStatus'] == "deactivate"){
        $ann = Tblannouncements::findFirst('annID='.$data['annID']);
        $ann->annStatus = "deactivate";
        if ($ann->save()) {
          // code...
        }
      }
      else {
        // code...
        $ann = Tblannouncements::findFirst('annID='.$data['annID']);
        if($data['annEnd'] < strtotime('today') AND $data['annStatus'] == "deactivate"){
          $ann->annStatus = "activated";
        }
        if ($ann->save()) {
          // code...
        }
      }
    }
  }


  $numberPage = 1;
  $this->persistent->searchAnnParams = null;
  if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
    Tag::resetInput();
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $this->_deleteAnn($id);
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      // $query = Criteria::fromInput($this->di, 'Tblusers', array('userName' => $this->request->getPost('search_text'), 'userFirstname' => $this->request->getPost('search_text')));
      // $this->persistent->searchUserParams = $query->getParams();
      // $this->session->set("user_search_text", $this->request->getPost('search_text'));
      $keyword = $this->request->getPost('search_text');
      $this->session->set("user_search_text", $keyword);
    } else {
      // $numberPage = $this->request->getQuery("page", "int");
      $searchtext = $this->session->get("user_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "activate") {
      $id = $this->request->getPost('recordID');
      if(!empty($id)){
        $ann = Tblannouncements::findFirst('annID='+$id);
        $ann->annStatus = "activated";
        $timeEnd = $ann->annEnd; 
        if($timeEnd < strtotime('today')){      
          return $this->dispatcher->forward(array(
          'controller' => 'admin',
          'action' => 'editannouncement',
          'params' => array($id)
          ));
        }else{
          if($ann->save()){
            $this->flash->success("Announcement has been activated");
          }else {
            $this->flash->error("Something went wrong. Please try again later.");
          }
        }
      }
    }elseif ($this->request->isPost() && $this->request->getPost('action') == "deactivate") {
      $id = $this->request->getPost('recordID');
      if(!empty($id)){
        $ann = Tblannouncements::findFirst('annID='+$id);
        $ann->annStatus = "deactivated";
        if($ann->save()){
          $this->flash->success("Announcement has been deactivated");
        } else {
          $this->flash->error("Something went wrong. Please try again later.");
        }
      }
    }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
    $id = $this->request->getPost('tbl_id');
    if(!empty($id)){
      $this->_deleteAnn($id);
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      // $query = Criteria::fromInput($this->di, 'Tblusers', array('userName' => $this->request->getPost('search_text'), 'userFirstname' => $this->request->getPost('search_text')));
      // $this->persistent->searchUserParams = $query->getParams();
      // $this->session->set("user_search_text", $this->request->getPost('search_text'));
      $keyword = $this->request->getPost('search_text');
      $this->session->set("user_search_text", $keyword);
    } else {
      // $numberPage = $this->request->getQuery("page", "int");

      $searchtext = $this->session->get("user_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';

    }
    /**/
  }elseif($this->request->isPost() && $this->request->getPost('action') == "edit"){
    $id = $this->request->getPost('recordID');

    if(!empty($id)){

      return $this->dispatcher->forward(array(
        'controller' => 'admin',
        'action' => 'editannouncement',
        'params' => array($id)
        ));
    }

  }elseif($this->request->isPost() && $this->request->getPost('clear_search')){

    $this->session->remove("ann_search_text");
    $this->session->remove("ann_filter_date");
    $this->session->remove("ann_filter_date_from");
    $this->session->remove("ann_filter_date_to");
    $this->persistent->searchAnnParams = null;

   // unset($_POST);
  }else{

    if ($this->request->isPost()) {

      $keyword = trim($this->request->getPost('search_text'));
      $query = Criteria::fromInput($this->di, 'Tblannouncements', array('annTitle' => $keyword));
      $this->persistent->searchAnnParams = $query->getParams();
      $this->session->set("ann_search_text", $keyword);

    } else {

      $numberPage = $this->request->getQuery("page", "int");


    }
  }


  $parameters = array();
  if ($this->persistent->searchAnnParams) {
    $parameters = $this->persistent->searchAnnParams;
  }

  // SORTING
  // Added a server side sorting on all tables using the columns shown
  // Dont forget to insert na href params on the view
  $sort = $this->request->getQuery("sort");
  $order = "annID DESC"; // default order selection
  $this->view->titleHref = "annTitle-asc";

  $this->view->statusHref = "annStatus-asc";
  $this->view->dateHref = "annDate-asc";
  $this->view->startHref = "annStart-asc";
  $this->view->endHref = "annEnd-asc";
  $arr = explode("-", $sort);
  switch ($arr[0]) {
    case 'annTitle':
    $arr[1] == "asc" ? $this->view->titleHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->titleIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'annStatus':
    $arr[1] == "asc" ? $this->view->statusHref = $arr[0] . "-desc" : "";
    $order = "annEnd $arr[1]";
    $this->view->statusIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'annDate':
    $arr[1] == "asc" ? $this->view->dateHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->dateIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'annStart':
    $arr[1] == "asc" ? $this->view->startHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->startIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'annEnd':
    $arr[1] == "asc" ? $this->view->endHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->endIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    default:
    break;
  }

  $builder = $this->modelsManager->createBuilder()
  ->columns('annID, annTitle, annDate, annStart, annEnd, annStatus')
  ->from('Tblannouncements')->orderBy($order);

  if(!empty($parameters)){

    $this->flash->notice('Search results for "' . $this->session->get("ann_search_text") .'"');
    $builder->andWhere($parameters['conditions'], $parameters['bind']);

  } else {

    $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):"";
    $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):"";

    if(($fromDate && $toDate) || $this->session->has("ann_filter_date") ) {

      if($fromDate > $toDate) {

        return $this->flash->warning("Invalid request. End date must be later than the start date.");
      }

      if($this->session->has("ann_filter_date")) {

        if(!$fromDate && !$toDate){

          $fromDate = trim($this->session->get('ann_filter_date_from'));
          $toDate = trim($this->session->get('ann_filter_date_to'));
        }

      } else {

        $this->session->set("ann_filter_date", true);
        $this->session->set("ann_filter_date_from", $fromDate);
        $this->session->set("ann_filter_date_to", $toDate);
      }

      $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
      $builder->where("annDate BETWEEN $fromDate AND $toDate");
    }
  }

  $paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
    "builder" => $builder,
    "order" => "annDate DESC",
    "limit"=> 10,
    "page" => $numberPage
    ));

  // Get the paginated results
  $this->view->page = $paginator->getPaginate();

  /*get date today*/
  $this->view->day=time();
  // $this->view->active="activated";
}

/*
* Add new annoucemnets
*/
public function createannocementsAction(){

  \Phalcon\Tag::prependTitle('Create Announcements | ');
  $this->view->menu = $this->_menuActive('annrole');
  $form = new CreateannForm();
  $form->csrf = $this->security->getToken();
  $this->view->titleError = null;
  $this->view->contentError = null;
        $postcheck = Tblannouncements::findFirst('annTitle="'.$this->request->getPost('ann_title', 'striptags').'"');

        // $startDateConvert = $this->request->getPost('ann_start');
        // $convertStartDate = new DateTime($startDateConvert, new DateTimeZone('Asia/Taipei'));
        // $startDate = strtotime($convertStartDate);
        //
        // $endDateConvert = $this->request->getPost('ann_end');
        // $convertStartDate = new DateTime($endDateConvert, new DateTimeZone('Asia/Taipei'));
        // $endDate = strtotime($convertStartDate);
        //
        // $timeNowConvert = time();
        // $convertTimeNow = new DateTime($timeNowConvert, new DateTimeZone('Asia/Taipei'));
        // $timeNow = strtotime($convertTimeNow);

        // var_dump(time(),$startDate, $endDate);

        $startDate = strtotime($this->request->getPost('ann_start'));
        $endDateSet = new DateTime($this->request->getPost('ann_end'));

        $endDateSet->setTime(23, 59, 59);
        $endDateSet = $endDateSet->format('Y-m-d H:i:s');
        $endDate = strtotime($endDateSet);

        $dateNow = strtotime(date("Y-m-d H:i:s"));
        // var_dump(time(),$startDate, $endDate);
        // $view->disable();
        $ann = new Tblannouncements();
        $ann->assign(array(
          'annTitle' => trim($this->request->getPost('ann_title', 'striptags')),
          'annDesc' => trim($this->request->getPost('ann_content')),
          'annStart' => $startDate,
          'annEnd' => $endDate,
          'annDate' => $dateNow
          ));

  if($this->request->isPost()){
    /*if($this->security->getSessionToken() == $this->request->getPost('csrf')){*/
      $validation = new Phalcon\Validation();
        $this->view->titleErr = null;
        $this->view->contentErr = null;
        $this->view->startErr = null;
        $this->view->endErr = null;
//Title validation
        $validation ->add('ann_title', new PresenceOf(array(
        'message' => 'Title is required',
        )));
//Content validation
        $validation ->add('ann_content', new PresenceOf(array(
        'message' => 'Content is required',
        )));
//Start date validation
        $validation ->add('ann_start', new PresenceOf(array(
        'message' => 'Start date is required',
        )));
//End date validation
        $validation ->add('ann_end', new PresenceOf(array(
        'message' => 'End date is required',
        )));

      if ($form->isValid($this->request->getPost()) != true) {

        if(count($validation->validate($_POST))){
//execute validation for username
        foreach ($validation->getMessages()->filter('ann_title') as $message) {
          $this->view->titleErr .= "<div class='label label-danger' >".$message."</div> ";
        }
//execute validation for username
        foreach ($validation->getMessages()->filter('ann_content') as $message) {
          $this->view->contentErr .= "<div class='label label-danger' >".$message."</div> ";
        }
//execute validation for start date
        foreach ($validation->getMessages()->filter('ann_start') as $message) {
          $this->view->startErr .= "<div class='label label-danger' >".$message."</div> ";
        }
//execute validation for end date
        foreach ($validation->getMessages()->filter('ann_end') as $message) {
          $this->view->endErr .= "<div class='label label-danger' >".$message."</div> ";
        }

        }else if($postcheck==true){

          $this->flash->error('Announcement title already taken.');

        }else{

          if($startDate > $endDate){

            $this->flash->error("Start date should not be greater than the End date");

          }else{

            // var_dump($this->request->getPost('ann_end'));
            // $view->disable();
              if($ann->save()){

                $this->flash->success("Announcement has been successfully created.");
                header( 'Refresh:1; url=/admin/announcements', true, 303);
              }

              if (!$ann->save()) {

              $this->flash->error($ann->getMessages());
            }
            // else {
            //   $this->flash->success("Announcement was created successfully.");
            //   Tag::resetInput();
            // }
          }

        }
      }

    /*}else{
    Tag::resetInput();
  }*/
}

$this->view->form = $form;
}

/*
* View announcement
*/

public function viewannouncementAction($id){
  \Phalcon\Tag::prependTitle('Announcements | ');
  $this->view->menu = $this->_menuActive('annrole');
  $numberPage = 1;

  if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
    Tag::resetInput();
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $this->_deleteAnn($id);
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      // $query = Criteria::fromInput($this->di, 'Tblusers', array('userName' => $this->request->getPost('search_text'), 'userFirstname' => $this->request->getPost('search_text')));
      // $this->persistent->searchUserParams = $query->getParams();
      // $this->session->set("user_search_text", $this->request->getPost('search_text'));
      $keyword = $this->request->getPost('search_text');
      $this->session->set("user_search_text", $keyword);
    } else {
      // $numberPage = $this->request->getQuery("page", "int");
      $searchtext = $this->session->get("user_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
  }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
    $id = $this->request->getPost('tbl_id');
    if(!empty($id)){
      $this->_deleteAnn($id);
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      // $query = Criteria::fromInput($this->di, 'Tblusers', array('userName' => $this->request->getPost('search_text'), 'userFirstname' => $this->request->getPost('search_text')));
      // $this->persistent->searchUserParams = $query->getParams();
      // $this->session->set("user_search_text", $this->request->getPost('search_text'));
      $keyword = $this->request->getPost('search_text');
      $this->session->set("user_search_text", $keyword);
    } else {
      // $numberPage = $this->request->getQuery("page", "int");
      $searchtext = $this->session->get("user_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
  }elseif($this->request->isPost() && $this->request->getPost('action') == "edit"){
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      return $this->dispatcher->forward(array(
        'controller' => 'admin',
        'action' => 'editannouncement',
        'params' => array($id)
        ));
    }
  }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
    $this->session->remove("ann_search_text");
    $this->session->remove("ann_filter_date");
    $this->session->remove("ann_filter_date_from");
    $this->session->remove("ann_filter_date_to");
    $this->persistent->searchAnnParams = null;
    unset($_POST);
  }else{
    if ($this->request->isPost()) {
      $keyword = trim($this->request->getPost('search_text'));
      $query = Criteria::fromInput($this->di, 'Tblannouncements', array('annTitle' => $keyword));
      $this->persistent->searchAnnParams = $query->getParams();
      $this->session->set("ann_search_text", $keyword);
    } else {
      $numberPage = $this->request->getQuery("page", "int");
    }
  }


  $parameters = array();
  if ($this->persistent->searchAnnParams) {
    $parameters = $this->persistent->searchAnnParams;
  }

  // SORTING
  // Added a server side sorting on all tables using the columns shown
  // Dont forget to insert na href params on the view
  $sort = $this->request->getQuery("sort");
  $order = "annEnd DESC"; // default order selection
  $this->view->titleHref = "annTitle-asc";
  $this->view->statusHref = "annStatus-asc";
  $this->view->dateHref = "annDate-asc";
  $this->view->startHref = "annStart-asc";
  $this->view->endHref = "annEnd-asc";
  $arr = explode("-", $sort);
  switch ($arr[0]) {
    case 'annTitle':
    $arr[1] == "asc" ? $this->view->titleHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->titleIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'annStatus':
    $arr[1] == "asc" ? $this->view->statusHref = $arr[0] . "-desc" : "";
    $order = "annEnd $arr[1]";
    $this->view->statusIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'annDate':
    $arr[1] == "asc" ? $this->view->dateHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->dateIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'annStart':
    $arr[1] == "asc" ? $this->view->startHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->startIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'annEnd':
    $arr[1] == "asc" ? $this->view->endHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->endIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    default:
    break;
  }

  $builder = $this->modelsManager->createBuilder()
  ->columns('annID, annTitle, annDate, annStart, annEnd')
  ->from('Tblannouncements')->orderBy($order);

  if(!empty($parameters)){
    $this->flash->notice('Search results for "<strong>' . $this->session->get("ann_search_text") .'</strong>"');
    $builder->andWhere($parameters['conditions'], $parameters['bind']);
  } else {
    $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):"";
    $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):"";
    if(($fromDate && $toDate) || $this->session->has("ann_filter_date") ) {
      if($fromDate > $toDate) {
        return $this->flash->warning("Invalid request. End date must be later than the start date.");
      }
      if($this->session->has("ann_filter_date")) {
        if(!$fromDate && !$toDate){
          $fromDate = trim($this->session->get('ann_filter_date_from'));
          $toDate = trim($this->session->get('ann_filter_date_to'));
        }
      } else {
        $this->session->set("ann_filter_date", true);
        $this->session->set("ann_filter_date_from", $fromDate);
        $this->session->set("ann_filter_date_to", $toDate);
      }
      $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
      $builder->where("annDate BETWEEN $fromDate AND $toDate");
    }
  }

  $paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
    "builder" => $builder,
    "order" => "annDate DESC",
    "limit"=> 10,
    "page" => $numberPage
    ));

  // Get the paginated results
  $this->view->page = $paginator->getPaginate();

  /*get date today*/
  $this->view->day=time();
  $this->view->menu = $this->_menuActive('annrole');
  $ann = Tblannouncements::findFirst('annID='.$id);
  $this->view->ann = $ann;
  $this->view->form = $form;
}

/*
* Edit announcement
*/
public function editannouncementAction($id){
  // $view->disaled();
  \Phalcon\Tag::prependTitle('Edit Announcement | ');
  $this->view->menu = $this->_menuActive('annrole');
  $form = new CreateannForm();
  $this->view->titleError = null;
  $this->view->contentError = null;
  $ann = Tblannouncements::findFirst('annID='.$id);
  $this->view->ann = $ann;

  // $startDate = strtotime($this->request->getPost('ann_start'));
  // $endDate = strtotime($this->request->getPost('ann_end'));

  $startDate = strtotime($this->request->getPost('ann_start'));
  $endDateSet = new DateTime($this->request->getPost('ann_end'));

  $endDateSet->setTime(23, 59, 59);
  $endDateSet = $endDateSet->format('Y-m-d H:i:s');
  $endDate = strtotime($endDateSet);


  $ann->assign(array(
    'annTitle' => trim($this->request->getPost('ann_title', 'striptags')),
    'annDesc' => trim($this->request->getPost('ann_content')),
    'annStart' => $startDate,
    'annEnd' => $endDate
    ));

  $validation = new Phalcon\Validation();
  $this->view->titleErr = null;
  $this->view->contentErr = null;
  $this->view->startErr = null;
  $this->view->endErr = null;
//Title validation
  $validation ->add('ann_title', new PresenceOf(array(
  'message' => 'Title is required',
  )));
//Content validation
  $validation ->add('ann_content', new PresenceOf(array(
  'message' => 'Content is required',
  )));
//Start date validation
  $validation ->add('ann_start', new PresenceOf(array(
  'message' => 'Start date is required',
  )));
//End date validation
  $validation ->add('ann_end', new PresenceOf(array(
  'message' => 'End date is required',
  )));

  if($this->request->isPost() && $this->request->getPost('ann_update')){

    if($this->security->getSessionToken() == $this->request->getPost('csrf')){

       // if ($form->isValid($this->request->getPost()) == false) {

      $annExist = false;

      if($this->request->getPost("orig_title") != $this->request->getPost("ann_title")){

        $annExist = Tblannouncements::findFirst('annTitle="'.$this->request->getPost('ann_title', 'striptags').'"');

      }
    /*if($this->security->getSessionToken() == $this->request->getPost('csrf')){*/
         if(count($validation->validate($_POST))){
//execute validation for username
        foreach ($validation->getMessages()->filter('ann_title') as $message) {
          $this->view->titleErr .= "<div class='label label-danger' >".$message."</div> ";
        }
//execute validation for username
        foreach ($validation->getMessages()->filter('ann_content') as $message) {
          $this->view->contentErr .= "<div class='label label-danger' >".$message."</div> ";
        }
//execute validation for start date
        foreach ($validation->getMessages()->filter('ann_start') as $message) {
          $this->view->startErr .= "<div class='label label-danger' >".$message."</div> ";
        }
//execute validation for end date
        foreach ($validation->getMessages()->filter('ann_end') as $message) {
          $this->view->endErr .= "<div class='label label-danger' >".$message."</div> ";
        }

        }elseif($annExist==true){
        $this->flash->error('Announcement title already taken.');
      }else{



        if($startDate > $endDate){

          $this->flash->error("Start date should not be greater than the End date");

        }else{

          if (!$ann->save()) {

            $this->flash->error($ann->getMessages());

          } else {

            Tag::resetInput();
            $form->clear();
            $this->flash->success("Announcement was updated successfully");
            $this->response->redirect('/admin/announcements'); 
            // header( 'Refresh:1; url=/admin/announcements', true, 303);

          }

        }
      }
      // }
    }else{
      Tag::resetInput();
    }
  }
  $this->view->form = $form;
  $ann = Tblannouncements::findFirst('annID='.$id);
  $this->view->ann = $ann;

  // var_dump($ann->annTitle);
  // die();
}

/*
* delete announcement
*/



/*
* Partners
*/

private function _createPartnerMenu($index = false, $curPartID = null){
  $userSess = $this->session->get('auth');
  $userID = $userSess['abk_id'];
  $partnerCount = count(Tblusers::find("userLevel = 2"));
  $userInfo = Tblusers::findFirst('userID='.$userID);

  if($partnerCount){
    $this->view->partnerscount = $partnerCount;
  }

  /*$phql = 'SELECT * FROM Tblpartners ' .
  ' INNER JOIN Tblusers ON Tblpartners.userID = Tblusers.userID WHERE Tblusers.userStatus = "active"';
  $result = $this->modelsManager->executeQuery($phql);
  */
  if($userInfo->userLevel==2){
    $partnerInfo = Tblpartners::findFirst('userID='.$userID);
    $partnerID = $partnerInfo->partnerID;
    $partnerUserAdmin = false;
  }else{
    $partnerUserAdmin = true;
  }

  $AdminMenu = '
  <li><a href="/admin/createpartner">Create Partner</a></li>
  <li><a href="/admin/partners">All ABK Partners <span class="badge">'.$partnerCount.'</span></a></li>
  ';

  if(!$index){
    $menu = '';
    if($partnerUserAdmin){
      $this->view->showBackToList = true;
      $menu .= $AdminMenu;
    }else{
      if(!is_null($curPartID) && $partnerID != $curPartID){
        $this->response->redirect('admin/partnersinfo/'.$partnerID);
      }else{
        $menu .='
        <li><a href="/admin/partnersinfo/'.$partnerID.'">Partner Details</a></li>
        <li><a href="/admin/partnersinfo/'.$partnerID.'/events">Current Events</a></li>
        <li><a href="/admin/partnersPictures/'.$partnerID.'">Pictures</a></li>
        <li><a href="/admin/partners">All ABK Partners</a></li>';
        $this->view->showBackToList = false;
      }
    }

    return $menu;
  }else{
    if(!$partnerUserAdmin){
      if($this->router->getActionName() == "partners"){
        $this->view->showBackToList = true;
        return $menu ='
        <li><a href="/admin/partnersinfo/'.$partnerID.'">Partner Details</a></li>
        <li><a href="/admin/partnersinfo/'.$partnerID.'/events">Current Events</a></li>
        <li><a href="/admin/partnersPictures/'.$partnerID.'">Pictures</a></li>
        <li><a href="/admin/partners">All ABK Partners</a></li>';
      }
      $this->response->redirect('admin/partnersinfo/'.$partnerID);
    }else{
      $this->view->showBackToList = true;
      return $AdminMenu;
    }
  }
}

public function partnersAction(){
  \Phalcon\Tag::prependTitle('Partners | ');
  $this->view->menu = $this->_menuActive('partrole', array('partnerIndex'=>true));
  $auth = $this->session->get('auth');
  $this->view->userlevel = $auth['abk_userlevel'];

  if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
    Tag::resetInput();
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "activate") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $user = Tblusers::findFirst('userID='+$id);
      $user->userStatus = "active";
      if($user->save()){
        $this->flash->success("Partner Account has been activated");
      } else {
        $this->flash->error("Something went wrong. Please try again later.");
      }
    }
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "deactivate") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $user = Tblusers::findFirst('userID='+$id);
      $user->userStatus = "deactivate";
      if($user->save()){
        $this->flash->success("Partner Account has been deactivated");
      } else {
        $this->flash->error("Something went wrong. Please try again later.");
      }
    }
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $this->_deleteUser($id);
      $this->flash->success("Partner Account has been successfully deleted.");
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      $keyword = $this->request->getPost('search_text');
      $this->session->set("user_search_text", $keyword);
    } else {
      // $numberPage = $this->request->getQuery("page", "int");
      $searchtext = $this->session->get("user_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
  }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
    $id = $this->request->getPost('tbl_id');
    if(!empty($id)){
      $this->_deleteUser($id);
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      $keyword = $this->request->getPost('search_text');
      $this->session->set("user_search_text", $keyword);
    } else {
      // $numberPage = $this->request->getQuery("page", "int");
      $searchtext = $this->session->get("user_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
  }elseif($this->request->isPost() && $this->request->getPost('action') == "edit"){
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      return $this->dispatcher->forward(array(
        'controller' => 'admin',
        'action' => 'editPartner',
        'params' => array($id)
        ));
    }
  }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
    $this->session->remove("partner_search_text");
    $this->session->remove("partner_filter_date");
    $this->session->remove("partner_filter_date_from");
    $this->session->remove("partner_filter_date_to");
    unset($_POST);
  }else{
    if ($this->request->isPost()) {
      $keyword = trim($this->request->getPost('search_text'));
      $this->session->set("partner_search_text", $keyword);
    }else{
      $searchtext = $this->session->get("partner_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
  }

  if(!empty($keyword)){
    $where = ' AND (partnerName LIKE "%'.$keyword.'%" OR userName LIKE "%'.$keyword.'%"
    OR CONCAT(userLastname, ", ", userFirstname) LIKE "%'.$keyword.'%" OR userLastname LIKE "%'.$keyword.'%" OR userFirstname LIKE "%'.$keyword.'%"  OR userEmail LIKE "%'.$keyword.'%")';
    $this->flash->notice('Search results for "<strong>' . $keyword .'</strong>"');
  }
   if($this->request->getPost('submit')){
  //else{
  //   $where = null;
    $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):"";
    $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):"";
    if(($fromDate && $toDate) || $this->session->has("partner_filter_date") ) {
      if($fromDate > $toDate) {
        return $this->flash->warning("Invalid request or end date should be required. End date must be later than the start date.");
      }
      if($fromDate < $toDate){
          $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
          if(empty($fromDate)){
          return $this->flash->warning("From date is required!");
        }
      }
      if($this->session->has("partner_filter_date")) {
        if(!$fromDate && !$toDate){
          return $this->flash->warning("From and To date is required!");
          $fromDate = trim($this->session->get('partner_filter_date_from'));
          $toDate = trim($this->session->get('partner_filter_date_to'));
        }
      } else {
        $this->session->set("partner_filter_date", true);
        $this->session->set("partner_filter_date_from", $fromDate);
        $this->session->set("partner_filter_date_to", $toDate);
      }
      // $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
      $where = " AND (dateCreated BETWEEN $fromDate AND $toDate)";
    }else{
        return $this->flash->warning("From and To date is required!");
    }
  }


  $numberPage = $this->request->getQuery("page", "int");
  $numberPage = empty($numberPage)?1:$numberPage;

  // SORTING
  // Added a server side sorting on all tables using the columns shown
  // Dont forget to insert na href params on the view
  $sort = $this->request->getQuery("sort");
  $order = "ORDER BY dateCreated ASC";
  $this->view->usernameHref = "userName-asc";
  $this->view->nameHref = "name-asc";
  $this->view->partnernameHref = "partnerName-asc";
  $this->view->datecreatedHref = "dateCreated-asc";
  $this->view->positionHref = "userPosition-asc";
  $this->view->statusHref = "userStatus-asc";
  $arr = explode("-", $sort);
  switch ($arr[0]) {
    case 'userName':
    $arr[1] == "asc" ? $this->view->usernameHref = $arr[0] . "-desc" : "";
    $order = " ORDER BY $arr[0] $arr[1]";
    $this->view->usernameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'name':
    $arr[1] == "asc" ? $this->view->nameHref = $arr[0] . "-desc" : "";
    $order = " ORDER BY $arr[0] $arr[1]";
    $this->view->nameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'partnerName':
    $arr[1] == "asc" ? $this->view->partnernameHref = $arr[0] . "-desc" : "";
    $order = " ORDER BY $arr[0] $arr[1]";
    $this->view->partnernameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'dateCreated':
    $arr[1] == "asc" ? $this->view->datecreatedHref = $arr[0] . "-desc" : "";
    $order = " ORDER BY $arr[0] $arr[1]";
    $this->view->datecreatedIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'userPosition':
    $arr[1] == "asc" ? $this->view->positionHref = $arr[0] . "-desc" : "";
    $order = " ORDER BY $arr[0] $arr[1]";
    $this->view->positionIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'userStatus':
    $arr[1] == "asc" ? $this->view->statusHref = $arr[0] . "-desc" : "";
    $order = " ORDER BY $arr[0] $arr[1]";
    $this->view->statusIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    default:
    break;
  }

  $phql = 'SELECT
  Tblusers.userID,
  Tblusers.userName,
  CONCAT(Tblusers.userLastname, ", ",Tblusers.userFirstname) AS name,
  Tblusers.dateCreated,
  Tblusers.userPosition,
  Tblusers.userStatus,
  Tblpartners.partnerID,
  Tblpartners.partnerName
  FROM Tblusers
  LEFT JOIN Tblpartners ON Tblpartners.userID = Tblusers.userID
  WHERE userLevel != 0'.$where.$order;
  $result = $this->modelsManager->executeQuery($phql);

  $dataArray = array();
  foreach ($result as $key => $value) {
    $dataArray[] = array(
      'userID'=>$value->userID,
      'username'=>$value->userName,
      'name'=>$value->name,
      'dateCreated'=>$value->dateCreated,
      'position'=>$value->userPosition,
      'status'=>$value->userStatus,
      'partnerID'=>$value->partnerID,
      'partnerName'=>$value->partnerName
      );
  }


  $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
    "data" => $dataArray,
    "limit"=> 10,
    "page" => $numberPage
    ));
  $this->view->page = $paginator->getPaginate();


}

/*
* View partner Info
*/
public function partnersinfoAction($id, $tab = null){
  \Phalcon\Tag::prependTitle('Partner Details | ');
  $numberPage = $this->request->getQuery("page", "int");
  $numberPage = !empty($numberPage)?$numberPage:1;
  $this->view->menu = $this->_menuActive('partrole', array('partnerCurPartID'=>$id));
  $this->view->partnerID = $id;
  $partner = Tblpartners::findFirst('partnerID='.$id);
  $this->view->partner = $partner;
  $auth = $this->session->get('auth');
  $this->view->userlevel = $auth['abk_userlevel'];

  $this->view->tab = $this->_partnersTab($id, $tab);
  if($tab == 'events'){
    \Phalcon\Tag::prependTitle('Current Events | ');
    $eventParameters = array();

    if($this->request->isPost() && $this->request->getPost('clear_search')){
      $this->session->remove("event_search_text");
      $this->persistent->searchEventParams = null;
      unset($_POST);
    }elseif($this->request->isPost() && $this->request->getPost('searchBtn')){
      if ($this->request->isPost()) {
        $query = Criteria::fromInput($this->di, 'Tblpartnerevents', array('eventTitle' => $this->request->getPost('search_text'), 'partnerID'=>$id));
        $this->persistent->searchEventParams = $query->getParams();
        $this->session->set("event_search_text", $this->request->getPost('search_text'));
      }
    }elseif($this->request->isPost()){
      $delIDSingle = $this->filter->sanitize($this->request->getPost('delEventSingle'), array("int"));
      if(!empty($delIDSingle)){
        $delEvent = Tblpartnerevents::findFirst('eventID='.$delIDSingle);
        $delEvent->delete();
        $this->flash->success("Event has been deleted successfully.");
      }elseif($this->request->getPost('tbl_id')){
        $param = null;
        foreach ($this->request->getPost('tbl_id') as $eID) {
          $delEvntID = $this->filter->sanitize($eID, array("int"));
          $param .= 'eventID = '. $delEvntID . ' OR ';
        }
        $param = substr($param, 0, -4);
        $delEvent = Tblpartnerevents::find($param);
        $delEvent->delete();
        $this->flash->success("Selected event(s) were deleted successfully.");
      }
    }


    if ($this->persistent->searchEventParams) {
      $eventParameters = $this->persistent->searchEventParams;
    }

    // SORTING
    // Added a server side sorting on all tables using the columns shown
    // Dont forget to insert na href params on the view
    $sort = $this->request->getQuery("sort");
    $order = "eventID";
    $this->view->titleHref = "eventTitle-asc";
    $this->view->venueHref = "eventVenue-asc";
    $this->view->dateHref = "eventDate-asc";
    $arr = explode("-", $sort);
    switch ($arr[0]) {
      case 'eventTitle':
      $arr[1] == "asc" ? $this->view->titleHref = $arr[0] . "-desc" : "";
      $order = "$arr[0] $arr[1]";
      $this->view->titleIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;
      case 'eventVenue':
      $arr[1] == "asc" ? $this->view->venueHref = $arr[0] . "-desc" : "";
      $order = "$arr[0] $arr[1]";
      $this->view->venueIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;
      case 'eventDate':
      $arr[1] == "asc" ? $this->view->dateHref = $arr[0] . "-desc" : "";
      $order = "$arr[0] $arr[1]";
      $this->view->dateIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;
      default:
      break;
    }

    $builder = $this->modelsManager->createBuilder()
    ->columns('eventID, eventTitle, eventVenue, eventDate, dateCreated, partnerID')
    ->from('Tblpartnerevents')->where('partnerID = '.$id);

    if(!empty($eventParameters)){
      $this->flash->notice('Search results for "<strong>' . $this->session->get("event_search_text") .'</strong>"');
      $builder->andWhere($eventParameters['conditions'], $eventParameters['bind']);
    }

    $builder->orderBy($order);

    $paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
      "builder" => $builder,
      "limit"=> 10,
      "page" => $numberPage
      ));
    $this->view->page = $paginator->getPaginate();
    $this->view->pick("admin/partnerscurrentevents");

  }elseif($tab == 'edit'){
    $this->view->partnerAlbumSelect = $this->_getPartnerAlbumSelect($id);
    $this->view->partnerGallery = $this->_getAllPictures(null, $id);

    if($this->request->isPost() && $this->request->getPost('saveInfo')){
      $partner->assign(array('partnerInfo'=>$this->request->getPost('partnerInfo')));
      $partner->save();
      $this->flash->success("Partner information updated successfully.");
    }
    $this->view->pick("admin/editPartnerInfo");
  }

}

public function editEventAction($eventID){
  \Phalcon\Tag::prependTitle('Event | ');
  $this->view->event = $event = Tblpartnerevents::findFirst('eventID='.$eventID);
  $id = $event->partnerID;
  $auth = $this->session->get('auth');
  $this->view->userlevel = $auth['abk_userlevel'];

  $this->view->viewOnly = isset($_REQUEST['viewonly'])?false:true;

  $this->view->menu = $this->_menuActive('partrole');
  $this->view->partnerID = $id;

  $this->view->partnerAlbumSelect = $this->_getPartnerAlbumSelect($id);
  $this->view->partnerGallery = $this->_getAllPictures(null, $id);

  $partner = Tblpartners::findFirst('partnerID='.$id);
  $this->view->partner = $partner;
  $this->view->tab = $this->_partnersTab($id, 'events');
  $form = new CreatepartnersForm(null, array('createPartnerEvent'=>true));

  if($this->request->isPost() && $this->request->getPost('savePartnerEvent')){
    if($this->security->getSessionToken() == $this->request->getPost('csrf')){
      if ($form->isValid($this->request->getPost()) != false) {
        $error = false;
        $data = array(
          'eventDate'=>strtotime($this->request->getPost('event_date', 'striptags')),
          'eventVenue'=>$this->request->getPost('event_venue', 'striptags'),
          'eventDetails' => $this->request->getPost('event_details')
          );
        if($this->request->getPost('event_name', 'striptags')!= $this->request->getPost('orig_event_name')){


          $eventExist = Tblpartnerevents::query()
          ->where("eventTitle = :name:")
          ->andWhere("partnerID = :partnerID:")
          ->bind(array("name" => $this->request->getPost('event_name'), "partnerID"=>$id))
          ->execute();

          if($eventExist->count() == true){
            $this->flash->error('Event title already exist');
            $error = true;
          }else{
            $data['eventTitle'] = $this->request->getPost('event_name', 'striptags');
          }
        }


        if(!$error){
          $event->assign($data);
          if (!$event->save()) {
            $this->flash->error($event->getMessages());
          } else {
            $this->flash->success("Event updated successfully.");
            Tag::resetInput();
          }
        }

      }
    }else{
      Tag::resetInput();
    }
  }

  $this->view->evtForm = $form;
}

public function createAlbumAjaxAction($fetch = null){
  $this->view->disable();

  if($this->request->isPost() && $this->request->isAjax()){
    $response = array();
    if($fetch){
      $albums = Tblpartnersalbums::find();
      $response['albums'] = '';
      foreach ($albums as $key => $value) {
        $response['albums'] .= '<option value="'.$value->albumID.'">'.$value->albumName.'</option>';
      }

    }else{
      $response['success'] = false;
      $response['message'] = 'Unable to save album name';
      if(!empty($_POST['partnerID']) && !empty($_POST['albumName'])){
        $albumName = trim(filter_var($_POST['albumName'], FILTER_SANITIZE_STRING));
        $partnerID = trim(filter_var($_POST['partnerID'], FILTER_SANITIZE_NUMBER_INT));

        //$albumCheck = Tblpartnersalbums::findFirst('albumName="'.$albumName.'"');

        $albumCheck = Tblpartnersalbums::query()
        ->where("albumName = :name:")
        ->andWhere("partnerID = :partnerID:")
        ->bind(array("name" => $albumName, "partnerID"=>$partnerID))
        ->execute();

        if($albumCheck->count() == true){
          $response['success'] = false;
          $response['message'] = 'Album name already exist';
        }else{
          $picturesPath = '../public/img/partnerspictures/';
          if(!file_exists($picturesPath.$partnerID)){
            mkdir($picturesPath.$partnerID);
          }
          $picturesPath .= $partnerID.'/';
          if (strpbrk($albumName, "\\/?%*:|\"<>") === FALSE) {
            if(mkdir($picturesPath.$albumName)){
              if(mkdir($picturesPath.$albumName.'/thumbnail')){
                $album = new Tblpartnersalbums();
                $album->assign(array(
                  'partnerID' => $partnerID,
                  'albumName' => $albumName,
                  'coverID' => 0,
                  'dateCreated' => time()
                  ));
                $album->save();

                $response['success'] = true;
                $response['message'] = 'New album created';

                $albums = Tblpartnersalbums::find();
                $response['albums'] = '<option vale="">--Album--</option>';
                foreach ($albums as $key => $value) {
                  $response['albums'] .= '<option value="'.$value->albumID.'">'.$value->albumName.'</option>';
                }
              }else{
                $response['success'] = false;
                $response['message'] = 'Unable to create thumbnail album '.$albumName;
              }
            }else{
              $response['success'] = false;
              $response['message'] = 'Unable to create album '.$albumName;
            }
          }
          else {
            $response['success'] = false;
            $response['message'] = 'Unable to create album '.$albumName.': Invalid directory name';
          }
        }
      }
    }
    echo json_encode($response);
  }
}

public function editAlbumAjaxAction(){
  $this->view->disable();

  $dataArray = array();
  $dataArray['success'] = false;
  $dataArray['message'] = '<span class="label label-danger">Album not found.</span>';
  if($this->request->isPost() && $this->request->isAjax()){
    $albumID = $this->request->getPost('albumID');
    $album = Tblpartnersalbums::findFirst('albumID = '.$albumID);
    if($this->request->getPost('action')=='get'){
      $html = '
      <div id="editAlbumResult"></div>
      <input type="hidden" id="saveHiddenAlbumID" name="saveHiddenAlbumID" value="'.$albumID.'">
      <label>Album Name</label>
      <input type="text" id="saveAlbumName" name="saveAlbumName" value="'.$album->albumName.'" placeholder="Album Name" class="form-control">
      ';
      $dataArray['success'] = true;
      $dataArray['message'] = $html;
    }elseif($this->request->getPost('action')=='save'){
      $albumName = trim(filter_var($_POST['albumName'], FILTER_SANITIZE_STRING));
      if($album->albumName!=$albumName){
        $albumCheck = Tblpartnersalbums::findFirst('albumName="'.$albumName.'"');
        if($albumCheck == true){
          $dataArray['success'] = false;
          $dataArray['message'] = '<span class="label label-danger">Album name already exist</span>';
        }else{
          $folder = '../public/img/partnerspictures/'.$this->request->getPost('partnerID').'/'.$album->albumName;
          if(!is_dir($folder)){
            $dataArray['success'] = false;
            $dataArray['message'] = '<span class="label label-danger">Album "'.$folder.'" not found.</span>';
          }else{
            $newFolder = '../public/img/partnerspictures/'.$this->request->getPost('partnerID').'/'.$albumName;
            rename($folder, $newFolder);
            $album->assign(array('albumName' => $albumName));
            $album->save();
            $dataArray['success'] = true;
            $dataArray['message'] = '<span class="label label-success">Album name updated.</span>';
          }
        }
      }else{
        $dataArray['success'] = true;
        $dataArray['message'] = '<span class="label label-success">Album name updated.</span>';
      }
    }elseif($this->request->getPost('action')=='delete'){
      /*
      $folder = '../public/img/partnerspictures/'.$album->partnerID.'/'.$album->albumName;
      $thumbFolder = '../public/img/partnerspictures/'.$album->partnerID.'/'.$album->albumName.'/thumbs';
      chmod($thumbFolder, 0777);
      chmod($folder, 0777);
      $picture = Tblpartnerspictures::find('albumID='.$albumID);
      if($picture == true){
      foreach ($picture as $key => $value) {
      unlink($folder.'/'.$value->pictureFilename);
      unlink($thumbFolder.'/'.$value->pictureFilename);
    }
    rmdir($thumbFolder);
    rmdir($folder);
    $picture->delete();
  }else{
  rmdir($thumbFolder);
  rmdir($folder);
  $picture->delete();
}
*/
$folder = '../public/img/partnerspictures/'.$album->partnerID.'/'.$album->albumName;
if($this->_deletefolder($folder)){
  $album->delete();
  $dataArray['success'] = true;
  $dataArray['message'] = '<span>Album deleted.</span>';
}else{
  $dataArray['success'] = false;
  $dataArray['message'] = '<span>Unable to delete folder.</span>';
}
}
}

echo json_encode($dataArray);
}


//partners pictures
public function partnersPicturesAction($id=1, $album = null){
  \Phalcon\Tag::prependTitle('Partner Pictures | ');
  $auth = $this->session->get('auth');
  $this->view->userlevel = $auth['abk_userlevel'];
  $numberPage = $this->request->getQuery("page", "int");
  $numberPage = !empty($numberPage)?$numberPage:1;

  $this->view->menu = $this->_menuActive('partrole', array('partnerCurPartID'=>$id));
  $this->view->partnerID = $id;
  $partner = Tblpartners::findFirst('partnerID='.$id);
  $this->view->partner = $partner;

  $this->view->tab = $this->_partnersTab($id, 'pictures');
  $this->view->albumID = $album;


  if(is_null($album)){
    $this->view->albumView = true;
    $andWhere = '';

    if($this->request->isPost() && $this->request->getPost('clear_search')){
      $this->session->remove("picture_search_album_text");
      unset($_POST);
    }elseif($this->request->isPost() && $this->request->getPost('searchBtn')){
      $andWhere .= ' AND Tblpartnersalbums.albumName LIKE "%'.$this->request->getPost('search_album_text').'%"';
      $this->session->set("picture_search_album_text", $this->request->getPost('search_album_text'));
    }elseif($this->request->isPost()){
      $albumIDs = $this->request->getPost('tbl_id');
      if(!empty($albumIDs)){
        $param = null;
        foreach ($albumIDs as $keyAlbum) {
          $keyAlbum = $this->filter->sanitize($keyAlbum, array("int"));
          $param .= 'partnerID = '. $keyAlbum . ' OR ';
          $delAlbum = Tblpartnersalbums::findFirst('albumID = '.$keyAlbum);
          $albumPath = '../public/img/partnerspictures/'.$id.'/'.$delAlbum->albumName;
          if(file_exists($albumPath)){
            if($this->_deletefolder($albumPath)){
              $delAlbum->delete();
            }
          }else{
            $delAlbum->delete();
          }

        }
        $this->flash->success("Selected albums were deleted successfully");
      }
    }

    if($this->session->get("picture_search_album_text")){
      $andWhere .= ' AND Tblpartnersalbums.albumName LIKE "%'.$this->session->get('picture_search_album_text').'%"';
      $this->flash->notice('Search results for "<strong>' . $this->session->get("picture_search_album_text") .'</strong>"');
    }

    // SORTING
    // Added a server side sorting on all tables using the columns shown
    // Dont forget to insert na href params on the view
    $sort = $this->request->getQuery("sort");
    $order = "Tblpartnersalbums.albumName";
    $this->view->nameHref = "albumName-asc";
    $this->view->picturesHref = "number_photos-asc";
    $this->view->dateHref = "dateCreated-asc";
    $arr = explode("-", $sort);
    switch ($arr[0]) {
      case 'albumName':
      $arr[1] == "asc" ? $this->view->nameHref = $arr[0] . "-desc" : "";
      $order = "Tblpartnersalbums.$arr[0] $arr[1]";
      $this->view->nameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;
      case 'number_photos':
      $arr[1] == "asc" ? $this->view->picturesHref = $arr[0] . "-desc" : "";
      $order = "$arr[0] $arr[1]";
      $this->view->picturesIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;
      case 'dateCreated':
      $arr[1] == "asc" ? $this->view->dateHref = $arr[0] . "-desc" : "";
      $order = "Tblpartnersalbums.$arr[0] $arr[1]";
      $this->view->dateIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;
      default:
      break;
    }

    $phql = 'SELECT
    COUNT(Tblpartnerspictures.pictureID) as number_photos,
    Tblpartnersalbums.albumName,
    Tblpartnersalbums.dateCreated,
    Tblpartnersalbums.albumID
    FROM Tblpartnersalbums
    LEFT JOIN Tblpartnerspictures ON Tblpartnersalbums.albumID = Tblpartnerspictures.albumID
    WHERE Tblpartnersalbums.partnerID='.$id.'
    '.$andWhere.'
    GROUP BY Tblpartnersalbums.albumName
    ORDER BY '.$order;
    $result = $this->modelsManager->executeQuery($phql);

    $dataArray = array();
    foreach ($result as $key => $value) {
      $dataArray[] = array(
        'picCount'=>(!$value->number_photos)?'empty':$value->number_photos,
        'albumID'=>$value->albumID,
        'albumName'=>$value->albumName,
        'dateCreated'=>$value->dateCreated
        );
    }

    $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
      "data" => $dataArray,
      "limit"=> 10,
      "page" => $numberPage
      ));
    $this->view->page = $paginator->getPaginate();

  }else{
    $this->view->albumView = false;
    $this->view->currentAlbum = $album;
    $albumCurrent = Tblpartnersalbums::findFirst('albumID='.$album);
    $this->view->albumCurrent = $albumCurrent->albumName;
    $this->view->albums = Tblpartnersalbums::find('partnerID = '.$id);

    $this->view->selectedAlbum = (!is_null($album))?$album:null;

    if($this->request->isPost() && $this->request->getPost('clear_search')){
      $this->session->remove("picture_search_text");
      unset($_POST);
    }
    if($this->request->isPost()){
      if($this->request->getPost('moveALbumID')!=''){
        $tbl_id = $this->request->getPost('tbl_id');
        $this->_movePictures($tbl_id, $_POST['moveALbumID']);
      }else{
        $pictureHiddenID = $this->request->getPost('pictureHiddenID');
        if(!empty($pictureHiddenID)){
          $this->_deletePicture($pictureHiddenID, $id, $album);
        }else{
          $tbl_id = $this->request->getPost('tbl_id');
          if(!empty($tbl_id)){
            $this->_deletePicture($tbl_id, $id, $album);
          }
        }
      }
    }

    $where = 'WHERE Tblpartnerspictures.partnerID = '.$id;
    $where .= (!empty($album))?' AND Tblpartnerspictures.albumID = '.$album:'';

    if($this->request->isPost() && $this->request->getPost('searchBtn')){
      $where .= ' AND pictureCaption LIKE "%'.$this->request->getPost('search_text').'%"';
      $this->session->set("picture_search_text", $this->request->getPost('search_text'));
    }

    if($this->session->get("picture_search_text")){
      $this->flash->notice('Search results for "<strong>' . $this->session->get("picture_search_text") .'</strong>"');
    }

    $phql = 'SELECT Tblpartnersalbums.albumName,
    Tblpartnerspictures.pictureID,
    Tblpartnerspictures.partnerID,
    Tblpartnerspictures.albumID,
    Tblpartnerspictures.pictureFilename,
    Tblpartnerspictures.pictureCaption,
    Tblpartnerspictures.pictureSize,
    Tblpartnerspictures.dateUploaded
    FROM Tblpartnerspictures
    LEFT JOIN Tblpartnersalbums ON Tblpartnersalbums.albumID = Tblpartnerspictures.albumID
    '.$where;
    $result = $this->modelsManager->executeQuery($phql);


    $dataArray = array();
    foreach ($result as $key => $value) {
      $dataArray[] = array(
        'pictureID'=>$value->pictureID,
        'partnerID'=>$value->albumID,
        'albumID'=>$value->partnerID,
        'albumName'=>$value->albumName,
        'pictureFilename'=>$value->pictureFilename,
        'pictureCaption'=>$value->pictureCaption,
        'pictureSize'=>$this->_bytesToSize1024($value->pictureSize),
        'dateUploaded'=>$value->dateUploaded,
        'path'=>$this->url->get().'img/partnerspictures/'.$value->partnerID.'/'.$value->albumName.'/'.$value->pictureFilename,
        'thumbPath'=>$this->url->get().'img/partnerspictures/'.$value->partnerID.'/'.$value->albumName.'/thumbnail/'.$value->pictureFilename
        );
    }

    $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
      //$paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
      "data" => $dataArray,
      "limit"=> 10,
      "page" => $numberPage
      ));
    $this->view->page = $paginator->getPaginate();
  }
}

private function _movePictures($id, $albumID){
  $param=null;
  if(is_array($id)){
    foreach ($id as $i) {
      $param .= 'pictureID = '. $this->filter->sanitize($i, array("int")) . ' OR ';
    }
    $param = substr($param, 0, -4);
  }else{
    $id = $this->filter->sanitize($id, array("int"));
    $param = 'pictureID=' . $id;
  }

  $picture = Tblpartnerspictures::find($param);
  if (!$picture) {
    $this->flash->error("Unable to move picture(s), please try again.");
  }else{
    $fromAlbum = Tblpartnersalbums::findFirst('albumID='.$picture[0]->albumID);
    $album = Tblpartnersalbums::findFirst('albumID='.$albumID);
    $fromFolder = '../public/img/partnerspictures/'.$picture[0]->partnerID.'/'.$fromAlbum->albumName;
    $fromThumbFolder = '../public/img/partnerspictures/'.$picture[0]->partnerID.'/'.$fromAlbum->albumName.'/thumbnail';
    $folder = '../public/img/partnerspictures/'.$album->partnerID.'/'.$album->albumName;
    $thumbFolder = '../public/img/partnerspictures/'.$album->partnerID.'/'.$album->albumName.'/thumbnail';
    #chmod($folder, 0777);
    #hmod($thumbFolder, 0777);
    #chmod($fromFolder, 0777);
    #chmod($fromThumbFolder, 0777);

    $albumName = $album->albumName;

    $movedCount = 0;
    for($x=0; $x < count($picture); $x++) {
      $fileName = $picture[$x]->pictureFilename;
      $theName = pathinfo($fileName, PATHINFO_FILENAME);
      $theExt = pathinfo($fileName, PATHINFO_EXTENSION);

      $path = $folder;

      $uniqueFilename = $fileName;
      $increment = null;
      while(file_exists($path.'/'.$theName . $increment . '.' . $theExt)) {
        $increment++;
        $uniqueFilename = $theName . $increment . '.' . $theExt;
      }

      if(rename($fromFolder.'/'.$fileName, $folder.'/'.$uniqueFilename) && rename($fromThumbFolder.'/'.$fileName, $thumbFolder.'/'.$uniqueFilename)){
        $picture[$x]->assign(array('albumID' => $albumID));
        $picture[$x]->save();
        $movedCount++;
      }
    }

    #chmod($folder, 0755);
    #chmod($thumbFolder, 0755);
    #chmod($fromFolder, 0755);
    #chmod($fromThumbFolder, 0755);

    $successMess = ($movedCount == 1)?'Picture moved to ':$movedCount.' Pictures moved to';
    $this->flash->success($successMess." ".$albumName);
  }
}

/*delete picture*/
private function _deletePicture($id, $partnerID, $albumID){
  $param=null;
  if(is_array($id)){
    $newid = array();
    foreach ($id as $i) {
      $param .= 'pictureID = '. $this->filter->sanitize($i, array("int")) . ' OR ';
    }
    $param = substr($param, 0, -4);
  }else{
    $id = $this->filter->sanitize($id, array("int"));
    $param = 'pictureID=' . $id;
  }

  $picture = Tblpartnerspictures::find($param);
  if (!$picture) {
    $this->flash->error("Unable to delete picture(s), please try again.");
  }else{
    $album = Tblpartnersalbums::find('albumID='.$albumID);
    $folder = '../public/img/partnerspictures/'.$album[0]->partnerID.'/'.$album[0]->albumName.'/';
    $thumbFolder = '../public/img/partnerspictures/'.$album[0]->partnerID.'/'.$album[0]->albumName.'/thumbnail/';

    $deleteCount = 0;
    for($x=0; $x < count($picture); $x++) {
      $fileName = $picture[$x]->pictureFilename;

      if(unlink($folder.$fileName) && unlink($thumbFolder.$fileName)){
        $picture[$x]->delete();
        $deleteCount++;
      }
    }

    $successMess = ($deleteCount == 1)?'Picture deleted':$deleteCount.' Pictures deleted';
    $this->flash->success($successMess);
  }
}

private function _bytesToSize1024($bytes, $precision = 2)
{
  // human readable format -- powers of 1024
  //
  $unit = array('B','KB','MB','GB','TB','PB','EB');

  return @round(
    $bytes / pow(1024, ($i = floor(log($bytes, 1024)))), $precision
    ).' '.$unit[$i];
}


/*
*ajax upload for partner's pictures
*/

public function partnerAjaxEditPictureAction($pictureID){
  $this->view->disable();
  $picture = Tblpartnerspictures::findFirst('pictureID='.$pictureID);

  if($this->request->isPost() && $this->request->isAjax()){
    $data = array();
    $pictureCaption = filter_var($_POST['pictureCaption'], FILTER_SANITIZE_STRING);
    if(!empty($pictureCaption)){
      if($picture->albumID != $_POST['editAlbumSelect']){
        $album = Tblpartnersalbums::findFirst('albumID = '.$picture->albumID);

        $folder = '../public/img/partnerspictures/'.$album->partnerID.'/'.$album->albumName.'/';
        $thumbFolder = '../public/img/partnerspictures/'.$album->partnerID.'/'.$album->albumName.'/thumbnail/';

        $changeAlbum = Tblpartnersalbums::findFirst('albumID = '.$_POST['editAlbumSelect']);
        $changeFolder = '../public/img/partnerspictures/'.$album->partnerID.'/'.$changeAlbum->albumName.'/';
        $changeThumbFolder = '../public/img/partnerspictures/'.$album->partnerID.'/'.$changeAlbum->albumName.'/thumbnail/';

        /*                    chmod($folder, 0777);
        chmod($thumbFolder, 0777);

        chmod($changeAlbum, 0777);
        chmod($changeThumbFolder, 0777);*/


        $theName = pathinfo($picture->pictureFilename, PATHINFO_FILENAME);
        $theExt = pathinfo($picture->pictureFilename, PATHINFO_EXTENSION);

        $path = $changeFolder;

        $uniqueFilename = $picture->pictureFilename;
        $increment = null;
        while(file_exists($path.$theName . $increment . '.' . $theExt)) {
          $increment++;
          $uniqueFilename = $theName . $increment . '.' . $theExt;
        }

        rename($thumbFolder.$picture->pictureFilename, $changeThumbFolder.$uniqueFilename);
        rename($folder.$picture->pictureFilename, $changeFolder.$uniqueFilename);

        $data = array(
          'pictureCaption' => $pictureCaption,
          'albumID' => $_POST['editAlbumSelect'],
          );
        $picture->assign($data);
        $picture->save();

        if($this->request->getPost('albumCover')){
          $album->assign(array('coverID'=>$pictureID));
          $album->save();
        }

        $data['rename'] = $picture->pictureID;

        /*                    chmod($folder, 0755);
        chmod($thumbFolder, 0755);
        chmod($changeAlbum, 0755);
        chmod($changeThumbFolder, 0755);*/
        $data['newCaption'] = $pictureCaption;
        $data['resultText'] = '<span class="label label-success">You have successfully updated the picture\'s caption</span>';
      }else{
        $picture->assign(array('pictureCaption' => $pictureCaption));
        $picture->save();
      }

      $album = Tblpartnersalbums::findFirst('albumID = '.$picture->albumID);
      if(isset($_POST['coverSelected'])){
        $album->assign(array('coverID'=>$pictureID));
        $album->save();
      }

      $data['post']= $_POST;
      $data['result'] = true;
      $data['newCaption'] = $pictureCaption;
      $data['resultText'] = '<span class="label label-success">You have successfully updated the picture\'s caption</span>';
    }else{
      $data['result'] = false;
      $data['resultText'] = '<span class="label label-danger">Please fill in caption</span>';
    }

    echo json_encode($data);
  }else{
    $albums = Tblpartnersalbums::find('partnerID = '.$picture->partnerID);
    $options = '';
    foreach ($albums as $key => $value) {
      $selected = ($picture->albumID == $value->albumID)?'selected':'';
      $options .= '<option '.$selected.' value="'.$value->albumID.'">'.$value->albumName.'</option>';
    }

    $album = Tblpartnersalbums::findFirst('albumID = '.$picture->albumID);

    $coverChecked = $album->coverID == $pictureID?'checked':'';

    echo '
    <select id="editAlbumSelect" name="editAlbumSelect" class="albums form-control">
      <option value="">--Album--</option>
      '.$options.'
    </select>
    <br />
    <div><img src="'.$this->url->get().'img/partnerspictures/'.$album->partnerID.'/'.$album->albumName.'/thumbnail/'.$picture->pictureFilename.'" alt="'.$picture->pictureCaption.'" title="'.$picture->pictureCaption.'"></div>
    <br />
    <div>
      <input type="hidden" name="pictureID" id="pictureID" value="'.$pictureID.'">
      <label for="pictureCaption">Caption</label>
      <div id="editResult"></div>
      <textarea class="form-control" name="pictureCaption" id="pictureCaption">'.$picture->pictureCaption.'</textarea>
      <br /><label><input '.$coverChecked.' value="1" type="checkbox" name="albumCover" id="albumCover"> Make album cover</label>
    </div>
    ';
  }
}

public function partnerAjaxUploadPictureAction($partnerID, $albumID){
  $this->view->disable();

  $album = Tblpartnersalbums::findFirst('albumID = '.$albumID);
  $albumName = $album->albumName;

  $data = array();
  $folder = '../public/img/partnerspictures/'.$partnerID.'/'.$albumName.'/';
  $thumbFolder = '../public/img/partnerspictures/'.$partnerID.'/'.$albumName.'/thumbnail/';

  /*chmod($folder, 0777);
  chmod($thumbFolder, 0777);*/

  $filesResult = array();

  $maxFileSize = '';
  $validFileTypes = array(
    'png' => 'image/png',
    'jpe' => 'image/jpeg',
    'jpeg' => 'image/jpeg',
    'jpg' => 'image/jpeg',
    'gif' => 'image/gif',
    'bmp' => 'image/bmp'
    );

  if($this->request->hasFiles() && $this->request->isAjax()){
    $uploads = $this->request->getUploadedFiles();
    foreach($uploads as $upload){

      $filename = $upload->getname();
      $extension = strtolower(substr($filename, strrpos($filename, '.')+1));
      $mimeType = $upload->getRealType();
      $fileSize = $upload->getSize();

      $newName = md5(uniqid(rand(), true)).'-'.strtolower($filename);
      $path = $folder.$newName;
      $thumPath = $thumbFolder.$newName;


      //validate file type
      $validFiletype = false;
      if (array_key_exists($extension, $validFileTypes)) {
        $validFiletype = ($validFileTypes[$extension] == $mimeType)?true:false;
      }

      $fileStatus = '';
      if(!$validFiletype){
        $fileStatus = 'Invalid';
      }elseif(!$upload->moveTo($path)){
        $fileStatus = 'Failed';
      }else{
        $thumb = new Phalcon\Image\Adapter\GD($path);
        $thumb->resize(200, 200);
        $thumb->save($thumPath);

        $picture = new Tblpartnerspictures();
        $picture->assign(array(
          'partnerID' => $partnerID,
          'albumID' => $albumID,
          'pictureFilename' => $newName,
          'pictureSize'=>$fileSize,
          'pictureCaption' => $filename,
          'dateUploaded' => time()
          ));
        $picture->save();
        $fileStatus = 'Success';
      }

      $filesResult[] = array(
        'caption'=>$filename,
        'filename'=>$newName,
        'status'=>$fileStatus
        );
    }

    $filesResultText = '';
    foreach ($filesResult as $key) {
      $filesResultText .= '<li>'.$key['caption'].' - '.$key['status'].'</li>';
    }

    $data['result'] = $filesResultText;
    /*chmod($folder, 0755);
    chmod($thumbFolder, 0755);*/
  }else{
    $response->redirect();
  }
  echo json_encode($data);
}

/*tab for partners*/
private function _partnersTab($partnerID, $active = null){
  $partnerUrl = $this->url->get().'admin/partnersinfo/'.$partnerID.'/';
  $tab = '<ul class="nav nav-tabs" role="tablist">';
  $tab .= ($active == '' || $active == 'edit')?'<li class="active">':'<li>';
  $tab .= '<a href="'.$partnerUrl.'">Partner Details</a></li>';
  $tab .= ($active == 'events')?'<li class="active">':'<li>';
  $tab .= '<a href="'.$partnerUrl.'events">Current Events</a></li>';
  $tab .= ($active == 'pictures')?'<li class="active">':'<li>';
  $tab .= '<a href="/admin/partnersPictures/'.$partnerID.'">Pictures</a></li>';
  $tab .='</ul>';
  return $tab;
}

/*
*
* Create new partner event
*
*/
public function createpartnereventAction($id){
  $this->view->menu = $this->_menuActive('partrole');
  $auth = $this->session->get('auth');
  $this->view->userlevel = $auth['abk_userlevel'];
  $this->view->Ename = null;
  $this->view->Edate = null;
  $this->view->Evenue = null;
  $this->view->Edetails = null;
  $this->view->errnotice=null;

  $this->view->partnerID = $id;
  $partner = Tblpartners::findFirst('partnerID='.$id);
  $this->view->partner = $partner;
  $this->view->tab = $this->_partnersTab($id, 'events');
  $form = new CreatepartnersForm(null, array('createPartnerEvent'=>true));
  $this->view->evtForm = $form;
  $this->view->partnerAlbumSelect = $this->_getPartnerAlbumSelect($id);
  $this->view->partnerGallery = $this->_getAllPictures('all', $id);

  if($this->request->getPost('savePartnerEvent')){
    // $this->view->errnotice .= "<div class='label label-success'>OK.</div>";
     // if($this->security->getSessionToken() == $this->request->getPost('csrf')){
     //validation counting params
      $w="";$x="";$y="";$z="";
      $a = trim($this->request->getpost('event_name','striptags'));
      $b = trim($this->request->getpost('event_date','striptags'));
      $c = trim($this->request->getpost('event_venue','striptags'));
      $d = $this->request->getpost('event_details');
      //check if event name alreadyh exists
      $eventExist = Tblpartnerevents::findFirst([
          'conditions' => "eventTitle='". $a ."'"
      ]);
      //programmatical validation long method
        //validate event name
      if(empty($a)){
            $this->view->Ename .="<div class='label label-danger'>Name is required.</div>";
            $w="a";
      }elseif($eventExist){
            $this->view->Ename .="<div class='label label-danger'>Name already exists.</div>";
            $w="a";
      }else{
            $this->view->Ename .="";
            $w="OK";
      }
      //validate event date
      if(empty($b)){
            $this->view->Edate .="<div class='label label-danger'>Date is required.</div>";
            $y="a";
      }else{
            $this->view->Edate .="";
            $y="OK";
      }
      //validate event venue
      if(empty($c)){
            $this->view->Evenue .="<div class='label label-danger'>Venue is required</div>";
            $x="a";
      }else{
            $this->view->Evenue .="";
            $x="OK";
      }
      //validate event details
      if(empty($d)){
            $this->view->Edetails .="<div class='label label-danger'>Details is required</div>";
            $z="a";
      }else{
            $this->view->Edetails .="";
            $z="OK";
      }
        //progarmmatical validation long method

      //check if fields are valid
      if($w == "OK" && $x == "OK" && $y == "OK" && $z == "OK"){
          //create new event
          $event = new Tblpartnerevents();
          $event->assign(array(
           'partnerID'=>$id,
           'eventTitle' => trim($this->request->getPost('event_name', 'striptags')),
           'eventDate'=>strtotime($this->request->getPost('event_date', 'striptags')),
           'eventVenue'=>trim($this->request->getPost('event_venue', 'striptags')),
           'eventDetails' => trim($this->request->getPost('event_details')),
           'dateCreated' => time()
            ));
          if(!$event->save()){
            $this->flash->error($event->getMessages());
          }else{
           header('refresh:1; url=/admin/partnersinfo/'.$id.'/events');
            $this->view->errnotice .="<div class='alert alert-success'>New event created successfully.</div>";
          }
      }
  }
  $this->view->evtForm = $form;
}


public function createpartnerAction(){
  \Phalcon\Tag::prependTitle('Create Partner | ');
  $this->view->menu = $this->_menuActive('partrole');
  $form = new CreatepartnersForm(null, array('partner'=>true));

  if ($this->request->isPost()) {
    if($this->security->getSessionToken() == $this->request->getPost('csrf')){
    $roles = array();
    $roles[] = $partrole = 'partrole';
    if(empty($umrole) && empty($annrole) && empty($pmrole) && empty($partrole) && empty($townrole) && empty($conrole) && empty($pagerole) && empty($extrole) && empty($postrole)){
      $this->view->roleError = "<div class='label label-danger'>At least one role should be selected.</div>";
    }
    if ($form->isValid($this->request->getPost()) != false) {
      $userName = Tblusers::findFirst("userName='".$this->request->getPost('username', 'striptags')."'");
      $userEmail = Tblusers::findFirst("userEmail='".$this->request->getPost('email', 'striptags')."'");
      if($userName==true) {
         $this->view->userfnamecheck .= "<div class='label label-danger'>Username already taken.</div>";
      }elseif(!preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i', $this->request->getPost('email', 'striptags'))){
          $this->view->emailcheck .="<div class='label label-danger'>Invalid email format.</div>";
      }else if($userEmail==true) {
        $this->view->emailcheck .= "<div class='label label-danger'>Email Address already taken</div>";
      } else {
        $user = new Tblusers();
        $password = sha1($this->request->getPost('password'));
        $user->assign(array(
          'userName' => trim($this->request->getPost('username', 'striptags')),
          'userEmail' => trim($this->request->getPost('email', 'striptags')),
          'userPassword' => trim($password),
          'userFirstname' => trim($this->request->getPost('firstname', 'striptags')),
          'userLastname' => trim($this->request->getPost('lastname', 'striptags')),
          'userMiddlename' => trim($this->request->getPost('middlename', 'striptags')),
          'userAddress' => trim($this->request->getPost('address', 'striptags')),
          'userCompany' => trim($this->request->getPost('company', 'striptags')),
          'userContact' => trim($this->request->getPost('contact', 'striptags')),
          'userPosition' => trim($this->request->getPost('position', 'striptags')),
          'userStatus' => trim($this->request->getPost('status', 'striptags')),
          'userLevel' => 2,
          'dateCreated' => time()
          ));
        // if($user){
        //   $this->flash->success("Partner has been successfully addedsssss.");
        // }

        if (!$user->save()) {
          $this->flash->error($user->getMessages());
        } else {
          $this->_insertRoles($user->userID, $roles);
          $partner = new Tblpartners();
          $partner->assign(array(
            'userID'=>$user->userID,
            'partnerName'=>$this->request->getPost('partnerName', 'striptags'),
            'partnerInfo'=>$this->request->getPost('partnerInfo')
            ));
          $save = $partner->save();
          // $this->flash->success("A Partner is successfully added");
          // send email on success registration
          if ($save) {
            $username = $user->userName;
            $password = $this->request->getPost('password');
            $abk_user = $this->session->get('auth');
            $fullname = $abk_user['abk_fullname'];

            $body = "
            <h4>An ABK Partner Account has been created</h4>
            <p>Congratulations, a new ABK Partner Account has been created for you by your ABK Foundation administrator, $fullname.</p>
            <p>Your username is : <strong>$username</strong></p>
            <p>Your password is : <strong>$password</strong></p>
            <p>You can login your account at <a href='http://angbayanko.org/admin' target='_blank'>http://angbayanko.org/admin</a></p>
            <p>Sincerely,</p>
            <p>ABK Foundation</p>
            ";

            $sub = "ABK Foundation: Registration Successful";
            $emailsend = $user ->userEmail;

             if($this->sendmailer($emailsend, $sub, $body)) {
              $this->flash->success("An ABK Partner has been successfully created. Account details were sent on the registered email address.");
              header('refresh:1; /admin/partners');  
             }
              //else {
             // $this->flash->success("Partner has been successfully added.");
            // }
          }
          $this->view->roleError = null;
          Tag::resetInput();
        }
      }
    }else {
       echo 'invalid form';
    //VALIDATION IF FORM IS INVALID
    $userName = Tblusers::findFirst("userName='".$this->request->getPost('username', 'striptags')."'");
    $userEmail = Tblusers::findFirst("userEmail='".$this->request->getPost('email', 'striptags')."'");
    if($userName==true) {
      $this->view->userfnamecheck .= "<div class='label label-danger'>Username already taken.</div>";
    }
    if($userEmail==true) {
      $this->view->emailcheck .= "<div class='label label-danger'>Email address already taken.</div>";
    }

    //more validations
      $this->view->usernameErr = null;
      $this->view->emailErr= null;
      $this->view->fnameErr= null;
      $this->view->lnameErr= null;
      $this->view->contactErr= null;
      $this->view->passErr= null;
      $this->view->repassErr= null;
      $this->view->parnternameErr= null;
      $this->view->parnterinfoErr= null;

      $validation = new Phalcon\Validation();
      //username validation
        $validation ->add('username', new PresenceOf(array(
        'message' => 'Username is required.',
        )));

        $validation ->add('username', new StringLength(array(
        'min' => 6,
        'messageMinimum' => 'Username should have at least 6 minimum characters'
        )));

        $validation ->add('username', new Regex(array(
        'message'    => 'Username is invalid. Avoid spaces and symbols.',
        'pattern'    => '/^[a-zA-Z0-9._]+/',
        'allowEmpty' => false
        )));
//email validation
        $validation ->add('email', new PresenceOf(array(
        'message' => 'Email Address is required',
        )));

        $validation ->add('email', new Regex(array(
        'message'    => 'Not a valid email format',
        'pattern'    => '/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i',
        'allowEmpty' => false
        )));
//password validation
        $validation ->add('password', new PresenceOf(array(
        'message' => 'Password is required',
        )));

        $validation ->add('password', new StringLength(array(
        'min' => 8,
        'messageMinimum' => 'Password should have at least 8 minimum characters'
        )));
//firstname validation
        $validation ->add('firstname', new PresenceOf(array(
        'message' => 'Firstname is required',
        )));
//lastname validation
        $validation ->add('lastname', new PresenceOf(array(
        'message' => 'Lastname is required',
        )));
//coontact validation
        $validation ->add('contact', new PresenceOf(array(
        'message' => 'Contact number is required',
        )));
        $validation ->add('contact', new StringLength(array(
        'min' => 11,
        'messageMinimum' => 'Contact number should only have 11 digits'
        )));
        $validation ->add('contact', new Regex(array(
        'message'    => 'Not a valid contact number format',
        'pattern'    => '/^[0-9]*$/',
        'allowEmpty' => false
        )));
//partnername validation
        $validation ->add('partnerName', new PresenceOf(array(
        'message' => 'Partnername is required',
        )));
//partnerinfo validation
        $validation ->add('partnerInfo', new PresenceOf(array(
        'message' => 'Partner Details is required',
        )));
      //validation start
      if($password != $repassword){
          $this->view->repassErr.= "<div class='label label-danger'>Password doesn't match confirmation.</div> ";
          }else if(count($validation->validate($_POST))){
            //execute validation for username
                  foreach ($validation->getMessages()->filter('username') as $message) {
                    $this->view->usernameErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
            //execute validation for email
                  foreach ($validation->getMessages()->filter('email') as $message) {
                    $this->view->emailErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
            //execute validation for password
                    foreach ($validation->getMessages()->filter('password') as $message) {
                       $this->view->passErr .= "<div class='label label-danger'>".$message."</div> ";
                    }
            //execute repassword
                    if($password != $repassword){
                      $this->view->repassErr.="<div class='label label-danger'>Password doesn't match confirmation.</div> ";
                    }
            //execute validation for firstname and lastname
                  foreach ($validation->getMessages()->filter('firstname') as $message) {
                    $this->view->fnameErr .= "<div class='label label-danger'>".$message."</div> ";
                  }

                  foreach ($validation->getMessages()->filter('lastname') as $message) {
                    $this->view->lnameErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
            //execute validation for contact
                  foreach ($validation->getMessages()->filter('contact') as $message) {
                    $this->view->contactErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
            //execute validation for partnerName
                  foreach ($validation->getMessages()->filter('partnerName') as $message) {
                    $this->view->partnernameErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
            //execute validation for contact
                  foreach ($validation->getMessages()->filter('partnerInfo') as $message) {
                    $this->view->partnerinfoErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
      }
    //more validations end
    //VALIDATION IF FORM IS INVALID
    }

    } else {
    //     Tag::resetInput();
    }
  }
  $this->view->form = $form;
}

public function editPartnerAction($userID){

  \Phalcon\Tag::prependTitle('Edit Partner | ');
  $this->view->menu = $this->_menuActive('partrole');
  $this->view->roleError = null;
  $user = Tblusers::findFirst($userID);
  $this->view->user = $user;
  $partner = Tblpartners::findFirst('userID='.$userID);
  $this->view->partner = $partner;
  $this->view->passError = null;
  $this->view->repassError = null;
  $this->view->oldpassError = null;
  $form = new CreatepartnersForm(null, array('partner'=>true, 'edit'=>true));


  if ($this->request->isPost()) {
    if($this->request->getPost('update_password')){
      $validation = new Phalcon\Validation();
      $validation->add('password', new PresenceOf(array(
        'message' => 'The password is required'
        )));
      /* $validation->add('oldpassword', new PresenceOf(array(
      'message' => 'The old password is required',
      'cancelOnFail' => true
      )));*/
      $validation->add('password', new StringLength(array(
        'max' => 20,
        'min' => 8,
        'messageMaximum' => 'Thats an exagerated password.',
        'messageMinimum' => 'Password should be Minimum of 8 characters.'
        )));

      $validation->add('repassword', new PresenceOf(array(
        'message' => 'Retyping your password is required'
        )));
      $validation->add('repassword', new Confirmation(array(
        'message' => 'Password doesn\'t match confirmation',
        'with' => 'password'
        )));

      if(count($validation->validate($_POST))){
      /* foreach ($validation->getMessages()->filter('oldpassword') as $message) {
      $this->view->oldpassError .= "<div class='label label-danger'>".$message."</div> ";
    }*/
    foreach ($validation->getMessages()->filter('password') as $message) {
      $this->view->passError .= "<div class='label label-danger'>".$message."</div> ";
    }
    foreach ($validation->getMessages()->filter('repassword') as $message) {
      $this->view->repassError .= "<div class='label label-danger'>".$message."</div> ";
    }
  }else{
    /*if(sha1($this->request->getPost('oldpassword')) == $user->userPassword) {
    $user->assign(array(
    'userPassword' => sha1($this->request->getPost('password'))
  ));
} else {
$this->view->oldpassError = "<div class='label label-danger'>Wrong Password</div> ";
Tag::resetInput();
}*/

if (!$user->save()) {
  $this->flash->error($user->getMessages());

} else {

  $this->flash->success("Partner password was updated successfully. <a href='/admin/partners'>Back to Partner List</a>");

  Tag::resetInput();
}
}
}elseif($this->request->getPost('update_button')){

  if ($form->isValid($_POST)) {
    $form->getMessages();
  }else{
    $userName = Tblusers::findFirst("userName='".$this->request->getPost('username', 'striptags')."' AND userID != ".$userID);
    $userEmail = Tblusers::findFirst("userEmail='".$this->request->getPost('email', 'striptags')."' AND userID != ".$userID);
    if($userName==true){
      $this->flash->error("Username already taken");
    }elseif($userEmail==true){
      $this->flash->error("Email Address already taken");
    }else{
      $user->assign(array(
        'userName' => trim($this->request->getPost('username', 'striptags')),
        'userEmail' => trim($this->request->getPost('email', 'striptags')),
        'userFirstname' => trim($this->request->getPost('firstname', 'striptags')),
        'userLastname' => trim($this->request->getPost('lastname', 'striptags')),
        'userMiddlename' => trim($this->request->getPost('middlename', 'striptags')),
        'userAddress' => trim($this->request->getPost('address', 'striptags')),
        'userCompany' => trim($this->request->getPost('company', 'striptags')),
        'userContact' => trim($this->request->getPost('contact', 'striptags')),
        'userPosition' => trim($this->request->getPost('position', 'striptags')),
        'userStatus' => trim($this->request->getPost('status', 'striptags'))
        ));

      $hiddenPartnerName = $this->request->getPost('hiddenPartnerName', 'striptags');
      $partnerName = $this->request->getPost('partnerName', 'striptags');

      $checkPartnerName = false;
      if($partnerName != $hiddenPartnerName){
        $checkPartnerName = Tblpartners::findFirst('partnerName="'.$partnerName.'" AND userID != '.$userID);
      }

      if($checkPartnerName==true){
        $this->flash->error("Partner name already taken");
      }else{
        $partner->assign(array(
          'partnerName'=>$partnerName,
          'partnerInfo'=>$this->request->getPost('partnerInfo')
          ));
        if (!$user->save() || !$partner->save()) {
          $this->flash->error($user->getMessages());
        }else {
          header('refresh:1;url=/admin/partners');
          $this->view->errsucc.="<div class='alert alert-success'>Partner was updated successfully.</div>";
          $this->view->roleError = null;
        }
      }
    }
  }
}
}

$this->tag->setDefault("status", $user->userStatus);
$this->view->form = $form;
}

private function _deleteAnn($id){
  $param=null;
  if(is_array($id)){
    $newid = array();
    foreach ($id as $i) {
      $param .= 'annID = '. $this->filter->sanitize($i, array("int")) . ' OR ';
    }
    $param = substr($param, 0, -4);
  }else{
    $id = $this->filter->sanitize($id, array("int"));
    $param = 'annID=' . $id;
  }

  $post = Tblannouncements::find($param);
  if (!$post) {
    $this->flash->error("Post was not found");
  }


  if (!$post->delete()) {
    foreach ($post->getMessages() as $message) {
      $this->flash->error((string) $message);
    }

    return $this->forward("admin/announcements");
  } else {
    $this->flash->success("Announcement was successfully deleted.");
  }
}


//new upload gallery
public function ajaxmoveuploadpartnerpicturesAction($filename, $albumID){
  $this->view->disable();
  $path = null;

  $album = Tblpartnersalbums::findFirst('albumID='.$albumID);
  $partnerID = $album->partnerID;
  $partnerAlbum = $album->albumName;

  $name = pathinfo($filename, PATHINFO_FILENAME);
  $ext = pathinfo($filename, PATHINFO_EXTENSION);

  $path = '../public/img/partnerspictures/'.$partnerID.'/'.$partnerAlbum;

  $uniqueFilename = $filename;
  $increment = null;
  while(file_exists($path.'/'.$name . $increment . '.' . $ext)) {
    $increment++;
    $uniqueFilename = $name . $increment . '.' . $ext;
  }

  if(is_file('../public/server/php/files/'.$filename)){
    if(!is_dir($path)){
      mkdir($path);
    }

    rename('../public/server/php/files/'.$filename, $path.'/'.$uniqueFilename);
  }
  if(is_file('../public/server/php/files/thumbnail/'.$filename)){
    if(!is_dir($path.'/thumbnail')){
      mkdir($path.'/thumbnail');
    }
    rename('../public/server/php/files/thumbnail/'.$filename, $path.'/thumbnail/'.$uniqueFilename);
  }

  $filename = $uniqueFilename;

  $picture = new Tblpartnerspictures();
  $picture->assign(array(
    'partnerID' => $partnerID,
    'albumID' => $albumID,
    'pictureFilename' => $filename,
    'pictureSize'=>filesize($path.'/'.$filename),
    'pictureCaption' => $filename,
    'dateUploaded' => time()
    ));
  $picture->save();
  $lastID = $picture->pictureID;


  $imgpath = $this->url->get().'img/partnerspictures/'.$partnerID.'/'.$partnerAlbum.'/'.$filename;
  $newfilename = strlen($filename) > 15 ? substr($filename,0,15)."...".$ext : $filename;

  echo $html = '
  <div class="program-digital-assets-library pull-left" style="position: relative">
    <div style="padding-left: 25px; width: 100%; word-wrap:break-word;">'.$newfilename.'</div>
    <a href="'.$imgpath.'" class="prettyPhoto[pp_gal]"><img src="'.$imgpath.'" alt=""></a>
    <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="'.$imgpath.'">
    <button class="btn btn-xs btn-danger delete-recent-upload-pic" data-picture-id="'.$lastID.'" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
    <div class="clearfix"></div>
  </div>
  ';
}

public function deleteRecentPicUploadAction($pictureID){
  $this->view->disable();
  $response = array();
  $phql = 'SELECT Tblpartnerspictures.partnerID, Tblpartnerspictures.albumID, Tblpartnerspictures.pictureFilename, Tblpartnersalbums.albumName FROM Tblpartnerspictures
  LEFT JOIN Tblpartnersalbums ON Tblpartnersalbums.albumID = Tblpartnerspictures.albumID WHERE pictureID = '.$pictureID;
  $result = $this->modelsManager->executeQuery($phql);

  if(!empty($result)){
    $path = '../public/img/partnerspictures/'.$result[0]['partnerID'].'/'.$result[0]['albumName'].'/';
    $imgPath = $path.$result[0]['pictureFilename'];
    $thumbPath = $path.'thumbnail/'.$result[0]['pictureFilename'];

    $deleteImg = $deleteImgThumb = false;


    if(is_file($imgPath)){
      $deleteImg = unlink($imgPath);
    }

    if(is_file($thumbPath)){
      $deleteImgThumb = unlink($thumbPath);
    }

    if($deleteImg && $deleteImgThumb){
      $picture = Tblpartnerspictures::findFirst('pictureID='.$pictureID);
      $picture->delete();
      $response['success'] = true;
    }else{
      /*                $response['deleteImg'] = $deleteImg;
      $response['deleteImgThumb'] = $deleteImgThumb;
      $response['message'] = 'unable to delete file '.$thumbPath;*/
      $response['success'] = false;
    }
  }else{
    $response['success'] = false;
  }

  echo json_encode($response);
}

public function deleteRecentNewsletterUploadAction($pictureID){
  $this->view->disable();
  $response = array();
  // $phql = "SELECT * FROM Tblpagesimg WHERE imgID = '".$pictureID . "'";
  // $result = $this->modelsManager->executeQuery($phql);
  $result = Tblpagesimg::findFirst("imgID="+$pictureID);

  if(!empty($result)){
    $path = '../public/'.$result->imgpath;
    $imgPath = $path.$result->fileName;
    $thumbPath = $path.'thumbnail/'.$result->fileName;

    $deleteImg = $deleteImgThumb = false;


    if(is_file($imgPath)){
      $deleteImg = unlink($imgPath);
    }

    if(is_file($thumbPath)){
      $deleteImgThumb = unlink($thumbPath);
    }

    if($deleteImg && $deleteImgThumb){
      $picture = Tblpagesimg::findFirst('imgID='.$pictureID);
      $picture->delete();
      $response['success'] = true;
    }else{
      /*                $response['deleteImg'] = $deleteImg;
      $response['deleteImgThumb'] = $deleteImgThumb;
      $response['message'] = 'unable to delete file '.$thumbPath;*/
      $response['success'] = false;
    }
  }else{
    $response['success'] = false;
  }

  echo json_encode($response);
}

private function _getPartnerAlbumSelect($partnerID){
  $album = Tblpartnersalbums::find('partnerID='.$partnerID);

  $html = '';
  $options = null;
  foreach ($album as $key => $value) {
    $options .= '<option value="'.$value->albumID.'">'.$value->albumName.'</option>';
  }
  $options = is_null($options)?'<option value="">No album found</option>':'<option value="all">All Pictures</option>'.$options;
  return $html = '<select class="form-control" name="partnerAlbumSelect" id="partnerAlbumSelect">'.$options.'</select>';
}

private function _getAllPictures($albumID = null, $partnerID){
  //$pictures = Tblpartnerspictures::find('albumID='.$albumID);
  $where = 'WHERE Tblpartnerspictures.partnerID = '.$partnerID;
  $where = is_null($albumID) || $albumID == 'all'?$where:$where.' AND Tblpartnerspictures.albumID = '.$albumID;
  $phql = 'SELECT Tblpartnerspictures.pictureID, Tblpartnerspictures.partnerID, Tblpartnerspictures.albumID, Tblpartnerspictures.pictureFilename, Tblpartnersalbums.albumName FROM Tblpartnerspictures
  LEFT JOIN Tblpartnersalbums ON Tblpartnersalbums.albumID = Tblpartnerspictures.albumID '.$where;
  $result = $this->modelsManager->executeQuery($phql);

  $html = null;

  foreach ($result as $key => $value) {
    $imgpath = $this->url->get().'img/partnerspictures/'.$value['partnerID'].'/'.$value['albumName'].'/'.$value['pictureFilename'];
    $filename = $value['pictureFilename'];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    $newfilename = strlen($filename) > 15 ? substr($filename,0,15)."...".$ext : $filename;
    $html .= '
    <div class="program-digital-assets-library pull-left" style="position: relative">
      <div style="padding-left: 25px; width: 100%; word-wrap:break-word;">'.$newfilename.'</div>
      <a href="'.$imgpath.'" class="prettyPhoto[pp_gal]"><img src="'.$imgpath.'" alt=""></a>
      <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="'.$imgpath.'">
      <button class="btn btn-xs btn-danger delete-recent-upload-pic" data-picture-id="'.$value['pictureID'].'" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
      <div class="clearfix"></div>
    </div>
    ';
  }

  return $html;
}

public function ajaxGetAllPicturesAction($albumID, $partnerID){
  $this->view->disable();
  echo $this->_getAllPictures($albumID, $partnerID);
}


public function pagesAction(){
  \Phalcon\Tag::prependTitle('Pages | ');
  $this->view->menu = $this->_menuActive('pagerole');

  if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
    Tag::resetInput();
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $this->_deletePage($id);
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      $keyword = trim($this->request->getPost('search_text'));
      $this->session->set("user_search_text", $keyword);
    } else {
      $searchtext = $this->session->get("pages_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
  }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
    $id = $this->request->getPost('tbl_id');
    if(!empty($id)){
      $this->_deletePage($id);
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      $keyword = trim($this->request->getPost('search_text'));
      $this->session->set("user_search_text", $keyword);
    } else {
      // $numberPage = $this->request->getQuery("page", "int");
      $searchtext = $this->session->get("pages_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
  }elseif($this->request->isPost() && $this->request->getPost('action') == "edit"){
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      return $this->dispatcher->forward(array(
        'controller' => 'admin',
        'action' => 'editPartner',
        'params' => array($id)
        ));
    }
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "activate") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $page = Tblpages::findFirst('pageID='+$id);
      $page->pageActive = 1;
      if($page->save()){
        $this->flash->success("Page has been activated");
      } else {
        $this->flash->error("Something went wrong. Please try again later.");
      }
    }
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "deactivate") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $page = Tblpages::findFirst('pageID='+$id);
      $page->pageActive = 0;
      if($page->save()){
        $this->flash->success("Page has been deactivated");
      } else {
        $this->flash->error("Something went wrong. Please try again later.");
      }
    }
  }elseif($this->request->getPost('ddhiddenaction') == 'disable'){
    if($delActivity){
      $delActivity->assign(array('pageActive'=>0));
      $delActivity->save();
      $this->flash->success("Activity was disabled.");
    }
  } elseif($this->request->isPost() && $this->request->getPost('clear_search')){
    $this->session->remove("pages_search_text");
    $this->session->remove("pages_filter_date");
    $this->session->remove("pages_filter_date_from");
    $this->session->remove("pages_filter_date_to");
    unset($_POST);
  }else{
    if ($this->request->isPost()) {
      $keyword = trim($this->request->getPost('search_text'));
      $this->session->set("pages_search_text", $keyword);
    }else{
      $searchtext = $this->session->get("pages_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
  }

  if(!empty($keyword)){
    $where = ' AND pageTitle LIKE "%'.$keyword.'%"';
    $this->flash->notice('Search results for "<strong>' . $keyword .'</strong>"');
  }else{
    $where = null;
    $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):null;
    $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):null;
    if(($fromDate && $toDate) || $this->session->has("pages_filter_date") ) {
      if($fromDate > $toDate) {
        return $this->flash->warning("Invalid request. End date must be later than the start date.");
      }
      if($this->session->has("pages_filter_date")) {
        if(!$fromDate && !$toDate){
          $fromDate = trim($this->session->get('pages_filter_date_from'));
          $toDate = trim($this->session->get('pages_filter_date_to'));
        }
      } else {
        $this->session->set("pages_filter_date", true);
        $this->session->set("pages_filter_date_from", $fromDate);
        $this->session->set("pages_filter_date_to", $toDate);
      }
      $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
      $where = " AND (pageLastUpdated BETWEEN $fromDate AND $toDate)";
    }
  }

  $numberPage = $this->request->getQuery("page", "int");
  $numberPage = empty($numberPage)?1:$numberPage;

  // SORTING
  // Added a server side sorting on all tables using the columns shown
  // Dont forget to insert na href params on the view
  // NOTE: Remove ORDER clause on _getPages and _getSubpages private functions
  $sort = $this->request->getQuery("sort");
  $order = "";
  $this->view->titleHref = "pageTitle-asc";
  $this->view->slugHref = "pageSlug-asc";
  $this->view->lastupdateHref = "pageLastUpdated-asc";
  $this->view->statusHref = "pageActive-asc";
  $this->view->orderHref = "pageOrder-asc";
  $arr = explode("-", $sort);
  switch ($arr[0]) {
    case 'pageTitle':
    $arr[1] == "asc" ? $this->view->titleHref = $arr[0] . "-desc" : "";
    $order = "ORDER BY $arr[0] $arr[1]";
    $this->view->titleIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'pageSlug':
    $arr[1] == "asc" ? $this->view->slugHref = $arr[0] . "-desc" : "";
    $order = "ORDER BY $arr[0] $arr[1]";
    $this->view->slugIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'pageLastUpdated':
    $arr[1] == "asc" ? $this->view->lastupdateHref = $arr[0] . "-desc" : "";
    $order = "ORDER BY $arr[0] $arr[1]";
    $this->view->lastupdateIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'pageActive':
    $arr[1] == "asc" ? $this->view->statusHref = $arr[0] . "-desc" : "";
    $order = "ORDER BY $arr[0] $arr[1]";
    $this->view->statusIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'pageOrder':
    $arr[1] == "asc" ? $this->view->orderHref = $arr[0] . "-desc" : "";
    $order = "ORDER BY $arr[0] $arr[1]";
    $this->view->orderIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    default:
    break;
  }

  $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
    "data" => $this->_getPages("  ".$where.$order),
    "limit"=> 10,
    "page" => $numberPage
    ));
  $this->view->page = $paginator->getPaginate();

}

private function _getPages($where = null){
  $phql = 'SELECT * FROM Tblpages WHERE pageType ="pages" AND pageParent="0" '.$where;
  $result = $this->modelsManager->executeQuery($phql);

  $dataArray = array();
  foreach ($result as $key => $value) {
    $dataArray[] = array(
      'pageID'=>$value->pageID,
      'specialPage'=>$value->specialPage,
      'pageTitle'=>$value->pageTitle,
      'pageSlug'=>$value->pageSlug,
      'pageKeywords'=>$value->pageKeywords,
      'pageContent'=>$value->pageContent,
      'pageParent'=>$value->pageParent,
      'pageType'=>$value->pageType,
      'pageOrder'=>$value->pageOrder,
      'pageActive'=>$value->pageActive,
      'pageBanner'=>$value->pageBanner,
      'pageLastUpdated'=>$value->pageLastUpdated,
      'stages'=>null
      );
    $this->subpages = array();
    $this->_getSubPages($value->pageID, '--', $where);
    foreach ($this->subpages as $key2 => $value2) {
      $dataArray[] = $value2;
    }
  }

  return $dataArray;
}

private function _getSubPages($pageID, $stages, $where =null){
  $phql = 'SELECT * FROM Tblpages WHERE pageType ="pages" AND pageParent="'.$pageID.'" '.$where;
  $result = $this->modelsManager->executeQuery($phql);

  //$dataArray = array();
  //$this->subpages = array();
  foreach ($result as $key => $value) {
    $this->subpages[] = array(
      'pageID'=>$value->pageID,
      'pageTitle'=>$value->pageTitle,
      'pageSlug'=>$value->pageSlug,
      'pageKeywords'=>$value->pageKeywords,
      'pageContent'=>$value->pageContent,
      'pageParent'=>0,
      'pageType'=>$value->pageType,
      'pageOrder'=>$value->pageOrder,
      'pageActive'=>$value->pageActive,
      'pageBanner'=>$value->pageBanner,
      'pageLastUpdated'=>$value->pageLastUpdated,
      'stages'=>$stages
      );
    $stages.='--';
    $this->subpages[] = $this->_getSubPages($value->pageID, $stages);
  }

  $this->subpages = array_filter($this->subpages);

  //return array_filter($dataArray);
}

public function createpageAction(){
  \Phalcon\Tag::prependTitle('Create Page | ');
  $this->view->pictures = $pictures = Tblpagesimg::find("imgpath = 'img/pages/'");
  $this->view->menu = $this->_menuActive('pagerole');
  $this->view->titleError = null;
  $this->view->contenterror = null;
  if($this->request->isPost() && $this->request->getPost('save_page')){
    /*if($this->security->getSessionToken() == $this->request->getPost('csrf')){*/
      $content=$this->request->getPost('page_content');
      $checkPage = Tblpages::findFirst('pageTitle="'.$this->request->getPost('page_title', 'striptags').'" AND pageType ="pages"');

      // if($checkPage){
      //    $this->view->titleError .= "<div class='label label-danger'>Page title is required.</div> ";

      // }elseif ($checkPage == 'pageTitle') {
      // $this->view->titleError .= "<div class='label label-danger'>Page title already taken.</div> ";
      // }
      if($checkPage){
         $this->view->titleError .= "<div class='label label-danger'>Page title already taken.</div> ";

      }
      else{
        $pageOrder = 0;
        if($this->request->getPost('page_order')){
          $pageOrder = $this->request->getPost('page_order', 'int');
        }
        $pageActive = $this->request->getPost('page_active', 'int');
        $pageActive = empty($pageActive)?0:$pageActive;

        $hasError = false;
        $page = new Tblpages();
        $page->assign(array(
          'specialPage'=> 0,
          'pageParent' => 0,
          'pageTitle' => trim($this->request->getPost('page_title', 'striptags')),
          'pageSlug' => trim($this->request->getPost('hpage_slug', 'striptags'),'-'),
          'pageKeywords' => trim($this->request->getPost('page_keyword', 'striptags')),
          'pageContent' => trim($this->request->getPost('page_content')),
          'pageType' => 'pages',
          'pageOrder' => $pageOrder,
          'pageActive' =>  $pageActive,
          'pageBanner' =>  $this->request->getPost('pageBannerUrl', 'striptags'),
          'pageLastUpdated' => time()
          ));
        // if($page->pageTitle){
        //    $this->view->titleError .= "<div class='label label-danger'>Page title already taken.</div> ";
        //   $hasError = true;
        // }
        if(!trim($this->request->getPost('page_content'))){
          $this->view->page_content_error = "<div class='label label-danger'>Page Content is required.</div>";
          $hasError = true;
        }

        if(!$hasError) {
          $page->save();
          $this->flash->success("Page was created successfully.");
          Tag::resetInput();
        }
      }
    /*}else{
    Tag::resetInput();
  }*/
}

$allPages = $this->_getPages();

$pagesOption = null;
$selectedParent = $this->request->getPost('page_parent');
foreach ($allPages as $key => $value) {
  $selected = $selectedParent == $value['pageID']?'selected':'';
  $pagesOption .= '<option '.$selected.' value="'.$value['pageID'].'">'.$value['stages'].' '.$value['pageTitle'].'</option>';
}
$this->view->pagesOption = $pagesOption;
}

public function editpageAction($pageID= null){
  \Phalcon\Tag::prependTitle('Edit Page | ');
  $this->view->pictures = $pictures = Tblpagesimg::find();
  $this->view->menu = $this->_menuActive('pagerole');
  $this->view->titleError = null;

  $this->view->pageInfo = $pageInfo = Tblpages::findFirst('pageID='.$pageID);

  if($pageInfo){

    if($this->request->isPost() && $this->request->getPost('save_page')){
      if($this->security->getSessionToken() == $this->request->getPost('csrf')){

        if($this->request->getPost('hpage_title', 'striptags') == $this->request->getPost('page_title', 'striptags')){

          $checkPage = Tblpages::findFirst('pageTitle="'.$this->request->getPost('page_title', 'striptags').'" AND pageType="pages"');
          if($checkPage){
            $this->view->titleError .= "<div class='label label-danger'>Page title already exists.</div> ";
          }
        }else{

          $pageOrder = 0;
          if($this->request->getPost('page_order')){
            $pageOrder = $this->request->getPost('page_order', 'int');
          }

          $page = $pageInfo;
          $page->assign(array(
            'pageTitle' => $this->request->getPost('page_title', 'striptags'),
            'pageParent' => 0,
            'pageSlug' => trim($this->request->getPost('hpage_slug', 'striptags'),'-'),
            'pageKeywords' => $this->request->getPost('page_keyword', 'striptags'),
            'pageContent' => $this->request->getPost('page_content'),
            'pageType' => 'pages',
            'pageOrder' => $pageOrder,
            'pageActive' =>  $this->request->getPost('page_active', 'int'),
            'pageBanner' =>  $this->request->getPost('pageBannerUrl', 'striptags'),
            'pageLastUpdated' => time()
            ));
          if (!$page->save()) {
            $this->flash->error($page->getMessages());
          } else {
            $this->flash->success("Page was successfully updated.<a href='/admin/pages'> Back to Page List</a>");
            Tag::resetInput();
          }
        }
      }else{
        Tag::resetInput();
      }
    }
  }else{
    $this->flash->error('Page was not found');
  }

  $allPages = $this->_getPages();
  $pagesOption = null;
  $selectedParent = $pageInfo->pageParent;
  foreach ($allPages as $key => $value) {
    if($value['pageID'] != $pageID){
      $selected = $selectedParent == $value['pageID']?'selected':'';
      $pagesOption .= '<option '.$selected.' value="'.$value['pageID'].'">'.$value['stages'].' '.$value['pageTitle'].'</option>';
    }
  }
  $this->view->pagesOption = $pagesOption;
}

public function ajaxmoveuploadpagesAction($filename, $null = 0, $imgfolder){
  $this->view->disable();

  if(is_file('../public/server/php/files/'.$filename)){
    $path = '../public/img/'.$imgfolder;

    $name = pathinfo($filename, PATHINFO_FILENAME);
    $ext = pathinfo($filename, PATHINFO_EXTENSION);

    $uniqueFilename = $filename;
    $increment = null;
    while(file_exists($path.'/'.$name . $increment . '.' . $ext)) {
      $increment++;
      $uniqueFilename = $name . $increment . '.' . $ext;
    }
    rename('../public/server/php/files/'.$filename, $path.'/'.$uniqueFilename);
    rename('../public/server/php/files/thumbnail/'.$filename, $path.'/thumbnail/'.$uniqueFilename);

    $picture = new Tblpagesimg();
    $picture->assign(array(
      'imgpath' => 'img/'.$imgfolder.'/',
      'fileName' => $uniqueFilename
      ));
    $picture->save();
    $lastID = $picture->imgID;

    $imgpath = $this->url->get().'img/'.$imgfolder.'/'.$uniqueFilename;
    $newfilename = strlen($uniqueFilename) > 15 ? substr($uniqueFilename,0,15)."...".$ext : $uniqueFilename;

    echo $html = '
    <div class="program-digital-assets-library pull-left" style="position: relative">
      <a href="'.$imgpath.'" class="prettyPhoto[pp_gal]"><img src="'.$imgpath.'" alt=""></a>
      <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="'.$imgpath.'">
      <button class="btn btn-xs btn-danger delete-recent-upload-page-pic" data-picture-id="'.$lastID.'" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
      <div class="clearfix"></div>
    </div>
    ';
  }
}


public function ajaxmoveuploadnewsletterAction($filename, $null = 0){
  $this->view->disable();

  if(is_file('../public/server/php/files/'.$filename)){
    $path = '../public/img/newsletter';

    $name = pathinfo($filename, PATHINFO_FILENAME);
    $ext = pathinfo($filename, PATHINFO_EXTENSION);

    $uniqueFilename = $filename;
    $increment = null;
    while(file_exists($path.'/'.$name . $increment . '.' . $ext)) {
      $increment++;
      $uniqueFilename = $name . $increment . '.' . $ext;
    }
    rename('../public/server/php/files/'.$filename, $path.'/'.$uniqueFilename);
    rename('../public/server/php/files/thumbnail/'.$filename, $path.'/thumbnail/'.$uniqueFilename);

    $picture = new Tblpagesimg();
    $picture->assign(array(
      'imgpath' => 'img/newsletter/',
      'fileName' => $uniqueFilename
      ));
    $picture->save();
    $lastID = $picture->imgID;

    $imgpath = $this->url->get().'img/newsletter/'.$uniqueFilename;
    $newfilename = strlen($uniqueFilename) > 15 ? substr($uniqueFilename,0,15)."...".$ext : $uniqueFilename;

    echo $html = '
    <div class="program-digital-assets-library pull-left" style="position: relative">
      <a href="'.$imgpath.'" class="prettyPhoto[pp_gal]"><img src="'.$imgpath.'" alt=""></a>
      <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="http://abk.gotitgenius.com'.$imgpath.'">
      <button class="btn btn-xs btn-danger delete-recent-upload-page-pic" data-picture-id="'.$lastID.'" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
      <div class="clearfix"></div>
    </div>
    ';
  }

}

public function deleteRecentPagesPicUploadAction($id){
  $this->view->disable();
  $response = array();

  $pic = Tblpagesimg::findFirst('imgID='.$id);
  if($pic){
    $path = $pic->imgpath;
    $thumbPath = $pic->imgpath.'thumbnail/';
    $filename = $pic->fileName;

    $deleteImg = $deleteImgThumb = false;
    if(is_file($path.$filename)){
      $deleteImg = unlink($path.$filename);
    }

    if(is_file($thumbPath.$filename)){
      $deleteImgThumb = unlink($thumbPath.$filename);
    }

    if($deleteImg && $deleteImgThumb){
      $pic->delete();
    }

    $response['success'] = true;
  }else{
    $response['success'] = false;
  }

  echo json_encode($response);
}

private function _deletePage($id){
  $param=null;
  if(is_array($id)){
    $newid = array();
    foreach ($id as $i) {
      $param .= 'pageID = '. $this->filter->sanitize($i, array("int")) . ' OR ';
    }
    $param = substr($param, 0, -4);
  }else{
    $id = $this->filter->sanitize($id, array("int"));
    $param = 'pageID=' . $id;
  }

  $page = Tblpages::find($param);
  if (!$page) {
    $this->flash->error("Page was not found");
  }

  if (!$page->delete()) {
    foreach ($products->getMessages() as $message) {
      $this->flash->error((string) $message);
    }
    return $this->forward("admin/pages");
  } else {
    $this->flash->success("Page was successfully deleted.");
  }
}

public function newsletterAction(){
  \Phalcon\Tag::prependTitle('View Subscribers | ');
  $this->view->menu = $this->_menuActive('enmrole');
  $numberPage = 1;
  if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
    Tag::resetInput();
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $this->_generalDelete($id, 'Tblnewsletteremails', 'nleID', 'Subscriber');
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      $keyword = trim($this->request->getPost('search_text'));
      $this->session->set("enm_search_text", $keyword);
    } else {
        // $numberPage = $this->request->getQuery("page", "int");
      $searchtext = $this->session->get("enm_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
  }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
    $id = $this->request->getPost('tbl_id');
    if(!empty($id)){
      $this->_generalDelete($id, 'Tblnewsletteremails', 'nleID', 'Subscribers');
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      $keyword = trim($this->request->getPost('search_text'));
      $this->session->set("enm_search_text", $keyword);
    } else {
        // $numberPage = $this->request->getQuery("page", "int");
      $searchtext = $this->session->get("enm_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
  }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
    $this->session->remove("enm_search_text");
    $this->session->remove("enm_filter_date");
    $this->session->remove("enm_filter_date_from");
    $this->session->remove("enm_filter_date_to");
    $this->persistent->searchenmParams = null;
    unset($_POST);
  }else{
    if ($this->request->isPost()) {
      $keyword = $this->request->getPost('search_text');
      $keyword = trim(preg_replace('/\s+/',' ', $keyword));
      $query = Criteria::fromInput($this->di, 'Tblnewsletteremails', array('name' => $keyword));
      $this->persistent->searchenmParams = $query->getParams();
      $this->session->set("enm_search_text", $keyword);
        /*
        $query = Criteria::fromInput($this->di, 'Tblpartnerevents', array('eventTitle' => $this->request->getPost('search_text'), 'partnerID'=>$id));
        $this->persistent->searchEventParams = $query->getParams();
        $this->session->set("event_search_text", $this->request->getPost('search_text'));
        */
      } else {
        $numberPage = $this->request->getQuery("page", "int");
      }
    }


    $parameters = array();
    if ($this->persistent->searchenmParams) {
      $parameters = $this->persistent->searchenmParams;
    }

    // SORTING
    // Added a server side sorting on all tables using the columns shown
    // Dont forget to insert na href params on the view
    $sort = $this->request->getQuery("sort");
    $order = "nleID";
    $this->view->emailHref = "email-asc";
    $this->view->nameHref = "name-asc";
    $this->view->dateaddedHref = "dateAdded-asc";
    $arr = explode("-", $sort);
    switch ($arr[0]) {
      case 'email':
      $arr[1] == "asc" ? $this->view->emailHref = $arr[0] . "-desc" : "";
      $order = "$arr[0] $arr[1]";
      $this->view->emailIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;
      case 'name':
      $arr[1] == "asc" ? $this->view->nameHref = $arr[0] . "-desc" : "";
      $order = "$arr[0] $arr[1]";
      $this->view->nameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;
      case 'dateAdded':
      $arr[1] == "asc" ? $this->view->dateaddedHref = $arr[0] . "-desc" : "";
      $order = "$arr[0] $arr[1]";
      $this->view->dateaddedIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;
      default:
      break;
    }

    $builder = $this->modelsManager->createBuilder()
    ->columns('nleID, email, name, dateAdded')
    ->from('Tblnewsletteremails')->orderBy($order);

    if(!empty($parameters)){
      $this->flash->notice('Search results for "<strong>' . $this->session->get("enm_search_text") .'</strong>"');
      $builder->andWhere($parameters['conditions'], $parameters['bind']);
    } else {
      $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):"";
      $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):"";
      if(($fromDate && $toDate) || $this->session->has("enm_filter_date") ) {
        if($fromDate > $toDate) {
          return $this->flash->warning("Invalid request. End date must be later than the start date.");
        }
        if($this->session->has("enm_filter_date")) {
          if(!$fromDate && !$toDate){
            $fromDate = trim($this->session->get('enm_filter_date_from'));
            $toDate = trim($this->session->get('enm_filter_date_to'));
          }
        } else {
          $this->session->set("enm_filter_date", true);
          $this->session->set("enm_filter_date_from", $fromDate);
          $this->session->set("enm_filter_date_to", $toDate);
        }
        $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
        $builder->where("dateAdded BETWEEN $fromDate AND $toDate");
      }
    }

    $paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
      "builder" => $builder,
      "limit"=> 10,
      "page" => $numberPage
      ));

    // Get the paginated results
    $this->view->page = $paginator->getPaginate();
  }

  private function _generalDelete($id, $table, $field, $itemname=NULL, $showMessage = true, $fieldIsInt = true){
    $param=null;
    if(is_array($id)){
      foreach ($id as $i) {
        if(!$fieldIsInt){
          $param .= $field.' = "'. $this->filter->sanitize($i, array("string")) . '" OR ';
        }else{
          $param .= $field.' = '. $this->filter->sanitize($i, array("int")) . ' OR ';
        }

      }
      $param = substr($param, 0, -4);
    }else{

      if(!$fieldIsInt){
        $param = $field.'="' . $this->filter->sanitize($id, array("string")).'"';
      }else{
        $param = $field.'=' . $this->filter->sanitize($id, array("int"));
      }

    }

    $post = $table::find($param);
    if (!$post) {
      $this->flash->error($itemname." not found");
    }else{
      if(!$post->delete()){
        $this->flash->error((string) $message);
        exit;
      }
    }


    if($showMessage){
      $this->flash->success($itemname." has been successfully deleted.");
    }
  }

// public function inqdeleteAction($inquiryID){
//   $this->view->setRenderLevel(View::LEVEL_NO_RENDER);
//       // $this->_generalDelete($inquiryID, 'Tblinquiries', 'inqID', 'Contact');
//       echo json_encode($inquiryID);
//       if(empty($inquiryID)){
//         echo json_encode(">>".$inquiryID);
//       }else{

//        echo json_encode("success");
//       }
// }

//I'll make you work Inquiries
  public function inquiriesAction($inquiryID = null){
    \Phalcon\Tag::prependTitle('Contacts | ');
    $this->view->menu = $this->_menuActive('conrole');
    $this->view->deleteinq = null;
    $this->view->searchResultStat="";

    if(!empty($inquiryID) || $this->request->getPost('sendReply')){

      if(empty($inquiryID)){
        $inquiryID = $this->request->getPost('sendToID', 'int');
      }

      $phql = "Select Tblinquiryreplies.inqReplyMessage as message, Tblinquiryreplies.inqReplyDate as dateReply, CONCAT(Tblusers.userFirstname,'".$x."',Tblusers.userLastname) as sender from Tblinquiryreplies LEFT JOIN Tblusers ON Tblusers.userID = Tblinquiryreplies.inqReplySender WHERE Tblinquiryreplies.inqID ='".$inquiryID."'";
      $this->view->inquiryReplies = $result = $this->modelsManager->executeQuery($phql);
      $this->view->viewlist = false;
      $this->view->inquiry = $inquiry = Tblinquiries::findFirst('inqID='.$inquiryID);
      //if to send reply to the sender
      if($this->request->getPost('sendReply')){
        $this->view->viewlist = false;
        $replyMessage = $this->request->getPost('replyMessage', 'striptags');
         // $this->view->errSend .= "<div class='alert alert-success'>". $body ."</div>";
         //sending mode
        if(!empty($replyMessage)){

           $inquiry = Tblinquiries::findFirst('inqID='.$inquiryID);

           $to = $inquiry->inqEmail;
           $subject = "ANG BAYAN KO : RE: ".$inquiry->inqSubject;
           $body = "
           <p>Dear <strong>$inquiry->inqSender</strong>,</p>
           <p>Thank you for sending us your enquiry through ABK Foundation.</p>
           <p>$replyMessage</p>
           <p>We hope that this email helps you to progress your enquiry.</p>
           <p style='text-align:right'>Yours Truly,</p>
           <p style='text-align:right'><strong>$senderName</strong></p>
           ";

           $mailObjects = array(
           'From'=> 'angbayanko.org@no-reply.com',
           'Reply-To' => 'ramonluzano@gmail.com',
           'FromName' => 'angbayanko.org@no-reply.com',
           'AddAddress'=> $to,
           'Subject' => $subject,
           'Body' => $body
           );

              $abk_user = $this->session->get('auth');
              $replies = new Tblinquiryreplies();
              $replies->assign(array(
                'inqID'=>$inquiryID,
                'inqReplySender'=>$abk_user['abk_id'],
                'inqReplyMessage'=>$replyMessage,
                'inqReplyDate'=>time()
                ));

              $inquiry->inqLatestReplyDate = $replies->inqReplyDate;
              if($replies->save()){
                  $inquiry->save();
                  $this->_sendmail($mailObjects);
                  $this->flash->success("Your reply has been successfully sent.");
                  $phql = "Select Tblinquiryreplies.inqReplyMessage as message, Tblinquiryreplies.inqReplyDate as dateReply, CONCAT(Tblusers.userFirstname,'".$x."',Tblusers.userLastname) as sender from Tblinquiryreplies LEFT JOIN Tblusers ON Tblusers.userID = Tblinquiryreplies.inqReplySender WHERE Tblinquiryreplies.inqID ='".$inquiryID."'";
                  $this->view->inquiryReplies = $result = $this->modelsManager->executeQuery($phql);
                  $this->view->viewlist = false;
                  $this->view->inquiry = $inquiry = Tblinquiries::findFirst('inqID='.$inquiryID);
                  // header('refresh:2;url=/admin/inquiries/'.$inquiryID);
                  // unset($_POST);

              }else{
                echo '<div class="alert alert-danger">Unable to send reply.</div>';
              }

        }else{
              echo '<div class="alert alert-danger">Please enter your message.</div>';
        }

      }

    }else{
      $this->view->viewlist = true;
      $numberPage = 1;

      if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
        Tag::resetInput();
      }elseif($this->request->isPost() && $this->request->getPost('action') == "delete"){//elseif :valid
        $id = $this->request->getPost('recordID');
        //if A1
              if(!empty($id)){
                $this->_generalDelete($id, 'Tblinquiries', 'inqID', 'Contact');
                $this->view->deleteinq = "<div class='alert alert-success'>Contact was successfully deleted.</div>";
              }
        //if A1 end
        /* stay on search pages*/
        if ($this->request->isPost()) {
          $keyword = trim($this->request->getPost('search_text'));
          $keyword = trim(preg_replace('/\s+/',' ', $keyword));
          $this->session->set("user_search_text", $keyword);
        } else {
          // $numberPage = $this->request->getQuery("page", "int");
          $searchtext = $this->session->get("user_search_text");
          $keyword = !empty($searchtext)?$searchtext:'';
        }
        /**/
      }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
        if(!empty($id)){
          $this->_generalDelete($id, 'Tblinquiries', 'inqID', 'Inquiries');
        }
        /* stay on search pages*/
        if ($this->request->isPost()) {
          $keyword = trim($this->request->getPost('search_text'));
          $keyword = trim(preg_replace('/\s+/',' ', $keyword));
          $this->session->set("inq_search_text", $keyword);
        } else {
          $searchtext = $this->session->get("inq_search_text");
          $keyword = !empty($searchtext)?$searchtext:'';
        }
        /**/
      }if($this->request->isPost() && $this->request->getPost('clear_search')){
        $this->session->remove("inq_search_text");
        $this->session->remove("inq_filter_date");
        $this->session->remove("inq_filter_date_from");
        $this->session->remove("inq_filter_date_to");
        $this->persistent->searchinqParams = null;
        // unset($_POST);
      }else{

        if ($this->request->isPost()) {
          $searchstat = $this->request->getPost('searchstat');
          $keyword = $this->request->getPost('search_text');
          $keyword = trim(preg_replace('/\s+/',' ', $keyword));

          if(empty($keyword) && $searchstat != "status"){
            $query = Criteria::fromInput($this->di, 'Tblinquiries', array('inqStatus' => $searchstat));
            $this->view->searchResultStat='<div class="alert alert-info">Search results for "'.$searchstat = $searchstat?'Reviewed" status'.'</div>':'New'.'" status'.'</div>';
            $this->persistent->searchinqParams = $query->getParams();
          }elseif($searchstat != "status"){
            $query = Criteria::fromInput($this->di, 'Tblinquiries', array('inqSubject' => $keyword));
            $query->orWhere('inqEmail LIKE "%'.$keyword.'%" OR inqSender LIKE "%'.$keyword.'%"');
            $query->andWhere('inqStatus LIKE "%'.$searchstat.'%"');
            $this->persistent->searchinqParams = $query->getParams();
            $this->view->searchResultStat='<div class="alert alert-info">Search results for "'.$keyword.'" and "'.$searchstat = $searchstat?'Reviewed" status'.'</div>':'New'.'" status'.'</div>';
          }elseif(empty($keyword) && $searchstat = "status"){
            $numberPage = $this->request->getQuery("page", "int");
          }else{
            $query = Criteria::fromInput($this->di, 'Tblinquiries', array('inqSubject' => $keyword));
            $query->orWhere('inqEmail LIKE "%'.$keyword.'%" OR inqSender LIKE "%'.$keyword.'%"');
            $this->persistent->searchinqParams = $query->getParams();
            $this->view->searchResultStat='<div class="alert alert-info">Search results for "'.$keyword.'"</div>';
          }
        }else{
          $keyword = !empty($searchtext)?$searchtext:'';
          $numberPage = $this->request->getQuery("page", "int");
        }
      }

      $parameters = array();

      if ($this->persistent->searchinqParams) {
        $parameters = $this->persistent->searchinqParams;
      }

      // SORTING
      // Added a server side sorting on all tables using the columns shown
      // Dont forget to insert na href params on the view
      $sort = $this->request->getQuery("sort");
      //$order = "inqDate DESC, inqLatestReplyDate DESC"; // default order selection
      $order = "inqDate";
      $this->view->statusHref = "inqStatus-asc";
      $this->view->subjectHref = "inqSubject-asc";
      $this->view->senderHref = "inqSender-asc";
      $this->view->emailHref = "inqEmail-asc";
      $this->view->dateHref = "inqDate-asc";
      $this->view->replydateHref = "inqLatestReplyDate-asc";
      $arr = explode("-", $sort);
      switch ($arr[0]) {
        case 'inqStatus':
        $arr[1] == "asc" ? $this->view->statusHref = $arr[0] . "-desc" : "";
        $order = "$arr[0] $arr[1]";
        $this->view->statusIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'inqSubject':
        $arr[1] == "asc" ? $this->view->subjectHref = $arr[0] . "-desc" : "";
        $order = "$arr[0] $arr[1]";
        $this->view->subjectIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'inqSender':
        $arr[1] == "asc" ? $this->view->senderHref = $arr[0] . "-desc" : "";
        $order = "$arr[0] $arr[1]";
        $this->view->senderIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'inqEmail':
        $arr[1] == "asc" ? $this->view->emailHref = $arr[0] . "-desc" : "";
        $order = "$arr[0] $arr[1]";
        $this->view->emailIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'inqDate':
        $arr[1] == "asc" ? $this->view->dateHref = $arr[0] . "-desc" : "";
        $order = "$arr[0] $arr[1]";
        $this->view->dateIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'inqLatestReplyDate':
        $arr[1] == "asc" ? $this->view->replydateHref = $arr[0] . "-desc" : "";
        $order = "$arr[0] $arr[1]";
        $this->view->replydateIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        default:
        break;
      }
      //SORTING END

       //PAGINATION
      $builder = $this->modelsManager->createBuilder()
      ->columns('Tblinquiries.inqID, inqSender, inqSubject, inqEmail, inqStatus, inqDate, inqLatestReplyDate')
      ->from('Tblinquiries')->orderBy($order);

      if(!empty($parameters) && !$this->request->getPost('filter-date')){
        $this->flash->notice('Search results for "<strong>' . $this->session->get("inq_search_text") .'</strong>"');
        $builder->andWhere($parameters['conditions'], $parameters['bind']);
      }
      // else
      if($this->request->getPost('filter-date'))
      {
        $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):"";
        $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):"";
        if(($fromDate && $toDate) || $this->session->has("inq_filter_date") ) {
          if($fromDate > $toDate) {
            $this->view->searchResultStat1='<div class="alert alert-warning" style="color: red;">Invalid request. End date must be later than the start date.</div>';
          }
          if($this->session->has("inq_filter_date")) {
            if(!$fromDate && !$toDate){
              $fromDate = trim($this->session->get('inq_filter_date_from'));
              $toDate = trim($this->session->get('inq_filter_date_to'));
              $this->view->searchResultStat1='<div class="alert alert-warning" style="color: red;">From and To date is required</div>';
            }
          }
          if($fromDate < $toDate) {
            $this->view->searchResultStat1='<div class="alert alert-info">List filtered with dates between "'.date("F j, Y", $fromDate).'" and "'. date("F j, Y", $toDate) .'"</div>';
            if(empty($fromDate)){
                $this->view->searchResultStat1='<div class="alert alert-warning" style="color: red;">From date is required</div>';
            }
            $builder->where("inqDate BETWEEN $fromDate AND $toDate");
          }
          // if(empty($fromDate) || empty($toDate)){
          //     $this->view->searchResultStat1='<div class="alert alert-warning" style="color: red;">From and To date is required</div>';
          // }
          else {
            $this->session->set("inq_filter_date", true);
            $this->session->set("inq_filter_date_from", $fromDate);
            $this->session->set("inq_filter_date_to", $toDate);
          }
          // $this->view->searchResultStat='<div class="alert alert-info">List filtered with dates between "'.date("F j, Y", $fromDate).'" and "'. date("F j, Y", $toDate) .'"</div>';
        }else{
          $this->view->searchResultStat1='<div class="alert alert-warning" style="color: red;">From and To date is required</div>';
        }
      }

      $paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
        "builder" => $builder,
        "limit"=> 10,
        "page" => $numberPage
        ));

      // Get the paginated results
      $this->view->page = $paginator->getPaginate();

      //if on pagination start

      //if on pagination end
      //PAGINATION
    }
  }
//I'll make you work inquiries

  public function volunteersAction($volID = null){
    \Phalcon\Tag::prependTitle('Volunteers | ');


    $this->view->menu = $this->_menuActive('vmrole');
    if(!empty($volID)){
//for the list======================================
      $volFilter = null;
      $numberPage = 1;
      if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
        Tag::resetInput();
      }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
        $id = $this->request->getPost('recordID');
        if(!empty($id)){
          $this->_generalDelete($id, 'Tblvolunteers', 'volunteerID', 'Volunteer');
        }
        /* stay on search pages*/
        if ($this->request->isPost()) {
          // $query = Criteria::fromInput($this->di, 'Tblusers', array('userName' => $this->request->getPost('search_text'), 'userFirstname' => $this->request->getPost('search_text')));
          // $this->persistent->searchUserParams = $query->getParams();
          // $this->session->set("user_search_text", $this->request->getPost('search_text'));
          $keyword = trim($this->request->getPost('search_text'));
          $this->session->set("user_search_text", $keyword);
          $keyword = trim(preg_replace('/\s+/',' ', $keyword));
        } else {
          // $numberPage = $this->request->getQuery("page", "int");
          $searchtext = $this->session->get("user_search_text");
          $keyword = !empty($searchtext)?$searchtext:'';
        }
        /**/
      }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
        $id = $this->request->getPost('tbl_id');
        if(!empty($id)){
          $this->_generalDelete($id, 'Tblvolunteers', 'volunteerID', 'Volunteers');
        }
        /* stay on search pages*/
        if ($this->request->isPost()) {
          // $query = Criteria::fromInput($this->di, 'Tblusers', array('userName' => $this->request->getPost('search_text'), 'userFirstname' => $this->request->getPost('search_text')));
          // $this->persistent->searchUserParams = $query->getParams();
          // $this->session->set("user_search_text", $this->request->getPost('search_text'));
          $keyword = $this->request->getPost('search_text');
          $this->session->set("user_search_text", $keyword);
          $keyword = trim(preg_replace('/\s+/',' ', $keyword));
        } else {
          // $numberPage = $this->request->getQuery("page", "int");
          $searchtext = $this->session->get("user_search_text");
          $keyword = !empty($searchtext)?$searchtext:'';
        }
        /**/
      }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
        $this->session->remove("vol_search_text");
        $this->session->remove("vol_filter_date");
        $this->session->remove("vol_filter_date_from");
        $this->session->remove("vol_filter_date_to");
        $this->persistent->searchvolParams = null;
        unset($_POST);
      }else{
        if ($this->request->isPost()) {
          $keyword = $this->request->getPost('search_text');
          $this->session->set("vol_search_text", $keyword);
          $keyword = trim(preg_replace('/\s+/',' ', $keyword));

          $volFilter = $this->request->getPost('volFilter');
          $this->session->set("volFilter", $volFilter);
        } else {
          echo $volFilter = $this->session->get("volFilter");
          $searchtext = $this->session->get("vol_search_text");
          $keyword = !empty($searchtext)?$searchtext:'';
          $numberPage = $this->request->getQuery("page", "int");
        }
      }

      $phql = 'SELECT activityID, activity, Tblporgramactivities.programID, programName
      FROM Tblporgramactivities
      LEFT JOIN Tblprograms ON Tblprograms.programID = Tblporgramactivities.programID
      ORDER BY Tblprograms.programID
      ';
      $activityOptions =  $this->modelsManager->executeQuery($phql);
      $actOptions = null;
      $pID = 0;
      $withSelected = false;
      foreach ($activityOptions as $key => $value) {
        if($pID != $value->programID){
          $pID = $value->programID;
          $actOptions .= '<optgroup label="'.$value->programName.'">';
        }
        if($volFilter == $value->activityID){
          $selected = 'selected';
          $withSelected = true;
        }else{
          $selected = '';
        }
        $actOptions .= '<option '.$selected.' value="'.$value->activityID.'">'.$value->activity.'</option>';
        if($pID != $value->programID){
          $actOptions .= '</optgroup>';
        }
      }
      $actOptions .= !empty($actOptions)?'</optgroup>':'';
      $default = $withSelected?'':'selected';
      $actOptions .= '<option '.$default.' value="">All Activities</option>';
      $this->view->activityOptions = $actOptions;
      $this->view->viewlist = true;


      if(!empty($keyword)){
        $keyword = trim(preg_replace('/\s+/',' ', $keyword));
        $this->flash->notice('Search results for "<strong>' . $this->session->get("vol_search_text") .'</strong>"');
        $andWhere = 'AND (username LIKE "%'.$keyword.'%" OR fname LIKE "%'.$keyword.'%" OR mname LIKE "%'.$keyword.'%" OR lname LIKE "%'.$keyword.'%" OR extname LIKE "%'.$keyword.'%" OR email LIKE "%'.$keyword.'%" OR phone LIKE "%'.$keyword.'%")';
      }else{
        $andWhere = null;
        //$andWhere .= !empty($volFilter)?' AND Tblvolunteeractivities.activityID = '.$volFilter;
        $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):"";
        $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):"";
        if(($fromDate && $toDate) || $this->session->has("vol_filter_date") ) {
          if($fromDate > $toDate) {
            return $this->flash->warning("Invalid request. End date must be later than the start date.");
          }
          if($this->session->has("vol_filter_date")) {
            if(!$fromDate && !$toDate){
              $fromDate = trim($this->session->get('vol_filter_date_from'));
              $toDate = trim($this->session->get('vol_filter_date_to'));
            }
          } else {
            $this->session->set("vol_filter_date", true);
            $this->session->set("vol_filter_date_from", $fromDate);
            $this->session->set("vol_filter_date_to", $toDate);
          }
          $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
          $andWhere = " AND (Tblvolunteers.dateAdded BETWEEN $fromDate AND $toDate)";
        }
      }

      // SORTING
      // Added a server side sorting on all tables using the columns shown
      // Dont forget to insert na href params on the view
      $sort = $this->request->getQuery("sort");
      $order = "";
      $this->view->nameHref = "name-asc";
      $this->view->usernameHref = "username-asc";
      $this->view->emailHref = "email-asc";
      $this->view->phoneHref = "phone-asc";
      $this->view->dateaddedHref = "dateAdded-asc";
      $arr = explode("-", $sort);
      switch ($arr[0]) {
        case 'name':
        $arr[1] == "asc" ? $this->view->nameHref = $arr[0] . "-desc" : "";
        $custom = 'CONCAT(fname, " ",mname, " ", lname)';
        $order = " ORDER BY $custom $arr[1]";
        $this->view->nameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'username':
        $arr[1] == "asc" ? $this->view->usernameHref = $arr[0] . "-desc" : "";
        $order = " ORDER BY $arr[0] $arr[1]";
        $this->view->usernameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'email':
        $arr[1] == "asc" ? $this->view->emailHref = $arr[0] . "-desc" : "";
        $order = " ORDER BY $arr[0] $arr[1]";
        $this->view->emailIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'phone':
        $arr[1] == "asc" ? $this->view->phoneHref = $arr[0] . "-desc" : "";
        $order = " ORDER BY $arr[0] $arr[1]";
        $this->view->phoneIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'dateAdded':
        $arr[1] == "asc" ? $this->view->dateaddedHref = $arr[0] . "-desc" : "";
        $order = " ORDER BY Tblvolunteers.$arr[0] $arr[1]";
        $this->view->dateaddedIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        default:
        break;
      }

      $andWhere .= !empty($volFilter)?' AND Tblvolunteeractivities.activityID = '.$volFilter:$andWhere;

      $phql = 'SELECT DISTINCT Tblvolunteers.volunteerID,username, title, fname, mname, lname, extname, email, phone, Tblvolunteers.dateAdded
      FROM Tblvolunteeractivities
      LEFT JOIN Tblvolunteers ON Tblvolunteers.volunteerID = Tblvolunteeractivities.volunteerID
      WHERE status = 1 '.$andWhere.$order;
      $volunteers =  $this->modelsManager->executeQuery($phql);

      // echo count($volunteers);

      $dataArray = array();
      foreach ($volunteers as $key => $value) {
        $name = $value->fname.' '.$value->mname.' '.$value->lname;
        $name .= !empty($value->extname)?' '.$value->extname:'';
        $dataArray[] = array(
          'volunteerID'=>$value->volunteerID,
          'username'=>$value->username,
          'name'=>$name,
          'title' => $title,
          'email'=>$value->email,
          'phone'=>$value->phone,
          'dateAdded' => $value->dateAdded
          );
      }

      $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
        "data" => $dataArray,
        "limit"=> 10,
        "page" => $numberPage
        ));
      $this->view->page = $paginator->getPaginate();
//for the list end==================================



      // $this->view->viewlist = false;
      $this->view->volunteer = $volunteer = Tblvolunteers::findFirst('volunteerID='.$volID);
      $phql = 'SELECT  Tblprograms.programName, Tblporgramactivities.activity
      FROM Tblvolunteeractivities
      LEFT JOIN Tblporgramactivities ON Tblporgramactivities.activityID = Tblvolunteeractivities.activityID
      LEFT JOIN Tblprograms ON Tblprograms.programID = Tblporgramactivities.programID
      WHERE Tblvolunteeractivities.volunteerID = '.$volID.' AND Tblvolunteeractivities.activityID != 0
      ORDER BY Tblprograms.programName
      ';
      $volunteerActivities =  $this->modelsManager->executeQuery($phql);
      $acitvityHtml = null;
      if(count($volunteerActivities) > 0){

        $proName = $volunteerActivities[0]->programName;
        $acitvityHtml = '<strong>'.$proName.'</strong><ul>';
        foreach ($volunteerActivities as $key => $value) {
          if($proName != $value->programName){
            $proName = $value->programName;
            $acitvityHtml .= '</ul><strong>'.$proName.'</strong><ul>';
          }
          $acitvityHtml .= '<li>'.$value->activity.'</li>';
        }
        $acitvityHtml .= '</ul>';
      }

      if(!empty($acitvityHtml)){
        $acitvityHtml = '<hr />
        <div>
          <h3>Signed up activities</h3>
          '.$acitvityHtml.'
        </div>';
      }

      $this->view->volunteerActivities = $acitvityHtml;

      $volunteerSuggActivities = null;
      $sugg = Tblvolunteeractivities::find('volunteerID = '.$volID.' AND activityID = 0');
      foreach ($sugg as $key => $value) {
        $volunteerSuggActivities .= '<li>'.$value->custom.'</li>';
      }

      if(!empty($volunteerSuggActivities)){
        $volunteerSuggActivities = '<hr />
        <div>
          <h3>Suggested activities</h3>
          '.$volunteerSuggActivities.'
        </div>';
      }

      $this->view->volunteerSuggActivities = $volunteerSuggActivities;

    }else{


      $volFilter = null;
      $numberPage = 1;
      if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
        Tag::resetInput();
      }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
        $id = $this->request->getPost('recordID');
        if(!empty($id)){
          $this->_generalDelete($id, 'Tblvolunteers', 'volunteerID', 'Volunteer');
        }
        /* stay on search pages*/
        if ($this->request->isPost()) {
          // $query = Criteria::fromInput($this->di, 'Tblusers', array('userName' => $this->request->getPost('search_text'), 'userFirstname' => $this->request->getPost('search_text')));
          // $this->persistent->searchUserParams = $query->getParams();
          // $this->session->set("user_search_text", $this->request->getPost('search_text'));
          $keyword = trim($this->request->getPost('search_text'));
          $this->session->set("user_search_text", $keyword);
          $keyword = trim(preg_replace('/\s+/',' ', $keyword));
        } else {
          // $numberPage = $this->request->getQuery("page", "int");
          $searchtext = $this->session->get("user_search_text");
          $keyword = !empty($searchtext)?$searchtext:'';
        }
        /**/
      }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
        $id = $this->request->getPost('tbl_id');
        if(!empty($id)){
          $this->_generalDelete($id, 'Tblvolunteers', 'volunteerID', 'Volunteers');
        }
        /* stay on search pages*/
        if ($this->request->isPost()) {
          // $query = Criteria::fromInput($this->di, 'Tblusers', array('userName' => $this->request->getPost('search_text'), 'userFirstname' => $this->request->getPost('search_text')));
          // $this->persistent->searchUserParams = $query->getParams();
          // $this->session->set("user_search_text", $this->request->getPost('search_text'));
          $keyword = $this->request->getPost('search_text');
          $this->session->set("user_search_text", $keyword);
          $keyword = trim(preg_replace('/\s+/',' ', $keyword));
        } else {
          // $numberPage = $this->request->getQuery("page", "int");
          $searchtext = $this->session->get("user_search_text");
          $keyword = !empty($searchtext)?$searchtext:'';
        }
        /**/
      }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
        $this->session->remove("vol_search_text");
        $this->session->remove("vol_filter_date");
        $this->session->remove("vol_filter_date_from");
        $this->session->remove("vol_filter_date_to");
        $this->persistent->searchvolParams = null;
        unset($_POST);
      }else{
        if ($this->request->isPost()) {
          $keyword = $this->request->getPost('search_text');
          $this->session->set("vol_search_text", $keyword);
          $keyword = trim(preg_replace('/\s+/',' ', $keyword));

          $volFilter = $this->request->getPost('volFilter');
          $this->session->set("volFilter", $volFilter);
        } else {
          echo $volFilter = $this->session->get("volFilter");
          $searchtext = $this->session->get("vol_search_text");
          $keyword = !empty($searchtext)?$searchtext:'';
          $numberPage = $this->request->getQuery("page", "int");
        }
      }

      $phql = 'SELECT activityID, activity, Tblporgramactivities.programID, programName
      FROM Tblporgramactivities
      LEFT JOIN Tblprograms ON Tblprograms.programID = Tblporgramactivities.programID
      ORDER BY Tblprograms.programID
      ';
      $activityOptions =  $this->modelsManager->executeQuery($phql);
      $actOptions = null;
      $pID = 0;
      $withSelected = false;
      foreach ($activityOptions as $key => $value) {
        if($pID != $value->programID){
          $pID = $value->programID;
          $actOptions .= '<optgroup label="'.$value->programName.'">';
        }
        if($volFilter == $value->activityID){
          $selected = 'selected';
          $withSelected = true;
        }else{
          $selected = '';
        }
        $actOptions .= '<option '.$selected.' value="'.$value->activityID.'">'.$value->activity.'</option>';
        if($pID != $value->programID){
          $actOptions .= '</optgroup>';
        }
      }
      $actOptions .= !empty($actOptions)?'</optgroup>':'';
      $default = $withSelected?'':'selected';
      $actOptions .= '<option '.$default.' value="">All Activities</option>';
      $this->view->activityOptions = $actOptions;
      $this->view->viewlist = true;


      if(!empty($keyword)){
        $keyword = trim(preg_replace('/\s+/',' ', $keyword));
        $this->flash->notice('Search results for "<strong>' . $this->session->get("vol_search_text") .'</strong>"');
        $andWhere = 'AND (username LIKE "%'.$keyword.'%" OR fname LIKE "%'.$keyword.'%" OR mname LIKE "%'.$keyword.'%" OR lname LIKE "%'.$keyword.'%" OR extname LIKE "%'.$keyword.'%" OR email LIKE "%'.$keyword.'%" OR phone LIKE "%'.$keyword.'%")';
      }if($this->request->getPost('submit')){
      // else{
        // $andWhere = null;
        //$andWhere .= !empty($volFilter)?' AND Tblvolunteeractivities.activityID = '.$volFilter;
        $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):"";
        $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):"";
        if(($fromDate && $toDate) || $this->session->has("vol_filter_date") ) {
          if($fromDate > $toDate) {
            return $this->flash->warning("Invalid request. End date must be later than the start date.");
          }
            // if($fromDate < $toDate) {
            //   return $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
            // }
          if($this->session->has("vol_filter_date")) {
            if(!$fromDate && !$toDate){
              $fromDate = trim($this->session->get('vol_filter_date_from'));
              $toDate = trim($this->session->get('vol_filter_date_to'));
              return $this->flash->warning("From and To date is required.");
            }
          } else {
            $this->session->set("vol_filter_date", true);
            $this->session->set("vol_filter_date_from", $fromDate);
            $this->session->set("vol_filter_date_to", $toDate);
          }
          //return $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
          $andWhere = " AND (Tblvolunteers.dateAdded BETWEEN $fromDate AND $toDate)";
        }
        else {
          return $this->flash->warning("From and To date is required.");
        }

      }

      // SORTING
      // Added a server side sorting on all tables using the columns shown
      // Dont forget to insert na href params on the view
      $sort = $this->request->getQuery("sort");
      $order = "";
      $this->view->nameHref = "name-asc";
      $this->view->usernameHref = "username-asc";
      $this->view->emailHref = "email-asc";
      $this->view->phoneHref = "phone-asc";
      $this->view->dateaddedHref = "dateAdded-asc";
      $arr = explode("-", $sort);
      switch ($arr[0]) {
        case 'name':
        $arr[1] == "asc" ? $this->view->nameHref = $arr[0] . "-desc" : "";
        $custom = 'CONCAT(fname, " ",mname, " ", lname)';
        $order = " ORDER BY $custom $arr[1]";
        $this->view->nameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'username':
        $arr[1] == "asc" ? $this->view->usernameHref = $arr[0] . "-desc" : "";
        $order = " ORDER BY $arr[0] $arr[1]";
        $this->view->usernameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'email':
        $arr[1] == "asc" ? $this->view->emailHref = $arr[0] . "-desc" : "";
        $order = " ORDER BY $arr[0] $arr[1]";
        $this->view->emailIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'phone':
        $arr[1] == "asc" ? $this->view->phoneHref = $arr[0] . "-desc" : "";
        $order = " ORDER BY $arr[0] $arr[1]";
        $this->view->phoneIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'dateAdded':
        $arr[1] == "asc" ? $this->view->dateaddedHref = $arr[0] . "-desc" : "";
        $order = " ORDER BY Tblvolunteers.$arr[0] $arr[1]";
        $this->view->dateaddedIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        default:
        break;
      }

      $andWhere = !empty($volFilter)?' AND Tblvolunteeractivities.activityID = '.$volFilter:$andWhere;
      if($order == ""){
         $phql = 'SELECT DISTINCT Tblvolunteers.volunteerID,username, title, fname, mname, lname, extname, email, phone, Tblvolunteers.dateAdded
        FROM Tblvolunteeractivities
        LEFT JOIN Tblvolunteers ON Tblvolunteers.volunteerID = Tblvolunteeractivities.volunteerID
        WHERE status = 1 '.$andWhere.' ORDER BY Tblvolunteers.dateAdded ASC';
        $volunteers =  $this->modelsManager->executeQuery($phql);
      }else{
        $phql = 'SELECT DISTINCT Tblvolunteers.volunteerID,username, title, fname, mname, lname, extname, email, phone, Tblvolunteers.dateAdded
        FROM Tblvolunteeractivities
        LEFT JOIN Tblvolunteers ON Tblvolunteers.volunteerID = Tblvolunteeractivities.volunteerID
        WHERE status = 1 '.$andWhere.$order.'ORDER BY Tblvolunteers.dateAdded ASC';
        $volunteers =  $this->modelsManager->executeQuery($phql);
      }


      // echo count($volunteers);

      $dataArray = array();
      foreach ($volunteers as $key => $value) {
        $name = $value->fname.' '.$value->mname.' '.$value->lname;
        $name .= !empty($value->extname)?' '.$value->extname:'';
        $dataArray[] = array(
          'volunteerID'=>$value->volunteerID,
          'username'=>$value->username,
          'name'=>$name,
          'email'=>$value->email,
          'phone'=>$value->phone,
          'dateAdded' => $value->dateAdded
          );
      }

      $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
        "data" => $dataArray,
        "limit"=> 10,
        "page" => $numberPage
        ));
      $this->view->page = $paginator->getPaginate();
    }
  }
//////////////////////////////////////////////////////////////////////////////////////////////////
  public function townsAction($view = "list"){
    \Phalcon\Tag::prependTitle('Towns | ');
    $this->view->menu = $this->_menuActive('townrole');
    $this->view->delete="";


    if($view == 'list'){
      $this->view->viewlist = true;
      $numberPage = 1;
      if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
        Tag::resetInput();
      }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
        $id = $this->request->getPost('recordID');
        if(!empty($id)){
          $this->_deleteTown($id);
          $this->view->delete="<div class='alert alert-success'>Town was successfully deleted.</div>";

        }
        /* stay on search pages*/
        if ($this->request->isPost()) {
          $keyword = $this->request->getPost('search_text');
          $this->session->set("user_search_text", $keyword);
          $keyword = trim(preg_replace('/\s+/',' ', $keyword));
        } else {
          // $numberPage = $this->request->getQuery("page", "int");
          $searchtext = $this->session->get("user_search_text");
          $keyword = !empty($searchtext)?$searchtext:'';
        }
        /**/
      }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
        $id = $this->request->getPost('tbl_id');
        if(!empty($id)){
          $this->_deleteTown($id);
          $this->view->delete="<div class='alert alert-success'>Town was successfully deleted.</div>";

        }
        /* stay on search pages*/
        if ($this->request->isPost()) {
          $keyword = $this->request->getPost('search_text');
          $this->session->set("user_search_text", $keyword);
          $keyword = trim(preg_replace('/\s+/',' ', $keyword));
        } else {
          $searchtext = $this->session->get("user_search_text");
          $keyword = !empty($searchtext)?$searchtext:'';
        }
        /* end of search page */
      }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
        $this->session->remove("town_search_text");
        $this->session->remove("town_filter_date");
        $this->session->remove("town_filter_date_from");
        $this->session->remove("town_filter_date_to");
        $this->persistent->searchtownParams = null;
        unset($_POST);
        header('refresh:1; url=/admin/towns');
      }else{
        if ($this->request->isPost()) {
          $keyword = $this->request->getPost('search_text');
          $keyword = trim(preg_replace('/\s+/',' ', $keyword));
          $query = Criteria::fromInput($this->di, 'Tbltowns', array('townName' => $keyword));
          $this->persistent->searchtownParams = $query->getParams();
          $this->session->set("town_search_text", $keyword);

        } else {
          $numberPage = $this->request->getQuery("page", "int");
        }
      }


      $parameters = array();
      if ($this->persistent->searchtownParams) {
        $parameters = $this->persistent->searchtownParams;
      }

      // SORTING
      // Added a server side sorting on all tables using the columns shown
      // Dont forget to insert na href params on the view
      $sort = $this->request->getQuery("sort");

      $this->view->townnameHref = "townName-asc";
      $this->view->latitudeHref = "townLat-asc";
      $this->view->longitudeHref = "townLong-asc";
      $this->view->dateaddedHref = "dateAdded-asc";
      $arr = explode("-", $sort);
      switch ($arr[0]) {
        case 'townName':
        $arr[1] == "asc" ? $this->view->townnameHref = $arr[0] . "-desc" : "";
        $order = "$arr[0] $arr[1]";
        $this->view->townnameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'townLat':
        $arr[1] == "asc" ? $this->view->latitudeHref = $arr[0] . "-desc" : "";
        $order = "$arr[0]*1 $arr[1]";
        $this->view->latitudeIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'townLong':
        $arr[1] == "asc" ? $this->view->longitudeHref = $arr[0] . "-desc" : "";
        $order = "$arr[0]*1 $arr[1]";
        $this->view->longitudeIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        case 'dateAdded':
        $arr[1] == "asc" ? $this->view->dateaddedHref = $arr[0] . "-desc" : "";
        $order = "$arr[0] $arr[1]";
        $this->view->dateaddedIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
        break;
        default:
        break;
      }

      $builder = $this->modelsManager->createBuilder()
      ->columns('townID,townName, townLat, townLong, dateAdded')
      ->from('Tbltowns')->orderBy($order);

      if(!empty($parameters)){
        $this->flash->notice('Search results for "<strong>' . $this->session->get("town_search_text") .'</strong>"');
        $builder->andWhere($parameters['conditions'], $parameters['bind']);
      }
      // else
      if($this->request->getPost('submits'))
      {
        $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):"";
        $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):"";
        if(($fromDate && $toDate) || $this->session->has("town_filter_date") ) {
          if($fromDate > $toDate) {
            return $this->flash->warning("Invalid request. End date must be later than the start date.");
          }
          if($fromDate < $toDate){
              $this->view->flash='<div class="alert alert-info"> List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"</div>';
              $builder->where("dateAdded BETWEEN $fromDate AND $toDate");
              if(empty($fromDate)){
                  return $this->flash->warning("From date is required");
              }
          }
          if($this->session->has("town_filter_date")) {
            if(!$fromDate && !$toDate){
              $fromDate = trim($this->session->get('town_filter_date_from'));
              $toDate = trim($this->session->get('town_filter_date_to'));
              return $this->flash->warning("From and To date is required.");
            }
          } else {
            $this->session->set("town_filter_date", true);
            $this->session->set("town_filter_date_from", $fromDate);
            $this->session->set("town_filter_date_to", $toDate);
          }
          // $this->flash->notice(' dfsf List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');

        }
        if(empty($fromDate) || empty($toDate) ){
          return $this->flash->warning("From and To date is required.");
        }
      }
      else{
        if(empty($this->request->getPost('search_text')) && ($this->request->getPost('search'))){
            return $this->flash->warning("Searh by text is required.");
        }

      }

      $paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
        "builder" => $builder,
        "limit"=> 10,
        "page" => $numberPage
        ));

      // Get the paginated results
      $this->view->page = $paginator->getPaginate();
    }else{
      $this->view->viewlist = false;
      $where = null;
      if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
        Tag::resetInput();
      }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
        $this->session->remove("town_search_text");
        $this->session->remove("town_filter_date");
        $this->session->remove("town_filter_date_from");
        $this->session->remove("town_filter_date_to");
        $this->persistent->searchtownParams = null;
        unset($_POST);
      }else{
        if ($this->request->isPost()) {
          $keyword = $this->request->getPost('search_text');
          $keyword = trim(preg_replace('/\s+/',' ', $keyword));
          $this->session->set("town_search_text", $keyword);
        }

        if($this->session->get("town_search_text")){
          $where .= ' WHERE townName LIKE "%'.$this->session->get('town_search_text').'%"';
          $this->flash->notice('Search results for "<strong>' . $this->session->get("town_search_text") .'</strong>"');
        } else {
          $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):"";
          $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):"";
          if(($fromDate && $toDate) || $this->session->has("town_filter_date") ) {
            if($fromDate > $toDate) {
              return $this->flash->warning("Invalid request. End date must be later than the start date.");
            }
            if($this->session->has("town_filter_date")) {
              if(!$fromDate && !$toDate){
                $fromDate = trim($this->session->get('town_filter_date_from'));
                $toDate = trim($this->session->get('town_filter_date_to'));
                  return $this->flash->warning("From and To date is required.");
              }
            } else {
              $this->session->set("town_filter_date", true);
              $this->session->set("town_filter_date_from", $fromDate);
              $this->session->set("town_filter_date_to", $toDate);
            }
           // $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
            $builder->where("dateAdded BETWEEN $fromDate AND $toDate");
          }
        }
      }


      $phql = 'SELECT * FROM Tbltowns'.$where;
      $result = $this->modelsManager->executeQuery($phql);


      $townArray = array();
      foreach ($result as $key => $value) {
        $townArray[] = array(
          'townID'=>$value->townID,
          'townName'=>$value->townName,
          'townLat'=>$value->townLat,
          'townLong'=>$value->townLong,
          'townInfo'=>$this->_truncateHtml(trim($value->townInfo), 150)
          );

      }
      $this->view->townMarkers = $townArray;
      $this->view->viewTownsMap = 1;
    }
  }

  public function townsgalleryAction($townID, $filename = NULL){
    \Phalcon\Tag::prependTitle('Town Pictures | ');
    $this->view->townID = $townID;
    $this->view->town = Tbltowns::findFirst('townID = '.$townID);

    $this->_prinTownTab(3, $townID);


    if ($this->request->isPost() == true) {
      if ($this->request->isAjax() == true) {
        $this->view->disable();
        $response = array();
        $albumName = $this->request->getPost('albumName', 'striptags');

        if (strpbrk($albumName, "\\/?%*:|\"<>") === FALSE) {
          if(!empty($albumName)){
            $editAlbumIDPost = $this->request->getPost('editAlbumID');
            if(!empty($editAlbumIDPost)){
              $origAlbumName = $this->request->getPost('origAlbumName');
              if($origAlbumName != $albumName){
                $path = '../public/img/towns/'.$townID.'/'.$origAlbumName;

                $checkAlbum = Tbltownalbums::findFirst('townID = '.$townID.' AND albumName = "'.$albumName.'"');
                if(!$checkAlbum){
                  if(rename($path, '../public/img/towns/'.$townID.'/'.$albumName)){
                    $album = Tbltownalbums::findFirst('albumID='.$this->request->getPost('editAlbumID', 'int'));
                    if($album){
                      $album->assign(array(
                        'townID'=>$townID,
                        'albumName'=>$albumName,
                        'dateCreated'=>time()
                        ));
                      $album->save();
                      $response['success'] = true;
                      $response['message'] = '<span class="label label-success">Album successfully updated.</span>';
                    }
                  }else{
                    $response['success'] = false;
                    $response['message'] = '<span class="label label-danger">Unable to rename album name.</span>';
                  }
                }else{
                  $response['success'] = false;
                  $response['message'] = '<span class="label label-danger">Album name already exist.</span>';
                }
              }else{
                $response['success'] = true;
                $response['message'] = '';
              }
            }else{

              $checkAlbumName = Tbltownalbums::findFirst('townID = '.$townID.' AND albumName = "'.$albumName.'"');
              if(!$checkAlbumName){
                $path = '../public/img/towns/'.$townID;
                if(!is_dir($path)){
                  mkdir($path);
                }

                $path .= '/'.$albumName;

                if(mkdir($path)){
                  $album = new Tbltownalbums();
                  $album->assign(array(
                    'townID'=>$townID,
                    'albumName'=>$albumName,
                    'dateCreated'=>time()
                    ));
                  if($album->save()){
                    $response['success'] = true;
                    $response['message'] = '<span class="label label-success">Album successfully created.</span>';
                  }else{
                    $response['success'] = true;
                    foreach ($post->getMessages() as $message) {
                      $response['message'] = (string) $message;
                    }
                  }
                }else{
                  $response['success'] = false;
                  $response['message'] = '<span class="label label-danger">Unable to create album.</span>';
                }
              }else{
                $response['success'] = false;
                $response['message'] = '<span class="label label-danger">Album name already exist.</span>';
              }
            }
          }else{
            $response['success'] = false;
            $response['message'] = '<span class="label label-danger">Album name is required.</span>';
          }
        }
        else {
          $response['success'] = false;
          $response['message'] = '<span class="label label-danger">Album name contains invalid character(s).</span>';
        }
        echo json_encode($response);
      }
    }

    $this->view->menu = $this->_menuActive('townrole');
    $numberPage = 1;
    if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
      Tag::resetInput();
    }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
      $id = $this->request->getPost('recordID');
      if(!empty($id)){
        $townAlbum = Tbltownalbums::findFirst('albumID='.$id);
        $albumName = $townAlbum->albumName;
        $path = '../public/img/towns/'.$albumName;

        $townAlbum->delete();
        $townPic = Tbltownpictures::find('albumID = '.$id);
        $townPic->delete();
        $this->flash->success($albumName." was deleted successfully.");
        /* if($this->_deletefolder($path)){
        $townAlbum->delete();
        $townPic = Tbltownpictures::find('albumID = '.$id);
        $townPic->delete();
        $this->flash->success($albumName." was deleted successfully.");
      }else{
      $this->flash->error("Unable to delete ".$albumName.".");
    }*/
  }
}elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
  $id = $this->request->getPost('tbl_id');
  if(!empty($id)){
    $param=null;
    foreach ($id as $i) {
      $param .= 'albumID = '. $this->filter->sanitize($i, array("int")) . ' OR ';
    }
    $param = substr($param, 0, -4);

    $post = Tbltownalbums::find($param);
    if (!$post) {
      $this->flash->error("Failed to delete selected albums.");
    }else{
      foreach ($post as $key => $value) {
        $albumName = $value->albumName;
        $path = '../public/img/towns/'.$albumName;
        $this->_deletefolder($path);
      }
      if (!$post->delete()) {
        foreach ($post->getMessages() as $message) {
          $this->flash->error((string) $message);
        }
        return $this->forward("admin/townsgallery");
      } else {
        $townPic = Tbltownpictures::find($param);
        $townPic->delete();
        $this->flash->success("Album(s) deleted successfully.");
      }
    }

  }
}

$andWhere = '';

if($this->request->isPost() && $this->request->getPost('clear_search')){
  $this->session->remove("town_album_search_album_text");
  unset($_POST);
}elseif($this->request->isPost() && $this->request->getPost('searchBtn')){
  $andWhere .= ' AND Tbltownalbums.albumName LIKE "%'.$this->request->getPost('search_album_text', 'striptags').'%" ';
  $this->session->set("town_album_search_album_text", $this->request->getPost('search_album_text'));
  $this->flash->notice('Search results for "<strong>' . $this->request->getPost('search_album_text') .'</strong>"');
}else{
  if($this->session->get("town_album_search_album_text")){
    $andWhere .= ' AND Tbltownalbums.albumName LIKE "%'.$this->session->get('town_album_search_album_text').'%" ';
    $this->flash->notice('Search results for "<strong>' . $this->session->get("town_album_search_album_text") .'</strong>"');
  }
}

// SORTING
// Added a server side sorting on all tables using the columns shown
// Dont forget to insert na href params on the view
$sort = $this->request->getQuery("sort");
$order = "Tbltownalbums.albumID DESC";
$this->view->nameHref = "albumName-asc";
$this->view->picturesHref = "number_photos-asc";
$this->view->dateHref = "dateCreated-asc";
$arr = explode("-", $sort);
switch ($arr[0]) {
  case 'albumName':
  $arr[1] == "asc" ? $this->view->nameHref = $arr[0] . "-desc" : "";
  $order = "Tbltownalbums.$arr[0] $arr[1]";
  $this->view->nameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
  break;
  case 'number_photos':
  $arr[1] == "asc" ? $this->view->picturesHref = $arr[0] . "-desc" : "";
  $order = "$arr[0] $arr[1]";
  $this->view->picturesIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
  break;
  case 'dateCreated':
  $arr[1] == "asc" ? $this->view->dateHref = $arr[0] . "-desc" : "";
  $order = "Tbltownalbums.$arr[0] $arr[1]";
  $this->view->dateIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
  break;
  default:
  break;
}

$phql = 'SELECT
COUNT(Tbltownpictures.pictureID) as number_photos,
Tbltownalbums.albumName,
Tbltownalbums.dateCreated,
Tbltownalbums.albumID
FROM Tbltownalbums
LEFT JOIN Tbltownpictures ON Tbltownalbums.albumID = Tbltownpictures.albumID
WHERE Tbltownalbums.townID = '.$townID.' '.$andWhere.'
GROUP BY Tbltownalbums.albumName
ORDER BY '.$order;
$result = $this->modelsManager->executeQuery($phql);

$dataArray = array();
foreach ($result as $key => $value) {
  $dataArray[] = array(
    'picCount'=>(!$value->number_photos)?'empty':$value->number_photos,
    'albumID'=>$value->albumID,
    'albumName'=>$value->albumName,
    'dateCreated'=>$value->dateCreated
    );
}

$paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
  "data" => $dataArray,
  "limit"=> 10,
  "page" => $numberPage
  ));
$this->view->page = $paginator->getPaginate();
}

public function townAlbumAction($townID, $albumID, $filename = NULL){
  $this->view->townID = $townID;
  $this->view->town = Tbltowns::findFirst('townID = '.$townID);
  $this->_prinTownTab(3, $townID);

  $numberPage = $this->request->getQuery("page", "int")?$this->request->getQuery("page", "int"):1;
  //$numberPage = 1;

  $this->view->menu = $this->_menuActive('townrole');
  $album = Tbltownalbums::findFirst('albumID = '.$albumID);
  $this->view->albumName = $albumName = $album->albumName;
  $this->view->albumCoverID = $album->coverID;
  $this->view->albumID = $albumID;

  if ($this->request->isAjax() == true) {
    $this->view->disable();
    if($this->request->getPost('deleteLastTownPic')){
      $response = array('success'=>false);
      $deleteLastTownPic = $this->request->getPost('deleteLastTownPic');
      $townPic = Tbltownpictures::findFirst('pictureID = '.$deleteLastTownPic);
      if($townPic){
        if(is_file('../public/img/towns/'.$townID.'/'.$albumName.'/'.$townPic->pictureFilename)){
          if(unlink('../public/img/towns/'.$townID.'/'.$albumName.'/'.$townPic->pictureFilename)){
            if(is_file('../public/img/towns/'.$townID.'/'.$albumName.'/thumbnail/'.$townPic->pictureFilename)){
              unlink('../public/img/towns/'.$townID.'/'.$albumName.'/thumbnail/'.$townPic->pictureFilename);
            }
            $townPic->delete();
            $response['success']=true;
          }else{
            $response['message']='unable to delete file';
          }
        }else{
          $response['message']='File not found';
        }
      }

      echo json_encode($response);
    }else{
      $path = null;

      $name = pathinfo($filename, PATHINFO_FILENAME);
      $ext = pathinfo($filename, PATHINFO_EXTENSION);

      $path = '../public/img/towns/'.$townID.'/'.$albumName;

      $uniqueFilename = $filename;
      $increment = null;
      while(file_exists($path.'/'.$name . $increment . '.' . $ext)) {
        $increment++;
        $uniqueFilename = $name . $increment . '.' . $ext;
      }

      if(is_file('../public/server/php/files/'.$filename)){
        if(!is_dir($path)){
          mkdir($path);
        }

        rename('../public/server/php/files/'.$filename, $path.'/'.$uniqueFilename);
      }
      if(is_file('../public/server/php/files/thumbnail/'.$filename)){
        if(!is_dir($path.'/thumbnail')){
          mkdir($path.'/thumbnail');
        }
        rename('../public/server/php/files/thumbnail/'.$filename, $path.'/thumbnail/'.$uniqueFilename);
      }

      $filename = $uniqueFilename;

      $picture = new Tbltownpictures();
      $picture->assign(array(
        'townID' => $townID,
        'albumID' => $albumID,
        'pictureFilename' => $filename,
        'pictureSize'=>filesize($path.'/'.$filename),
        'pictureCaption' => $filename,
        'dateUploaded' => time()
        ));
      $picture->save();
      $lastID = $picture->pictureID;


      $imgpath = $this->url->get().'img/towns/'.$townID.'/'.$albumName.'/'.$filename;
      $newfilename = strlen($filename) > 15 ? substr($filename,0,15)."...".$ext : $filename;

      echo $html = '
      <div class="program-digital-assets-library pull-left" style="position: relative">
        <div style="padding-left: 25px; width: 100%; word-wrap:break-word;">'.$newfilename.'</div>
        <a href="'.$imgpath.'" class="prettyPhoto[pp_gal]"><img src="'.$imgpath.'" alt=""></a>
        <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="'.$imgpath.'">
        <button class="btn btn-xs btn-danger delete-recent-upload-townpic" data-picture-id="'.$lastID.'" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
        <div class="clearfix"></div>
      </div>
      ';
    }
  }else{

    $albums = Tbltownalbums::find('townID = '.$townID);
    $albumOptions = null;
    foreach ($albums as $key => $value) {
      $selected = $value->albumID==$albumID?'selected':'';
      $albumOptions .= '<option '.$selected.' value="'.$value->albumID.'">'.$value->albumName.'</option>';
    }
    $albumOptions = '<select name="townPicalbumID" class="form-control">'.$albumOptions.'</select>';
    $this->view->albumOptions = $albumOptions;

    $albumOptions2 = null;
    foreach ($albums as $key => $value) {
      $selected = $value->albumID==$albumID?'selected':'';
      $albumOptions2 .= '<option '.$selected.' value="/admin/townAlbum/'.$townID.'/'.$value->albumID.'">'.$value->albumName.'</option>';
    }
    $albumOptions2 = '<select id="albumOptions2" class="form-control">'.$albumOptions2.'</select>';
    $this->view->albumOptions2 = $albumOptions2;

    if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf') && $this->security->getSessionToken() != $this->request->getPost('town_csrf')){
      Tag::resetInput();
    }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
      $id = $this->request->getPost('recordID');
      if(!empty($id)){
        $townPic = Tbltownpictures::findFirst('pictureID='.$id);
        if($townPic){
          $townPicName = $townPic->pictureCaption;

          $path = '../public/img/towns/'.$townID.'/'.$albumName.'/'.$townPic->pictureFilename;
          if(unlink($path)){
            unlink('../public/img/towns/'.$townID.'/'.$albumName.'/thumbnail/'.$townPic->pictureFilename);
            $townPic->delete();
            $this->flash->success($townPicName." was deleted successfully.");
          }else{
            $this->flash->error("Unable to delete ".$albumName.".");
          }
        }

      }
    }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
      $id = $this->request->getPost('tbl_id');
      if(!empty($id)){
        $param=null;
        foreach ($id as $i) {
          $param .= 'pictureID = '. $this->filter->sanitize($i, array("int")) . ' OR ';
        }
        $param = substr($param, 0, -4);

        $post = Tbltownpictures::find($param);
        if (!$post) {
          $this->flash->error("Failed to delete selected pictures.");
        }else{
          foreach ($post as $key => $value) {
            $path = '../public/img/towns/'.$townID.'/'.$albumName.'/'.$value->pictureFilename;
            if (unlink($path)) {
              unlink('../public/img/towns/'.$townID.'/'.$albumName.'/thumbnail/'.$value->pictureFilename);
            }
          }

          if (!$post->delete()) {
            foreach ($post->getMessages() as $message) {
              $this->flash->error((string) $message);
            }
            return $this->forward("admin/townAlbum/".$albumID);
          } else {
            $this->flash->success("Album(s) deleted successfully.");
          }
        }

      }
    }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
      $this->session->remove("town_picture_search_text");
      unset($_POST);
    }elseif($this->request->isPost() && $this->request->getPost('save_town_pic')){
      $pictureCaption = $this->request->getPost('pictureCaption', 'striptags');
      $townPicalbumID = $this->request->getPost('townPicalbumID');
      $hTownPic = $this->request->getPost('hTownPic');

      if($this->request->getPost('albumCover')){
        $album->assign(array('coverID'=>$hTownPic));
        $album->save();
        $this->view->albumCoverID = $hTownPic;
      }

      $townPic = Tbltownpictures::findFirst('pictureID='.$hTownPic);
      if($townPic){
        if($townPicalbumID!=$albumID){
          $path = '../public/img/towns/'.$townID.'/'.$albumName.'/'.$townPic->pictureFilename;
          $thumbpath = '../public/img/towns/'.$townID.'/'.$albumName.'/thumbnail/'.$townPic->pictureFilename;

          $renameAlbum = Tbltownalbums::findFirst('albumID='.$townPicalbumID);
          $renamePath = '../public/img/towns/'.$townID.'/'.$renameAlbum->albumName.'/'.$townPic->pictureFilename;
          if(!is_dir('../public/img/towns/'.$townID.'/'.$renameAlbum->albumName.'/thumbnail')){
            mkdir('../public/img/towns/'.$townID.'/'.$renameAlbum->albumName.'/thumbnail');
          }
          $renamethumbpath = '../public/img/towns/'.$townID.'/'.$renameAlbum->albumName.'/thumbnail/'.$townPic->pictureFilename;

          rename($path, $renamePath);
          rename($thumbpath, $renamethumbpath);
        }
        $townPic->assign(array('albumID'=>$townPicalbumID, 'pictureCaption'=>$pictureCaption));
        if($townPic->save()){
          $this->flash->success("Picture was updated successfully.");
        }else{
          $this->flash->error("Picture failed to update.");
        }
      }
    }


    $where = null;
    if($this->request->isPost() && $this->request->getPost('searchBtn')){
      $where .= ' AND pictureCaption LIKE "%'.$this->request->getPost('search_text').'%"';
      $this->session->set("town_picture_search_text", $this->request->getPost('search_text'));
    }

    if($this->session->get("town_picture_search_text")){
      $this->flash->notice('Search results for "<strong>' . $this->session->get("town_picture_search_text") .'</strong>"');
    }

    $phql = 'SELECT *
    FROM Tbltownpictures
    WHERE Tbltownpictures.townID = '.$townID.' AND Tbltownpictures.albumID = '.$albumID.$where;
    $result = $this->modelsManager->executeQuery($phql);


    $dataArray = array();
    foreach ($result as $key => $value) {
      $dataArray[] = array(
        'pictureID'=>$value->pictureID,
        'pictureFilename'=>$value->pictureFilename,
        'pictureCaption'=>$value->pictureCaption,
        'pictureSize'=>$this->_bytesToSize1024($value->pictureSize),
        'dateUploaded'=>$value->dateUploaded,
        'path'=>$this->url->get().'img/towns/'.$townID.'/'.$albumName.'/'.$value->pictureFilename,
        'thumbPath'=>$this->url->get().'img/towns/'.$townID.'/'.$albumName.'/thumbnail/'.$value->pictureFilename
        );
    }

    $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
      //$paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
      "data" => $dataArray,
      "limit"=> 10,
      "page" => $numberPage
      ));
    $this->view->page = $paginator->getPaginate();
  }

}

public function ajaxTowneventView($id){

}

public function towneventsAction($townID, $action = NULL, $eventID = NULL){
  \Phalcon\Tag::prependTitle('Town Events | ');
  $this->view->townID = $townID;
  $this->view->town = Tbltowns::findFirst('townID = '.$townID);
  $this->_prinTownTab(2, $townID);
  $this->view->menu = $this->_menuActive('townrole');
  if ($this->request->isAjax() == true) {
    $this->view->disable();
    $response = array();
    if($this->request->getPost('albumID')){
      $townPictures = $this->_getAllTownPictures($townID, $this->request->getPost('albumID'));
      $response['message'] = $townPictures;
    }elseif($this->request->getPost('pictureID') && $this->request->getPost('albumName')){
      $townPic = Tbltownpictures::findFirst('pictureID='.$this->request->getPost('pictureID'));
      $albumName = $this->request->getPost('albumName');
      if($townPic){
        $townPicName = $townPic->pictureCaption;

        $path = '../public/img/towns/'.$townID.'/'.$albumName.'/'.$townPic->pictureFilename;
        if(unlink($path)){
          unlink('../public/img/towns/'.$townID.'/'.$albumName.'/thumbnail/'.$townPic->pictureFilename);
          $townPic->delete();
          $response['success'] = true;
        }else{
          $response['success'] = false;
        }
      }
    }
    echo json_encode($response);
  }elseif(!empty($action) && $action=="new"){
    $form = new CreatepartnersForm(null, array('createPartnerEvent'=>true));
     $this->view->evtForm = $form;
     $this->view->action = 'new';
     $this->view->Ename = null;
     $this->view->Edate = null;
     $this->view->Evenue = null;
     $this->view->Edetails = null;
     $this->view->errnotice=null;

     //townpictures
     $albums = Tbltownalbums::find();
     $albumOptions = '<option value="all">All Pictures</option>';
     foreach ($albums as $key => $value) {
        $albumOptions .= '<option value="'.$value->albumID.'">'.$value->albumName.'</option>';
     }
     $albumOptions = '<select id="townAlbumSelect" name="townAlbumSelect" class="form-control">'.$albumOptions.'</select>';
     $this->view->albumOptions = $albumOptions;
     $this->view->allTownPictures = $this->_getAllTownPictures($townID);

     if($this->request->getPost('saveEventPartner')){
          //validation counting params
          $w="";$x="";$y="";$z="";
          $a = trim($this->request->getpost('event_name','striptags'));
          $b = trim($this->request->getpost('event_date','striptags'));
          $c = trim($this->request->getpost('event_venue','striptags'));
          $d = $this->request->getpost('event_details');
          //check if event name alreadyh exists
          $eventExist = Tbltownevents::findFirst([
            'conditions' => "eventTitle='". $a ."'AND townID='". $townID ."'"
          ]);

        //programmatical validation long method
          if(empty($a)){
                $this->view->Ename .="<div class='label label-danger'>Name is required.</div>";
                $w="a";
          }elseif($eventExist){
                $this->view->Ename .="<div class='label label-danger'>Name already exists.</div>";
                $w="a";
          }else{
                $this->view->Ename .="";
                $w="OK";
          }

          if(empty($b)){
                $this->view->Edate .="<div class='label label-danger'>Date is required.</div>";
                $y="a";
          }else{
                $this->view->Edate .="";
                $y="OK";
          }

          if(empty($c)){
                $this->view->Evenue .="<div class='label label-danger'>Venue is required</div>";
                $x="a";
          }else{
                $this->view->Evenue .="";
                $x="OK";
          }

          if(empty($d)){
                $this->view->Edetails .="<div class='label label-danger'>Details is required</div>";
                $z="a";
          }else{
                $this->view->Edetails .="";
                $z="OK";
          }
        //progarmmatical validation long method

        //check if fields are valid
          if($w == "OK" && $x == "OK" && $y == "OK" && $z == "OK"){
              //create new event
              $event = new Tbltownevents();
              $event->assign(array(
                'townID'=>$townID,
                'eventTitle' => trim($this->request->getPost('event_name', 'striptags')),
                'eventDate'=>strtotime($this->request->getPost('event_date', 'striptags')),
                'eventVenue'=>trim($this->request->getPost('event_venue', 'striptags')),
                'eventDetails' => trim($this->request->getPost('event_details')),
                'dateCreated' => time()
                ));
              if(!$event->save()){
                $this->flash->error($event->getMessages());
              }else{
               header('refresh:1; url=/admin/townevents/'.$townID.'/new');
                $this->view->errnotice .="<div class='alert alert-success'>Town event has been added successfully.</div>";
              }
          }
     }


    $this->view->evtForm = $form;

  }elseif(!empty($action) && $action == "edit"){
    $form = new CreatepartnersForm(null, array('createPartnerEvent'=>true));
    $this->view->evtForm = $form;
    $this->view->action = 'edit';
    $this->view->errJoke = 'False';
    //validation message carriers
    $this->view->Ename = null;
    $this->view->Edate = null;
    $this->view->Evenue = null;
    $this->view->Edetails = null;
    $this->view->errnotice=null;
     //event pictures
     $albums = Tbltownalbums::find();
     $albumOptions = '<option value="all">All Pictures</option>';
     foreach ($albums as $key => $value) {
       $albumOptions .= '<option value="'.$value->albumID.'">'.$value->albumName.'</option>';
     }
     $albumOptions = '<select id="townAlbumSelect" name="townAlbumSelect" class="form-control">'.$albumOptions.'</select>';
     $this->view->albumOptions = $albumOptions;
     $this->view->allTownPictures = $this->_getAllTownPictures($townID);
     $this->view->event = $event = Tbltownevents::findFirst('eventID='.$eventID);
      //if save button is clicked
      if($this->request->getPost('saveEventPartner')){
        //validation counting params
        $w="";$x="";$y="";$z="";
        //posted params
        $a = trim($this->request->getpost('event_name','striptags'));
        $b = trim($this->request->getpost('event_date','striptags'));
        $c = trim($this->request->getpost('event_venue','striptags'));
        $d = $this->request->getpost('event_details');
        //original event name
        $orig_title = $this->request->getPost('orig_event_name');

        if($a != $orig_title){//edit event start->
          //check if event name alreadyh exists
          $eventExist = Tbltownevents::findFirst([
            'conditions' => "eventTitle='". $a ."'AND townID='". $townID ."'"
          ]);

          //programmatical validation long method
          if(empty($a)){//check if event name is empty
                $this->view->Ename .="<div class='label label-danger'>Name is required.</div>";
                $w="a";
          }elseif($eventExist){//check if event name already exists
                $this->view->Ename .="<div class='label label-danger'>Name already exists.</div>";
                $w="a";
          }else{
                $this->view->Ename .="";
                $w="OK";
          }

          if(empty($b)){//check if event date is empty
                $this->view->Edate .="<div class='label label-danger'>Date is required.</div>";
                $y="a";
          }else{
                $this->view->Edate .="";
                $y="OK";
          }

          if(empty($c)){//check if event venue is empty
                $this->view->Evenue .="<div class='label label-danger'>Venue is required</div>";
                $x="a";
          }else{
                $this->view->Evenue .="";
                $x="OK";
          }

          if(empty($d)){//check if event details is empty
                $this->view->Edetails .="<div class='label label-danger'>Details is required</div>";
                $z="a";
          }else{
                $this->view->Edetails .="";
                $z="OK";
          }
          //progarmmatical validation long method

          //check if fields are valid
          if($w == "OK" && $x == "OK" && $y == "OK" && $z == "OK"){
              //edit event
              $this->view->event = $event = Tbltownevents::findFirst('eventID='.$eventID);
              //assign the values to the data array
              $event->assign(array(
                'eventTitle' => trim($this->request->getPost('event_name', 'striptags')),
                'eventDate'=> strtotime($this->request->getPost('event_date', 'striptags')),
                'eventVenue'=> trim($this->request->getPost('event_venue', 'striptags')),
                'eventDetails' => trim($this->request->getPost('event_details'))
              ));
              //check if successfull
              if(!$event->save()){
               $this->flash->error($event->getMessages());
              }else{
                header('refresh:2; url=/admin/townevents/'.$townID);
                $this->view->errnotice .="<div class='alert alert-success'>Town event has been updated successfully.</div>";
                $this->view->errJoke = "Success";
              }
          }
        }else{//if event name is not changed
          //progarmmatical validation long method
          if(empty($b)){//check if event date is empty
                $this->view->Edate .="<div class='label label-danger'>Date is required.</div>";
                $y="a";
          }else{
                $this->view->Edate .="";
                $y="OK";
          }

          if(empty($c)){//check if event venue is empty
                $this->view->Evenue .="<div class='label label-danger'>Venue is required</div>";
                $x="a";
          }else{
                $this->view->Evenue .="";
                $x="OK";
          }

          if(empty($d)){//check if event details is empty
                $this->view->Edetails .="<div class='label label-danger'>Details is required</div>";
                $z="a";
          }else{
                $this->view->Edetails .="";
                $z="OK";
          }
          //progarmmatical validation long method

          //check if fields are valid
          if($x == "OK" && $y == "OK" && $z == "OK"){
              //edit event
              $this->view->event = $event = Tbltownevents::findFirst('eventID='.$eventID);
              //assign the values to the data array
              $event->assign(array(
                'eventDate'=> strtotime($this->request->getPost('event_date', 'striptags')),
                'eventVenue'=> trim($this->request->getPost('event_venue', 'striptags')),
                'eventDetails' => trim($this->request->getPost('event_details'))
              ));
              //check if successfull
              if(!$event->save()){
               $this->flash->error($event->getMessages());
              }else{
                header('refresh:2; url=/admin/townevents/'.$townID);
                $this->view->errnotice .="<div class='alert alert-success'>Town event has been updated successfully.</div>";
                $this->view->errJoke = "Success";
              }
          }
        }//edit event end->
      }
      $this->view->evtForm = $form;
  }elseif(!empty($action) && $action == 'view'){
      $this->view->action = 'view';
      $this->view->event = Tbltownevents::findFirst('eventID='.$eventID);
  }else{
    $this->view->action = 'list';
    $numberPage = 1;
    if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
      Tag::resetInput();
    }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
      $id = $this->request->getPost('recordID');
      if(!empty($id)){
        $this->_generalDelete($id, 'Tbltownevents', 'eventID', 'Town event');
      }
    }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
      $id = $this->request->getPost('tbl_id');
      if(!empty($id)){
        $this->_generalDelete($id, 'Tbltownevents', 'eventID', 'Town event(s)');
      }
    }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
      $this->session->remove("town_event_search_text");
      $this->persistent->searchteParams = null;
      unset($_POST);
    }else{
      if ($this->request->isPost()) {
        $keyword = $this->request->getPost('search_text');
        $keyword = trim(preg_replace('/\s+/',' ', $keyword));
        $query = Criteria::fromInput($this->di, 'Tbltownevents', array('eventTitle' => $keyword));
        $this->persistent->searchteParams = $query->getParams();
        $this->session->set("town_event_search_text", $keyword);
      } else {
        $numberPage = $this->request->getQuery("page", "int");
      }
    }


    $parameters = array();
    if ($this->persistent->searchteParams) {
      $parameters = $this->persistent->searchteParams;
    }

    // SORTING
    // Added a server side sorting on all tables using the columns shown
    // Dont forget to insert na href params on the view
    $sort = $this->request->getQuery("sort");
    $order = "dateCreated DESC";
    $this->view->titleHref = "eventTitle-asc";
    $this->view->venueHref = "eventVenue-asc";
    $this->view->dateHref = "eventDate-asc";
    $arr = explode("-", $sort);
    switch ($arr[0]) {
      case 'eventTitle':
      $arr[1] == "asc" ? $this->view->titleHref = $arr[0] . "-desc" : "";
      $order = "$arr[0] $arr[1]";
      $this->view->titleIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;
      case 'eventVenue':
      $arr[1] == "asc" ? $this->view->venueHref = $arr[0] . "-desc" : "";
      $order = "$arr[0] $arr[1]";
      $this->view->venueIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;
      case 'eventDate':
      $arr[1] == "asc" ? $this->view->dateHref = $arr[0] . "-desc" : "";
      $order = "$arr[0] $arr[1]";
      $this->view->dateIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
      break;
      default:
      break;
    }

    $builder = $this->modelsManager->createBuilder()
    ->columns('eventID, eventTitle, eventDate, eventVenue, dateCreated')
    ->from('Tbltownevents')->where('townID = '.$townID)->orderBy($order);

    if(!empty($parameters)){
      $this->flash->notice('Search results for "<strong>' . $this->session->get("town_event_search_text") .'</strong>"');
      $builder->andWhere($parameters['conditions'], $parameters['bind']);
    }

    $paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
      "builder" => $builder,
      "limit"=> 10,
      "page" => $numberPage
      ));

    // Get the paginated results
    $this->view->page = $paginator->getPaginate();
  }
}

private function _getAllTownPictures($townID, $albumID = null){
  $where = is_null($albumID) || $albumID == 'all'?null:' WHERE Tbltownpictures.albumID = '.$albumID;
  $phql = 'SELECT Tbltownpictures.pictureID, Tbltownpictures.albumID, Tbltownpictures.pictureFilename, Tbltownalbums.albumName FROM Tbltownpictures
  LEFT JOIN Tbltownalbums ON Tbltownalbums.albumID = Tbltownpictures.albumID '.$where;
  $result = $this->modelsManager->executeQuery($phql);

  $html = null;

  foreach ($result as $key => $value) {
    $imgpath = $this->url->get().'img/towns/'.$townID.'/'.$value['albumName'].'/'.$value['pictureFilename'];
    $filename = $value['pictureFilename'];
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    $newfilename = strlen($filename) > 15 ? substr($filename,0,15)."...".$ext : $filename;
    $html .= '
    <div class="program-digital-assets-library pull-left" style="position: relative">
      <div style="padding-left: 25px; width: 100%; word-wrap:break-word;">'.$newfilename.'</div>
      <a href="'.$imgpath.'" class="prettyPhoto[pp_gal]"><img src="'.$imgpath.'" alt=""></a>
      <input type="text" onclick="this.focus();this.select()" name="picturename" class="form-control" value="'.$imgpath.'">
      <button class="btn btn-xs btn-danger delete-recent-upload-town-pic" data-album-name="'.$value['albumName'].'" data-picture-id="'.$value['pictureID'].'" style="position: absolute; top: 0px; left:0px; z-index:999999"><i class="icon-remove"></i> </button>
      <div class="clearfix"></div>
    </div>
    ';
  }

  $html = is_null($html)?'No pictures found.':$html;

  return $html;
}

public function pintownAction($townID = NULL){
  \Phalcon\Tag::prependTitle('Pin Town | ');
  $this->view->menu = $this->_menuActive('townrole');
  $form = new TownForm();
  $form->csrf = $this->security->getToken();
  $this->view->errTName=null;
  $this->view->errTInfo=null;

  if(!empty($townID)){
    $this->view->town = $town = Tbltowns::findFirst('townID='.$townID);
    $this->view->editMode = true;
    $this->view->message="Update Town";
    $this->view->head="Edit Town";
    $this->view->cancel="<a href='/admin/towns' class='btn btn-default'>Cancel</a>";
  }else{
    $this->view->editMode = false;
    $this->view->message="Save Town";
    $this->view->head="Add Town";
    $this->view->cancel="";
  }

  if($this->request->getPost('saveTown')){
    if($this->security->getSessionToken() != $this->request->getPost('csrf')){
      $data = array(
        'townName'=>trim($this->request->getPost('townName', 'striptags')),
        'townLat'=>trim($this->request->getPost('townLat')),
        'townLong' => trim($this->request->getPost('townLong')),
        'townInfo'=> trim($this->request->getPost('townInfo')),
        'dateAdded' => time()
        );

      if($townID != null){//start->
          //params
          $tInfo=$this->request->getPost('townInfo');
          $tName=$this->request->getPost('townName');
          $origTownName = $this->request->getPost('origTownName');

          //condittion if changed Tname
          if($origTownName != $tName){//start-->
            // $this->view->errNotice .= "<div class='alert alert-success'>EDIT ITO</div>";

              //check tname if null
              if(empty($tName)){
              $this->view->errTName.="<label class='label label-danger'>Town Name is required.</label>";
              }
              //check if tInfo is null
              if(empty($tInfo) || preg_match('/^\s*$/',$tInfo)){
                $this->view->errTInfo.="<label class='label label-danger'>Town Information is required.</label>";
              }else{
                 $this->view->errTInfo.="";
              }

            //check if the town name exists
            $checkTown = Tbltowns::findFirst('townName="'.$tName.'"');
            //condition if Tname don't exist yet
            if(!$checkTown){//start--->
              $town->assign($data);

              if (!$town->save()) {
                $this->flash->error($town->getMessages());
              } else {
                $this->view->errNotice.="<div class='alert alert-success'>Town successfully updated.</div>";
                Tag::resetInput();
              }
            }else{
              $this->view->errTName .= "<label class='label label-danger'>Town Name already exists.</label>";
            }//end--->
          }else{//end-->
            //check if tInfo is null
            if(empty($tInfo) || preg_match('/^\s*$/',$tInfo)){
              $this->view->errTInfo.="<label class='label label-danger'>Town Information is required.</label>";
            }else{
              $this->view->errTInfo.="";
            }

            $town->assign($data);
            if (!$town->save()) {
              $this->flash->error($town->getMessages());
            } else {
              $this->view->errNotice.="<div class='alert alert-success'>Town successfully updated.</div>";
              Tag::resetInput();
            }

          }
      }else{//end->
      //check if town name exists
      $checkTown = Tbltowns::findFirst('townName="'.$this->request->getPost('townName', 'striptags').'"');

        if(!$checkTown){
          $tInfo=$this->request->getPost('townInfo');
          $tName=$this->request->getPost('townName');
          $town = new Tbltowns();
          $town->assign($data);
          //check if tname is null
          if(empty($tName)){
            $this->view->errTName.="<label class='label label-danger'>Town Name is required.</label>";
          }
          //check if tInfo is null
          if(empty($tInfo)){
            $this->view->errTInfo.="<label class='label label-danger'>Town Information is required.</label>";
          }elseif (!$town->save()) {
            $this->flash->error($town->getMessages());
          } else {
            $this->view->errNotice.="<div class='alert alert-success'>Town successfully added.</div>";
            Tag::resetInput();
          }
        }else{
          $this->view->errTName .= "<label class='label label-danger'>Town Name already exists.</label>";
        }
      }
    }else{
      Tag::resetInput();
    }
  }
  $this->view->pintown = 1;
  $this->view->form = $form;
}

public function viewtownAction($townID){
  \Phalcon\Tag::prependTitle('Town Information | ');
  $this->view->menu = $this->_menuActive('townrole');
  $this->view->town = $town = Tbltowns::findFirst('townID = '.$townID);
  $this->_prinTownTab(1, $townID);

  $townArray = array();
  $townArray[] = array(
    'townID'=>$town->townID,
    'townName'=>$town->townName,
    'townLat'=>$town->townLat,
    'townLong'=>$town->townLong,
    'townInfo'=>$this->_truncateHtml(trim($town->townInfo), 150)
    );

  $this->view->viewTown = 1;
  $this->view->townMarkers = $townArray[0];
}


private function _prinTownTab($selected, $townID){
  $townTab = '<ul class="nav nav-tabs">';
  $townTab .= $selected == 1?'<li class="active">':'<li>';
  $townTab .= '<a href="/admin/viewtown/'.$townID.'">Town Information</a></li>';
  $townTab .= $selected == 2?'<li class="active">':'<li>';
  $townTab .= '<a href="/admin/townevents/'.$townID.'">Town Events</a></li>';
  $townTab .= $selected == 3?'<li class="active">':'<li>';
  $townTab .= '<a href="/admin/townsgallery/'.$townID.'">Town Pictures</a></li>';
  $townTab .= $selected == 4?'<li class="active">':'<li>';
  $townTab .= '<a href="/admin/townpartners/'.$townID.'">ABK Partners</a></li>';
  $townTab .= '</ul>';
  $this->view->townTab = $townTab;
}

public function townpartnersAction($townID){
  \Phalcon\Tag::prependTitle('Town Partners | ');
  $this->view->menu = $this->_menuActive('townrole');
  //$this->view->town = $town = Tbltowns::findFirst('townID = '.$townID);
  $this->_prinTownTab(4, $townID);

  if($this->request->isPost() && $this->request->getPost('savePartner')){
    if($this->security->getSessionToken() == $this->request->getPost('csrf_add_partner')){

      if($this->request->getPost('partnerSelect')){
        $partnerIDs = $this->request->getPost('partnerSelect');
        for($x = 0; $x < count($partnerIDs); $x++){
          $townPartner = new Tbltownpartners();
          $townPartner->assign(array(
            'townID'=>$townID,
            'partnerID'=>$partnerIDs[$x]
            ));
          $townPartner->save();
        }
        $this->flash->success("Partner was successfully added.");
      }
    }
  }

  $partnerOptions = null;


  $disbledPartners = Tbltownpartners::find('townID = '.$townID);
  $disbledPartnersArray = array();
  foreach ($disbledPartners as $key => $value) {
    $disbledPartnersArray[] = $value->partnerID;
  }

  $phql = 'SELECT Tblpartners.partnerID, Tblpartners.partnerName
  FROM Tblpartners
  LEFT JOIN Tblusers ON Tblpartners.userID = Tblusers.userID
  WHERE Tblusers.userLevel = 2
  ORDER BY partnerName ASC';
  $partners = $this->modelsManager->executeQuery($phql);


  foreach ($partners as $key => $value) {
    $disabled = in_array($value->partnerID, $disbledPartnersArray)?'disabled':'';
    $partnerOptions .= '<option '.$disabled.' value="'.$value->partnerID.'">'.$value->partnerName.'</option>';
  }
  $this->view->partnerOptions = $partnerOptions;

  $numberPage = $this->request->getQuery("page")?$this->request->getQuery("page", "int"):1;
  $andWhere = null;

  if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
    Tag::resetInput();
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $this->_generalDelete($id, 'Tbltownpartners', 'partnerID', 'ABK Partner');
    }
  }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
    $id = $this->request->getPost('tbl_id');
    if(!empty($id)){
      $this->_generalDelete($id, 'Tbltownpartners', 'partnerID', 'ABK Partner(s)');
    }
  }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
    $this->session->remove("tp_search_text");
    $this->persistent->searchinqParams = null;
    unset($_POST);
  }else{
    if ($this->request->isPost()) {
      $keyword = $this->request->getPost('search_text');
      $keyword = trim(preg_replace('/\s+/',' ', $keyword));
      $this->session->set("tp_search_text", $keyword);
    }else{
      $keyword = $this->session->get("tp_search_text")?$this->session->get("tp_search_text"):null;
    }


  }

  if(!empty($keyword)){
    $andWhere = ' AND Tblpartners.partnerName LIKE "%'.$keyword.'%" ';
    $this->flash->notice('Search results for "<strong>' . $keyword .'</strong>"');
  }

  // SORTING
  // Added a server side sorting on all tables using the columns shown
  // Dont forget to insert na href params on the view
  $sort = $this->request->getQuery("sort");
  $order = "ORDER BY Tblpartners.partnerID DESC";
  $this->view->nameHref = "partnerName-asc";
  $arr = explode("-", $sort);
  switch ($arr[0]) {
    case 'partnerName':
    $arr[1] == "asc" ? $this->view->nameHref = $arr[0] . "-desc" : "";
    $order = " ORDER BY $arr[0] $arr[1]";
    $this->view->nameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    default:
    break;
  }

  $phql = 'SELECT Tblpartners.*
  FROM Tbltownpartners
  LEFT JOIN Tblpartners ON Tblpartners.partnerID = Tbltownpartners.partnerID
  WHERE Tbltownpartners.townID = '.$townID.$andWhere.$order;
  $result = $this->modelsManager->executeQuery($phql);

  $dataArray = array();
  foreach ($result as $key => $value) {
    $dataArray[] = array(
      'partnerID'=> $value->partnerID,
      'partnerName'=> $value->partnerName
      );
  }

  $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
    //$paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
    "data" => $dataArray,
    "limit"=> 10,
    "page" => $numberPage
    ));
  $this->view->page = $paginator->getPaginate();
  $this->view->townPartners = 1;
}

private function _deleteTown($id, $table='Tbltowns', $field='townID', $itemname='Town'){
  $param=null;
  if(is_array($id)){
    foreach ($id as $i) {
      $param .= $field.' = '. $this->filter->sanitize($i, array("int")) . ' OR ';
      $townAlbum = Tbltownalbums::findFirst('townID='.$i);
      $picturesPath = '../public/img/towns/'.$i;
      if($this->_deletefolder($picturesPath)){
        $townAlbum->delete();
      }

      $townEvents = Tbltownevents::find('townID='.$i);
      $townEvents->delete();
    }
    $param = substr($param, 0, -4);
  }else{
    $id = $this->filter->sanitize($id, array("int"));
    $townAlbum = Tbltownalbums::find('townID='.$id);
    $picturesPath = '../public/img/towns/'.$id;
    if($this->_deletefolder($picturesPath)){
      $townAlbum->delete();
    }

    $townEvents = Tbltownevents::find('townID='.$id);
    $townEvents->delete();

    $param = $field.'=' . $id;
  }

  $post = $table::find($param);
  if (!$post) {
    $this->flash->error($itemname." not found");
  }


  if (!$post->delete()) {
    foreach ($post->getMessages() as $message) {
      $this->flash->error((string) $message);
    }
    return $this->forward("admin/newsletter");
  } else {
    /*$this->flash->success($itemname." was deleted");*/
  }
}

public function suggestedprogramsAction($suggID = ''){
  $this->persistent->searchvolParams = null;
 $this->session->remove("sugg_search_text");
 $this->view->value = null;
 $this->view->keyword = null;
 $this->view->program = null;
 $this->view->andWhere = null;
 \Phalcon\Tag::prependTitle('Suggested Programs | ');
 $this->view->menu = $this->_menuActive('pmrole');

 $viewMode = false;
 if(!empty($suggID)){
   $phql = 'SELECT custom, customDesc, Tblvolunteeractivities.dateAdded, vActivityID, Tblvolunteeractivities.volunteerID,
   title, fname, mname, lname, extname, username, address, email, phone
   FROM Tblvolunteeractivities
   LEFT JOIN Tblvolunteers ON Tblvolunteers.volunteerID = Tblvolunteeractivities.volunteerID
   WHERE activityID !=0 AND status !=0 AND vActivityID ='.$suggID;
   $suggested = $this->modelsManager->executeQuery($phql);

   if($suggested){
     $viewMode = true;
   }
 }

 $this->view->viewMode = $viewMode;


 if($viewMode){
   $dataArray = array();
   foreach ($suggested as $key => $value) {
     $dataArray[] = array(
       'vActivityID'=>$value->vActivityID,
       'progTitle'=> $value->custom,
       'progDesc'=> $value->customDesc,
       'dateAdded'=>$value->dateAdded,
       'suggBy'=>$value->title.' '.$value->fname.' '.$value->mname.' '.$value->lname.' '.$value->extname,
       'address'=>$value->address,
       'email'=>$value->email,
       'phone'=>$value->phone
       );
   }
   $this->view->suggested = $dataArray[0];

 }else{
   $numberPage = 1;
   if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
     Tag::resetInput();
   }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
     $id = $this->request->getPost('recordID');
     if(!empty($id)){
       $volunteerAct = Tblvolunteeractivities::findFirst('vActivityID = '.$id);
       if($volunteerAct){
         $volunteer = Tblvolunteers::findFirst('volunteerID = '.$volunteerAct->volunteerID);
         $volunteer->delete();
         $volunteerAct->delete();
         $this->flash->success("Suggested Program was deleted <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>");
       }

       /* stay on search pages*/
       if ($this->request->isPost()) {
         // $query = Criteria::fromInput($this->di, 'Tblusers', array('userName' => $this->request->getPost('search_text'), 'userFirstname' => $this->request->getPost('search_text')));
         // $this->persistent->searchUserParams = $query->getParams();
         // $this->session->set("user_search_text", $this->request->getPost('search_text'));
         $keyword = $this->request->getPost('search_text');
         $keyword = trim(preg_replace('/\s+/',' ', $keyword));
         $this->session->set("user_search_text", $keyword);
       } else {
         // $numberPage = $this->request->getQuery("page", "int");
         $searchtext = $this->session->get("user_search_text");
         $keyword = !empty($searchtext)?$searchtext:'';
       }
       /**/
     }
   }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
     $id = $this->request->getPost('tbl_id');
     if(!empty($id)){
       if(is_array($id)){
         foreach ($id as $i) {
           $volunteerAct = Tblvolunteeractivities::findFirst('vActivityID = '.$i);
           if($volunteerAct){
             $volunteer = Tblvolunteers::findFirst('volunteerID = '.$volunteerAct->volunteerID);
             $volunteer->delete();
             $volunteerAct->delete();
           }
         }
       }
       $this->flash->success("Suggested Program was deleted <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>");
     }
     /* stay on search pages*/
     if ($this->request->isPost()) {
       // $query = Criteria::fromInput($this->di, 'Tblusers', array('userName' => $this->request->getPost('search_text'), 'userFirstname' => $this->request->getPost('search_text')));
       // $this->persistent->searchUserParams = $query->getParams();
       // $this->session->set("user_search_text", $this->request->getPost('search_text'));
       $keyword = $this->request->getPost('search_text');
       $keyword = trim(preg_replace('/\s+/',' ', $keyword));
       $this->session->set("user_search_text", $keyword);
     } else {
       // $numberPage = $this->request->getQuery("page", "int");
       $searchtext = $this->session->get("user_search_text");
       $keyword = !empty($searchtext)?$searchtext:'';
     }
     /**/
   }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
     $this->session->remove("sugg_search_text");
     $this->session->remove("sugg_filter_date");
     $this->session->remove("sugg_filter_date_from");
     $this->session->remove("sugg_filter_date_to");
     $this->persistent->searchvolParams = null;
     unset($_POST);
   }else{
     if ($this->request->isPost()) {
       $keyword = $this->request->getPost('search_text');
       $keyword = trim(preg_replace('/\s+/',' ', $keyword));
       $this->session->set("sugg_search_text", $keyword);
     } else {
       $searchtext = $this->session->get("sugg_search_text");
       $keyword = !empty($searchtext)?$searchtext:'';
       $numberPage = $this->request->getQuery("page", "int");
     }
   }
   if(!empty(!$keyword)){

     $keyword = trim(preg_replace('/\s+/',' ', $keyword));
     // $this->flash->notice('Search results for "<strong>' . $this->session->get("sugg_search_text") .'</strong>"');
     // $this->flash->notice("Sorry, no records were found. Please try again.");
     $andWhere = 'AND (custom LIKE "%'.$keyword.'%" OR username LIKE "%'.$keyword.'%" OR fname LIKE "%'.$keyword.'%" OR lname LIKE "%'.$keyword.'%" OR extname LIKE "%'.$keyword.'%")';
   }

   if(!empty($keyword)){
     $keyword = trim(preg_replace('/\s+/',' ', $keyword));

     // $this->flash->notice("Sorry, no records were found. Please try again.");
     $andWhere = 'AND (custom LIKE "%'.$keyword.'%" OR username LIKE "%'.$keyword.'%" OR fname LIKE "%'.$keyword.'%" OR lname LIKE "%'.$keyword.'%" OR extname LIKE "%'.$keyword.'%")';
      $this->flash->notice('Search results for "<strong>' . $this->session->get("sugg_search_text") .'</strong>"');
   } else {
     $andWhere = null;
     $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):"";
     $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):"";
     if(($fromDate && $toDate) || $this->session->has("sugg_filter_date") ) {
       if($fromDate > $toDate) {
         return $this->flash->warning("Invalid request. End date must be later than the start date.");
       }
       if($this->session->has("sugg_filter_date")) {
         if(!$fromDate && !$toDate){
           $fromDate = trim($this->session->get('sugg_filter_date_from'));
           $toDate = trim($this->session->get('sugg_filter_date_to'));
         }
       } else {
         $this->session->set("sugg_filter_date", true);
         $this->session->set("sugg_filter_date_from", $fromDate);
         $this->session->set("sugg_filter_date_to", $toDate);
       }
       $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
       $andWhere = " AND (Tblvolunteeractivities.dateAdded BETWEEN $fromDate AND $toDate)";
     }
   }

   // SORTING
   // Added a server side sorting on all tables using the columns shown
   // Dont forget to insert na href params on the view
   $sort = $this->request->getQuery("sort");
   $order = "";
   $this->view->usernameHref = "username-asc";
   $this->view->nameHref = "name-asc";
   $this->view->programHref = "custom-asc";
   $this->view->dateHref = "dateAdded-asc";
   $arr = explode("-", $sort);
   switch ($arr[0]) {
     case 'username':
     $arr[1] == "asc" ? $this->view->usernameHref = $arr[0] . "-desc" : "";
     $order = " ORDER BY $arr[0] $arr[1]";
     $this->view->usernameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
     break;
     case 'name':
     $arr[1] == "asc" ? $this->view->nameHref = $arr[0] . "-desc" : "";
     $order = " ORDER BY $arr[0] $arr[1]";
     $this->view->nameIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
     break;
     case 'custom':
     $arr[1] == "asc" ? $this->view->programHref = $arr[0] . "-desc" : "";
     $order = " ORDER BY $arr[0] $arr[1]";
     $this->view->programIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
     break;
     case 'dateAdded':
     $arr[1] == "asc" ? $this->view->dateHref = $arr[0] . "-desc" : "";
     $order = " ORDER BY Tblvolunteeractivities.$arr[0] $arr[1]";
     $this->view->dateIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
     break;
     default:
     break;
   }

   $phql = 'SELECT custom, Tblvolunteeractivities.dateAdded, vActivityID, Tblvolunteeractivities.volunteerID,
   title, CONCAT(lname, " ",fname, " ", extname) AS name, username, status
   FROM Tblvolunteeractivities
   LEFT JOIN Tblvolunteers ON Tblvolunteers.volunteerID = Tblvolunteeractivities.volunteerID WHERE status !=0 AND activityID !=0
   '.$andWhere.$order;
   $suggested = $this->modelsManager->executeQuery($phql);

   $dataArray = array();
   foreach ($suggested as $key => $value) {
     $dataArray[] = array(
       'program'=>$value->custom,
       'name'=>$value->name,
       'username'=>$value->username,
       'dateAdded'=>$value->dateAdded,
       'volunteerID'=>$value->volunteerID,
       'vActivityID'=>$value->vActivityID
       );
      }

   $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
     "data" => $dataArray,
     "limit"=> 10,
     "page" => $numberPage
     ));
   $this->view->page = $paginator->getPaginate();
 }
}

public function donationsAction(){
  \Phalcon\Tag::prependTitle('Donations | ');
  $this->view->menu = $this->_menuActive('donrole');
  $numberPage = 1;


  if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
    Tag::resetInput();
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $this->_generalDelete($id, 'Tbldonations', 'txn_id', 'Donation',true, false);
    }
  }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
    $id = $this->request->getPost('tbl_id');
    if(!empty($id)){
      $this->_generalDelete($id, 'Tbldonations', 'txn_id', 'Donation(s)',true, false);
    }
  }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
    $this->session->remove("don_search_text");
    unset($_POST);
  }else{
    if ($this->request->isPost()) {
      $keyword = $this->request->getPost('search_text');
      $this->session->set("don_search_text", $keyword);
    } else {
      $searchtext = $this->session->get("don_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
      $numberPage = $this->request->getQuery("page", "int");
    }
  }
  $andWhere = null;
  if(!empty($keyword)){
    $this->flash->notice('Search results for "<strong>' . $this->session->get("don_search_text") .'</strong>"');
    $andWhere = 'WHERE fname LIKE "%'.$keyword.'%" OR lname LIKE "%'.$keyword.'%" OR txn_id LIKE "%'.$keyword.'%" OR payer_email LIKE "%'.$keyword.'%"';
  }

  $phql = 'SELECT * FROM Tbldonations '.$andWhere.' ORDER BY donation_date DESC';
  $suggested = $this->modelsManager->executeQuery($phql);

  $dataArray = array();
  foreach ($suggested as $key => $value) {
    $dataArray[] = array(
      'txn_id'=>$value->txn_id,
      'fname'=>$value->fname,
      'lname'=>$value->lname,
      'phone'=>$value->phone,
      'amount'=>$value->payment_amount,
      'email'=>$value->payer_email,
      'date'=>$value->donation_date
      );
  }

  $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
    "data" => $dataArray,
    "limit"=> 10,
    "page" => $numberPage
    ));
  $this->view->page = $paginator->getPaginate();

}

public function createnewsletterAction(){
  \Phalcon\Tag::prependTitle('Create Newsletter | ');
  $this->view->menu = $this->_menuActive('enmrole');
  $this->view->errNLSubject = null;
  $this->view->errNLMessage = null;

  // digital assets
  $this->view->script = '<script> var CURRENT_FOLDER_CAT = "newsletter"; </script>';
  $this->view->pictures = $pictures = Tblpagesimg::find([
    "imgpath" => 'img/newsletter/',
    "order" => 'imgID DESC'
  ]);
  $this->view->folders = $this->_getFolderAssets();

  if($this->request->getPost('send')){

    $validation = new Phalcon\Validation();
    $validation
    ->add('subject', new PresenceOf(array(
      'message' => 'Subject is required',
      )))->add('message', new PresenceOf(array(
      'message' => 'Message is required',
      'cancelOnFail' => true
      )))
      ->add('subject', new StringLength(array(
        'max' => 255,
        'min' => 1,
        'messageMaximum' => 'Your subject is too long',
        'messageMinimum' => 'Your subject must be atleast 1 character long'
        )))
      ->add('message', new StringLength(array(
        'max' => 255,
        'min' => 1,
        'messageMaximum' => 'Your message is too long',
        'messageMinimum' => 'Your message must be atleast 1 character long'
        )))
      ;

      $validation->setFilters('subject', 'trim');
      $validation->setFilters('message', 'trim');

      $messages = $validation->validate($_POST);
      $errMessage = null;
      if (count($messages)) {

        foreach ($validation->getMessages()->filter('subject') as $message) {
          $this->view->errNLSubject .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('message') as $message) {
          $this->view->errNLMessage .= "<div class='label label-danger'>".$message."</div> ";
        }
    }else{
        if($this->sendnewsletter($this->request->getPost('subject', 'striptags'), $this->request->getPost('message'))){
          $this->flash->success("Newsletter successfully sent.");
          unset($_POST);
        }else{
        //   echo '<div class="alert alert-danger alert-dismissible" role="alert">
        //   <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        //   Unable to send newsletter.
        // </div>';
          $this->flash->success("Newsletter successfully sent.");
          header('refresh:1;url=/admin/createnewsletter');
      }

      //   if($this->sendnewsletter($this->request->getPost('subject', 'striptags'), $this->request->getPost('message'))){
      //     $this->flash->success("Newsletter successfully sent.");
      //     unset($_POST);
      //     header('refresh:1;url=/admin/createnewsletter');
      //   }else{
      //     echo '<div class="alert alert-danger alert-dismissible" role="alert">
      //     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      //     Unable to send newsletter.
      //   </div>';
      // }
    }
    /*}*/
  }elseif($this->request->getPost('reset')){
    // header('refresh:1;url=/admin/createnewsletter');
  }

$programID = "newsletter"; //Temporary
$this->view->prog = $programID ; //Temporary

$image_files = array();
// $files = glob('../public/img/programs/'.$programID.'/*.*');
$files = glob('../public/img/newsletter/*.*');
usort($files, create_function('$a,$b', 'return filemtime($a)<filemtime($b);'));
foreach($files as $filename){
  $image_files[] = basename($filename);
}
$this->view->digital_assets = $image_files;
}

public function forgotpasswordAction()
{
  $this->view->emailError = null;
  /*&& !$this -> req->getpots('loginFormActive')*/
  if($this->request->isPost())
  {
    $email = $this->request->getPost("email");
    if($user = Tblusers::findFirst("userEmail = '$email' AND userStatus = 'active'")) {
      $a = '';
      for ($i = 0; $i < 6; $i++) {
        $a .= mt_rand(0, 9);
      }
      $token = sha1($a);
      $user->userForgotToken = $token;
      $user->save();

      $body = "
      <h4>Having trouble signing in?</h4>
      <p>No worries, resetting your account password is easy.</p>
      <p>Please <a href='http://abk.gotitgenius.com/admin/resetpassword/$email/$token'>click here</a> to change your account password.</p>
      <p>Sincerely,</p>
      <p>ABK Foundation</p>
      ";

      $mailObjects = array(
        'From'=> 'angbayanko.org@no-reply.com',
        'FromName' => 'angbayanko.org',
        'AddAddress'=> $user->userEmail,
        'Subject' => "ABK Foundation: Password Reset",
        'Body' => $body
        );

      if($this->_sendmail($mailObjects)) {
        $this->flash->success("A password reset link has been sent to your mailbox. ");
      } else {
        $this->flash->warning("There seems to be a problem while sending an email. Please try again later.");
      }
    } else {
      $message = "No account found with that email address";
      $this->view->emailError = "<div class='label label-danger'>".$message."</div>";
    }
  }

  $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

}
public function resetpasswordAction($email, $token)
{
  if(!$user = Tblusers::findFirst("userEmail = '$email' AND userForgotToken = '$token'")) {
    $this->response->redirect('/admin');
  }

  $this->view->passError = null;
  $this->view->repassError = null;
  $this->view->email = $email;
  $this->view->token = $token;

  if($this->request->isPost()) {
    $validation = new Phalcon\Validation();
    $validation->add('password', new PresenceOf(array(
      'message' => 'The password is required',
      'cancelOnFail' => true
      )));
    $validation->add('password', new StringLength(array(
      'max' => 20,
      'min' => 8,
      'messageMaximum' => 'Thats an exagerated password.',
      'messageMinimum' => 'Password should be Minimum of 8 characters.'
      )));

    $validation->add('repassword', new PresenceOf(array(
      'message' => 'Retyping your password is required'
      )));
    $validation->add('repassword', new Confirmation(array(
      'message' => 'Password doesn\'t match confirmation',
      'with' => 'password'
      )));

    if(count($validation->validate($_POST))){
      foreach ($validation->getMessages()->filter('password') as $message) {
        $this->view->passError .= "<div class='label label-danger'>".$message."</div> ";
      }
      foreach ($validation->getMessages()->filter('repassword') as $message) {
        $this->view->repassError .= "<div class='label label-danger'>".$message."</div> ";
      }
    } else {
      // http://abk.gotitgenius.com/admin/resetpassword/jl@mailinator.com/e4f82fbf3aa759990f43aac3dca36ac75fccc52d
      $user->userPassword = sha1($this->request->getPost("password"));
      $user->userForgotToken = null;
      $save = $user->save();

      if($save) {
        $this->flash->success("New password has been set. <a href='/admin/'>Go Back to CMS Log In</a>");
      } else {
        $this->flash->notice("There seems to be a problem while processing your request. Please try again later.");
      }
    }
  }

  $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
}

public function settingsAction($id = null){
  \Phalcon\Tag::prependTitle('Settings | ');
  $userid = $this->session->get('auth');
  $this->view->menu = null;
  $this->view->partnerSubMenu = $this->_createPartnerMenu();
  $this->view->imgUrlError = null;
  $this->view->vidUrlError = null;
  $this->view->slides = Tblslides::find();
  $this->view->edit = $edit = false;
  $form = new SliderForm();
  $form->csrf = $this->security->getToken();

  if($id){
    $this->view->edit = $edit = true;
    $eslide = Tblslides::findFirst("slideID="+$id);
    $this->view->eslide = $eslide;
  }

  if ($this->request->isPost() && $this->request->getPost('action') == "delete") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $slide = Tblslides::findFirst('slideID='+$id);
      if($slide->delete()){
        $this->flash->success("Selected slide has been successfully deleted.");
        $this->view->slides = Tblslides::find();
      } else {
        $this->flash->error("Something went wrong. Please try again later.");
      }
    }
  } else {
    if($this->request->isPost() && $edit && $id) {
      if($form->isValid($this->request->getPost())){
        $image = @getimagesize($this->request->getPost('imageUrl'));
        $video = $this->request->getPost('videoUrl');
        $hasError = false;

        if($video){
          $rx = '~
          ^(?:https?://)?              # Optional protocol
          (?:www\.)?                  # Optional subdomain
          (?:youtube\.com|youtu\.be)  # Mandatory domain name
          /embed/([^&]+)           # URI with video id as capture group 1
          ~x';
          if(!preg_match($rx, $video)){
            $this->view->vidUrlError .= "<div class='label label-danger'>URL is invalid</div>";
            $hasError = true;
          }
        }

        if(!$image){
          $this->view->imgUrlError .= "<div class='label label-danger'>URL is invalid</div>";
          $hasError = true;
        } else {
          list($width, $height, $type, $attr) = $image;
          if($height < 255 || $width < 900){
            $this->view->imgUrlError .= "<div class='label label-danger'>Image does not meet the required sizes (900 x 255).</div>";
            $hasError = true;
          }
        }

        if(!$hasError){
          $eslide->assign(array(
            'slideImgUrl' => $this->request->getPost('imageUrl'),
            'slideVideoUrl' => $this->request->getPost('videoUrl') ? $this->request->getPost('videoUrl') : null,
            'slideVideoAlignment' => $this->request->getPost('alignment')
            ));
          if($eslide->save()){
            $this->flash->success("Current slider has been successfully updated.");
            $this->view->slides = Tblslides::find();
          } else {
            $this->flash->warning("There seems to be a problem while processing your request. Please try again later.");
          }
        }
      }
    } else if($this->request->isPost()) {
      if($form->isValid($this->request->getPost())){
        $image = @getimagesize($this->request->getPost('imageUrl'));
        $video = $this->request->getPost('videoUrl');
        $hasError = false;

        if($video){
          $rx = '~
          ^(?:https?://)?              # Optional protocol
          (?:www\.)?                  # Optional subdomain
          (?:youtube\.com|youtu\.be)  # Mandatory domain name
          /embed/([^&]+)           # URI with video id as capture group 1
          ~x';
          if(!preg_match($rx, $video)){
            $this->view->vidUrlError .= "<div class='label label-danger'>URL is invalid</div>";
            $hasError = true;
          }
        }

        if(!$image){
          $this->view->imgUrlError .= "<div class='label label-danger'>URL is invalid</div>";
          $hasError = true;
        } else {
          list($width, $height, $type, $attr) = $image;
          if($height < 255 || $width < 900){
            $this->view->imgUrlError .= "<div class='label label-danger'>Image does not meet the required sizes (900 x 255).</div>";
            $hasError = true;
          }
        }

        if(!$hasError){
          $slider = new Tblslides();
          $slider->assign(array(
            'slideImgUrl' => $this->request->getPost('imageUrl'),
            'slideVideoUrl' => $this->request->getPost('videoUrl') ? $this->request->getPost('videoUrl') : null,
            'slideVideoAlignment' => $this->request->getPost('alignment')
            ));
          if($slider->save()){
            $this->flash->success("New slider has been successfully added.");
            $this->view->slides = Tblslides::find();
          } else {
            $this->flash->warning("There seems to be a problem while processing your request. Please try again later.");
          }
        }
      }
    }
  }
  $this->view->form = $form;
}

public function AbkrescuesettingsAction()
{
 $socialtbl = Socialmedia::find();
 $this->view->socialtbl = $socialtbl->toArray();
 $this->view->abkdetails  = Abkresdetails::findFirst("id = 1")->toArray();
 $form = new CreateuserForm();
 if($this->request->isPost() && $this->request->getPost('save_page')){
  $soc = $this->request->getPost('socialicon');
  foreach ($soc as $key => $soc) {
    $find = Socialmedia::findFirst("icon = $key ");
    if($find){
      $find->link = $soc;
    }
    if($find->save()){
      $hasError = false;
    }else{
      $hasError = true;
    }
  }
  $email = $this->request->getPost('email');
  $contact = $this->request->getPost('contact');
  if (empty($email) || empty($contact) ) {
    if (empty($email) && empty($contact) ) {
     $msg = "Email and Contact is Required.";
   }else if (empty($email)){
    $msg = "Email is Required.";
  }else{
    $msg = "Contact is Required.";
  }
  $this->flash->warning($msg);
}
else{
  $details = Abkresdetails::findFirst("id = 1");
  if($details){
    $details->phone = trim($this->request->getPost('contact', 'striptags'));
    $details->email = trim($this->request->getPost('email', 'striptags'));
    $details->global_mem = trim($this->request->getPost('global_mem', 'striptags'));
    $details->accomp_miss = trim($this->request->getPost('accomp_miss', 'striptags'));
    $details->worth_donat = trim($this->request->getPost('worth_donat', 'striptags'));
    $details->date_updated = date('Y-m-d H:i:s');
    if($details->save()){
     $hasError = false;
   }else{
    $hasError = true;
  }
}
if($hasError == false){
  $this->flash->success("Settings has been successfully updated");
  $this->view->socialtbl = Socialmedia::find()->toArray();
  $this->view->abkdetails  = Abkresdetails::findFirst("id = 1")->toArray();
} else {
  $this->flash->warning("There seems to be a problem while processing your request. Please try again later.");
}
}

}
$this->view->form = $form;
}

public function AbkrescreateeventsAction()
{
  $this->view->menu = null;
  $form = new CreateEventForm();
  $this->view->form = $form;
  $request=new Phalcon\Http\Request();

  if($this->request->isPost() && $this->request->getPost('save_event')){


    $validation = new Phalcon\Validation();
    $validation ->add('eventname', new PresenceOf(array(

        'message' => 'Event name is required'
      )));
    // $validation ->add('eventname', new Regex(array(
    //     'message'    => 'Eventname is invalid. Avoid spaces and symbols.',
    //     'pattern'    => '/^[a-zA-Z0- ]+$/',
    //     'allowEmpty' => false
    //     )));
      $validation ->add('eventslugs', new PresenceOf(array(

        'message' => 'Event url is required'
      )));
      $validation ->add('desc', new PresenceOf(array(

        'message' => 'Short description is required'
      )));
      $validation ->add('location', new PresenceOf(array(

        'message' => 'Location is required'
      )));
      $validation ->add('ldesc', new PresenceOf(array(

        'message' => 'Long description is required'
      )));
      $validation->add('ldesc', new StringLength(array(
          'max' => 100,
          'min' => 1,
          'messageMaximum' => 'Maxinum of 100 characters.',
          'messageMinimum' => 'Minimum of 1 character.'
        )));
       $validation ->add('startdate', new PresenceOf(array(

        'message' => 'Start Date is required'
      )));
       $validation ->add('enddate', new PresenceOf(array(

        'message' => 'End Date is required'
      )));
        $validation ->add('starttime', new PresenceOf(array(

        'message' => 'Start Time is required'
      )));
        $validation ->add('endtime', new PresenceOf(array(

        'message' => 'End Time is required'
      )));


      if(count($validation->validate($_POST))){
        foreach ($validation->getMessages()->filter('eventslugs') as $message) {
          $this->view->urlErr .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('ldesc') as $message) {
          $this->view->ldesclimit .= "<div class='label label-danger'>".$message."</div> ";
           $this->view->ldescreq .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('eventname') as $message) {
          $this->view->nameErr .= "<div class='label label-danger'>".$message."</div> ";
          $this->view->Spaces .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('desc') as $message) {
          $this->view->shortdesc.= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('location') as $message) {
          $this->view->location.= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('startdate') as $message) {
          $this->view->startdate.= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('enddate') as $message) {
          $this->view->enddate.= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('starttime') as $message) {
          $this->view->starttime.= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('endtime') as $message) {
          $this->view->endtime.= "<div class='label label-danger'>".$message."</div> ";
        }
      }
      else{
      $endtime = $request->getPost("endtime");
      $starttime = $request->getPost("starttime");
      $starttime  = (!$starttime ? $starttime : date("H:i", strtotime($starttime)) );
      $endtime  = (!$endtime ? $endtime : date("H:i", strtotime($endtime)) );

      if ($request->getPost("startdate") == $request->getPost("enddate") && $endtime < $starttime ) {
        $this->flash->warning("Time Range is Invalid");
      }
      else if($request->getPost("enddate") < $request->getPost("startdate")){
      $this->flash->warning("Date Range is Invalid");
      }else{
      $event = new Events();
      $event->assign(
        array(
          "eventname"=>$request->getPost("eventname"),
          "slugs"=>$request->getPost("eventslugs"),
          "shortdesc" => $request->getPost("desc"),
          "longdesc" => $request->getPost("ldesc"),
          "location" => $request->getPost("location"),
          "startdate"=>$request->getPost("startdate"),
          "enddate"=>$request->getPost("enddate"),

          "starttime"=>$starttime,
          "endtime"=>$endtime,
          "date_created"=>date('Y-m-d H:i:s'),
          "date_updated"=>date('Y-m-d H:i:s'),
          "status"=>1
         ));

      // $this->view->form = $form;
      // Tag::resetInput();


      $checkeventname=Events::findFirst("eventname='".$this->request->getPost('eventname')."'");
      $checheventurl=Events::findFirst("slugs='".$this->request->getPost('eventslugs')."'");

       if($checkeventname){

        //$this->flash->warning("Event Name is already exist");
        $this->view->checktitle.="<div class='alert alert-warning'>Event Name is already exist.<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a></div>";


       }else if($checheventurl){

        $this->flash->warning("Event Url is already exist");

       }else{
           if($event->save())
          {
            //$this->flash->success("Event has been successfully added.");
            $this->view->success.="<div class='alert alert-success'>Event has been successfully added.</div>";
            Tag::resetInput();
             $form->clear();


          }else{
            $this->flash->error($event->getMessages());
            // Tag::resetInput();
          }
       }
      // return $this->response->redirect("admin/abkresevents");
    } //date end
  }


}



}


public function AbkreseventsAction()
{
    $this->view->menu = null;
    $this->view->user = null;
  if($this->request->isPost() && $this->request->getPost('action') == "edit"){
    $id = $this->request->getPost('recordID');
    $this->view->id = $id;
    var_dump($id);
    if(!empty($id)){
      // var_dump($id);
      return $this->dispatcher->forward(array(
        'controller' => 'admin',
        'action' => 'Abkreseditevents',
        'params' => array($id)
        ));
    }
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $page = Events::find("id='".$id."'");
      if (!$page) {
        $this->flash->error("Page was not found");
      }

      if (!$page->delete()) {
        foreach ($products->getMessages() as $message) {
          $this->flash->error((string) $message);
        }
        $this->forward("admin/abkresevents");
      } else {
        $this->flash->success("Page was successfully deleted.");
      }
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      $keyword = trim($this->request->getPost('search_text'));
      $this->session->set("eventuser_search_text", $keyword);
    } else {
      $searchtext = $this->session->get("event_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
    //activate events
  }
  elseif ($this->request->isPost() && $this->request->getPost('action') == "activate") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $page = Events::findFirst('id='+$id);
      $page->status = 1;
      if($page->save()){
        $this->flash->success("Events has been activated.<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>");
      } else {
        $this->flash->error("Something went wrong. Please try again later.");
      }
    }
    //deactivate events
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "deactivate") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $page = Events::findFirst('id='+$id);
      $page->status = 0;
      if($page->save()){
        $this->flash->success("Events has been deactivated. <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>");
      } else {
        $this->flash->error("Something went wrong. Please try again later.");
      }
    }
  }
  elseif($this->request->isPost() && $this->request->getPost('clear_search')){
    $this->session->remove("event_search_text"); $this->session->remove("event_filter_date");
    $this->session->remove("event_filter_date_from"); $this->session->remove("event_filter_date_to");
    unset($_POST);
  }else{
    if ($this->request->isPost()) {
      $keyword = trim($this->request->getPost('search_text'));
      $this->session->set("event_search_text", $keyword);
    }else{
      $searchtext = $this->session->get("event_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
  }

  if(!empty($keyword)){
    $where = ' eventname LIKE "%'.$keyword.'%"';
    $this->flash->notice('Search results for "<strong>' . $keyword .'</strong>"');
  }else{
    $where = null;
    $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):null;
    $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):null;
    if(($fromDate && $toDate) || $this->session->has("event_filter_date") ) {
      if($fromDate > $toDate) {
        return $this->flash->warning("Invalid request. End date must be later than the start date.");
      }
      if($this->session->has("event_filter_date")) {
        if(!$fromDate && !$toDate){
          $fromDate = trim($this->session->get('pages_filter_date_from'));
          $toDate = trim($this->session->get('pages_filter_date_to'));
        }
      } else {
        $this->session->set("event_filter_date", true);
        $this->session->set("event_filter_date_from", $fromDate);
        $this->session->set("event_filter_date_to", $toDate);
      }
      $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
      $fromDate = date('Y-m-d', strtotime($this->request->getPost('fromDate')));
      $toDate = date('Y-m-d', strtotime($this->request->getPost('toDate')));
      $where = " (date_updated BETWEEN '".$fromDate."' AND '".$toDate."')";
    }
  }

  $numberPage = $this->request->getQuery("page", "int"); $numberPage = empty($numberPage)?1:$numberPage;
  $events = Events::find($where)->toArray();
  $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
    "data" => $events,
    "limit"=> 10,
    "page" => $numberPage
    ));
  $this->view->page = $paginator->getPaginate();
}

public function AbkresediteventsAction($id)
{

  // $this->view->menu = null;
  $this->view->urlErr=null;
  $this->view->nameErr=null;
  $this->view->ldesclimit=null;
  $this->view->shortdesc=null;
  $this->view->location=null;
  $this->view->success=null;
  $form = new CreateEventForm();
  $request=new Phalcon\Http\Request();
  $event = Events::findFirstById($id);
  $this->view->event = $event;



  if($this->request->isPost() && $this->request->getPost('save_event')){

     $validation = new Phalcon\Validation();
    $validation ->add('eventname', new PresenceOf(array(

        'message' => 'Event name is required'
      )));
      $validation ->add('eventslugs', new PresenceOf(array(

        'message' => 'Event url is required'
      )));
      $validation ->add('desc', new PresenceOf(array(

        'message' => 'Short description is required'
      )));
      $validation ->add('location', new PresenceOf(array(

        'message' => 'Location is required'
      )));
      $validation ->add('ldesc', new PresenceOf(array(

        'message' => 'Long description is required'
      )));
      $validation->add('ldesc', new StringLength(array(
          'max' => 100,
          'min' => 1,
          'messageMaximum' => 'Maxinum of 100 characters.',
          'messageMinimum' => 'Minimum of 1 character.'
        )));
       $validation ->add('startdate', new PresenceOf(array(

        'message' => 'Start Date is required'
      )));
       $validation ->add('enddate', new PresenceOf(array(

        'message' => 'End Date is required'
      )));
        $validation ->add('starttime', new PresenceOf(array(

        'message' => 'Start Time is required'
      )));
        $validation ->add('endtime', new PresenceOf(array(

        'message' => 'End Time is required'
      )));

      if(count($validation->validate($_POST))){
        foreach ($validation->getMessages()->filter('eventslugs') as $message) {
          $this->view->urlErr .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('ldesc') as $message) {
          $this->view->ldesclimit .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('eventname') as $message) {
          $this->view->nameErr .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('desc') as $message) {
          $this->view->shortdesc.= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('location') as $message) {
          $this->view->location.= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('startdate') as $message) {
          $this->view->startdate.= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('enddate') as $message) {
          $this->view->enddate.= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('starttime') as $message) {
          $this->view->starttime.= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('endtime') as $message) {
          $this->view->endtime.= "<div class='label label-danger'>".$message."</div> ";
        }
      }
      else{
    $endtime = $request->getPost("endtime");
    $starttime = $request->getPost("starttime");
    $starttime  = (!$starttime ? $starttime : date("H:i", strtotime($starttime)) );
    $endtime  = (!$endtime ? $endtime : date("H:i", strtotime($endtime)) );

    if ($request->getPost("startdate") == $request->getPost("enddate") && $endtime < $starttime ) {

      $this->flash->warning("Time Range is Invalid");

    }elseif($request->getPost("enddate") < $request->getPost("startdate")){

      $this->flash->warning("Date Range is Invalid");

    }else{
      $event = Events::findfirst($id);
      $event->assign(
        array(
          "eventname"=>$request->getPost("eventname"),
          "slugs"=>$request->getPost("eventslugs"),
          "shortdesc" => $request->getPost("desc"),
          "longdesc" => $request->getPost("ldesc"),
          "location" => $request->getPost("location"),
          "startdate"=>$request->getPost("startdate"),
          "enddate"=>$request->getPost("enddate"),
          "starttime"=>$starttime,
          "endtime"=>$endtime,
          "date_created"=>date('Y-m-d H:i:s'),
          "date_updated"=>date('Y-m-d H:i:s'),
          "status"=>1
          ));
      if($event->save())
      {
        //$this->flash->success("Event has been successfully updated");
        $this->view->success.="<div class='alert alert-success'>Event has been successfully updated.</div>";
            //Tag::resetInput();



      }else{
        $this->flash->error($event->getMessages());
      }
    }
      }



  }
  // $this->view->id = $id;
  $this->view->form = $form;
}

public function AbkresaboutusAction()
{
  $this->view->menu = null;
  $form = new CreateEventForm();
  $request=new Phalcon\Http\Request();
  $this->view->aboutus = Aboutus::findFirstById(1)->toArray();

  if($this->request->isPost() && $this->request->getPost('save_page')){
    $aboutus = Aboutus::findFirstById(1);
    $aboutus->assign(
      array(
        "description"=>$request->getPost("description"),
        "date_updated"=>date('Y-m-d H:i:s')
        ));
    if($aboutus->save())
    {
      $this->flash->success("About us successfully updated");
      $this->view->aboutus = Aboutus::findFirstById(1)->toArray();
    }else{
      $this->flash->error($aboutus->getMessages());
    }
  }
  $this->view->form = $form;
}

public function AbkrescreateactivityAction()
{
  $this->view->pictures = $pictures = Tblpagesimg::find("imgpath = 'img/activities/'");
  $request=new Phalcon\Http\Request();
  $this->view->find = Activities::find()->toArray();
  $this->view->acttil=null;
  $this->view->actslug=null;
  $this->view->actcon=null;
  $this->view->actkey=null;
  $this->view->actban=null;
    $this->view->titleError  = null;
  $this->view->descriptionError = null;
  $this->view->contenterror = null;
   $this->view->actdis=null;
  $this->view->success=null;

  if($this->request->isPost() && $this->request->getPost('save_page')){

    $validation = new Phalcon\Validation();
     $validation ->add('page_title', new PresenceOf(array(

        'message' => 'Activity Title is required'
      )));
     $validation ->add('hpage_slug', new PresenceOf(array(

        'message' => 'Slug is required'
      )));
    $validation ->add('page_content', new PresenceOf(array(

        'message' => 'Activity Content is required'
      )));
    $validation ->add('page_keyword', new PresenceOf(array(

        'message' => 'Activity Keyword is required'
      )));
    $validation ->add('pageBannerUrl', new PresenceOf(array(

        'message' => 'Activity Banner is required'
      )));
     $validation ->add('page_description', new PresenceOf(array(

        'message' => 'Activity Description is required'
      )));

    if(count($validation->validate($_POST))){
        foreach ($validation->getMessages()->filter('page_content') as $message) {
          $this->view->actcon .= "<div class='label label-danger'>".$message."</div> ";
        }
         foreach ($validation->getMessages()->filter('page_keyword') as $message) {
          $this->view->actkey .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('page_title') as $message) {
          $this->view->acttil .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('hpage_slug') as $message) {
          $this->view->actslug .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('pageBannerUrl') as $message) {
          $this->view->actban .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('page_description') as $message) {
          $this->view->actdis .= "<div class='label label-danger'>".$message."</div> ";
        }
      }

    $activity = new Activities();
    $activity->assign(
      array(
        "title"=>$request->getPost("page_title"),
        "slugs"=>$request->getPost("hpage_slug"),
        "keyword" => $request->getPost("page_keyword"),
        "activity_description" => $request->getPost("page_description"),
        "content" => $request->getPost("page_content"),
        "status"=>$request->getPost("page_active"),
        "banner"=>$request->getPost("pageBannerUrl"),
        "sort"=> 0,
        "featured"=>$request->getPost("page_featured"),
        "date_created"=>date('Y-m-d H:i:s'),
        "date_updated"=>date('Y-m-d H:i:s')
        ));
    if($activity->save())
    {
      $this->view->success.="<div class='alert alert-success'>Activity has been successfully added</div>";
      header('Refresh: 1; URL=/admin/abkresactivities');
      // $this->flash->success("Activity has been successfully added");
      Tag::resetInput();
    }else{
      $this->flash->error($activity->getMessages());
    }
  }

}

public function AbkresactivitiesAction()
{


$this->view->user = null;
$this->view->menu = null;
$this->view->abk_user = null;



    $where= [];


  if ($this->request->isPost() && $this->request->getPost('action') == "delete") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $page = Activities::find("id='".$id."'");
      if (!$page) {
        $this->flash->error("Page was not found");
      }

      if (!$page->delete()) {
        foreach ($products->getMessages() as $message) {
          $this->flash->error((string) $message);
        }
        $this->forward("admin/abkresevents");
      } else {
        $this->flash->success("Activity was successfully deleted.");
      }
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      $keyword = trim($this->request->getPost('search_text'));
      $this->session->set("eventuser_search_text", $keyword);
    } else {
      $searchtext = $this->session->get("event_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "feature") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $f = Activities::findFirst('featured=1');
      if($f){
        $f->featured = null;
        $f->save();
      }
      $page = Activities::findFirst('id='+$id);
      $page->featured = 1;
      if($page->save()){
        $this->flash->success("Activity has been featured.<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>");
      } else {
        $this->flash->error("Something went wrong. Please try again later.");
      }
    }
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "activate") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $page = Activities::findFirst('id='+$id);
      $page->status = 1;
      if($page->save()){
        $this->flash->success("Activity has been activated.<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>");
      } else {
        $this->flash->error("Something went wrong. Please try again later.");
      }
    }
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "deactivate") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $page = Activities::findFirst('id='+$id);
      $page->status = 0;
      if($page->save()){
        $this->flash->success("Activity has been deactivated. <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>");
      } else {
        $this->flash->error("Something went wrong. Please try again later.");
      }
    }
  }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
    $this->session->remove("event_search_text"); $this->session->remove("event_filter_date");
    $this->session->remove("event_filter_date_from"); $this->session->remove("event_filter_date_to");
    unset($_POST);
  }else{
    if ($this->request->isPost()) {
      $keyword = trim($this->request->getPost('search_text'));
      $this->session->set("event_search_text", $keyword);
    }else{
      $searchtext = $this->session->get("event_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
  }

  if(!empty($keyword)){
    // $where = preg_replace('/[^A-Za-z0-9]/', "", $keyword);
    $where = 'title LIKE "%'.$keyword.'%"';
    $this->flash->notice('Search results for "<strong>' . $keyword .'</strong>"');
  }else{
    // $where = null;
    $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):null;
    $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):null;
    if(($fromDate && $toDate) || $this->session->has("event_filter_date") ) {
      if($fromDate > $toDate) {
        return $this->flash->warning("Invalid request. End date must be later than the start date.");
      }
      if($this->session->has("event_filter_date")) {
        if(!$fromDate && !$toDate){
          $fromDate = trim($this->session->get('pages_filter_date_from'));
          $toDate = trim($this->session->get('pages_filter_date_to'));
        }
      } else {
        $this->session->set("event_filter_date", true);
        $this->session->set("event_filter_date_from", $fromDate);
        $this->session->set("event_filter_date_to", $toDate);
      }
      $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
      $fromDate = date('Y-m-d', strtotime($this->request->getPost('fromDate')));
      $toDate = date('Y-m-d', strtotime($this->request->getPost('toDate')));
      $where = " (date_updated BETWEEN '".$fromDate."' AND '".$toDate."')";
    }
  }

  $numberPage = $this->request->getQuery("page", "int"); $numberPage = empty($numberPage)?1:$numberPage;
  $activities = Activities::find($where)->toArray();
  $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
    "data" => $activities,
    "limit"=> 10,
    "page" => $numberPage
    ));
  $this->view->page = $paginator->getPaginate();
}

public function AbkreseditactivityAction($id)
{
  $request=new Phalcon\Http\Request();
  $activity = Activities::findFirst($id)->toArray();
   $form = new CreatepostForm();
  $this->view->form = $form;
  $this->view->activity = $activity;
  $request=new Phalcon\Http\Request();
  $this->view->pictures = $pictures = Tblpagesimg::find("imgpath = 'img/activities/'");

  if($this->request->isPost() && $this->request->getPost('save_page')){

    $validation = new Phalcon\Validation();
     $validation ->add('page_title', new PresenceOf(array(

        'message' => 'Activity Title is required'
      )));
     $validation ->add('hpage_slug', new PresenceOf(array(

        'message' => 'Slug is required'
      )));
    $validation ->add('page_content', new PresenceOf(array(

        'message' => 'Activity Content is required'
      )));
    $validation ->add('page_keyword', new PresenceOf(array(

        'message' => 'Activity Keyword is required'
      )));
    $validation ->add('pageBannerUrl', new PresenceOf(array(

        'message' => 'Activity Banner is required'
      )));
     $validation ->add('page_description', new PresenceOf(array(

        'message' => 'Activity Description is required'
      )));

    if(count($validation->validate($_POST))){
        foreach ($validation->getMessages()->filter('page_content') as $message) {
          $this->view->actcon .= "<div class='label label-danger'>".$message."</div> ";
        }
         foreach ($validation->getMessages()->filter('page_keyword') as $message) {
          $this->view->actkey .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('page_title') as $message) {
          $this->view->acttil .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('hpage_slug') as $message) {
          $this->view->actslug .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('pageBannerUrl') as $message) {
          $this->view->actban .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('page_description') as $message) {
          $this->view->actdis .= "<div class='label label-danger'>".$message."</div> ";
        }

      }


    $activity = Activities::findFirst("id='".$id."'");
    $activity->assign(
      array(
        "title"=>$request->getPost("page_title"),
        "slugs"=>$request->getPost("hpage_slug"),
        "keyword" => $request->getPost("page_keyword"),
        "activity_description" => $request->getPost("page_description"),
        "content" => $request->getPost("page_content"),
        "status"=>$request->getPost("page_active"),
        "banner"=>$request->getPost("pageBannerUrl"),
        "date_created"=>date('Y-m-d H:i:s'),
        "date_updated"=>date('Y-m-d H:i:s')
        ));
    if($activity->save())
    {
      // $this->flash->success("Activity successfully updated.<a href='/admin/abkresactivities'> Back to Activity List</a>");
      $this->view->success.="<div class='alert alert-success'>Activity has been successfully updated</div>";
      header('Refresh: 1; URL=/admin/abkresactivities');
      Tag::resetInput();
      $this->view->activity = Activities::findFirst($id)->toArray();
    }else{
      $this->flash->error($activity->getMessages());
    }
  }
}

public function AbkrescueteamAction()
{
  if ($this->request->isPost() && $this->request->getPost('action') == "delete") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $page = Rescueteam::find("id='".$id."'");
      if (!$page) {
        $this->flash->error("Page was not found");
      }

      if (!$page->delete()) {
        foreach ($products->getMessages() as $message) {
          $this->flash->error((string) $message);
        }
        $this->forward("admin/abkrescueteam");
      } else {
        $this->flash->success("Volunteer was successfully deleted.");
      }
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      $keyword = trim($this->request->getPost('search_text'));
      $this->session->set("eventuser_search_text", $keyword);
    } else {
      $searchtext = $this->session->get("event_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
  }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
    $this->session->remove("event_search_text"); $this->session->remove("event_filter_date");
    $this->session->remove("event_filter_date_from"); $this->session->remove("event_filter_date_to");
    unset($_POST);
  }else{
    if ($this->request->isPost()) {
      $keyword = trim($this->request->getPost('search_text'));
      $this->session->set("event_search_text", $keyword);
    }else{
      $searchtext = $this->session->get("event_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
  }

  if(!empty($keyword)){
    $where = ' fname LIKE "%'.$keyword.'%" OR lname LIKE "%'.$keyword.'%" OR email LIKE "%'.$keyword.'%"
    OR country LIKE "%'.$keyword.'%"';
    $this->flash->notice('Search results for "<strong>' . $keyword .'</strong>"');
  }else{
    $where = null;
    $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):null;
    $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):null;
    if(($fromDate && $toDate) || $this->session->has("event_filter_date") ) {
      if($fromDate > $toDate) {
        return $this->flash->warning("Invalid request. End date must be later than the start date.");
      }
      if($this->session->has("event_filter_date")) {
        if(!$fromDate && !$toDate){
          $fromDate = trim($this->session->get('pages_filter_date_from'));
          $toDate = trim($this->session->get('pages_filter_date_to'));
        }
      } else {
        $this->session->set("event_filter_date", true);
        $this->session->set("event_filter_date_from", $fromDate);
        $this->session->set("event_filter_date_to", $toDate);
      }
      $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
      $fromDate = date('Y-m-d', strtotime($this->request->getPost('fromDate')));
      $toDate = date('Y-m-d', strtotime($this->request->getPost('toDate')));
      $where = " (date_created BETWEEN '".$fromDate."' AND '".$toDate."')";
    }
  }


  if($where == ""){
    $numberPage = $this->request->getQuery("page", "int"); $numberPage = empty($numberPage)?1:$numberPage;
    $activities = Rescueteam::find(['order'=>'date_created DESC' ])->toArray();
  }else{
    $numberPage = $this->request->getQuery("page", "int"); $numberPage = empty($numberPage)?1:$numberPage;
    $activities = Rescueteam::find($where)->toArray();
  }


  $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
    "data" => $activities,
    "limit"=> 10,
    "page" => $numberPage
    ));
  $this->view->page = $paginator->getPaginate();
}

  // Create Volunteer Function
public function createvolunteerAction()
{
 \Phalcon\Tag::prependTitle('Create Volunteer | ');
 $this->view->success=null;
 $this->view->fname=null;
 $this->view->lname=null;

 $form = new CreateuserForm();
 if ($this->request->isPost()) {

     $validation = new Phalcon\Validation();
     $validation ->add('email', new Regex(array(
                    'message'    => 'Enter a valid Email Address',
                    'pattern'    => '/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i',
                    'allowEmpty' => false
                    )));
     $validation ->add('firstname', new PresenceOf(array(

        'message' => 'Firstname is required'
      )));
     $validation ->add('lastname', new PresenceOf(array(

        'message' => 'Lastname is required'
      )));
      $validation ->add('contact', new PresenceOf(array(
        'message' => 'Contact number is required',
        )));
        $validation ->add('contact', new StringLength(array(
        'min' => 11,
        'messageMinimum' => 'Contact should only accept 11 digits'
        )));
     if(count($validation->validate($_POST))){
        foreach ($validation->getMessages()->filter('email') as $message) {
          $this->view->emailval .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('firstname') as $message) {
          $this->view->fname .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('lastname') as $message) {
          $this->view->lname .= "<div class='label label-danger'>".$message."</div> ";
        }
         foreach ($validation->getMessages()->filter('contact') as $message) {
                    $this->view->contactErr .= "<div class='label label-danger'>".$message."</div> ";
                  }
      }
      else{
          if ($form->isValid($this->request->getPost()) != true) {
            $chkE = $this->request->getPost('email');
            $emailcheck = Rescueteam::findFirst([
                'conditions' => 'email= "'.$chkE.'"'
            ]);
            if($emailcheck){
              $this->view->emailval .= "<div class='label label-danger'> Email is already exist.</div> ";
            }else{
              $user = new Rescueteam();
              $user->assign(array(
              'fname' => trim($this->request->getPost('firstname')),
              'lname' => trim($this->request->getPost('lastname')),
              'email' => trim($this->request->getPost('email')),
              'address' => trim($this->request->getPost('address')),
              'city' => trim($this->request->getPost('city')),
              'country' => trim($this->request->getPost('country')),
              'state' => trim($this->request->getPost('state')),
              'zip' => trim($this->request->getPost('zip')),
              'contact' => trim($this->request->getPost('contact')),
              'date_created' => date('Y-m-d H:i:s'),
              'date_updated' => date('Y-m-d H:i:s')
              ));
            if ($user->save()) {
              $this->view->success.="<div class='alert alert-success'>Volunteer was created successfully!</div>";
              // $this->flash->success("Volunteer was created successfully!");
              Tag::resetInput();
                 header('Refresh: 1; URL=/admin/abkrescueteam');

            } else {
              $this->flash->error($user->getMessages());
            }
            }
          }
      }


}
$this->view->form = $form;
}
//jointeam events
public function abkresjointeamAction()
{
  $form = new CreateEventForm();
  $request=new Phalcon\Http\Request();
  $this->view->jointeam = Jointeam::findFirstById(1)->toArray();
  if($this->request->isPost() && $this->request->getPost('save_page')){
    $jointeam = Jointeam::findFirstById(1);
    $jointeam->assign(
      array(
        "description"=>$request->getPost("description"),
        "date_updated"=>date('Y-m-d H:i:s')
        ));
    if($jointeam->save())
    {
      $this->flash->success("jointeam text successfully updated");
      $this->view->jointeam = Jointeam::findFirstById(1)->toArray();
    }else{
      $this->flash->error($jointeam->getMessages());
    }
  }
  $this->view->form = $form;
}
//donation events
public function abkresdonateAction()
{
  $form = new CreateEventForm();
  $request=new Phalcon\Http\Request();
  $this->view->donate = Donate::findFirstById(1)->toArray();
  if($this->request->isPost() && $this->request->getPost('save_page')){
   $donate = Donate::findFirstById(1);
   $donate->assign(
     array(
       "description"=>$request->getPost("description"),
       "date_updated"=>date('Y-m-d H:i:s')
       ));
   if($donate->save())
   {
     $this->flash->success("Donation text successfully updated");
     $this->view->donate = Donate::findFirstById(1)->toArray();
   }else{
     $this->flash->error($donate->getMessages());
   }
 }
 $this->view->form = $form;
}

public function editvolunteerAction($id){
  \Phalcon\Tag::prependTitle('Edit Volunteer | ');
  $volunteer = Rescueteam::findFirst($id);
  $this->view->volunteer = $volunteer;
  $form = new CreateuserForm($volunteer, array(
    'Edit' => true
    ));

  if ($this->request->isPost()) {

    $validation = new Phalcon\Validation();
    $validation ->add('firstname', new PresenceOf(array(

        'message' => 'Firstname is required'
      )));
    $validation ->add('lastname', new PresenceOf(array(

        'message' => 'Lastname is required'
      )));
    $validation ->add('email', new PresenceOf(array(

        'message' => 'Email Address is required'
      )));

    if(count($validation->validate($_POST))){
        foreach ($validation->getMessages()->filter('firstname') as $message) {
          $this->view->fname .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('lastname') as $message) {
          $this->view->lname .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('email') as $message) {
          $this->view->email .= "<div class='label label-danger'>".$message."</div> ";
        }
      }
      else{
        $volunteer->assign(array(
      'fname' => trim($this->request->getPost('firstname', 'striptags')),
      'lname' => trim($this->request->getPost('lastname', 'striptags')),
      'email' => trim($this->request->getPost('email', 'striptags')),
      'address' => trim($this->request->getPost('address', 'striptags')),
      'city' => trim($this->request->getPost('city', 'striptags')),
      'country' => trim($this->request->getPost('country', 'striptags')),
      'state' => trim($this->request->getPost('state', 'striptags')),
      'zip' => trim($this->request->getPost('zip', 'striptags')),
      'contact' => trim($this->request->getPost('contactnomask', 'striptags')),
      'date_updated' => date('Y-m-d H:i:s')
      ));
    if (!$volunteer->save()) {
      $this->flash->error($volunteer->getMessages());
    } else {
     $this->flash->success("Volunteer was updated successfully");
     header('Refresh: 1; URL=/admin/abkrescueteam');
     $this->view->roleError = null;
   }
      }
 }
 $this->view->form = $form;
}

public function allblogspostAction(){
 \Phalcon\Tag::prependTitle('Blogs Posts | ');
  $this->view->menu = $this->_menuActive('');
  $numberPage = 1;
  if($this->request->isPost() && $this->security->getSessionToken() != $this->request->getPost('csrf')){
    Tag::resetInput();
  }elseif ($this->request->isPost() && $this->request->getPost('action') == "delete") {
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      $this->_deleteBlog($id);
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      $keyword = $this->request->getPost('search_text');
      $this->session->set("user_search_text", $keyword);
    } else {
      // $numberPage = $this->request->getQuery("page", "int");
      $searchtext = $this->session->get("user_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
  }elseif($this->request->isPost() && $this->request->getPost('action') == "delete_selected"){
    $id = $this->request->getPost('tbl_id');
    if(!empty($id)){
      $this->_deleteBlog($id);
    }
    /* stay on search pages*/
    if ($this->request->isPost()) {
      // $query = Criteria::fromInput($this->di, 'Tblusers', array('userName' => $this->request->getPost('search_text'), 'userFirstname' => $this->request->getPost('search_text')));
      // $this->persistent->searchUserParams = $query->getParams();
      // $this->session->set("user_search_text", $this->request->getPost('search_text'));
      $keyword = $this->request->getPost('search_text');
      $this->session->set("user_search_text", $keyword);
    } else {
      // $numberPage = $this->request->getQuery("page", "int");
      $searchtext = $this->session->get("user_search_text");
      $keyword = !empty($searchtext)?$searchtext:'';
    }
    /**/
  }elseif($this->request->isPost() && $this->request->getPost('action') == "edit"){
    $id = $this->request->getPost('recordID');
    if(!empty($id)){
      return $this->dispatcher->forward(array(
        'controller' => 'admin',
        'action' => 'abkreseditblog',
        'params' => array($id)
        ));
    }
  }elseif($this->request->isPost() && $this->request->getPost('clear_search')){
    $this->session->remove("allBlogs_search_text");
    $this->session->remove("allBlogs_filter_date");
    $this->session->remove("allBlogs_filter_date_from");
    $this->session->remove("allBlogs_filter_date_to");
    $this->persistent->searchAllNewsParams = null;
    unset($_POST);
  }else{
    if ($this->request->isPost()) {
      $query = Criteria::fromInput($this->di, 'Tblblog', array('blogTitle' => trim($this->request->getPost('search_text'))));
      $this->persistent->searchAllNewsParams = $query->getParams();
      $this->session->set("allNews_search_text", $this->request->getPost('search_text'));
    } else {
      $numberPage = $this->request->getQuery("page", "int");
    }
  }


  $parameters = array();
  if ($this->persistent->searchAllNewsParams) {
    $parameters = $this->persistent->searchAllNewsParams;
  }

  // SORTING
  // Added a server side sorting on all tables using the columns shown
  // Dont forget to insert na href params on the view
  $sort = $this->request->getQuery("sort");
  $order = "blogPublishDate DESC";
  $this->view->blogtitleHref = "blogTitle-asc";
  $this->view->blogauthorHref = "blogAuthor-asc";
  $this->view->blogdateHref = "blogDate-asc";
  $this->view->publishdateHref = "blogPublishDate-asc";
  $arr = explode("-", $sort);
  switch ($arr[0]) {
    case 'blogTitle':
    $arr[1] == "asc" ? $this->view->blogtitleHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->blogttitleIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'blogDate':
    $arr[1] == "asc" ? $this->view->blogdateHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->blogdateIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'blogPublishDate':
    $arr[1] == "asc" ? $this->view->publishdateHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->publishdateIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    case 'blogAuthor':
    $arr[1] == "asc" ? $this->view->blogauthorHref = $arr[0] . "-desc" : "";
    $order = "$arr[0] $arr[1]";
    $this->view->blogauthorIndicator = $arr[1] == "asc" ? "icon-chevron-up" : "icon-chevron-down";
    break;
    default:
    break;
  }

  $builder = $this->modelsManager->createBuilder()
  ->columns('blogID, blogTitle, blogAuthor, blogDate, blogPublishDate, blogStatus')
  ->from('Tblblog')
  ->orderBy($order);

  if(!empty($parameters)){
    $this->flash->notice('Search results for "<strong>' . $this->session->get("allNews_search_text") .'</strong>"');
    $builder->andWhere($parameters['conditions'], $parameters['bind']);
  } else {
    $fromDate = $this->request->getPost('fromDate', 'striptags') ? strtotime($this->request->getPost('fromDate', 'striptags')):"";
    $toDate = $this->request->getPost('toDate', 'striptags') ? strtotime($this->request->getPost('toDate', 'striptags'). "+23 hours +59 minutes"):"";
    if(($fromDate && $toDate) || $this->session->has("allblog_filter_date") ) {
      if($fromDate > $toDate) {
        return $this->flash->warning("Invalid request. End date must be later than the start date.");
      }
      if($this->session->has("allblog_filter_date")) {
        if(!$fromDate && !$toDate){
          $fromDate = trim($this->session->get('allblog_filter_date_from'));
          $toDate = trim($this->session->get('allblog_filter_date_to'));
        }
      } else {
        $this->session->set("allblog_filter_date", true);
        $this->session->set("allblog_filter_date_from", $fromDate);
        $this->session->set("allblog_filter_date_to", $toDate);
      }
      $this->flash->notice('List filtered with dates between "<strong>' . date("F j, Y", $fromDate) .'</strong>" and "<strong>'. date("F j, Y", $toDate) .'</strong>"');
      $builder->where("blogDate BETWEEN $fromDate AND $toDate");
    }
  }

  $paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
    "builder" => $builder,
    "order" => "postDate DESC",
    "limit"=> 10,
    "page" => $numberPage
    ));

  // Get the paginated results
  $this->view->page = $paginator->getPaginate();
}

public function abkrescreateblogAction(){



  \Phalcon\Tag::prependTitle('Create Blog | ');
  // var_dump("test");
  // die;
  // $this->view->menu = $this->_menuActive('');

  $this->view->menu = null;
  $this->view->blogstatus = null;
  $programs = Tblprograms::find();
  $pages = Tblpages::find('specialPage=1');
  $this->view->script = '<script> var CURRENT_FOLDER_CAT = "post"; </script>';

  // $this->view->script = '<script> var CURRENT_PROGRAM_FOLDER = "post"; var CURRENT_FOLDER_CAT = "program"; </script>';
  $form = new CreateblogForm();
  $form->csrf = $this->security->getToken();
  $this->view->pictures = $pictures = Tblpagesimg::find();
  $this->view->titleError = null;
  $this->view->authoreError = null;
  $this->view->contentError = null;
  $this->view->checkedProgsError = null;
  $this->view->success=null;
  $this->view->folders = $this->_getFolderAssets();

  $this->_createPostFolder($form->blogID);
  // $this->_createPostFolder("post");

  $f = $this->_getFolderAssets();
  $toparray = array();
  foreach ($f as $fkey => $fvalue) {
    $toparray[] = $fkey;
    foreach($fvalue as $ff){
      $toparray[] = $ff;
      break;
    }
    break;
  }
  $this->view->dayear = $toparray[0];
  $this->view->damonth = $toparray[1];



  if($this->request->isPost() && $this->request->getPost('submit_post')){
    /*if($this->security->getSessionToken() == $this->request->getPost('csrf')){*/
      $validation = new Phalcon\Validation();
      $validation ->add('blog_author', new PresenceOf(array(

        'message' => 'Author is required'
      )));
      $validation ->add('page_title', new PresenceOf(array(

        'message' => 'Title is required'
      )));
       $validation ->add('blog_content', new PresenceOf(array(

        'message' => 'Content is required'
      )));
        $validation ->add('blogDatePublish', new PresenceOf(array(

        'message' => 'Publish Date is required'
      )));
          $validation ->add('pageBannerUrl', new PresenceOf(array(

        'message' => 'Featured image is required'
      )));
      if(count($validation->validate($_POST))){
        foreach ($validation->getMessages()->filter('blog_author') as $message) {
          $this->view->blogauth .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('page_title') as $message) {
          $this->view->blogtitle .= "<div class='label label-danger'>".$message."</div> ";
        }
         foreach ($validation->getMessages()->filter('blog_content') as $message) {
          $this->view->blogcontent .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('blogDatePublish') as $message) {
          $this->view->blogpubDate .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('pageBannerUrl') as $message) {
          $this->view->FeaImg .= "<div class='label label-danger'>".$message."</div> ";
        }
      }else{
         if ($form->isValid($this->request->getPost()) != true) {
        $postcheck = Tblblog::findFirst('blogTitle="'.$this->request->getPost('blog_title', 'striptags').'"');
        if($postcheck==true){
          $this->flash->error('Page title already taken.');
        }
        else{
          $post = new Tblblog();
          $poststatus = ($this->request->getPost('blog_status') ? $this->request->getPost('post_status') : 'publish');
          $post->assign(array(
            'blogTitle' => trim($this->request->getPost('page_title', 'striptags')),
            'blogAuthor' => trim($this->request->getPost('blog_author', 'striptags')),
            'blogSlug' =>  trim($this->request->getPost('hpage_slug', 'striptags'),'-'),
            'blogContent' => trim($this->request->getPost('blog_content', 'striptags')),
            'blogStatus' => $poststatus,
            'blogDate' => time(),
            'blogPublishDate' => ($this->request->getPost('startdate', 'striptags') ? strtotime($this->request->getPost('blogDatePublish', 'striptags')):time()),
            'blogFeatureImage' => trim($this->request->getPost('pageBannerUrl', 'striptags')),
            ));

          // if($post){
          //   $this->flash->success("Post was created successfully.");

          // }

          if ($post->save()) {
             $this->view->form = $form;
             //$this->flash->success("Post was created successfully.");
             $this->view->successbox.="<div class='alert alert-success'>Blog has been successfully added.</div>";
             Tag::resetInput();
             //$form->clear();

            //send email to subscribers
            // if($blogstatus == 'publish'){
            //   $body = $this->request->getPost('blog_content').'<br /><br />'
            //   .'http://angbayanko.org/post/news/'.trim($this->request->getPost('blog_slug', 'striptags'),'-');
            //   $this->sendnewsletter($this->request->getPost('blog_title', 'striptags'), $body);

            // }
          }
           else{
            $this->flash->error($post->getMessages());
              }

            Tag::resetInput();
        }

      }
    }


    }

    $this->view->form = $form;
    $this->view->programs = $programs;
    $this->view->pages = $pages;

$programID = "post"; //Temporary
$this->view->prog = $programID ; //Temporary

$image_files = array();
$files = glob('../public/img/programs/'.$programID.'/*.*');
usort($files, create_function('$a,$b', 'return filemtime($a)<filemtime($b);'));
foreach($files as $filename){
  $image_files[] = basename($filename);
}
$this->view->digital_assets = $image_files;
}


public function abkreseditblogAction($id){

   \Phalcon\Tag::prependTitle('Edit Blog | ');
  $this->view->menu = $this->_menuActive('');
  $programs = Tblprograms::find();
  $post = Tblblog::findFirst($id);
  $pages = Tblpages::find('specialPage=1');
  $this->view->script = '<script> var CURRENT_FOLDER_CAT = "post"; </script>';
  $form = new CreateblogForm();
  $this->view->titleError = null;
  $this->view->authorError = null;
  $this->view->contentError = null;
  $this->view->checkedProgsError = null;
  $this->view->folders = $this->_getFolderAssets();

  $f = $this->_getFolderAssets();
  $toparray = array();
  foreach ($f as $fkey => $fvalue) {
    $toparray[] = $fkey;
    foreach($fvalue as $ff){
      $toparray[] = $ff;
      break;
    }
    break;
  }
  $this->view->dayear = $toparray[0];
  $this->view->damonth = $toparray[1];

  if($this->request->isPost() && $this->request->getPost('update_blog')){

      $validation = new Phalcon\Validation();
      $validation ->add('blog_author', new PresenceOf(array(

        'message' => 'Author is required'
      )));
       $validation ->add('pageBannerUrl', new PresenceOf(array(
        'message' => 'Featured Image is required',
        )));
        $validation ->add('blog_title', new PresenceOf(array(

        'message' => 'Title is required'
      )));
       $validation ->add('blog_content', new PresenceOf(array(

        'message' => 'Content is required'
      )));
      if(count($validation->validate($_POST))){
        foreach ($validation->getMessages()->filter('blog_author') as $message) {
          $this->view->blogauth .= "<div class='label label-danger'>".$message."</div> ";
        }
         foreach ($validation->getMessages()->filter('pageBannerUrl') as $message) {
          $this->view->featimageError .= "<div class='label label-danger'>".$message."</div> ";
        }
        foreach ($validation->getMessages()->filter('page_title') as $message) {
          $this->view->blogtitle .= "<div class='label label-danger'>".$message."</div> ";
        }
         foreach ($validation->getMessages()->filter('blog_content') as $message) {
          $this->view->blogcontent .= "<div class='label label-danger'>".$message."</div> ";
        }
      }else{


      // $postcheck = Tblblog::findFirst('blogTitle="'.$this->request->getPost('blog_title', 'striptags').'" AND  blogID != "'.$id.'"');

      // if($postcheck==true){
      //   $this->flash->error('Page title already taken.');
      // }else{
        $post->assign(array(
          'blogTitle' => trim($this->request->getPost('blog_title', 'striptags')),
          'blogSlug' =>  trim($this->request->getPost('blog_slug', 'striptags'),'-'),
          'blogAuthor' => trim($this->request->getPost('blog_author', 'striptags')),
          'blogContent' => trim($this->request->getPost('blog_content')),
          'blogStatus' => ($this->request->getPost('blog_status') ? $this->request->getPost('post_status') : 'publish'),
          'blogPublishDate' => ($this->request->getPost('blogDatePublish', 'striptags') ? strtotime($this->request->getPost('blogDatePublish', 'striptags')):time()),
          'blogFeatureImage' => trim($this->request->getPost('pageBannerUrl', 'striptags'))
          ));
        if ($post->save()) {
          /*  $this->flash->error($post->getMessages());*/
          $this->flash->success("Blog has been  successfully updated.");
          header('Refresh: 1; URL=/admin/allblogspost');
            Tag::resetInput();
          }
        else {
             $this->flash->error($post->getMessages());
        }
  //    }//end of postcheck
      }

  }
  /**/
  $this->view->form = $form;
  $this->view->programs = $programs;
  $this->view->post = $post;
  $this->view->pages = $pages;

        $programID = "post"; //Temporary
        $this->view->prog = $programID ; //Temporary

        $image_files = array();
        $files = glob('../public/img/programs/'.$programID.'/*.*');
        usort($files, create_function('$a,$b', 'return filemtime($a)<filemtime($b);'));
        foreach($files as $filename){
          $image_files[] = basename($filename);
        }
        $this->view->digital_assets = $image_files;
      }

private function _deleteBlog($id){
  $param=null;
  if(is_array($id)){
    $newid = array();
    foreach ($id as $i) {
      $param .= 'blogID = '. $this->filter->sanitize($i, array("int")) . ' OR ';
    }
    $param = substr($param, 0, -4);
  }else{
    $id = $this->filter->sanitize($id, array("int"));
    $param = 'blogID=' . $id;
  }

  $post = Tblblog::find($param);
  if (!$post) {
    $this->flash->error("Post was not found");
  }

  if (!$post->delete()) {
    foreach ($post->getMessages() as $message) {
      $this->flash->error((string) $message);
    }
    return $this->forward("admin/allblogspost");
  } else {
    $this->flash->success("Blog has been successfully deleted.");
  }
}
//donation events
public function abkresexpertAction()
{
  $form = new CreateEventForm();
  $request=new Phalcon\Http\Request();
  $this->view->expert = Expert::findFirstById(1)->toArray();

  if($this->request->isPost() && $this->request->getPost('save_page')){
    $expert = Expert::findFirstById(1);
    $expert->assign(
      array(
        "description"=>$request->getPost("description"),
        "date_updated"=>date('Y-m-d H:i:s')
        ));
    if($expert->save())
    {
      $this->flash->success("Experties Content was successfully updated");
      $this->view->expert = Expert::findFirstById(1)->toArray();
    }else{
      $this->flash->error($expert->getMessages());
    }
  }
  $this->view->form = $form;

}

public function programlistAction($id=null){

  \Phalcon\Tag::prependTitle('Programs | ');
  $this->view->menu = $this->_menuActive('pmrole');
  $programs = Tblprograms::find();
  $form = new ProgramsForm();
  $form->csrf = $this->security->getToken();
  $this->view->titletaken = null;
  $this->view->edit = false;
  $this->view->pictureerror = null;

  $validation = new Phalcon\Validation();
  $this->view->titleErr = null;
  $this->view->tooltipErr= null;
  $this->view->taglineErr= null;
  $this->view->program_pictureErr = null;
  $this->view->program_urlErr = null;


  $validation ->add('title', new PresenceOf(array(
        'message' => 'Title is required',
        )));

  $validation ->add('tooltip', new PresenceOf(array(
        'message' => 'Tooltip is required',
        )));

  $validation ->add('tagline', new PresenceOf(array(
        'message' => 'Tagline is required',
        )));

  $validation ->add('program_picture', new PresenceOf(array(
        'message' => 'Please add pictures',
        )));
  $validation ->add('programurl', new PresenceOf(array(
        'message' => 'Program URL is required',
        )));

  if(!empty($id)){

    $this->view->edit = true;
    $eprogs = Tblprograms::findFirst("programID=".$id);
    $this->view->eprogs = $eprogs;
    $eprogsi = Tblprogramsimg::find("programID=".$id);
    $this->view->eprogsi = $eprogsi;
  }



  if ($this->request->isPost() && $this->request->getPost('action') == "delete") {

    $id = $this->request->getPost('recordID');

    if(!empty($id)){

      $this->_deleteProgram($id);
      $programs = Tblprograms::find();
      $this->view->programMenu = $programs;

    }

  }

  elseif($this->request->isPost() && $this->request->getPost('update-program')){

      $pictures = $this->request->getPost('program_picture');
      $programname = Tblprograms::findFirst("programName='".$this->request->getPost('title', 'striptags')."' AND programID != ".$id);

      if(count($validation->validate($_POST))){

        //execute validation for title
        foreach ($validation->getMessages()->filter('title') as $message) {
          $this->view->titleErr .= "<div class='label label-danger' >".$message."</div> ";
        }
        //execute validation for tooltip
        foreach ($validation->getMessages()->filter('tooltip') as $message) {
          $this->view->tooltipErr .= "<div class='label label-danger' >".$message."</div> ";
        }
        //execute validation for tagline
        foreach ($validation->getMessages()->filter('tagline') as $message) {
          $this->view->taglineErr .= "<div class='label label-danger' >".$message."</div> ";
        }
        //execute validation for program_picture
        foreach ($validation->getMessages()->filter('program_picture') as $message) {
          $this->view->program_pictureErr .= "<div class='label label-danger' >".$message."</div> ";
        }

        //execute validation for program_url
        foreach ($validation->getMessages()->filter('programurl') as $message) {
          $this->view->program_urlErr .= "<div class='label label-danger' >".$message."</div> ";
        }

        }else{

        $eprogs->assign(array(

          'programName' => $this->request->getPost('title', 'striptags'),
          'programTooltip' => $this->request->getPost('tooltip', 'striptags'),
          'programTagline' => $this->request->getPost('tagline', 'striptags'),
          'programPage' => $this->request->getPost('programurl', 'striptags'),
          'programLastUpdated' => time()
          ));

      if($programname){

        $this->view->titletaken.="<div class='label label-danger' >Program Name already taken</div>" ;

      }else{

        if (!$eprogs->save()) {

          $this->flash->error($user->getMessages());

        } else {
          //Clean other files not included
          $tpi = Tblprogramsimg::find('programID='.$id);
          foreach ($tpi as $t) {
            $ch = false;
            foreach($pictures as $pic){
              if($t->imgname == $pic){
                $ch=true;


              }
            }

            if(!$ch){
              if(is_file('../public/img/programs/'.$t->imgname)){
                unlink('../public/img/programs/'.$t->imgname);
              }

              if(is_file('../public/img/programs/thumbnail/'.$t->imgname)){
                unlink('../public/img/programs/thumbnail/'.$t->imgname);
              }
            }
          }

          $this->modelsManager->executeQuery("DELETE FROM Tblprogramsimg WHERE Tblprogramsimg.programID = ".$id);

          $phql = "INSERT INTO Tblprogramsimg ( imgname, imgpath, programID) "
          . "VALUES (:imgname:, :imgpath:, :programID:)";
          foreach($pictures as $pic){
            if(!is_file('../public/img/programs/'.$pic)){
              $newpicname = md5(uniqid(rand(),true)).$pic;
              $res = $this->modelsManager->executeQuery($phql,
                array(
                  'imgname' => $newpicname,
                  'imgpath' => 'public/programs/',
                  'programID' => $id,
                  )
                );
              rename('../public/server/php/files/'.$pic, '../public/img/programs/'.$newpicname);
              rename('../public/server/php/files/thumbnail/'.$pic, '../public/img/programs/thumbnail/'.$newpicname);
            }else{
              $res = $this->modelsManager->executeQuery($phql,
                array(
                  'imgname' => $pic,
                  'imgpath' => 'public/programs/',
                  'programID' => $id,
                  )
                );
            }
          }
          $form->clear();
          $this->flash->success("Program has been successfully updated ");
          $this->view->eprogsi = $eprogsi;
          header( 'Refresh:1; url=/admin/programs/0');
          $programs = Tblprograms::find();
          $this->view->programMenu = $programs;
          $eprogs = Tblprograms::findFirst("programID=".$id);
          $this->view->eprogs = $eprogs;
          $eprogsi = Tblprogramsimg::find("programID=".$id);

        }
      }
    }



  }elseif ($this->request->isPost()) {

    /*if($this->security->getSessionToken() == $this->request->getPost('csrf')){*/
      $progs = Tblprograms::findFirst("programName='".$this->request->getPost('title', 'striptags')."'");
      $tbp = new Tblprograms();

      $tbp->assign(array(
            'programName' => $this->request->getPost('title', 'striptags'),
            'programTooltip' => $this->request->getPost('tooltip', 'striptags'),
            'programTagline' => $this->request->getPost('tagline', 'striptags'),
            'programPage' => $this->request->getPost('programurl', 'striptags'),
            'programLastUpdated' => time()
            ));
          $pictures = $this->request->getPost('program_picture');

 //     if ($form->isValid($this->request->getPost()) != false) {



        if(count($validation->validate($_POST))){

        //execute validation for title
        foreach ($validation->getMessages()->filter('title') as $message) {
          $this->view->titleErr .= "<div class='label label-danger' >".$message."</div> ";
        }
        //execute validation for tooltip
        foreach ($validation->getMessages()->filter('tooltip') as $message) {
          $this->view->tooltipErr .= "<div class='label label-danger' >".$message."</div> ";
        }
        //execute validation for tagline
        foreach ($validation->getMessages()->filter('tagline') as $message) {
          $this->view->taglineErr .= "<div class='label label-danger' >".$message."</div> ";
        }
        //execute validation for program_picture
        foreach ($validation->getMessages()->filter('program_picture') as $message) {
          $this->view->program_pictureErr .= "<div class='label label-danger' >".$message."</div> ";
        }

        //execute validation for program_url
        foreach ($validation->getMessages()->filter('programurl') as $message) {
          $this->view->program_urlErr .= "<div class='label label-danger' >".$message."</div> ";
        }

        }else{
          if($progs){

            $this->view->titletaken = "<div class='label label-danger'>Program Name already exist.</div>";

          }else{

            if (!$tbp->save()) {

              $this->flash->error($tbp->getMessages());

            } else {

              $phql = "INSERT INTO Tblprogramsimg ( imgname, imgpath, programID) "
              . "VALUES (:imgname:, :imgpath:, :programID:)";

              foreach($pictures as $pic){

                $newpicname = md5(uniqid(rand(),true)).$pic;
                $res = $this->modelsManager->executeQuery($phql,

                  array(
                    'imgname' => $newpicname,
                    'imgpath' => 'public/programs/',
                    'programID' => $tbp->programID,
                    )
                  );

                rename('../public/server/php/files/'.$pic, '../public/img/programs/'.$newpicname);
                rename('../public/server/php/files/thumbnail/'.$pic, '../public/img/programs/thumbnail/'.$newpicname);

              }
              $this->_createProgramFolder($tbp->programID);
              $programs = Tblprograms::find();
              $this->view->programMenu = $programs;
              $form->clear();
              $this->flash->success("Program was created successfully");
              header( 'Refresh:1; url=/admin/programs', true, 303);
              $body = 'Please visit '
              .'http://angbayanko.org/programs/page/'.$this->request->getPost('programurl', 'striptags');
              $this->sendnewsletter('ANG BAYAN KO NEW PROGRAM - '.$this->request->getPost('title', 'striptags'), $body);

              Tag::resetInput();
            }
          }
        }
   //   }
/*  }else{
Tag::resetInput();
}*/
}

$this->view->form = $form;
$this->view->prog = $programs;
}

}
