<?php

class ProgramsController extends ControllerBase
{
    protected $breadCrumbs = "<a href='/'>Home</a> > <a href='/programs/'>Program List</a>";
    public function initialize()
    {
        parent::initialize();
        date_default_timezone_set('Asia/Manila');
    $this->view->bread_crumbs = $this->breadCrumbs ."  <a href=''>". $program->programName."</a>";
        $this->view->specialPagesLinks = $this->_getPagesLinks(1);
        $this->validateLoginVolunteer();

    }
    public function indexAction()
    {

            // $contact= Tblcontact::find();
            // $this->view->contacts=$contact;
            $prog= Tblprograms::find(' programID!=0 ORDER BY programID ASC');
            $this->view->progs=$prog;

             $numberPage = $this->request->getQuery("page", "int");
             $numberPage = !empty($numberPage)?$numberPage:1;

             $phql = 'SELECT
                     Tblprograms.programID,
                     Tblprograms.programName,
                     Tblprograms.programTagline,
                     Tblprograms.programPage
                     FROM Tblprograms
                      ORDER BY programID ASC';
             $result = $this->modelsManager->executeQuery($phql);

             $dataArray = array();
             foreach ($result as $key => $value) {
                 $dataArray[] = array(
                     'programID'=>$value->programID,
                     'programName'=>$value->programName,
                     'programTagline'=>$value->programTagline,
                     'programPage'=>$this->_truncateHtml($value->programPage),
                     );
             }

             $paginator = new Phalcon\Paginator\Adapter\NativeArray(array(
                 "data" => $dataArray,
                 "limit"=> 10,
                 "page" => $numberPage
                 ));
             $this->view->page = $paginator->getPaginate();
    }
    public function progListAction(){

    }

    public function pageAction($slug){

      $about=Tblother::findfirst("title='Main Tagline'");
       $this->view->about=$about;
       $contact= Tblcontact::find();
       $this->view->contacts=$contact;

    	  $program = Tblprograms::findFirst('programPage="'.$slug.'" ');
          // $subpages = Tblpages::find('pageParent = '.$page->pageID.' AND pageType="pages" AND pageActive="1"');
        $this->view->program = $program;
        $this->view->bread_crumbs = $this->breadCrumbs ."<a href=''></a>";
        // $this->view->bread_crumbs = $this->breadCrumbs ." > <a href=''>Program List</a>";
        $this->view->bread_crumbs = $this->breadCrumbs ." > <a href=''>". $program->programName."</a>";
        $this->view->program_title = $program->programName;
       $pages = Tblpages::find('pageParent="'.$program->programID.'" AND pageType="program" ORDER BY pageOrder');
       $pagescount = count(Tblpages::find('pageParent="'.$program->programID.'" AND pageType="program" AND pageActive=1 ORDER BY pageOrder'));
        $this->view->tab_active = $pages-pageSlug;
        $this->view->actOptions = null;

        //$this->session->remove('vol_auth');
        //code came from site controller
        // $this->view->page = $page =  Tblprograms::findFirst('pageSlug="'.$slug.'" AND pageType="pages" AND pageActive="1"');
        //
        // $subpages = Tblprograms::find('pageParent = "'.$page->programID.'" AND pageType="pages" AND pageActive="1"');
        //
        // $this->view->bread_crumbs = $this->breadCrumbs ." > <a href=''>". $page->pageTitle."</a>";
        //
        // $tabs = null;
        // foreach ($subpages as $key => $value) {
        //     $tabs .= '<li class=""><a href="/site/pages/'.$value->pageSlug.'">'.$value->pageTitle.'</a></li>';
        //
        // }
        // $this->view->subpages = $tabs;
        if($pages){
            $this->view->prog_pages = $pages;
        }

        if($pagescount){
            $this->view->pagescount = $pagescount;
        }

        $this->view->forumTopics = $this->getForumTopics($program->programID, $program->programName);

if($program->specialPage = 1){
        $numberPage = $this->request->getQuery("page", "int");
        /**/
        //Post
        $builder = $this->modelsManager->createBuilder()
            ->from('Tblpost')
            ->innerJoin('Tblpostcat', 'Tblpost.postID = Tblpostcat.postID')
            ->where('relatedID ='.$program->programID)
            ->andWhere("relatedtype = 'programs'")
            ->andWhere('postPublishDate <= '.time() )
            ->orderBy('postDate DESC');
        /**/

        $paginator = new Phalcon\Paginator\Adapter\QueryBuilder(array(
          "builder" => $builder,
          "limit"=> 5,
          "page" => $numberPage
          ));
        // Get the paginated results
        $this->view->post = $post = $paginator->getPaginate();
      }
    }

    public function getForumTopics($id, $programName){

        switch ($id) {
            case 1:
                $forum_id = 3; //Disaster Risk Management
                break;
            case 2:
                $forum_id = 2; //Create Your Personal Cause
                break;
            case 3:
                $forum_id = 4; //Healthcare for Everyone
                break;
            case 4:
                $forum_id = 5; //Economic Livelihood
                break;
            case 5:
                $forum_id = 6; //Early Chilhood Education
                break;
            case 6:
                $forum_id = 7; //Environmental Preservation
                break;
            default:
                $forum_id = null;
                break;
        }

        $topicHtml = null;
        if(!empty($forum_id)){
            $phql = 'SELECT
            Phpbbtopics.topic_id,
            Phpbbtopics.topic_title,
            Phpbbtopics.topic_time,
            Phpbbtopics.topic_views,
            Phpbbtopics.topic_replies,
            Phpbbtopics.topic_last_poster_name,
            Phpbbtopics.topic_last_post_time,
            Phpbbusers.username

            FROM Phpbbtopics
            LEFT JOIN Phpbbusers ON Phpbbusers.user_id = Phpbbtopics.topic_poster
            WHERE topic_approved = 1 AND forum_id = '.$forum_id.' ORDER BY topic_id DESC LIMIT 5
            ';
            //WHERE topic_approved = 1 && forum_id = '.$forum_id.' ORDER BY topic_id DESC LIMIT 5
            $result = $this->modelsManager->executeQuery($phql);

            foreach ($result as $key => $value) {
                $topicHtml .= '
                    <div class="annList">
                        <a href="/forum/viewtopic.php?f='.$forum_id.'&t='.$value->topic_id.'" class="pull-left ann-title">'.$value->topic_title.'</a>
                        <div>
                            <p>
                                '.date("D F j, Y g:i a", $value->topic_time).'
                                <br />by: <strong>'.$value->username.'</strong>
                                <br /><i class="icon-check"></i> views: '.$value->topic_views.' &nbsp;&nbsp;<i class="icon-mail-reply-all"></i> replies: '.$value->topic_replies.'
                                <br /><em>last post by:</em> <strong>'.$value->topic_last_poster_name.'</strong>
                                <em>on</em> '.date("D F j, Y g:i a", $value->topic_last_post_time).'
                            </p>
                        </div>
                    </div>
                ';
            }

            if(!empty($topicHtml)){
                $topicHtml .= '
                    <div class="annViewAll pull-right">
                        <a href="/forum/viewforum.php?f='.$forum_id.'">Visit '.$programName.' Forums</a>
                    </div>
                ';
            }else{
                $topicHtml .= '<div>No topics found. </div>';
            }
        }

        return $topicHtml;
    }

}
