<?php

use Phalcon\Mvc\Controller,
    Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email as EmailValidator,
    Phalcon\Validation\Validator\Regex,
    Phalcon\Validation\Validator\Confirmation,
    Phalcon\Validation\Validator\StringLength as StringLength;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


  include __DIR__ . '/../../app/library/phpmailer/src/Exception.php';
  include __DIR__ . '/../../app/library/phpmailer/src/PHPMailer.php';
  include __DIR__ . '/../../app/library/phpmailer/src/SMTP.php';

class ControllerBase extends Controller
{
    public $volUserId = null;
    protected function initialize()
    {
        Phalcon\Tag::setTitle('ABK CMS');
        $progs = Tblprograms::find();
        foreach ($progs as $p => $v) {
            $this->view->setVar("prog_menu".$v->programID,
                array(
                    'id' => $v->programID,
                    'title' => $v->programName,
                    'tagline' => $v->programTagline,
                    'url' => $v->programPage,
                    'banner' => $v->programBanner
                    ));
        }

        $this->view->announcements = $this->_getSideBarAnnouncements();
        $this->view->moreInfoLinks = $this->_getPagesLinks();
        $this->view->programLinks = $this->_getProgramsLinks();
        $this->view->specialPagesLinks = $this->_getPagesLinks(1);
        $this->_getPagesLinks(1, true);

        if ($this->session->has("vol_auth")) {
            $abk_vol = $this->session->get('vol_auth');
            //Retrieve its value
            $this->volUserId = $abk_vol['abk_vol_id'];
            $this->view->abk_vol_fullname = $abk_vol['abk_vol_fname'];
            $this->view->abk_vol_username = $abk_vol['abk_vol_username'];
        }

        if ($this->request->isAjax() && $this->request->getPost('tellfriendFormActive')){
            $this->tellAFriend();
        }
    }

    public function _getSideBarAnnouncements(){
        $phql = 'SELECT * FROM Tblannouncements WHERE  UNIX_TIMESTAMP() between annStart AND annEnd ORDER BY annID DESC LIMIT 5 ';
        $result = $this->modelsManager->executeQuery($phql);

        $dataArray = array();
        foreach ($result as $key => $value) {
            $dataArray[] = array(
                'annID'=>$value->annID,
                'annTitle'=>$value->annTitle,
                'annDesc'=>$this->_truncateHtml($value->annDesc),
                'annDate'=>$value->annStart,
                'annStatus'=>$value->annStatus
                );
        }

        return $dataArray;
    }

    public function _getPagesLinks($special = 0, $sideBar = false){
        $pages =  Tblpages::find('pageType="pages" AND pageActive="1" ORDER BY pageOrder ASC');
        $pagesLinks = null;

        if($sideBar){
            foreach ($pages as $key => $value) {
                $this->view->setVar("specialPage".$value->pageID,
                array(
                    'pageTitle'=>$value->pageTitle,
                    'pageUrl'=>$this->url->get().'site/pages/'.$value->pageSlug
                    )
                );
            }
        }else{
            foreach ($pages as $key => $value) {
                $pagesLinks .= '<li><a href="/site/pages/'.$value->pageSlug.'">'.$value->pageTitle.'</a></li>';
            }
        }
        return $pagesLinks;
    }

    public function _getProgramsLinks(){
        $programs =  Tblprograms::find();
        $programLinks = null;
        foreach ($programs as $key => $value) {
            $programLinks .= '<li><a href="/programs/page/'.$value->programPage.'">'.$value->programName.'</a></li>';
        }
        return $programLinks;
    }
    public function _sendmail($objects, $html = true){
          include __DIR__ . '/../../app/library/phpmailer/class.phpmailer.php';
          include __DIR__ . '/../../app/library/phpmailer/class.smtp.php';
          include __DIR__ . '/../../app/library/phpmailer/class.pop3.php';


          $mail = new PHPMailer;
          $mail->SMTPDebug = 0;
          $mail->IsSMTP();                                      // Set mailer to use SMTP
          $mail->Host = 'smtp.gmail.com';                       // Specify main and backup server
          $mail->Port = 587;                                    // Set the SMTP port
          $mail->SMTPAuth = true;                               // Enable SMTP authentication
          $mail->Username = "angbayanko.gotitgenius@gmail.com";            // SMTP username
          $mail->Password = 'abk12345678';                    // SMTP password
          $mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
          $mail->IsHTML($html);
          $mail->ClearReplyTos();
          $mail->AddReplyTo('ramonluzano@gmail.com', 'Ang Bayan ko');
          //add email address manually
          // $mail->AddAddress("momo.xd201311623@gmail.com");
          // $mail->setFrom('from@example.com', 'Mailer');
          // $mail->Body    = 'This is the HTML message body <b>in bold!</b>';

          foreach ($objects as $key => $value) {
             if($key == 'AddAddress'){
                 $tos = explode(',', $value);
                 foreach ($tos as $add) {
                     $mail->AddAddress(trim($add));
                 }
             }else{
                 $mail->$key = trim($value);
             }
         }
         $mail->Send();
         $mail->ClearAllRecipients();
         return $mail->Send();


      }



    private function tellAFriend(){
        $this->view->disable();
        $response = array();
        $validation = new Phalcon\Validation();
        $validation
            ->add('email', new PresenceOf(array(
                'message' => 'The email is required',
            )))
            ->add('message', new PresenceOf(array(
                'message' => 'The message is required',
                'cancelOnFail' => true
            )))
            ->add('email', new StringLength(array(
                  'max' => 255,
                  'min' => 1,
                  'messageMaximum' => 'The email is too long',
                  'messageMinimum' => 'The email must be atleast 1 character long'
            )))
            ->add('message', new StringLength(array(
                  'max' => 200,
                  'min' => 1,
                  'messageMaximum' => 'The message is too long',
                  'messageMinimum' => 'The message must be atleast 1 character long',
                  'cancelOnFail' => true
            )))
            ->add('email', new EmailValidator(array(
               'message' => 'The e-mail is not valid'
            )))
            ;

        $validation->setFilters('email', 'trim');
        $validation->setFilters('message', 'trim');

        $messages = $validation->validate($_POST);
        $errMessage = null;
        if (count($messages)) {
            foreach ($messages as $message) {
                $errMessage .= '<li>'.$message. '</li>';
            }
            $response['success'] = false;
            $response['message'] = '
                <div class="alert alert-danger alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong>Please fix the following:</strong>'.$errMessage.'
                </div>
            ';
        }else{
            $body = $this->request->getPost('message', 'striptags')."<br /><br /><a href='http://angbayanko.org/'>http://angbayanko.org/</a><br ><br />This is a auto generated email, please do not reply";

            $mailObjects = array(
            'From'=> 'angbayanko.org@no-reply.com',
            'FromName' => 'angbayanko.org',
            'AddAddress'=> $this->request->getPost('email', 'trim'),
            'Subject' => 'ANG BAYAN KO Website',
            'Body' =>  $body
            );

          // $this->sendnewsletter($mailObjects);
          $this->_sendmail($mailObjects);
            $response['success'] = true;
            $response['message'] = '



            <div class="modal-content border-flat">
              <form method="post" id="tellafriendModalForm">
                <input type="hidden" name="tellfriendFormActive" value="1">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title" id="townAlbumModalTitle">Tell A Friend</h4>

                </div>

                <div class="alert alert-success alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        An email has been sent to <strong>'.$this->request->getPost('email', 'trim').'</strong>. Thank you for telling your friend about "ANG BAYAN KO".
                </div>

                <div id="tellafriendModalBody" class="modal-body">
                  <div id="tellFriendErrorMessage"></div>
                  <label>Email</label>
                  <input type="text" name="email" class="form-control border-flat" placeholder="Your friends email">
                  <br />
                  <label>Your Message</label>
                  <textarea name="message" class="form-control border-flat limitChar" maxlength="200" placeholder="Your message"></textarea>
                  <div class="maxlength"></div>
                  <br />
                  <strong>http://angbayanko.org/</strong><br />
                  <small>ABK URL will be attached along with your message</small>
                </div>
                <div class="modal-footer" id="modal-footer">
                  <button type="button" class="btn btn-default border-flat" data-dismiss="modal" aria-hidden="true">Cancel</button>
                  <button type="submit" id="tellFriendModalBtn" class="border-flat btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            ';
        }
        echo json_encode($response);
    }

    public function sendnewsletter($subject, $message){
      $phql = 'SELECT email FROM Tblnewsletteremails';
    $suggested = $this->modelsManager->executeQuery($phql);
    if(count($suggested) > 0){
        $dataArray = array();
        foreach ($suggested as $key => $value) {
            $dataArray[] = $value->email;
        }

        $mailObjects = array(
        'From'=> 'angbayanko.org@no-reply.com',
        'FromName' => 'angbayanko.org',
        'AddAddress'=> implode(',', $dataArray),
        'Subject' => $subject,
        'Body' => $message
        );

        return $this->_sendmail($mailObjects);
    }else{
        return true;
    }

       }

    public function sendmailer($email,$subject, $message){
         if(count($suggested) > 0){
             $dataArray = array();
             foreach ($suggested as $key => $value) {
                 $dataArray[] = $value->email;
             }

             return $this->Mailer->sendMail($email,$subject,$message);
         }else{
             return true;
         }
     }

    public function validateLoginVolunteer(){
        if ($this->request->isAjax() == true) {
            $this->view->disable();
            if($this->request->getPost('loginFormActive')){

                $response = array();
                $response['success'] = false;

                $username = $this->request->getPost('username');
                $password = $this->request->getPost('password');

                $user = Tblvolunteers::findFirst("username='$username' AND status=1");
                if($user){
                    if(sha1($password) == $user->password){
                        $this->session->set('vol_auth', array(
                            'abk_vol_id' => $user->volunteerID,
                            'abk_vol_fname' => $user->fname,
                            'abk_vol_username' => $user->username
                        ));
                        $response['success'] = true;
                        $response['message'] = '<div class="alert alert-success"><i class="icon-ok"></i> You have successfully logged in. Welcome back <strong>'.$user->fname.'</strong></div>';
                    }
                }

                echo json_encode($response);
            }
        }
    }


    public function _truncateHtml($text, $length = 200, $ending = '...', $exact = true, $considerHtml = false) {
        if ($considerHtml) {
        // if the plain text is shorter than the maximum length, return the whole text
            if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
                return $text;
            }
        // splits all html-tags to scanable lines
            preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
            $total_length = strlen($ending);
            $open_tags = array();
            $truncate = '';
            foreach ($lines as $line_matchings) {
            // if there is any html-tag in this line, handle it and add it (uncounted) to the output
                if (!empty($line_matchings[1])) {
                // if it's an "empty element" with or without xhtml-conform closing slash
                    if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                    // do nothing
                // if tag is a closing tag
                    } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                    // delete tag from $open_tags list
                        $pos = array_search($tag_matchings[1], $open_tags);
                        if ($pos !== false) {
                            unset($open_tags[$pos]);
                        }
                // if tag is an opening tag
                    } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                    // add tag to the beginning of $open_tags list
                        array_unshift($open_tags, strtolower($tag_matchings[1]));
                    }
                // add html-tag to $truncate'd text
                    $truncate .= $line_matchings[1];
                }
            // calculate the length of the plain text part of the line; handle entities as one character
                $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
                if ($total_length+$content_length> $length) {
                // the number of characters which are left
                    $left = $length - $total_length;
                    $entities_length = 0;
                // search for html entities
                    if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                    // calculate the real length of all entities in the legal range
                        foreach ($entities[0] as $entity) {
                            if ($entity[1]+1-$entities_length <= $left) {
                                $left--;
                                $entities_length += strlen($entity[0]);
                            } else {
                            // no more characters left
                                break;
                            }
                        }
                    }
                    $truncate .= substr($line_matchings[2], 0, $left+$entities_length);
                // maximum lenght is reached, so get off the loop
                    break;
                } else {
                    $truncate .= $line_matchings[2];
                    $total_length += $content_length;
                }
            // if the maximum length is reached, get off the loop
                if($total_length>= $length) {
                    break;
                }
            }
        } else {
            $text = strip_tags($text);
            if (strlen($text) <= $length) {
                return $text;
            } else {
                $truncate = substr($text, 0, $length - strlen($ending));
            }
        }
    // if the words shouldn't be cut in the middle...
        if (!$exact) {
        // ...search the last occurance of a space...
            $spacepos = strrpos($truncate, ' ');
            if (isset($spacepos)) {
            // ...and cut the text in this position
                $truncate = substr($truncate, 0, $spacepos);
            }
        }
    // add the defined ending to the text
        $truncate .= $ending;
        if($considerHtml) {
        // close all unclosed html-tags
            foreach ($open_tags as $tag) {
                $truncate .= '</' . $tag . '>';
            }
        }
        return $truncate;
    }
}
