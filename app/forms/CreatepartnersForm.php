<?php

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Hidden,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Submit,
    Phalcon\Forms\Element\Check,
    Phalcon\Forms\Element\Numeric,
    Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\Email,
    Phalcon\Validation\Validator\Identical,
    Phalcon\Validation\Validator\StringLength,
    Phalcon\Validation\Validator\Regex,
    Phalcon\Validation\Validator\Alnum,
    Phalcon\Validation\Validator\Confirmation;

class CreatepartnersForm extends Form
{
    public function initialize($entity=null, $options = null)
    {

        if(isset($options['partner']) && $options['partner']){

            //user form
            // In edition the id is hidden
            if (isset($options['edit']) && $options['edit']) {
                $husername = new Hidden('huserName');
                $this->add($husername);
                $hemail = new Hidden('huserEmail');
                $this->add($hemail);
            }

            //User Account
            $name = new Text('username', array('class' => 'form-control', 'placeholder' => 'Username'));
            $name->setLabel('Username');
            $name->addFilter('trim');
            $name->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The username is required'
                    )),
                new StringLength(array(
                  'min' => 6,
                  'messageMinimum' => 'Username should have at least 6 minimum characters'
                  )),
                new Regex(array(
                    'message'    => 'Username is invalid. Avoid spaces and symbols.',
                    'pattern'    => '/^[a-zA-Z0-9_.]+/',
                    'allowEmpty' => false
                    ))
                ));
            $this->add($name);

            //Email
            $email = new Text('email', array('class' => 'form-control', 'placeholder' => 'Email Address'));
            $email->setLabel('Email Address');
            $email->addFilter('trim');
            $email->addValidators(array(
                new PresenceOf(array(
                    'message' => 'The Email is required'
                    )),/*
                new Email(array(
                    'message' => 'The Email is required'
                    )),*/
                new Regex(array(
                    'message'    => 'Not a valid email format',
                    'pattern'    => '/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i',
                    'allowEmpty' => false
                    ))
                ));
            $this->add($email);


            if (empty($options['edit'])) {

                 //Password
                $password = new Password('password', array('class' => 'form-control' , 'placeholder' => 'Password'));
                $password->setLabel('Password');
                $password->addFilter('trim');
                $password->addValidators(array(
                    new PresenceOf(array(
                        'message' => 'The password is required'
                        )),
                    new StringLength(array(
                      'max' => 20,
                      'min' => 8,
                      'messageMaximum' => 'Thats an exagerated password.',
                      'messageMinimum' => 'Password should be Minimum of 8 characters.'
                      )),
                    new Alnum(array(
                        'message' => 'Password should only have alphanumeric characters.'
                    )),

                    ));
                $this->add($password);

                //Re Password
                $repassword = new Password('repassword', array('class' => 'form-control' , 'placeholder' => 'Re-Type Password'));
                $repassword->setLabel('Re-Type Password');
                $repassword->addValidators(array(
                    new PresenceOf(array(
                        'message' => 'Retyping your password is required'
                        )),
                    new Confirmation(array(
                     'message' => 'Password doesn\'t match confirmation',
                     'with' => 'password'
                     ))
                    ));
                $this->add($repassword);
            }else{
                //Password
                /*$oldpassword = new Password('oldpassword', array('class' => 'form-control' , 'placeholder' => 'Old Password'));
                $oldpassword->setLabel('Old Password');
                $this->add($oldpassword); */

                //Password
                $password = new Password('password', array('class' => 'form-control' , 'placeholder' => 'Password'));
                $password->setLabel('New Password');
                $this->add($password);

                //Re Password
                $repassword = new Password('repassword', array('class' => 'form-control' , 'placeholder' => 'Re-Type Password'));
                $repassword->setLabel('Re-Type Password');
                $this->add($repassword);
            }

            //User Profile
            //firstname
            $firstname = new Text('firstname', array('class' => 'form-control' , 'placeholder' => 'First Name'));
            $firstname->setLabel('First Name');
            $firstname->addFilter('trim');
            $firstname->addValidators(array(
                new PresenceOf(array(
                    'message' => 'First Name is required'
                    )),
                new StringLength(array(
                  'min' => 2,
                  'messageMinimum' => 'First Name should have at least 2 minimum characters'
                  ))
                ));
            $this->add($firstname);
            //lastname
            $lastname = new Text('lastname', array('class' => 'form-control' , 'placeholder' => 'Last Name'));
            $lastname->setLabel('Last Name');
            $lastname->addFilter('trim');
            $lastname->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Last Name is required'
                    )),
                new StringLength(array(
                  'min' => 2,
                  'messageMinimum' => 'Last Name should have at least 2 minimum characters'
                  ))
                ));
            $this->add($lastname);

            //middlename
            $middlename = new Text('middlename', array('class' => 'form-control' , 'placeholder' => 'Middle Name'));
            $middlename->setLabel('Middle Name');
            $this->add($middlename);

            //Contact

            $contact = new Text('contact', array('class' => 'form-control' , 'placeholder' => '09xxxxxxxxx',
'onkeypress'=>'return isNumber(event)','maxlength'=> 11, 'id'=>'conreq'));
            $contact->setLabel('Contact no:');
            $contact->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Contact number is required'
                    ))
                ));
            $this->add($contact);

            //Address
            $address = new Text('address', array('class' => 'form-control' , 'placeholder' => 'Address'));
            $address->setLabel('Address');
            $this->add($address);

            //Company
            $company = new Text('company', array('class' => 'form-control' , 'placeholder' => 'Company'));
            $company->setLabel('Company');
            $this->add($company);

            //position
            $position = new Text('position', array('class' => 'form-control' , 'placeholder' => 'Position'));
            $position->setLabel('Position');
            $this->add($position);


            //Partner Profile
            //name
            $partnerName = new Text('partnerName', array('class' => 'form-control' , 'placeholder' => 'Partner Name'));
            $partnerName->setLabel('Partner Name');
            $partnerName->addFilter('trim');
            $partnerName->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Partner name is required'
                )),
                new StringLength(array(
                    'min' => 2,
                    'messageMinimum' => 'Partner name should have at least 2 minimum characters'
                ))
            ));
            $this->add($partnerName);

            //partners information / description
            $partnerDes = new TextArea('partnerInfo', array('class' => 'richText form-control'));
            $partnerDes->setLabel('Partner Details');
           $partnerDes->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Partner Details is required'
                ))
            ));
            $this->add($partnerDes);


        }else if(isset($options['createPartnerEvent']) && $options['createPartnerEvent']){
            /*
             * Create events forms
             */
            //event name
            $eventTitle = new Text('event_name', array('class'=>'form-control', 'placeholder' => 'Enter event Title'));
            $eventTitle->setLabel('Event Name');
            $eventTitle->addFilter('trim');
            $eventTitle->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Event name is required'
                ))
            ));
            $this->add($eventTitle);

            $eventDate = new Text('event_date', array('readonly'=>'readonly', 'class' => 'form-control form-control dtpicker', 'data-format'=>'yyyy-MM-dd'));
            $eventDate->setLabel('Event Date');
            $eventDate->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Event date is required'
                ))
            ));
            $this->add($eventDate);

            //event venue
            $eventVenue = new Text('event_venue', array('class'=>'form-control', 'placeholder' => 'Enter event venue'));
            $eventVenue->setLabel('Event Venue');
            $eventVenue->addFilter('trim');
            $eventVenue->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Event venue is required'
                ))
            ));
            $this->add($eventVenue);

            //event details
            //Content
            $eventDetails = new TextArea('event_details', array('class' => 'richText form-control'));
            $eventDetails->setLabel('Event Details');
            $eventDetails->addFilter('trim');
            $eventDetails->addValidators(array(
                new PresenceOf(array(
                    'message' => 'Content is required'
                ))
            ));
            $this->add($eventDetails);
        }

        //CSRF
        $csrf = new hidden('csrf');

        $csrf->addValidator(new Identical(array(
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        )));
                          // $csrf->addValidator(new Identical(array(
                          //       $this->security->checkToken() => 1,
                          //       'message' => 'CSRF-token validation failed'
                          //   )));

        $this->add($csrf);
    }
    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }
}
