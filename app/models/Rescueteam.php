<?php

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\StringLength as StringLength;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

class Rescueteam extends Model
{
	  	public $email;

 public function indexAction()
  {

		$validator = new Validation();

        $validator->add(
            'txtemail',
            new EmailValidator()
        );

          $validator->add('txtemail', new UniquenessValidator([
           'message' => ':field must be unique'
       ]));
          
    return $this->validate($validator);
  }
}