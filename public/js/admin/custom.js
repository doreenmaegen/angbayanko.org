/* JS */


/* Navigation */

$(document).ready(function(){

  $(window).resize(function()
  {
    if($(window).width() >= 765){
      $(".sidebar #nav").slideDown(350);
    }
    else{
      $(".sidebar #nav").slideUp(350);
    }
  });


  $("#nav > li > a").on('click',function(e){
    if($(this).parent().hasClass("has_sub")) {
      e.preventDefault();
    }

    if(!$(this).hasClass("subdrop")) {
        // hide any open menus and remove all other classes
        $("#nav li ul").slideUp(350);
        $("#nav li a").removeClass("subdrop");

        // open our new menu and add the open class
        $(this).next("ul").slideDown(350);
        $(this).addClass("subdrop");
      }

      else if($(this).hasClass("subdrop")) {
        $(this).removeClass("subdrop");
        $(this).next("ul").slideUp(350);
      }

    });
});

$(document).ready(function(){
  $(".sidebar-dropdown a").on('click',function(e){
    e.preventDefault();

    if(!$(this).hasClass("open")) {
        // hide any open menus and remove all other classes
        $(".sidebar #nav").slideUp(350);
        $(".sidebar-dropdown a").removeClass("open");

        // open our new menu and add the open class
        $(".sidebar #nav").slideDown(350);
        $(this).addClass("open");
      }

      else if($(this).hasClass("open")) {
        $(this).removeClass("open");
        $(".sidebar #nav").slideUp(350);
      }
    });

});

/* Widget close */

$('.wclose').click(function(e){
  e.preventDefault();
  var $wbox = $(this).parent().parent().parent();
  $wbox.hide(100);
});

/* Widget minimize */

$('.wminimize').click(function(e){
  e.preventDefault();
  var $wcontent = $(this).parent().parent().next('.widget-content');
  if($wcontent.is(':visible'))
  {
    $(this).children('i').removeClass('icon-chevron-up');
    $(this).children('i').addClass('icon-chevron-down');
  }
  else
  {
    $(this).children('i').removeClass('icon-chevron-down');
    $(this).children('i').addClass('icon-chevron-up');
  }
  $wcontent.toggle(500);
});

/* Calendar */

$(document).ready(function() {

  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  $('#calendar').fullCalendar({
    header: {
      left: 'prev',
      center: 'title',
      right: 'month,agendaWeek,agendaDay,next'
    },
    editable: true,
    events: [
    {
      title: 'All Day Event',
      start: new Date(y, m, 1)
    },
    {
      title: 'Long Event',
      start: new Date(y, m, d-5),
      end: new Date(y, m, d-2)
    },
    {
      id: 999,
      title: 'Repeating Event',
      start: new Date(y, m, d-3, 16, 0),
      allDay: false
    },
    {
      id: 999,
      title: 'Repeating Event',
      start: new Date(y, m, d+4, 16, 0),
      allDay: false
    },
    {
      title: 'Meeting',
      start: new Date(y, m, d, 10, 30),
      allDay: false
    },
    {
      title: 'Lunch',
      start: new Date(y, m, d, 12, 0),
      end: new Date(y, m, d, 14, 0),
      allDay: false
    },
    {
      title: 'Birthday Party',
      start: new Date(y, m, d+1, 19, 0),
      end: new Date(y, m, d+1, 22, 30),
      allDay: false
    },
    {
      title: 'Click for Google',
      start: new Date(y, m, 28),
      end: new Date(y, m, 29),
      url: 'http://google.com/'
    }
    ]
  });

});

/* Progressbar animation */

setTimeout(function(){

  $('.progress-animated .progress-bar').each(function() {
    var me = $(this);
    var perc = me.attr("data-percentage");

            //TODO: left and right text handling

            var current_perc = 0;

            var progress = setInterval(function() {
              if (current_perc>=perc) {
                clearInterval(progress);
              } else {
                current_perc +=1;
                me.css('width', (current_perc)+'%');
              }

              me.text((current_perc)+'%');

            }, 600);

          });

},600);

/* Slider */

$(function() {
        // Horizontal slider
        $( "#master1, #master2" ).slider({
          value: 60,
          orientation: "horizontal",
          range: "min",
          animate: true
        });

        $( "#master4, #master3" ).slider({
          value: 80,
          orientation: "horizontal",
          range: "min",
          animate: true
        });

        $("#master5, #master6").slider({
          range: true,
          min: 0,
          max: 400,
          values: [ 75, 200 ],
          slide: function( event, ui ) {
            $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
          }
        });


        // Vertical slider
        $( "#eq > span" ).each(function() {
            // read initial values from markup and remove that
            var value = parseInt( $( this ).text(), 10 );
            $( this ).empty().slider({
              value: value,
              range: "min",
              animate: true,
              orientation: "vertical"
            });
          });
      });



/* Support */

$(document).ready(function(){
  $("#slist a").click(function(e){
   e.preventDefault();
   $(this).next('p').toggle(200);
 });
});

/* Scroll to Top */

$(".totop").hide();
if ($('.totop').length) {

    var scrollTrigger = 100, // px
        backToTop = function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                $('.totop').addClass('show');
            } else {
                $('.totop').removeClass('show');
            }
        };
    backToTop();
    $(window).on('scroll', function () {
        backToTop();
    });
    $('.totop').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 0);
    });
}

$(document).ready(function() {

  $('.noty-alert').click(function (e) {
    e.preventDefault();
    noty({text: 'Some notifications goes here...',layout:'topRight',type:'alert',timeout:2000});
  });

  $('.noty-success').click(function (e) {
    e.preventDefault();
    noty({text: 'Some notifications goes here...',layout:'top',type:'success',timeout:2000});
  });

  $('.noty-error').click(function (e) {
    e.preventDefault();
    noty({text: 'Some notifications goes here...',layout:'topRight',type:'error',timeout:2000});
  });

  $('.noty-warning').click(function (e) {
    e.preventDefault();
    noty({text: 'Some notifications goes here...',layout:'bottom',type:'warning',timeout:2000});
  });

  $('.noty-information').click(function (e) {
    e.preventDefault();
    noty({text: 'Some notifications goes here...',layout:'topRight',type:'information',timeout:2000});
  });

});


/* Date picker */


$(function() {
  var date = new Date();
  $('.start').datetimepicker({
    pickTime: false,
    startDate: date
  }).on('changeDate',function (e){
    $('.start').datetimepicker('hide');
  });
});

$(function() {
  var date = new Date();
  $('.end').datetimepicker({
    pickTime: false,
    startDate: date
  }).on('changeDate',function (e){
    $('.end').datetimepicker('hide');
  });
});

$(function() {
  $('#datetimepicker2').datetimepicker({
    pickDate: false,
    pickSeconds: false,
    pick12HourFormat: true
  });
});

$(function() {
  $('.datetimepicker2').datetimepicker({
    pickDate: false,
    pickSeconds: false,
    pick12HourFormat: true,
    keepOpen:false
  });
});

/*
* JP - Date picker
* */
  // $('.datePicker').click(function(){
  //     $('.datePicker').hide();
  // });
$(function () {
  $('.datePicker').datetimepicker({
    pickTime: false,
    autoclose: true,
    startDate: new Date()
  }).on('changeDate',function (e){
    $('.datePicker').datetimepicker('hide');
  });


  $('#dateStart').datetimepicker({
    pickTime: false,
    startDate: new Date()
  });
  $('#dateEnd').datetimepicker({
    pickTime: false,
    startDate: new Date()
  });
});

var richtextarea = $( '.richText' ).ckeditor();




/* Modal fix */

$('.modal').appendTo($('body'));

/* Pretty Photo for Gallery*/

jQuery("a[class^='prettyPhoto']").prettyPhoto({
  overlay_gallery: false, social_tools: false
});


/* File Uploader */
$(function () {
  'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
    '//jquery-file-upload.appspot.com/' : BASE_URL + 'server/php/';
    $('#fileupload').fileupload({
      url: url,
      dataType: 'json',
      disableImageResize: /Android(?!.*Chrome)|Opera/
      .test(window.navigator.userAgent),
      previewMaxWidth: 200,
      previewMaxHeight: 200,
      previewCrop: true,
      limitMultiFileUploads: 4,
      done: function (e, data) {
        $.each(data.result.files, function (index, file) {
          $('#files tbody').append("<tr><td><img id='theImg' src='"+BASE_URL+"server/php/files/thumbnail/"+file.name+"'  /> <br/> "+file.name+" </td><td> <input type='hidden' name='program_picture[]' value='" + file.name + "' id='imgupload'  > <a class='btn btn-xs btn-danger delete_program_picture modal-control-button' data-recorid=''><i class='icon-remove'></i> </a> </td></tr>");
          $('#hidepic').hide();
        });
        // $.each(data.result.files, function (index, file) {
        //   $('#files tbody').append("<tr><td><img id='theImg' src='"+BASE_URL+"server/php/files/thumbnail/"+file.name+"'  /> <a class='btn btn-xs btn-danger delete_program_picture modal-control-button' data-recorid=''><i class='icon-remove'></i> </a> <br/>  </td><td> <input type='hidden' name='program_picture[]' value='" + file.name + "' id='imgupload'  > "+file.name+"</td></tr>");
        //   $('#hidepic').hide();
        // });
      },
      progressall: function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
          'width',
          progress + '%'
          );
      }
    }).prop('disabled', !$.support.fileInput)
    .parent().addClass($.support.fileInput ? undefined : 'disabled');
  });
/* File Uploader */
$(function () {
  'use strict';
  var newArray = new Array();
  $('#digitalAssets').fileupload({
    url: BASE_URL + 'server/php/',
    dataType: 'json',
    disableImageResize: /Android(?!.*Chrome)|Opera/
    .test(window.navigator.userAgent),
    previewMaxWidth: 100,
    previewMaxHeight: 100,
    previewCrop: true,
    limitMultiFileUploads: 4,
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        if(CURRENT_FOLDER_CAT == 'program'){
          console.log();

          $.get( BASE_URL + 'admin/ajaxmoveuploadprogram/'+ file.name+'/'+CURRENT_PROGRAM_FOLDER  ).done(function( data ) {
            $('.digital-assets-gallery').prepend(data);
            jQuery("a[class='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            console.log(data);
          });
        }else if(CURRENT_FOLDER_CAT == 'newsletter'){
          $.get( BASE_URL + 'admin/ajaxmoveuploadnewsletter/'+ file.name  ).done(function( data ) {
            $('.digital-assets-gallery').html(data + "<div class='clearfix'></div>");
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            console.log(data);
          });
        }else if(CURRENT_FOLDER_CAT == 'post'){
          $.get( BASE_URL + 'admin/ajaxmoveuploadpost/'+ file.name  ).done(function( data ) {
            $('.digital-assets-gallery').html(data + "<div class='clearfix'></div>");
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            console.log(data);
          });
        }
      });
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css(
        'width',
        progress + '%'
        );
    }
  }).prop('disabled', !$.support.fileInput)
.parent().addClass($.support.fileInput ? undefined : 'disabled');
});
/* Table Functions*/
$(document).ready(function(){
  $('.tbl_select_all').on('click',function(){
    var all = $(this);
    $('.table input:checkbox').each(function() {
     $(this).prop("checked", all.prop("checked"));
   });
    if($('.tbl_select_all').is(':checked')){
      $('.tblbottomcontrol').slideDown('fast');
      $("html, body").animate({ scrollTop: $(document).height() }, 1000);
    }else{
      $('.tblbottomcontrol').slideUp('fast');
    }
  });
  $('.tbl_select_row').on('click', function(){
    if($('.table input[type=checkbox]:checked').length){
      $('.tblbottomcontrol').slideDown('fast');
    }else{
      $('.tblbottomcontrol').slideUp('fast');
    }
  });
  $('.tbl_unselect_all').on('click', function(e){
    e.preventDefault();
    $('.table input[type=checkbox]').removeAttr('checked');
    $('.tblbottomcontrol').slideUp('fast');
  });
  $('.tbl_delete_all').on('click', function(){
    var tekis="";
    $('.table input:checkbox').each(function() {
     var curr = $(this);
     if(curr.is(':checked')){
       tekis += ' ,' + curr.closest('td').siblings('.name').text();
     }

   });
    tekis = tekis.substr(2);

    $modalnames = tekis;
    $modalaction = $(this).data('action');
    $modaltitle = setModalTitle($modalaction);
    $modalmessage = setModalMessage($modalaction);
    $modalid="tbl_id";
    setModal($modaltitle, $modalaction, $modalid, $modalmessage, $modalnames);
  });
  $('.modal-control-button').on('click', function(){
    tekis = ',' + $(this).closest('td').siblings('.name').text();
    $modalname = tekis.substr(1);
    $modalaction = $(this).data('action');
    $modaltitle = setModalTitle($modalaction);
    $modalmessage = setModalMessage($modalaction);
    $modalid=$(this).data('recorid');
    console.log($modalaction);
    console.log($modalid);
    setModal($modaltitle, $modalaction, $modalid, $modalmessage, $modalname);
  });
  $('.modal-btn-yes').on('click', function(){
    if($('.tbl-action').val()=='edit'){
      window.location.href = $('.tbl-edit-url').val() + $('.tbl-recordID').val();
    } else if($('.tbl-action').val()=='edit-slide'){
      console.log(window.location.origin + "/admin/settings/" + $('.tbl-recordID').val());
      window.location.href = window.location.origin + "/admin/settings/" + $('.tbl-recordID').val();
    } else if($('.tbl-action').val()=='manage'){
      window.location.href = window.location.origin + "/admin/programpages/" + $('.tbl-recordID').val();
    } else {
      $('#main-table-form').submit();
    }
  });
  $('.modal-record-view').on('click', function(){
    href = $(this).data('href');
    $.ajax({
      url: href,
    }).done(function( data ) {
      console.log(data);
      $('.modalView .modal-body').html(data);
    });
  });
});

/* Program JS*/
$(document).ready(function(){
  $(document).on('click', '.delete_program_picture',function(){
    $(this).closest('tr').remove();
  });
  $('#program-title').keyup(function(){
    $('.program-url').val(convertToSlug($(this).val()));
  });
  $('.program_page_title').keyup(function(){
    $(this).siblings('.program_page_url').text(convertToSlug($(this).val()));
  });
  $( '.programPageText' ).ckeditor({
    filebrowserUploadUrl : '/uploader/upload.php',
    filebrowserImageUploadUrl : '/uploader/upload.php?type=Images'
  });
  $('.program_page_title').keyup(function(){
   $(this).siblings('.program_page_url').text(convertToSlug($(this).val()));
   $(this).siblings('#program_page_slug').val(convertToSlug($(this).val()));
 });
  $( '.programPageText' ).ckeditor();
  $('#program_page_title').keyup(function(){
    $(this).siblings('#program_page_slug').val(convertToSlug($(this).val()));
  });
  $(document).on('click', '.digital-assets-delete',function(){
    element = $(this);
    deleteElem(element, 'program');
  });
  $('.delete-program-page').on('click', function(){
    element = $(this);
    $('.page_program_delete_id').val(element.data('pageid'));
  });
  $('.view-page-sample').on('click', function(){
    element = $(this);
    if ( element.is("[data-page-content-id]") ) {
      var programEditorValue = CKEDITOR.instances[element.data('page-content-id')].getData();
    }else {
      var programEditorValue = CKEDITOR.instances['programPageText' + element.data('pageid')].getData();
    }

    $('#modalView .modal-body').html('<style>p {padding: 0px 0px 10px 0px !important; margin: 0px;}</style>'+programEditorValue + '<div class="clearfix"></div>');
  });

});
/* News Post*/
$(document).ready(function(){
  $('#post_title').keyup(function(){
    element = $(this);
    $(this).siblings('.post-slug-text').text(convertToSlug($(this).val()));
    $(this).siblings('#post_slug').val(convertToSlug($(this).val()));
  });
  $('.post-add-media').on('click',function(){
    getDigitalAssets();
  });
  $('.post-year-tab').on('click',function(){
    $('#digital-assets-year').val($(this).text());
  });
  $('.post-assets-month').change(function(){
    $('#digital-assets-month').val($(this).val());
    getDigitalAssets();
  });
  $( '.programPageText' ).ckeditor({
    filebrowserUploadUrl : '/uploader/upload.php',
    filebrowserImageUploadUrl : '/uploader/upload.php?type=Images'});
  $(document).on('click', '.digital-assets-post-delete',function(){
    element = $(this);
    deleteElem(element, 'post');
  });
  $( '.newsPostContent' ).ckeditor();
});

function deleteElem(element, type){
  if(type=='program'){
    $.get( BASE_URL + 'admin/ajaxdeleteupload/'+ element.data('filename') +'/'+element.data('folder')  ).done(function( data ) {
      element.closest('.program-digital-assets-library').remove();
    });
  }else if(type='post'){
    $.get( BASE_URL + 'admin/ajaxdeleteupload/'+ element.data('filename') +'/'+element.data('folder')+'/post'  ).done(function( data ) {
      element.closest('.program-digital-assets-library').remove();
    });
  }
}
function getDigitalAssets(){
  $.get( BASE_URL + 'admin/ajaxviewdigitalassets/'+ $('#digital-assets-year').val() +'/'+$('#digital-assets-month').val() ).done(function( data ) {
    $('.digital-assets-monthly').html(data + "<div class='clearfix'></div>");
    jQuery("a[class^='prettyPhoto']").prettyPhoto({
      overlay_gallery: false, social_tools: false
    });
  });

}
function convertToSlug(Text)
{
  return Text
  .toLowerCase()
  .replace(/ /g,'-')
  .replace(/[^\w-]+/g,'')
  ;
}
function setModal(title, action, id, message,names){
  $('#modalPrompt .modal-title').text(title);
  $('#modalPromptcont .modal-title').text(title);
  $('.tbl-action').val(action);
  $('.tbl-recordID').val(id);
  $('#modalPrompt .modal-message').text(message);
  $('#modalPrompt .modal-list-names').text(names);
  $('#modalPromptview .modal-list-names').text(names);
  $('#modalPromptview .modal-list-message').text(message);
  $('#changewarning').text(title);
}
function setModalMessage(action){
  if(action=="delete"){

   var element = document.getElementById("headerColor");
   element.classList.add("red");
   element.classList.remove("blue");
   element.classList.remove("yellow");
   element.classList.remove("modal-header-color");
   return "Are you sure you want to delete record?";

  }else if(action=="delete_selected"){

    var element = document.getElementById("headerColor");
    element.classList.add("red");
    element.classList.remove("blue");
    element.classList.remove("yellow");
    element.classList.remove("green");
    return "Are you sure you want to delete records?";

  }else if(action=="edit"){3

     var element = document.getElementById("headerColor");
     element.classList.add("yellow");
     element.classList.remove("red");
     element.classList.remove("blue");
     element.classList.remove("green");
     return "Are you sure you want to edit record?";

  }else if(action=="activate"){

    var element = document.getElementById("headerColor");
    element.classList.add("green");
    element.classList.remove("blue");
    element.classList.remove("yellow");
    element.classList.remove("red");
    return "Are you sure you want to re-activate record?";


  }else if(action=="deactivate"){

   var element = document.getElementById("headerColor");
   element.classList.add("red");
   element.classList.remove("blue");
   element.classList.remove("yellow");
   element.classList.remove("green");
   return "Are you sure you want to deactivate record?";

  }else if(action=="feature"){

   var element = document.getElementById("headerColor");
   element.classList.add("blue");
   element.classList.remove("red");
   element.classList.remove("yellow");
   element.classList.remove("green");
  return "Are you sure you want to feature record?";

  }
  else if(action=="manage"){

   var element = document.getElementById("headerColor");
   element.classList.add("blue");
   element.classList.remove("red");
   element.classList.remove("yellow");
   element.classList.remove("green");
  return "Are you sure you want to manage record?";

  }
}

function setModalTitle(action){
 if(action=="delete"){
  return "Delete Warning";
}else if(action=="delete_selected"){
  return "Delete Warning";
}else if(action=="edit"){
  return "Edit Warning";
}else if(action=="activate"){
  return "Activate Warning";
}else if(action=="deactivate"){
  return "Deactivate Warning";
}else if(action=="feature"){
  return "Feature Warning";
}else if(action=="manage"){
  return "Manage Warning";
}
}
function datefunc(){
 var m = "AM";
 var gd = new Date();
 var secs = gd.getSeconds();
 var minutes = gd.getMinutes();
 var hours = gd.getHours();
 var day = gd.getDay();
 var month = gd.getMonth();
 var date = gd.getDate();
 var year = gd.getYear();

 if(year<1000){
   year += 1900;
 }
 if(hours==0){
   hours = 12;
 }
 if(hours>12){
   hours = hours - 12;
   m = "PM";
 }
 if(secs<10){
   secs = "0"+secs;
 }
 if(minutes<10){
   minutes = "0"+minutes;
 }

 var dayarray = new Array ("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
 var montharray = new Array ("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

 var fulldate = dayarray[day]+", "+montharray[month]+" "+date+", "+year+" at "+hours+":"+minutes+":"+secs+" "+m;
 $("#currentDate").html(hours+":"+minutes+":"+secs+" "+m);
 $("#currentDateDay").html(dayarray[day]+", "+montharray[month]+" "+date+", "+year);

 setTimeout("datefunc()", 1000);
}
datefunc();



//onclick partner edit caption link
$(".editCaptionLink").click(function(e){
  e.preventDefault();
  $("#modal-footer").hide();
  $("#editCaptionBody").html('<i class="icon-spinner icon-spin"></i> Loading...');
  var pictureID = $(this).data('picture-id');
  $( "#editCaptionBody" ).load("/admin/partnerAjaxEditPicture/"+pictureID, function(){
    $("#modal-footer").show();
  });
});

//when edit caption is submitted
$("#editPictureCaptionForm").submit(function(e){
  var pictureID = $("#pictureID").val();
  $.post("/admin/partnerAjaxEditPicture/"+pictureID, {pictureCaption:$("#pictureCaption").val(), editAlbumSelect:$("#editAlbumSelect").val(), coverSelected:$("#albumCover").val()}, function(data){
    console.log(data);
    //alert("asdasdsdsd");
    if(data.resultText != ''){
      if(data.result){
        $("#caption"+pictureID).text(data.newCaption);
      }
      $('#editResult').html(data.resultText);
      if(data.rename !=''){
        $("#picture"+data.rename).remove();
      }
    }
  }, 'json');

  e.preventDefault(); //STOP default action
});

//delete picture link onclick
$(".deletePictureLink").click(function(e){
  e.preventDefault();
  $("#pictureHiddenID").val($(this).data('picture-id'));
  $("#pictureAlbumHiddenID").val($(this).data('picture-album-id'));
  $("#deletePictureConfMessage").html('Are you sure you want to delete picture?');
});

//delete picture link onclick
$("#deleteAllPictureSelectedLink").click(function(e){
  e.preventDefault();
  $("#pictureHiddenID").val('');
  $("#pictureAlbumHiddenID").val($(this).data('picture-album-id'));
  $("#deletePictureConfMessage").html('Are you sure you want to delete all selected picture(s)?');
  $("#moveALbumID").val('');
});

//delete picture onclick
$("#deletePicture").click(function(){
  $("#picturesForm").submit();
});

$("#moveLink").click(function(e){
  e.preventDefault();
  $("#moveALbumID").val('');
});

$('#movePictureBtn').click(function(){
  $("#picturesForm").submit();
});

$('.tbl_unselect_all').on('click', function(e){
    e.preventDefault();
    $("#moveALbumID").val('');
  });

$("#moveAlbumSelect").change(function(){
  $("#moveALbumID").val($(this).val());
});


//jp upload image
$(function(){
  // Variable to store your files
  var files;

  // Add events
  $("#picturesFile").on('change', prepareUpload);
  $('form#uploadPictureForm').on('submit', uploadPartnerImage);

  // Grab the files and set them to our variable
  function prepareUpload(event){
    $("#progressBar").css('width', 0+'%');
    $("#startUploadBtn").prop('disabled', false);
    files = event.target.files;
  }

  // Catch the form submit and upload the files
  function uploadPartnerImage(event){
      event.stopPropagation(); // Stop stuff happening
      event.preventDefault(); // Totally stop stuff happening

      /*
      if(percentComplete === 100) {
              $("#startUploadBtn").prop('disabled', false);
              $("#partnerPreloader").html('Done');
             }else{
              $( "#startUploadBtn" ).prop( "disabled", true );
              $("#partnerPreloader").html('<i class="icon-spinner icon-spin"></i> Uploading...'+percentComplete);
             }
      */

      // Create a formdata object and add the files
      var data = new FormData();
      $.each(files, function(key, value){
        data.append(key, value);
      });

      //console.log(data);
      $.ajax({
        url: '/admin/partnerAjaxUploadPicture/'+$("#partnerID").val()+'/'+$("#uploadAlbumSelect").val(),
        type: 'POST',
        data: data,
        cache: false,
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        beforeSend: function(XMLHttpRequest){
          $("#picturesFile").hide();
          $(".progress").addClass('active');
          $(".progress").addClass('progress-striped');
          $("#partnerPreloader").html('<i class="icon-spinner icon-spin"></i> Uploading...<span id="progressText">0%</span>');
          $( "#startUploadBtn" ).prop( "disabled", true );
        },
        xhr: function(){
         var xhr = new window.XMLHttpRequest();

         xhr.upload.addEventListener("progress", function(evt){
           if (evt.lengthComputable) {
             var percentComplete = evt.loaded / evt.total;
             percentComplete=parseInt(percentComplete*100);

             $("#progressBar").html('');
            $("#progressBar").css('width', percentComplete+'%');
            $("#progressText").text(percentComplete+'%');
           }
         }, false);

         return xhr;
       },
       success: function(data){
          console.log(data.result);
          if(data.result != ''){
            $("#partnerPreloader").html(data.result);
          }
        },
       complete: function(data){
        $("#uploadPictureForm")[0].reset();
        $(".progress").removeClass('active');
        $(".progress").removeClass('progress-striped');
        $("#startUploadBtn").prop('disabled', false);
        $("#picturesFile").show();

        $("#afterUpload").click(function(){
          location.reload();
        });
        //$("#partnerPreloader").html('Done');
       },
        error: function(jqXHR, textStatus, errorThrown){
          // Handle errors here
          console.log('ERRORS: ' + textStatus);
          // STOP LOADING SPINNER
        }
      });
}


//jp create album ajax

  $("#deleteSelectedAlbumBtn").click(function(){
    $("#picturesForm").submit();
  });


$("button#createAlbumBtn").click(function(){

  $.post("/admin/createAlbumAjax", {partnerID: $("#partnerID").val(), 'albumName':$("#albumName").val()}, function(response){
    if(response.success == true){
      $('#noticeErr').addClass('alert alert-success');
      $("#noticeErr").html('Album added successfully.');
      $('#createAlbumModal').modal('toggle');
      window.location.reload();
    }else{
      //$("#albumResult").hide().html('<span class="label label-danger">'+response.message+'</span>').fadeIn();
      $('#erredit').addClass('label label-danger');
      $("#erredit").html(response.message).fadeIn();
      activateAlbumSelect();
    }

    if(response.albums != ''){
      $(".albums").html(response.albums);
    }

    $("#createAlbumClose").html('<a href="" class="btn btn-default">Close</a>');
    activateAlbumSelect();

    //setTimeout(function(){location.reload();}, 1000);

  }, 'json');
  activateAlbumSelect();
});

  function activateAlbumSelect(){
    $("#albumSelect").change(function () {
          location.href = $(this).val();
    })
  }

  activateAlbumSelect();

  $(".editAlbumNameLink").click(function(e){
    e.preventDefault();
    $("editAlbumContent").html('<span><i class="icon-spinner icon-spin"></i> Loading...</span>');
    $.post("/admin/editAlbumAjax/",{action:'get', albumID:$(this).data('album-id'), partnerID:$(this).data('partner-id')}, function(data){
      $("#editAlbumContent").html(data.message);
      if(data.success){
        $("#editAlbumBtn").show();
      }else{
        $("#editAlbumBtn").hide();
      }
      console.log(data);
    }, 'json');
  });

  $("button#editAlbumBtn").click(function(){

    $.post("/admin/editAlbumAjax/", {
      action:'save',albumID:$("#saveHiddenAlbumID").val(),albumName:$("#saveAlbumName").val(), partnerID:$(this).data('partner-id')}, function(data){
        console.log(data.message);

        $("#editAlbumResult").hide().html(data.message).fadeIn();
      if(data.success == true){
        $('#noticeErr').addClass('alert alert-success');
        $("#noticeErr").html('Album updated successfully.');
        $('#editAlbumModal').modal('toggle');
       // window.location.reload();
      }

    if(data.albums != ''){
      $(".albums").html(data.albums);
    }



      // if(data.success){
      // $('#noticeErr').addClass('alert alert-success');
      // $("#noticeErr").html('Album added successfully.');
      // $('#editAlbumModal').modal('toggle');
      // window.location.reload();
      //   $("#albumLink"+$("#saveHiddenAlbumID").val()).html($("#saveAlbumName").val());
      // }else{

      // }
      // setTimeout(function(){
      //   //location.reload();
      // }, 1000);
    }, 'json');
    activateAlbumSelect();
  });

  $('.deleteAlbumLink').click(function(e){
    e.preventDefault();
    $("#deleteHiddenAlbumID").val($(this).data('album-id'));
    if($(this).data('empty') == 1){
      $("#deleteMessage").html("Are you sure you want to delete <strong>"+$(this).data('album-name')+"</strong>?");
    }else{
      $("#deleteMessage").html("<strong>"+$(this).data('album-name')+" is not empty. </strong>Are you sure you want to delete?");
    }
  });

  $("#deleteAlbumBtn").click(function(e){
    e.preventDefault();
    $("#deleteMessage").hide().html('<span><i class="icon-spinner icon-spin"></i> Deleting...</span>');
    $.post("/admin/editAlbumAjax/", {action:'delete',albumID:$("#deleteHiddenAlbumID").val()}, function(data){
      $("#deleteMessage").hide().html(data.message).fadeIn();
      $("#album"+$("#deleteHiddenAlbumID").val()).remove();
    }, 'json');
    $('#noticeErr').addClass('alert alert-success');
    $("#noticeErr").html('Album has been successfully deleted.');
  });

  $("#addPictures").click(function(e){
    e.preventDefault();
    $("#uploadAlbumLabel").text($(this).data('album-name'));
    $("#uploadAlbumSelect").val($(this).data('album-id'));
  });

  $(".deletePartner").click(function(e){
    e.preventDefault();
    $("#singlePartnerID").val($(this).data('recorID'));
  });

  $(".deleteEventLink").click(function(e){
    e.preventDefault();
    $("#delEventSingle").val($(this).data('event-id'));
    $("span#deleteMessage").html('Are you sure you want to delete event <strong>"'+$(this).data('eventname')+'"</strong>?');
  });

  $("#deleteEventBtn").click(function(){
    $("#eventsForm").submit();
  });

  $("#deleteAllSelectedEventLink").click(function(e){
    e.preventDefault();
    $("#delEventSingle").val('');
    $("span#deleteMessage").html('Are you sure you want to delete selected event(s)?');
  });

  $("#changePass").click(function(){
    if($(this).prop('checked') == true){
      $("#changePassWrapper").slideDown();
    }else{
      $("#changePassWrapper").slideUp();
    }
  });

});


/* File Uploader */
$(function () {
  'use strict';
  var newArray = new Array();
  $('#digitalAssets2').fileupload({
    url: BASE_URL + 'server/php/',
    dataType: 'json',
    disableImageResize: /Android(?!.*Chrome)|Opera/
    .test(window.navigator.userAgent),
    previewMaxWidth: 100,
    previewMaxHeight: 100,
    previewCrop: true,
    limitMultiFileUploads: 4,
    beforeSend: function(){
      //$('#progress .progress-bar').css('width','0%');
      $("#selectImg").remove();
    },
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {

        var partnerID = $("#partner-id").val();
        var partnerAlbum = $("#partner-album").val();
        var albumID = $("#album-id").val();

        $.get(BASE_URL + 'admin/ajaxmoveuploadpartnerpictures/'+ file.name+'/'+albumID, function(data){
            $('.digital-assets-gallery').prepend(data);
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });

            $(".delete-recent-upload-pic").click(function(){
              var delBtn = $(this);
              $.post('/admin/deleteRecentPicUpload/'+$(this).data('picture-id'), function(response){
                console.log(response);
                if(response.success){
                  delBtn.parent().remove();
                }
              }, 'json');
            });
        });

      });
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css('width',progress + '%');

      /*setTimeout(function(){
        location.reload();
      }, 1000);*/
      //console.log(data);

      $("#closeUpload").click(function(){
        location.reload();
      });

    }
  }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
});


$(".delete-recent-upload-pic").click(function(){
  var delBtn = $(this);
  $.post('/admin/deleteRecentPicUpload/'+$(this).data('picture-id'), function(response){
    console.log(response);
    if(response.success){
      delBtn.parent().remove();
    }
  }, 'json');
});

$(".delete-recent-upload-newsletter").click(function(){
  var delBtn = $(this);
  $.post('/admin/deleteRecentNewsletterUpload/'+$(this).data('picture-id'), function(response){
    console.log(response);
    if(response.success){
      delBtn.parent().remove();
    }
  }, 'json');
});

$("#partnerAlbumSelect").change(function(){
  $("#partnerGalleryWrapper").html('<div class="padd "><i class="icon-spinner icon-spin"></i> Loading...</div>');
  $.get("/admin/ajaxGetAllPictures/"+$(this).val()+"/"+$("#partnerID").val(), function(data){
    $("#partnerGalleryWrapper").html(data);
  });
});

$("#page_title").keyup(function(){
  var slug = convertToSlug($(this).val());
  $("#page-slug").text(slug);
  $("#hpage_slug").val(slug);
});
//add codes by june
$("#post_title").keyup(function(){
  var slug = convertToSlug($(this).val());
  $("#page-slug").text(slug);
  $("#hpage_slug").val(slug);
});

$("#ann_title").keyup(function(){
  var slug = convertToSlug($(this).val());
  $("#page-slug").text(slug);
  $("#hpage_slug").val(slug);
});


$("#blog_title").keyup(function(){
  var slug = convertToSlug($(this).val());
  $("#page-slug").text(slug);
  $("#hpage_slug").val(slug);
});
function convertToSlug(Text){
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-');
}

$("#sideBarImg").blur(function(){
  var img = $(this).val();
  if(img!=''){
    $("#sideBarImgWrapper").html('<img src="'+img+'" style="width: 100%">');
    $("#removeSideBarImg").show();
  }
  else{
    $("#sideBarImgWrapper").html('');
    $("#removeSideBarImg").hide();
  }
});

$("#removeSideBarImg").click(function(){
  $("#sideBarImgWrapper").html('');
  $("#sideBarImg").val('');
  $(this).hide();
});

$('#imageBanner').hide();
if($("#imageUrl").val()){
  $("#imagePreview").html('<img src="'+$("#imageUrl").val()+'" style="width: 100%">');
  $('#imageBanner').show();
} else {
  $("#imagePreview").html('');
  $('#imageBanner').hide();
}
$('#embedVideo').hide();
if($("#videoUrl").val()){
  $("#videoPreview").html('<iframe src="'+$("#videoUrl").val()+'" class="embed-responsive-item" style="width: 100%;" frameborder=0>');
  $('#embedVideo').show();
} else {
  $("#videoPreview").html('');
  $('#embedVideo').hide();
}

$("#imageUrl").blur(function(){
  var img = $(this).val();
  if(img){
    $("#imagePreview").html('<img src="'+img+'" style="width: 100%">');
    $('#imageBanner').show();
  }else{
    $("#imagePreview").html('');
    $('#imageBanner').hide();
  }
});
$("#videoUrl").blur(function(){
  var img = $(this).val();
  if(img){
    $("#videoPreview").html('<iframe src="'+$("#videoUrl").val()+'" class="embed-responsive-item" style="width: 100%;" frameborder=0>');
    $('#embedVideo').show();
  }else{
    $("#videoPreview").html('');
    $('#embedVideo').hide();
  }
});

function removePreview(){
  $("#imagePreview").html('');
  $('#imageBanner').hide();
  $("#videoPreview").html('');
  $('#embedVideo').hide();
}

$("#pageBannerUrl").blur(function(){
  var img = $(this).val();
  if(img!=''){
    $("#pageBannerImgWrapper").html('<img src="'+img+'" style="width: 100%">');
    $("#removeBanner").show();
  }
  else{
    $("#pageBannerImgWrapper").html('');
    $("#removeBanner").hide();
  }
});

$("#removeBanner").click(function(){
  $("#pageBannerImgWrapper").html('');
  $("#pageBannerUrl").val('');
  $(this).hide();
});


/* File Uploader */
$(function () {
  'use strict';
  var newArray = new Array();
  $('#pagespictures').fileupload({
    url: BASE_URL + 'server/php/',
    dataType: 'json',
    disableImageResize: /Android(?!.*Chrome)|Opera/
    .test(window.navigator.userAgent),
    previewMaxWidth: 100,
    previewMaxHeight: 100,
    previewCrop: true,
    limitMultiFileUploads: 1,
    beforeSend: function(){
      $('#progress .progress-bar').css('width','0');
    },
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        console.log("wtf");
        $.get(BASE_URL + 'admin/ajaxmoveuploadpages/'+ file.name +'/0'+'/pages', function(data){
            $('#pages-gallery').prepend(data);
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            var fileCount = parseInt($("#fileCount").text());
            fileCount++;
            $("#fileCount").text(fileCount);
            activateDeletePagePicture();

        });

      });
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css('width',progress + '%');
    }
  }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
}).on('fileuploadadd', function (e, data) {
    var currentFile = data.files[0];
    console.log(data);
    if(currentFile.size > 2000000){
        data.abort();
        $('#imageError').html("<div class='alert alert-danger'>Image file size must be less than 2 MB.</div>");
        return false;
    } else {
        $('#imageError').html("");
    }
  });


$(function () {
  'use strict';
  var newArray = new Array();
  $('#actpictures').fileupload({
    url: BASE_URL + 'server/php/',
    dataType: 'json',
    disableImageResize: /Android(?!.*Chrome)|Opera/
    .test(window.navigator.userAgent),
    previewMaxWidth: 100,
    previewMaxHeight: 100,
    previewCrop: true,
    limitMultiFileUploads: 1,
    beforeSend: function(){
      $('#progress .progress-bar').css('width','0');
    },
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        $.get(BASE_URL + 'admin/ajaxmoveuploadpages/'+ file.name +'/0'+'/activities', function(data){
            $('#pages-gallery').prepend(data);
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            var fileCount = parseInt($("#fileCount").text());
            fileCount++;
            $("#fileCount").text(fileCount);
            activateDeletePagePicture();

        });

      });
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css('width',progress + '%');
    }
  }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
}).on('fileuploadadd', function (e, data) {
    var currentFile = data.files[0];
    console.log(data);
    if(currentFile.size > 2000000){
        data.abort();
        $('#imageError').html("<div class='alert alert-danger'>Image file size must be less than 2 MB.</div>");
        return false;
    } else {
        $('#imageError').html("");
    }
  });

function activateDeletePagePicture(){
  $(".delete-recent-upload-page-pic").click(function(e){
    e.preventDefault();
    var delBtn = $(this);
    $.post('/admin/deleteRecentPagesPicUpload/'+$(this).data('picture-id'), function(response){
      console.log(response);
      if(response.success){
        delBtn.parent().remove();
        var fileCount = parseInt($("#fileCount").text());
        if(fileCount > 0){
          fileCount--;
        }
        $("#fileCount").text(fileCount);
      }
    }, 'json');
  });
}
activateDeletePagePicture();

$(".delActConfLink, .disableActConfLink").click(function(e){
  e.preventDefault();
  $("#ddModalTitle").text($(this).data('action')+' Activity').css('text-transform', 'capitalize');
  $("#ddActionSpan").text($(this).data('action'));
  $("#activityLabelConf").text($(this).data('activity-title'));
  $("#hdelActivity").val($(this).data('activity-id'));
  $("#delActivitybtn").text($(this).data('action')).css('text-transform', 'capitalize');;

  if($(this).data('action')=='disable' || $(this).data('action')=='delete'){
    $("#delActivitybtn").removeClass('btn-success').addClass('btn-danger');
  }else{
    $("#delActivitybtn").removeClass('btn-danger').addClass('btn-success');
  }

  $("#ddhiddenaction").val($(this).data('action'));

  //console.log($(this).data('action'));
});

$(".editActLink").click(function(e){
  e.preventDefault();
  $("#saveEditActivity").prop('disabled', false);
  $("#activitytextArea").val($(this).data('activity-title'));
  $("#heditActivity").val($(this).data('activity-id'));
});

$('#activitytextArea').keyup(function(){

  if($(this).val().length > 0){
    $("#saveEditActivity").prop('disabled', false);
  }else{
    $("#saveEditActivity").prop('disabled', true);
  }
});

$("#delActivitybtn").click(function(){
  $("#activityForm").submit();
});

$("a#createTownAlbumLink").click(function(e){
  e.preventDefault();
  $("#albumName").val('');
  $("#editAlbumID").val('');
  $("#townAlbumModalTitle").text('Create Album');
});

$("a.editTownAlbumLink").click(function(e){
  e.preventDefault();
  $("#albumName").val($(this).data('album-name'));
  $("#origAlbumName").val($(this).data('album-name'));
  $("#editAlbumID").val($(this).data('album-id'));
  $("#townAlbumModalTitle").text('Edit Album');
  $("#albumResult").html('');
});

$("#createTownAlbumBtn").click(function(e){
  e.preventDefault();

  $.post(BASE_URL+'admin/townsgallery/'+$("#townID").val(), {'albumName':$("#albumName").val(), 'editAlbumID':$("#editAlbumID").val(), origAlbumName:$("#origAlbumName").val()}, function(response){
    $("#albumResult").hide().html(response.message).show();
    if(response.success){
      location.reload();
    }
  }, 'json');

});


/* File Uploader */
$(function () {
  'use strict';
  var newArray = new Array();
  $('#townPictures').fileupload({
    url: BASE_URL + 'server/php/',
    dataType: 'json',
    disableImageResize: /Android(?!.*Chrome)|Opera/
    .test(window.navigator.userAgent),
    previewMaxWidth: 100,
    previewMaxHeight: 100,
    previewCrop: true,
    limitMultiFileUploads: 4,
    beforeSend: function(){
      //$('#progress .progress-bar').css('width','0%');
      $("#selectImg").remove();
    },
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {
        var albumID = $("#albumID").val();

        $.get(BASE_URL + 'admin/townAlbum/'+$("#townID").val()+'/'+ albumID+'/'+file.name+'/0', function(data){
            $('.digital-assets-gallery').prepend(data);
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });

            $(".delete-recent-upload-townpic").click(function(){
              var delBtn = $(this);
              $.post(BASE_URL + 'admin/townAlbum/'+$("#townID").val()+'/'+ albumID, {'deleteLastTownPic':$(this).data('picture-id')}, function(response){
                console.log(response);
                if(response.success){
                  delBtn.parent().remove();
                }
              }, 'json');
            });
        });

      });
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css('width',progress + '%');

      /*setTimeout(function(){
        location.reload();
      }, 1000);*/
      //console.log(data);

      $("#closeUpload").click(function(){
        location.reload();
      });

    }
  }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
}).on('fileuploadadd', function (e, data) {
    var currentFile = data.files[0];
    console.log(data);
    if(currentFile.size > 2000000){
        data.abort();
        $('#imageError').html("<div class='alert alert-danger'>Image file size must be less than 2 MB.</div>");
        return false;
    } else {
        $('#imageError').html("");
    }
  });

$(".editTownPicLink").click(function(e){
  e.preventDefault();
  $("#edit-pic-thumbnail").attr('src', $(this).data('picture-src'));
  $("#pictureCaption").val($(this).data('caption'));
  $("#hTownPic").val($(this).data('picture-id'));
  if($(this).data('picture-id')==$("#albumCoverID").val()){
    $("#albumCover").prop('checked', true);
  }else{
    $("#albumCover").prop('checked', false);
  }
});

$("#albumOptions2").change(function () {
  location.href = $(this).val();
})

$("#townAlbumSelect").change(function(){
  $("#partnerGalleryWrapper").html('<i class="icon-spinner icon-spin"></i>Loading pictures.');
  $.post(BASE_URL + 'admin/townevents/'+$("#townID").val(), {albumID:$(this).val()}, function(response){
    $("#partnerGalleryWrapper").html(response.message);
  }, 'json');
});

$('#uploadTownPicture').on('hidden.bs.modal', function () {
    location.reload();
})

$(".delete-recent-upload-town-pic").click(function(){
  var dis = $(this);
  $.post(BASE_URL + 'admin/townevents/'+$("#townID").val(), {pictureID:$(this).data('picture-id'), albumName:$(this).data('album-name')}, function(response){
    if(response.success == true){
      dis.parent().remove();
    }else{
      alert('Unable to delete file');
    }
  }, 'json');
});

$("#addpartnerlink").click(function(e){
  e.preventDefault();

  //
});

$('#addpartner').on('shown.bs.modal', function () {
  $(".chosen-select").chosen({no_results_text: "No partner found."});
});

$('div.modal').on('hidden.bs.modal', function () {
    $(".tbl-action, .tbl-recordID, #hdelActivity, #ddhiddenaction").val('');
})


// if($('#contact')){
//   // $("#contact").mask("9999 999 9999");

// };


function resetTextarea() {
  $('iframe').contents().find('body').empty();
}

/* File Uploader */
$(function () {
  'use strict';
  var newArray = new Array();
  $('#newsletterpictures').fileupload({
    url: BASE_URL + 'server/php/',
    dataType: 'json',
    disableImageResize: /Android(?!.*Chrome)|Opera/
    .test(window.navigator.userAgent),
    previewMaxWidth: 100,
    previewMaxHeight: 100,
    previewCrop: true,
    limitMultiFileUploads: 4,
    beforeSend: function(){
      $('#progress .progress-bar').css('width','0');
    },
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {

        $.get(BASE_URL + 'admin/ajaxmoveuploadnewsletter/'+ file.name +'/0', function(data){
            $('#newsletter-gallery').prepend(data);
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            var fileCount = parseInt($("#fileCount").text());
            fileCount++;
            $("#fileCount").text(fileCount);
            activateDeletePagePicture();

            $(".delete-recent-upload-newsletter").click(function(){
              var delBtn = $(this);
              $.post('/admin/deleteRecentNewsletterUpload/'+$(this).data('picture-id'), function(response){
                console.log(response);
                if(response.success){
                  delBtn.parent().remove();
                }
              }, 'json');
            });
        });

      });
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css('width',progress + '%');
    }
  }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
}).on('fileuploadadd', function (e, data) {
    var currentFile = data.files[0];
    console.log(data);
    if(currentFile.size > 2000000){
        data.abort();
        $('#imageError').html("<div class='alert alert-danger'>Image file size must be less than 2 MB.</div>");
        return false;
    } else {
        $('#imageError').html("");
    }
  });

/* File Uploader */
$(function () {
  'use strict';
  var newArray = new Array();
  $('#postpictures').fileupload({
    url: BASE_URL + 'server/php/',
    dataType: 'json',
    disableImageResize: /Android(?!.*Chrome)|Opera/
    .test(window.navigator.userAgent),
    previewMaxWidth: 100,
    previewMaxHeight: 100,
    previewCrop: true,
    limitMultiFileUploads: 4,
    beforeSend: function(){
      $('#progress .progress-bar').css('width','0');
    },
    done: function (e, data) {
      $.each(data.result.files, function (index, file) {

        $.get(BASE_URL + 'admin/ajaxmoveuploadprogram/'+ file.name +'/post', function(data){
            $('#pages-gallery').prepend(data);
            jQuery("a[class^='prettyPhoto']").prettyPhoto({
              overlay_gallery: false, social_tools: false
            });
            var fileCount = parseInt($("#fileCount").text());
            fileCount++;
            $("#fileCount").text(fileCount);
            activateDeletePagePicture();

        });

      });
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $('#progress .progress-bar').css('width',progress + '%');
    }
  }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
}).on('fileuploadadd', function (e, data) {
    var currentFile = data.files[0];
    console.log(data);
    if(currentFile.size > 2000000){
        data.abort();
        $('#imageError').html("<div class='alert alert-danger'>Image file size must be less than 2 MB.</div>");
        return false;
    } else {
        $('#imageError').html("");
    }
  });


/* CHECK PASSWORD VALIDATION */
if($("#repassword")[0]){
  validatePassword();
}

$("#repassword")
  .change(function(){
    validatePassword();
  })
  .keyup(function(){
    validatePassword();
  })
  .focus(function(){
    validatePassword();
  });

$("#password")
  .change(function(){
    validatePassword();
  })
  .keypress(function(event){
    validatePassword();
  })
  .keyup(function(){
    validatePassword();
  });

$('#password').keypress(function(event){
  var keycode = (event.keyCode ? event.keyCode : event.which);
  if(keycode == '32'){
    $('#spaceerror').html("Avoid using spaces.").fadeIn();
    return false;
  }else{
    $("#spaceerror").fadeOut();
  }
});

function validatePassword()
{
  var pass = $('#password').val(),
      repass = $('#repassword').val();
//      error = false;

  console.log(pass);

  if (!pass && !repass){
    $('#passerror').html("");
    $('#repasserror').html("");
   // $("#savepassBtn").prop('disabled', true);
  }

  if (!pass || !repass){
   // $("#savepassBtn").prop('disabled', true);
  }

  if (pass != repass && pass && repass){
    $('#repasserror').html("Password doesn't match confirmation");
    $('#repasshide').hide();
  // $("#savepassBtn").prop('disabled', true);
  }
  if(pass.length < 8 && pass) {
    $('#passerror').html("Password should be minimum of 8 characters");
     $('#passhide').hide();
  //  $("#savepassBtn").prop('disabled', true);
  }
  if(pass.length >= 8) {
    $('#passerror').html("");
    $('#passhide').hide();
  }
  if(pass == repass && pass && repass) {
    $('#repasserror').html("");
    $('#repasshide').hide();
  }
  if(pass == repass && pass && repass && pass.length >= 8) {
  //  $("#savepassBtn").prop('disabled', false);
  }

}
/* END CHECK */

//Username validation
$("#username")
  .change(function(){
    txtboxusername();
  })
  .keyup(function(){
    txtboxusername();
  })
  .keypress(function(){
    txtboxusername();
  })
  .submit(function(){
    txtboxusername();
  })
  .load(function(){
    txtboxusername();
  });

 function txtboxusername() {

        var originalValue = $('#username').val(),
        charCount = $('#username').val();
           $('#userhide').hide();
        if(originalValue.match(/^[a-zA-Z0-9]+$/) || charCount.length < 6){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#userhide').hide();
            }
        if ((!originalValue.match(/^[a-zA-Z0-9]+$/))) {
          $('#valuser0').html("Username is invalid. Avoid spaces and symbols");
          $('#userhide').hide();
           // textBoxObj.value = originalValue.substring(0, charCount - 1);
        }else if(charCount.length < 6 ) {
            $('#valuser1').html("Username should have at least 6 minimum characters");
            $('#userhide').hide();
        }else if(empty(originalValue)){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#userhide').hide();
        }else{
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#userhide').hide();
        }
          }
//end username validation
//check email
$('#email')
  .change(function(){
    txtboxemail();
  })
  .keyup(function(){
    txtboxemail();
  });
  function txtboxemail(){
    var emailval = $('#email').val();
   if(emailval.match(/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i)){
        $('#emailhide').hide();
        //$('#valemail').html("Not a valid email address format");
    }
}
//end email validation
//check fname
$('#firstname')
  .change(function(){
    txtboxfirstname();
  })
  .keyup(function(){
    txtboxfirstname();
  })
  .keypress(function(){
    txtboxfirstname();
  })
  ;
  function  txtboxfirstname(){
    var firstname = $('#firstname').val();
    if(firstname != ""){
    $('#fnamehide').hide();
    }
}
//end fname validation
//check lname
$('#lastname')
  .change(function(){
    txtboxlastname();
  })
  .keyup(function(){
    txtboxlastname();
  });
  function txtboxlastname(){
    var lastname = $('#lastname').val();
    if(lastname != ""){
    $('#lnamehide').hide();
    }
}
//end  lname validation
//check contact
// $('#contact')
//   .change(function(){
//     txtboxcontact();
//   })
//   .keyup(function(){
//     txtboxcontact();
//   })
//   .keypress(function(){
//     txtboxcontact();
//   });
//  function txtboxcontact() {

      // $("#contact").mask("(9999) 999 9999");
        // if (!contactvalue.match(/^[0-9]*$/)) {
        //   $('#valcontact').html("Not a valid contact number format");
        //   $('#contacthide').hide();
        // }else if(contactvalue.length != 11){
        //   $('#valcontact').html("Contact number should only have 11 digits");
        //   $('#contacthide').hide();
        // }else{
        //     $('#valcontact').html("");
        // }
 // }
//end contact validation
// $("#contact").mask("(9999) 999 9999");
// $("#contact").on('keyup', function() {
//   if (!/^[_-]+$/.test(this.value)) {
//   $('#contacthide').hide();
//   }
// });
function isNumber(evt){
evt = (evt) ? evt : window.event;
  var charCode = (event.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
  }
  return true;
}
  $("#contact")
  .change(function(){
    con();
  })
  .keyup(function(){
    con();
  })
  ;
 function con(){
  // var con = $("#conreq").val();
  // if(con.length == 11){
  //   $('#contacthide').hide();
  // }
  $('#contacthide').hide();
  // else if(empty(con) == false){
  //   $('#contacthide').hide();
  // }

 }
//check roles



  function rolecheck(x) {

    var text = document.getElementById("rolehide");
    var checkBoxes = $('.checkbox');
    var count = 0;
    var divBoxesChecked = document.getElementById('boxesChecked');

    $.each(checkBoxes, function(i)   {

    if (checkBoxes[i].checked == true) {
      count++;
      text.style.display = "none";
    }

    });
    if(count == 0 ){
      text.style.display = "block";
    }
    console.log(count);


  }

//end roles validation
//check partnername
$("#partnerName")
  .change(function(){
    txtboxpartnername();
  })
  .keyup(function(){
    txtboxpartnername();
  })
  .keypress(function(){
    txtboxpartnername();
  });

 function txtboxpartnername() {

        var pnamevalue = $('#partnerName').val();

        if (!pnamevalue.match(/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i)) {
          $('#valpartnername').html("Not a valid format");
          $('#pnamehide').hide();
        }else{
            $('#valpartnername').html("");
        }
          }
//end partnername validation
//check partnername
$("#partnerInfo")
  .change(function(){
    txtboxpartnerinfo();
  })
  .keyup(function(){
    txtboxpartnerinfo();
  })
  .keypress(function(){
    txtboxpartnerinfo();
  });

 function txtboxpartnerinfo() {

        var pinfovalue = $('#partnerInfo').val();

        if (!pinfovalue.match(/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i)) {
          $('#valpartnerinfo').html("Not a valid format");
          $('#pinfohide').hide();
        }else{
            $('#valpartnerinfo').html("");
        }
          }
//end partnerinfo validation
//check post title
$("#post_title")
  .change(function(){
    txtboxpostTitle();
  })
  .keyup(function(){
    txtboxpostTitle();
  })
  .keypress(function(){
    txtboxpostTitle();
  });
function txtboxpostTitle(){
  var post_title= $("#post_title").val();
  if(post_title != ""){
    $("#post_title_hide").hide();
  }
}

$("#pageBannerUrl")
  .change(function(){
    txtboximageURL();
  })
  .keyup(function(){
    txtboximageURL();
  })
  .keypress(function(){
    txtboximageURL();
  });
function txtboximageURL(){
  var post_featimage= $("#pageBannerUrl").val();
  if(post_featimage != ""){
    $("#post_featimage_hide").hide();
  }
}
//end pagebanner(Featured image) validation
//AbkRescue Blog Validation

//blogtile
$("#page_title")
  .change(function(){
    txtboxblogtitle();
  })
  .keyup(function(){
    txtboxblogtitle();
  })
  .keypress(function(){
    txtboxblogtitle();
  })
  .submit(function(){
    txtboxblogtitle();
  })
  .load(function(){
    txtboxblogtitle();
  });

 function txtboxblogtitle() {

        var originalValue = $('#page_title').val(),
        charCount = $('#page_title').val();
           $('#blogTitle').hide();
        if(originalValue.match(/^[a-zA-Z0-9]+$/) || charCount.length < 6){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#blogTitle').hide();
            }
          }

$("#blog_title")
  .change(function(){
    txtboxblogtitle2();
  })
  .keyup(function(){
    txtboxblogtitle2();
  })
  .keypress(function(){
    txtboxblogtitle2();
  })
  .submit(function(){
    txtboxblogtitle2();
  })
  .load(function(){
    txtboxblogtitle2();
  });

 function txtboxblogtitle2() {

        var originalValue = $('#blog_title').val(),
        charCount = $('#blog_title').val();
           $('#blogTitle2').hide();
        if(originalValue.match(/^[a-zA-Z0-9]+$/) || charCount.length < 6){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#blogTitle2').hide();
            }
          }
          //end blogtitle

 //Author name
$("#blog_author")
  .change(function(){
    txtboxauthorname();
  })
  .keyup(function(){
    txtboxauthorname();
  })
  .keypress(function(){
    txtboxauthorname();
  })
  .submit(function(){
    txtboxauthorname();
  })
  .load(function(){
    txtboxauthorname();
  });

function txtboxauthorname() {

        var originalValue = $('#blog_author').val(),
        charCount = $('#blog_author').val();
           $('#authname').hide();
        if(originalValue.match(/^[a-zA-Z0-9]+$/) || charCount.length < 6){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#authname').hide();
            }
          }
  //end Author Name

 //Blog Content
$("#blog_content")
  .change(function(){
    txtboxblogcontent();
  })
  .keyup(function(){
    txtboxblogcontent();
  })
  .keypress(function(){
    txtboxblogcontent();
  })
  .submit(function(){
    txtboxblogcontent();
  })
  .load(function(){
    txtboxblogcontent();
  });

  function txtboxblogcontent() {


           $('#blogcont').hide();

          }
  //end Blog Content



 //Featured image
$("#pageBannerUrl")
  .change(function(){
    txtboximage();
  })
  .keyup(function(){
    txtboximage();
  })
  .keypress(function(){
    txtboximage();
  })
  .submit(function(){
    txtboximage();
  })
  .load(function(){
    txtboximage();
  });

  function txtboximage() {

        var originalValue = $('#pageBannerUrl').val(),
        charCount = $('#pageBannerUrl').val();
           $('#tbimage').hide();
        if(originalValue.match(/^[a-zA-Z0-9]+$/) || charCount.length < 6){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#tbimage').hide();
            }
          }

          $("#pubdate")
  .change(function(){
    txtboxpd();
  })
   .click(function(){
    txtboxpd();
  })
  .keyup(function(){
    txtboxpd();
  })
  .keypress(function(){
   txtboxpd();
  })
  .submit(function(){
   txtboxpd();
  })
  .load(function(){
    txtboxpd();
  });

  function txtboxpd() {

        var originalValue = $('#pubdate').val(),
        charCount = $('#pubdate').val();
           $('#pdate').hide();
        if(originalValue.match(/^[a-zA-Z0-9]+$/) || charCount.length < 6){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#pdate').hide();
            }
          }
  //end Featured image
//end AbkRescue Blog Validation

//ABK Events Validation
//eventname
$("#eventname")
  .change(function(){
    txtboxeventname();
  })
  .keyup(function(){
    txtboxeventname();
  })
  .keypress(function(){
    txtboxeventname();
  })
  .submit(function(){
    txtboxeventname();
  })
  .load(function(){
    txtboxeventname();
  });

 function txtboxeventname() {

        var originalValue = $('#eventname').val(),
        charCount = $('#eventname').val();
           $('#evtnm').hide();
        if(originalValue.match(/^[a-zA-Z0-9]+$/) || charCount.length < 6){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#evtnm').hide();
            }
          }
          //end eventname
//eventslugs
$("#eventslugs")
  .change(function(){
    txtboxeventurl();
  })
  .keyup(function(){
    txtboxeventurl();
  })
  .keypress(function(){
    txtboxeventurl();
  })
  .submit(function(){
    txtboxeventurl();
  })
  .load(function(){
    txtboxeventurl();
  });

 function txtboxeventurl() {

        var originalValue = $('#eventslugs').val(),
        charCount = $('#eventslugs').val();
           $('#urlval').hide();
        if(originalValue.match(/^[a-zA-Z0-9]+$/) || charCount.length < 6){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#urlval').hide();
            }
          }
          //end eventslug
//desc
$("#desc")
  .change(function(){
    txtboxeventdesc();
  })
  .keyup(function(){
    txtboxeventdesc();
  })
  .keypress(function(){
    txtboxeventdesc();
  })
  .submit(function(){
    txtboxeventdesc();
  })
  .load(function(){
    txtboxeventdesc();
  });

 function txtboxeventdesc() {

        var originalValue = $('#desc').val(),
        charCount = $('#desc').val();
           $('#shodesc').hide();
        if(originalValue.match(/^[a-zA-Z0-9]+$/) || charCount.length < 6){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#shodesc').hide();
            }
          }
          //end desc


//location
$("#location")
  .change(function(){
    txtboxlocation();
  })
  .keyup(function(){
    txtboxlocation();
  })
  .keypress(function(){
    txtboxlocation();
  })
  .submit(function(){
    txtboxlocation();
  })
  .load(function(){
    txtboxlocation();
  });

 function txtboxlocation() {

        var originalValue = $('#location').val(),
        charCount = $('#location').val();
           $('#loc').hide();
        if(originalValue.match(/^[a-zA-Z0-9]+$/) || charCount.length < 6){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#loc').hide();
            }
          }
          //end location

//long desc
$("#ldesc")
  .change(function(){
    txtboxldesc();
  })
  .keyup(function(){
    txtboxldesc();
  })
  .keypress(function(){
    txtboxldesc();
  })
  .submit(function(){
    txtboxldesc();
  })
  .click(function(){
    txtboxldesc();
  })
  .load(function(){
    txtboxldesc();
  });

 function txtboxldesc() {

        var originalValue = $('#ldesc').val(),
        charCount = $('#ldesc').val();
           $('#long').hide();
        if(originalValue.match(/^[a-zA-Z0-9]+$/) || charCount.length < 6){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#long').hide();
            }
          }



$("#valdate")
  .change(function(){
    txtboxstartdate();
  })
  .keyup(function(){
    txtboxstartdate();
  })
  .keypress(function(){
    txtboxstartdate();
  })
  .submit(function(){
    txtboxstartdate();
  })
  .click(function(){
    txtboxstartdate();
  })
  .load(function(){
    txtboxstartdate();
  });

 function txtboxstartdate() {


        var originalValue = $('#valdate').val(),
        charCount = $('#valdate').val();
           $('#valdate1').hide();
        if(originalValue.match(/^[0-9\-]/i)){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#valdate1').hide();
            }

          }
          //end start date


//end date
$("#ed")
  .change(function(){
    txtboxenddate();
  })
  .keyup(function(){
    txtboxenddate();
  })
  .keypress(function(){
    txtboxenddate();
  })
  .submit(function(){
    txtboxenddate();
  })
  .click(function(){
    txtboxenddate();
  })
  .load(function(){
    txtboxenddate();
  });

 function txtboxenddate() {

        var originalValue = $('#ed').val(),
        charCount = $('#ed').val();
           $('#valend').hide();
        if(originalValue.match(/^[0-9\-]/i)){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#valend').hide();
            }
          }
          //end end date

//start time
$("#st")
  .change(function(){
    txtboxstarttime();
  })
  .keyup(function(){
    txtboxstarttime();
  })
  .keypress(function(){
    txtboxstarttime();
  })
  .submit(function(){
    txtboxstarttime();
  })
  .click(function(){
    txtboxstarttime();
  })
  .load(function(){
    txtboxstarttime();
  });

 function txtboxstarttime() {

        var originalValue = $('#st').val(),
        charCount = $('#st').val();
           $('#valtimestart').hide();
        if(originalValue.match(/^[0-9\-]/i)){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#valtimestart').hide();
            }
          }
          //end start time

//end time
$("#et")
  .change(function(){
    txtboxendtime();
  })
  .keyup(function(){
    txtboxendtime();
  })
  .keypress(function(){
    txtboxendtime();
  })
  .submit(function(){
    txtboxendtime();
  })
  .click(function(){
    txtboxendtime();
  })
  .load(function(){
    txtboxendtime();
  });

 function txtboxendtime() {

        var originalValue = $('#et').val(),
        charCount = $('#et').val();
           $('#validendtime').hide();
        if(originalValue.match(/^[0-9\-]/i)){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#validendtime').hide();
            }
          }
          //end end time



//end ABK Events Validation

//AbkRescue Activities Validation

$("#page_title")
  .change(function(){
    txtboxacttitle();
  })
  .keyup(function(){
    txtboxacttitle();
  })
  .keypress(function(){
    txtboxacttitle();
  })
  .submit(function(){
    txtboxacttitle();
  })
  .load(function(){
    txtboxacttitle();
  });

 function txtboxacttitle() {

        var originalValue = $('#page_title').val(),
        charCount = $('#page_title').val();
           $('#at').hide();
        if(originalValue.match(/^[a-zA-Z0-9]+$/) || charCount.length < 6){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#at').hide();
            }
          }


$("#page_keyword")
  .change(function(){
    txtboxackey();
  })
  .keyup(function(){
    txtboxackey();
  })
  .keypress(function(){
    txtboxackey();
  })
  .submit(function(){
    txtboxackey();
  })
  .load(function(){
    txtboxackey();
  });

 function txtboxackey() {

        var originalValue = $('#page_keyword').val(),
        charCount = $('#page_keyword').val();
           $('#ak').hide();
        if(originalValue.match(/^[a-zA-Z0-9]+$/) || charCount.length < 6){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#ak').hide();
            }
          }

$("#page_description")
  .change(function(){
    txtboxacdes();
  })
  .keyup(function(){
    txtboxacdes();
  })
  .keypress(function(){
    txtboxacdes();
  })
  .submit(function(){
    txtboxacdes();
  })
  .load(function(){
    txtboxacdes();
  });

 function txtboxacdes() {

        var originalValue = $('#page_description').val(),
        charCount = $('#page_description').val();
           $('#ad').hide();
        if(originalValue.match(/^[a-zA-Z0-9]+$/) || charCount.length < 6){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#ad').hide();
            }
          }

$("#pageBannerUrl")
  .change(function(){
    txtboxpb();
  })
  .keyup(function(){
    txtboxpb();
  })
  .keypress(function(){
    txtboxpb();
  })
  .submit(function(){
    txtboxpb();
  })
  .load(function(){
    txtboxpb();
  });

 function txtboxpb() {

        var originalValue = $('#pageBannerUrl').val(),
        charCount = $('#pageBannerUrl').val();
           $('#pB').hide();
        if(originalValue.match(/^[a-zA-Z0-9]+$/) || charCount.length < 6){
            $('#valuser0').html("");
            $('#valuser1').html("");
            $('#pB').hide();
            }
          }



//end AbkRescue Activities Validation


//quick announcement
$("#ann_title")
  .change(function(){
    txtboxtitle();
  })
  .keyup(function(){
    txtboxtitle();
  })
  .keypress(function(){
    txtboxtitle();
  })
  .submit(function(){
    txtboxtitle();
  })
  .click(function(){
    txtboxtitle();
  })
  .load(function(){
    txtboxtitle();
  });

 function txtboxtitle() {

        var originalValue = $('#ann_title').val();

        if(originalValue != ""){
           $('#hidetitle').hide();

          }
          }
$("#ann_contentDash")
  .change(function(){
   txtboxcontent();
  })
  .keyup(function(){
   txtboxcontent();
  })
  .keypress(function(){
   txtboxcontent();
  })
  .submit(function(){
   txtboxcontent();
  })
  .click(function(){
   txtboxcontent();
  })
  .load(function(){
   txtboxcontent();
  });

 function txtboxcontent() {

        var originalValue = $('#ann_contentDash').val();

        if(originalValue != ""){
           $('#hidecontent').hide();

          }
          }
//end quick announcement

//program validation
$("#program-title")
  .change(function(){
   txtbox_program_title();
  })
  .keyup(function(){
   txtbox_program_title();
  })
  .keypress(function(){
   txtbox_program_title();
  });
 function txtbox_program_title() {

           $('#hidetitle').hide();
           $('#hideurl').hide();

          }


$("#tagline")
  .change(function(){
   txtbox_program_tagline();
  })
  .keyup(function(){
   txtbox_program_tagline();
  })
  .keypress(function(){
   txtbox_program_tagline();
  });

 function txtbox_program_tagline() {

        var originalValue = $('#tagline').val();

        if(originalValue != ""){
           $('#hidetagline').hide();

          }
          }

$("#tooltip")
  .change(function(){
   txtbox_program_tooltip();
  })
  .keyup(function(){
   txtbox_program_tooltip();
  })
  .keypress(function(){
   txtbox_program_tooltip();
  });

 function txtbox_program_tooltip() {

        var originalValue = $('#tooltip').val();

        if(originalValue != ""){
           $('#hidetooltip').hide();

          }
          }
$(document).ready(function(){

        for (var i in CKEDITOR.instances) {

                CKEDITOR.instances[i].on('change', function() {
                    console.log(CKEDITOR.instances[i].document.getBody().getText());
                    $('#errdetails').html("");
                });
        }
});
//end program validation
//hide error and succes message for 3 sec
  $("#conthide").show();
  setTimeout(function() {
 $("#conthide").hide(); }, 5000);
//
if($('#csrfToken')[0]) {
  $('#csrfToken').val($("#csrf").val());
}

$(function() {
  $('#fromDatepicker').datetimepicker({
    pickTime: false
  }).on('changeDate',function (e){
    $('#fromDatepicker').datetimepicker('hide');
  });
});

$(function() {
  $('#toDatepicker').datetimepicker({
    pickTime: false
  }).on('changeDate',function (e){
    $('#toDatepicker').datetimepicker('hide');
  });
});

$(function() {
  $("#featured").click(function() {
      if($('#featured').prop('checked')) {
        $('#actorder').attr('disabled', true);
      } else {
        $('#actorder').attr('disabled', false);
      }
  });
  if($('#featured').prop('checked')) {
    $('#actorder').attr('disabled', true);
  } else {
    $('#actorder').attr('disabled', false);
  }
});


$(document).ready(function(){

        for (var i in CKEDITOR.instances) {

                CKEDITOR.instances[i].on('change', function() {
                    console.log(CKEDITOR.instances[i].document.getBody().getText());
                    $('#errdetails').html("");
                });
        }
});
